<%-- 
    Document   : header
    Created on : May 23, 2023, 9:25:05 PM
    Author     : Naviank
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

    <head>
        <style>
            .phone-icon {
                position: fixed;
                bottom: 20px;
                right: 20px;
                background-color: #ff9800;
                color: #fff;
                width: 200px;
                height: 50px;
                border-radius: 50%;
                text-align: center;
                line-height: 50px;
                font-size: 24px;
                z-index: 9999;
                text-decoration: none;
                transition: background-color 0.3s ease;
            }

            .phone-icon:hover {
                background-color: #f57c00;
            }
        </style>
        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="Online Learning System - Group 2 SE1714" />

        <!-- OG -->
        <meta property="og:title" content="Online Learning System - Group 2 SE1714" />
        <meta property="og:description" content="Online Learning System - Group 2 SE1714" />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no">

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />

        <!-- PAGE TITLE HERE ============================================= -->
        <title>Online Learning System - Group 2 SE1714</title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/assets.css">

        <!-- TYPOGRAPHY ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/typography.css">

        <!-- SHORTCODES ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/shortcodes/shortcodes.css">

        <!-- STYLESHEETS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <link class="skin" rel="stylesheet" type="text/css" href="assets/css/color/color-1.css">

        <!-- REVOLUTION SLIDER CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/vendors/revolution/css/layers.css">
        <link rel="stylesheet" type="text/css" href="assets/vendors/revolution/css/settings.css">
        <link rel="stylesheet" type="text/css" href="assets/vendors/revolution/css/navigation.css">
        <!-- REVOLUTION SLIDER END -->	
    </head>

    <body>

        <!-- Header Top ==== -->
        <header class="header rs-nav header-transparent">
            <div class="top-bar">
                <div class="container">
                    <div class="row d-flex justify-content-between">
                        <div class="topbar-left">
                            <ul>
                                <!--<li><a href="javascript:;"><i class="fa fa-envelope-o"></i>Support@website.com</a></li>-->
                            </ul>
                        </div>
                    </div>
                    <div class="menu-links navbar-collapse collapse justify-content-start" id="menuDropdown">
                        <div class="menu-logo">
                            <a href="home"><img src="assets/images/logo.png" alt=""></a>
                        </div>
                        <c:set var="currentpage" value="${requestScope.xxx}"/>
                        <ul class="nav navbar-nav">	
                            <li class=${currentpage eq "home"? 'active':""}>
                                <a href="home">Home</a>
                            </li>
                            
                            <li class=${currentpage eq "course"? 'active':""}>
                                <a href="courseservlet">Course</a>
                            </li>
                            <li class= ${currentpage eq "bloglist"? 'active':""}>
                                <a href="bloglist">Blogs</a>
                            </li>
<!--                            <li>
                                <a href="attemptquiz">Attempt quiz demo</a>
                            </li>-->
                            <c:if test="${sessionScope.user != null}">

                                <li class= ${currentpage eq "mycourse"? 'active':""}>
                                    <a href="mycourse">My Courses</a>
                                </li>

                            </c:if>
                        </ul>

                        <div class="topbar-right">
                            <ul>
                                <li>
                                </li>
                                <c:if test="${sessionScope.user==null}">
                                    <li><a href="login.jsp">Login</a></li>
                                    <li><a href="register.jsp">Register</a></li>
                                    </c:if>

                                <c:if test="${sessionScope.user!=null}">

                                    <li> 
                                        <a href="userprofile">
                                            <img 
                                                src="${sessionScope.user.avatar}" 
                                                style="height: 48px; width: 48px; overflow: hidden; border-radius: 50%;"
                                                alt="${sessionScope.user.full_Name}"/>
                                        </a>
                                    </li>
                                    <li><a href="userprofile">${user.full_Name}</a></li>
                                    <li><a href="logout">Logout</a></li>

                                </c:if>                                    
                            </ul>
                        </div>

                    </div>
                    <!-- Navigation Menu END ==== -->
                </div>
            </div>
            <div class="sticky-header navbar-expand-lg">
                <div class="menu-bar clearfix">
                    <div class="container clearfix">
                        <!-- Header Logo ==== -->
                        <div class="menu-logo">
                            <a href="home"><img src="assets/images/logo-white.png" alt=""></a>
                        </div>
                        <!-- Mobile Nav Button ==== -->
                        <button class="navbar-toggler collapsed menuicon justify-content-end" type="button" data-toggle="collapse" data-target="#menuDropdown" aria-controls="menuDropdown" aria-expanded="false" aria-label="Toggle navigation">
                            <span></span>
                            <span></span>
                            <span></span>
                        </button>
                        <!-- Author Nav ==== -->
                        <div class="secondary-menu">
                            <div class="secondary-inner">

                                <div class="topbar-right">
                                    <ul>
                                        <li>
                                        </li>
                                        <c:if test="${sessionScope.user==null}">
                                            <li><a href="login.jsp">Login</a></li>
                                            <li><a href="register.jsp">Register</a></li>
                                            </c:if>

                                        <c:if test="${sessionScope.user!=null}">

                                            <li> 
                                                <a href="userprofile">
                                                    <img 
                                                        src="${sessionScope.user.avatar}" 
                                                        style="height: 48px; width: 48px; overflow: hidden; border-radius: 50%;"
                                                        alt="${sessionScope.user.full_Name}"/>
                                                </a>
                                            </li>
                                            <li><a href="userprofile">${user.full_Name}</a></li>
                                            <li><a href="logout">Logout</a></li>

                                        </c:if>                                    
                                    </ul>
                                </div>                                  
                            </div>
                        </div>



                        <div class="menu-links navbar-collapse collapse justify-content-start" id="menuDropdown">
                            <div class="menu-logo">
                                <a href="home"><img src="assets/images/logo.png" alt=""></a>
                            </div>
                            <c:set var="currentpage" value="${requestScope.xxx}"/>
                            <ul class="nav navbar-nav">	
                                <li class= ${currentpage eq "home"? "active":""}>
                                    <a href="home">Home</a>
                                </li>
                                <li><a href="#">Pages</a>
                                <li class= ${currentpage eq "courselist"? "active":""}>
                                    <a href="courseservlet">Courses</a>
                                </li>
                                <li class= ${currentpage eq "bloglist"? "active":""}>
                                    <a class=""  href="bloglist">Blogs</a>
                                </li>
                                <c:if test="${sessionScope.user != null}">

                                    <li class= ${currentpage eq "mycourse"? "active":""}>
                                        <a href="mycourse">My Courses</a>
                                    </li>

                                </c:if>
                            </ul>
                        </div>
                        <!-- Navigation Menu END ==== -->
                    </div>
                </div>
            </div>
        </header>
        <!-- Header Top END ==== -->

    </body>
</html>