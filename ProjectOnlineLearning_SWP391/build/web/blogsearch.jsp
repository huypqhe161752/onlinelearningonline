<%-- 
    Document   : newbloglist
    Created on : May 22, 2023, 5:57:32 PM
    Author     : Naviank
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">

    <head>

        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="Online Learning System - Group 2 SE1714" />

        <!-- OG -->
        <meta property="og:title" content="Online Learning System - Group 2 SE1714" />
        <meta property="og:description" content="Online Learning System - Group 2 SE1714" />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no">

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />

        <!-- PAGE TITLE HERE ============================================= -->
        <title>Online Learning System - Group 2 SE1714</title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/assets.css">

        <!-- TYPOGRAPHY ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/typography.css">

        <!-- SHORTCODES ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/shortcodes/shortcodes.css">

        <!-- STYLESHEETS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <link rel="stylesheet" type="text/css" href="assets/css/addition_style.css">
        <link class="skin" rel="stylesheet" type="text/css" href="assets/css/color/color-1.css">

        <!-- REVOLUTION SLIDER CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/vendors/revolution/css/layers.css">
        <link rel="stylesheet" type="text/css" href="assets/vendors/revolution/css/settings.css">
        <link rel="stylesheet" type="text/css" href="assets/vendors/revolution/css/navigation.css">
        <!-- REVOLUTION SLIDER END -->	
    </head>
    <body id="bg">
        <div class="page-wraper">
            <div id="loading-icon-bx"></div>
            <!-- Header Top ==== -->
            <jsp:include page="header.jsp"/>
            <!-- header END ==== -->
            <!-- Content -->
            <div class="page-content bg-white">
                <!-- inner page banner -->
                <div class="page-banner ovbl-dark" style="background-image:url(assets/images/banner/banner1.jpg); height: 150px">
                    <div class="container">
                        <div class="page-banner-entry">
                            <h1 class="text-white" style="margin-top: 50px">Blog List</h1>
                        </div>
                    </div>
                </div>
                <!-- Breadcrumb row -->
                <div class="breadcrumb-row">
                    <div class="container">
                        <ul class="list-inline">
                            <div class="row">
                                <li style="width: 64px;"><a href="home">Home</a></li>
                                <li style=""><a href="bloglist">Blog List</a></li>
                            </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- Breadcrumb row END -->
                <div class="content-block">
                    <div class="section-area section-sp1">
                        <div class="container">

                            <c:set var="currentorder" value="${requestScope.currentorder}"/>
                            <c:set var="currentpage" value="${requestScope.currentpage}"/>
                            <c:set var="currentcate" value="${requestScope.currentcate}"/>                            
                            <c:set var="currentfilt" value="${requestScope.currentfilt}"/> 



                            <!-- // -->
                            <ul class="content-menu list-inline ">
                                <div class="row">
                                    <li style="padding: 4px 8px; ">
                                        <button style="background-color: rgb(93, 4, 176); border-radius: 2px; border: 1px solid black; padding: 8px; color: white" onclick="location.href = 'bloglist?filter=1&cate=0&page=1&order=2'">
                                            Oldest posts first
                                        </button>
                                    </li>
                                    <li style="padding: 4px 8px; ">
                                        <button style="background-color: rgb(93, 4, 176); border-radius: 5px; border: 1px solid black; padding: 8px; color: white" onclick="location.href = 'bloglist?filter=3&cate=0&page=1&order=2'">
                                            Least views posts first
                                        </button>
                                    </li>
                                </div>
                                </li>
                            </ul>
                            <!-- // -->

                            <div class="row">
                                <!-- left part start -->
                                <div class="col-lg-8 col-xl-8 col-md-7">
                                    <!-- blog grid -->
                                    <div id="masonry" class="ttr-blog-grid-3 row">
                                        <c:forEach items="${requestScope.blogsearch}" var="b">
                                            <div class="post action-card col-xl-6 col-lg-6 col-md-6 col-xs-12 m-b40">
                                                <div class="recent-news">
                                                    <div class="action-box">
                                                        <a href="blogdetail?id_Blog=${b.id_Blog}"><img src="${b.thumbnail_Blog}" alt="post-image"></a>
                                                    </div>
                                                    <div class="info-bx">
                                                        <ul class="media-post">

                                                            <c:forEach items="${requestScope.listCate}" var="lc">


                                                                <c:if test="${lc.id_Catergory_Blog == b.id_CatergoryBlog}">

                                                                    <li><a href="#">${lc.name_Catergory_Blog}</a> </li>

                                                                </c:if>
                                                            </c:forEach>
                                                            <li></li>

                                                            <li><a href="#"><i class="fa fa-calendar"></i>${b.update_Date}</a></li>

                                                            <c:forEach items="${requestScope.listMarketer}" var="mu">

                                                                <c:if test="${b.id_User eq mu.id_User}">

                                                                    <li><a href="#"><i class="fa fa-user"></i>By ${mu.full_Name}</a></li>

                                                                </c:if>

                                                            </c:forEach>
                                                        </ul>
                                                        <div  style="height: 200px;">
                                                            <h5 class="post-title"><a href="blogdetail?id_Blog=${b.id_Blog}">${b.title}.</a></h5>
                                                            <p>${b.brief_Infor_Blog}.</p>
                                                        </div>
                                                        <div class="post-extra">
                                                            <a href="blogdetail?id_Blog=${b.id_Blog}" class="btn-link">READ MORE</a>
                                                            <a href="#" class="comments-bx"><i class="fa fa-eye"></i>${b.view} Views</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </c:forEach>
                                    </div>
                                    <!-- blog grid END -->
                                    <!-- Pagination -->
                                    <div class="pagination-bx rounded-sm gray clearfix">
                                        <ul class="pagination">

                                            <c:if  test="${currentpage > 1}">


                                                <li class="previous">
                                                    <a href="bloglist?filter=${currentfilt}&cate=${currentcate}&page=${currentpage - 1}&order=${currentorder}"><i class="ti-arrow-left"></i> Prev</a>
                                                </li>

                                            </c:if>
                                            <c:forEach begin="1" end="${requestScope.numofpage}" var="i">

                                                <li class="${i == currentpage?"active":""}">
                                                    <a href="bloglist?filter=${currentfilt}&cate=${currentcate}&page=${i}&order=${currentorder}">${i}</a>
                                                </li>

                                            </c:forEach>
                                            <c:if test="${currentpage < numofpage}">

                                                <li class="next">
                                                    <a href="bloglist?filter=${currentfilt}&cate=${currentcate}&page=${currentpage + 1}&order=${currentorder}">Next <i class="ti-arrow-right"></i></a>
                                                </li>

                                            </c:if>
                                        </ul>
                                    </div>
                                    <!-- Pagination END -->
                                </div>
                                <!-- left part END -->
                                <!-- Side bar start -->
                                <div class="col-lg-4 col-xl-4 col-md-5 sticky-top">
                                    <aside class="side-bar sticky-top">

                                        <div class="widget">
                                            <h6 class="widget-title">Search</h6>
                                            <div class="search-bx style-1">
                                                <form role="search" method="post">
                                                    <div class="input-group">
                                                        <input name="text" class="form-control" placeholder="Enter your keywords..." type="text">
                                                        <span class="input-group-btn">
                                                            <button type="submit" class="fa fa-search text-primary"></button>
                                                        </span> 
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="widget recent-posts-entry">
                                            <h6 class="widget-title">Recent Posts</h6>
                                            <div class="widget-post-bx">
                                                <c:forEach items="${requestScope.recentBlog}" var="rb">
                                                    <div class="widget-post clearfix">
                                                        <div class="ttr-post-media"> <img src="${rb.thumbnail_Blog}" width="200" height="143" alt=""> </div>
                                                        <div class="ttr-post-info">
                                                            <div class="ttr-post-header">
                                                                <h6 class="post-title"><a href="blogdetail?id_Blog=${rb.id_Blog}">${rb.title}.</a></h6>
                                                            </div>
                                                            <ul class="media-post">
                                                                <li><a href="#"><i class="fa fa-calendar"></i>${rb.update_Date}</a></li>



                                                                <li><a href="#"><i class="fa fa-eye"></i>${rb.view} Views</a></li>

                                                                <c:forEach items="${requestScope.listMarketer}" var="muu">

                                                                    <c:if test="${rb.id_User eq muu.id_User}">

                                                                        <li><a href="#"><i class="fa fa-user"></i>By ${muu.full_Name}</a></li>

                                                                    </c:if>

                                                                </c:forEach>                                                                 
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </c:forEach>
                                            </div>
                                        </div>
                                        <div class="widget widget-newslatter">
                                            <h6 class="widget-title">Newsletter</h6>
                                            <div class="news-box">
                                                <p>Enter your e-mail and subscribe to our newsletter.</p>
                                                <form class="subscription-form" action="http://educhamp.themetrades.com/demo/assets/script/mailchamp.php" method="post">
                                                    <div class="ajax-message"></div>
                                                    <div class="input-group">
                                                        <input name="dzEmail" required="required" type="email" class="form-control" placeholder="Your Email Address"/>
                                                        <button name="submit" value="Submit" type="submit" class="btn black radius-no">
                                                            <i class="fa fa-paper-plane-o"></i>
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="widget widget_gallery gallery-grid-4">
                                            <h6 class="widget-title">Our Gallery</h6>
                                            <ul>
                                                <li><div><a href="#"><img src="assets/images/gallery/pic2.jpg" alt=""></a></div></li>
                                                <li><div><a href="#"><img src="assets/images/gallery/pic1.jpg" alt=""></a></div></li>
                                                <li><div><a href="#"><img src="assets/images/gallery/pic5.jpg" alt=""></a></div></li>
                                                <li><div><a href="#"><img src="assets/images/gallery/pic7.jpg" alt=""></a></div></li>
                                                <li><div><a href="#"><img src="assets/images/gallery/pic8.jpg" alt=""></a></div></li>
                                                <li><div><a href="#"><img src="assets/images/gallery/pic9.jpg" alt=""></a></div></li>
                                                <li><div><a href="#"><img src="assets/images/gallery/pic3.jpg" alt=""></a></div></li>
                                                <li><div><a href="#"><img src="assets/images/gallery/pic4.jpg" alt=""></a></div></li>
                                            </ul>
                                        </div>
                                        <div class="widget widget_tag_cloud">
                                            <h6 class="widget-title">Interesting category</h6>
                                            <div class="tagcloud"> 
                                                <c:forEach items="${requestScope.listCate}" var="cl">

                                                    <a href="bloglist?filter=2&cate=${cl.id_Catergory_Blog}&page=">${cl.name_Catergory_Blog}</a> 

                                                </c:forEach>
                                            </div>
                                        </div>
                                    </aside>
                                </div>
                                <!-- Side bar END -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Left & right section END -->
            <!-- Content END-->
            <jsp:include page="footer.jsp"/>
            <!-- Footer END ==== -->
            <!-- scroll top button -->
            <button class="back-to-top fa fa-chevron-up" ></button>
        </div>
        <!-- External JavaScripts -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/vendors/bootstrap/js/popper.min.js"></script>
        <script src="assets/vendors/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/vendors/bootstrap-select/bootstrap-select.min.js"></script>
        <script src="assets/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
        <script src="assets/vendors/magnific-popup/magnific-popup.js"></script>
        <script src="assets/vendors/counter/waypoints-min.js"></script>
        <script src="assets/vendors/counter/counterup.min.js"></script>
        <script src="assets/vendors/imagesloaded/imagesloaded.js"></script>
        <script src="assets/vendors/masonry/masonry.js"></script>
        <script src="assets/vendors/masonry/filter.js"></script>
        <script src="assets/vendors/owl-carousel/owl.carousel.js"></script>
        <script src="assets/js/functions.js"></script>
        <script src="assets/js/contact.js"></script>
        <!--<script src='assets/vendors/switcher/switcher.js'></script>-->

    </body>

</html>
