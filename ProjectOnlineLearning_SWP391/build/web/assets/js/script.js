


function checkVideoProgress(id) {
    var videoElement = document.getElementById("video" + id);
    var progress = (videoElement.currentTime / videoElement.duration) * 100;

    var checkbox = document.getElementById("checktocheckbox" + id);

    checkbox.onchange = function () {
        if (checkbox.checked) {
            document.getElementById("checkboxStatus" + id).textContent = "Done";
        }
    };
    if (progress >= 90) {
        document.getElementById("showcheckboxstatus" + id).checked = true;
        document.getElementById("checktocheckbox" + id).checked = true;
        document.getElementById("checkcf" + id).checked = true;
        document.getElementById("checktocheckbox" + id).dispatchEvent(new Event('change'));
        videoElement.removeEventListener("timeupdate", checkVideoProgress);
    }
}


function toggleHiddenElement(id) {
  const hiddenElement = document.getElementById('hiddenElement'+id);

  if (hiddenElement.style.display === 'none') {
    hiddenElement.style.display = 'block';
  } else {
    hiddenElement.style.display = 'none';
  }
}


function redirect(subjectid, lessonid) {
    window.location.href = "lessonview?idsubject=" + subjectid + "&&idlesson=" + lessonid;
}

function submitFormAndRedirect1(lessonId, id) {
    var form = document.getElementById(id);
    var redirectvalue = lessonId;
    document.getElementById("t41").value = redirectvalue;
    form.submit();
}

function submitFormAndRedirect(lessonId, id) {
    var form = document.getElementById("FormLessonCheckBox" + id);
    var redirectvalue = lessonId;
    document.getElementById("t4").value = redirectvalue;
    form.submit();
}

