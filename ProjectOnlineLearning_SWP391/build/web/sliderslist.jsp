<%-- 
    Document   : sliderslist
    Created on : Jul 12, 2023, 7:38:17 PM
    Author     : Naviank
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from educhamp.themetrades.com/demo/admin/courses.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:10:19 GMT -->
    <head>
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/subjectlist.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
        <script src="assets/js/PopUp.js"></script>
        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="EduChamp : Education HTML Template" />

        <!-- OG -->
        <meta property="og:title" content="EduChamp : Education HTML Template" />
        <meta property="og:description" content="EduChamp : Education HTML Template" />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no">

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="../error-404.html" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />

        <!-- PAGE TITLE HERE ============================================= -->
        <title>EduChamp : Education HTML Template </title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/assets.css">
        <link rel="stylesheet" type="text/css" href="assestsAdmin/vendors/calendar/fullcalendar.css">

        <!-- TYPOGRAPHY ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/typography.css">

        <!-- SHORTCODES ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/shortcodes/shortcodes.css">

        <!-- STYLESHEETS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/style.css">
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/dashboard.css">
        <link class="skin" rel="stylesheet" type="text/css" href="assets/css/color/color-1.css">
        <link rel="stylesheet" type="text/css" href="assets/vendors/chosen/style.css">
        <link rel="stylesheet" type="text/css" href="assets/vendors/chosen/chosen.css">
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/quizzeslist.css">
        <style>
            .active{
                color: black;
            }
            .actived{
                color:orange;
            }
        </style>
    </head>
    <body class="ttr-opened-sidebar ttr-pinned-sidebar" onload="onloadSlidersList()">
        <!-- header start -->
        <header class="ttr-header">
            <div class="ttr-header-wrapper">
                <!--sidebar menu toggler start -->
                <div class="ttr-toggle-sidebar ttr-material-button">
                    <i class="ti-close ttr-open-icon"></i>
                    <i class="ti-menu ttr-close-icon"></i>
                </div>
                <!--sidebar menu toggler end -->
                <!--logo start -->
                <div class="ttr-logo-box">
                    <div>
                        <a href="home" class="ttr-logo">
                            <img alt="" class="ttr-logo-mobile" src="assets/images/logo.png" width="30" height="30">
                            <img alt="" class="ttr-logo-desktop" src="assets/images/logo-white.png" width="160" height="27">
                        </a>
                    </div>
                </div>
                <!--logo end -->
                <div class="ttr-header-menu">
                    <!-- header left menu start -->
                    <ul class="ttr-header-navigation">
                        <li>
                            <a href="home" class="ttr-material-button ttr-submenu-toggle">HOME</a>
                        </li>
                    </ul>
                    <!-- header left menu end -->
                </div>
                <div class="ttr-header-right ttr-with-seperator">
                    <!-- header right menu start -->
                    <ul class="ttr-header-navigation">

                        <c:if test="${sessionScope.user!=null}">

                            <li>
                                <a href="#" class="ttr-material-button">${sessionScope.user.full_Name}</a>
                            </li>

                            <li>
                                <a href="#" class="ttr-material-button ttr-submenu-toggle"
                                   ><span class="ttr-user-avatar"
                                       ><img
                                            alt=""
                                            src="${sessionScope.user.avatar}"
                                            width="32"
                                            height="32" /></span
                                    ></a>
                                <div class="ttr-header-submenu">
                                    <ul>
                                        <li><a onclick="openPopup1('profilemarketing1')">My profile</a></li>
                                        <li><a onclick="openPopup1('profilemarketingchagepass')">Change Password</a></li>
                                        <li><a href="logout">Logout</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li style="background-color: orange">
                                <a href="logout" class="ttr-material-button">Logout</a>
                            </li>
                        </c:if> 
                    </ul>
                    <!-- header right menu end -->
                </div>
                <!--header search panel start -->
                <div class="ttr-search-bar">
                    <form class="ttr-search-form">
                        <div class="ttr-search-input-wrapper">
                            <input type="text" name="qq" placeholder="search something..." class="ttr-search-input">
                            <button type="submit" name="search" class="ttr-search-submit"><i class="ti-arrow-right"></i></button>
                        </div>
                        <span class="ttr-search-close ttr-search-toggle">
                            <i class="ti-close"></i>
                        </span>
                    </form>
                </div>
                <!--header search panel end -->
            </div>
        </header>
        <!-- header end -->
        <!-- Left sidebar menu start -->
        <c:set value="${sessionScope.user}" var="su"/>
        <div class="ttr-sidebar">
            <div class="ttr-sidebar-wrapper content-scroll">
                <!-- side menu logo start -->
                <div class="ttr-sidebar-logo">
                    <a href="#"><img alt="" src="assets/images/logo.png" width="122" height="27"></a>
                    <div class="ttr-sidebar-toggle-button">
                        <i class="ti-arrow-left"></i>
                    </div>
                </div>
                <!-- side menu logo end -->
                <!-- sidebar menu start -->
                <nav class="ttr-sidebar-navi">
                    <ul>
                        <li>
                            <a href="dashboardmarketing" class="ttr-material-button">
                                <span id="active" class="ttr-icon"
                                      ><i class="ti-home"></i
                                    ></span>
                                <span id="active" class="ttr-label">Dashboard</span>
                            </a>
                        </li>
                        <li>
                            <a href="sliderslist" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-book"></i></span>
                                <span class="ttr-label actived">Management Slider</span>
                            </a>
                        </li>

                        <li>
                            <a href="postlistmarketing" class="ttr-material-button">
                                <span class="ttr-icon"
                                      ><i class="ti-layout-list-post"></i
                                    ></span>
                                <span class="ttr-label">Management Post</span>
                            </a>
                        </li>

                        <li>
                            <a href="" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-user"></i></span>
                                <span class="ttr-label">My Profile</span>
                                <span class="ttr-arrow-icon"
                                      ><i class="fa fa-angle-down"></i
                                    ></span>
                            </a>
                            <ul>
                                <li>
                                    <a onclick="openPopup1('profilemarketing1')" class="ttr-material-button"
                                       ><span class="ttr-label">User Profile</span></a
                                    >
                                </li>
                                <li>
                                    <a onclick="openPopup1('profilemarketingchagepass')" class="ttr-material-button"
                                       ><span class="ttr-label">Change Password</span></a
                                    >
                                </li>
                                <li>
                                    <a href="logout" class="ttr-material-button"
                                       ><span class="ttr-label">Logout</span></a
                                    >
                                </li>
                            </ul>
                        </li>
                        <li class="ttr-seperate"></li>
                    </ul>
                </nav>                    
                <!-- sidebar menu end -->
            </div>
        </div>
        <!-- Left sidebar menu end -->

        <!--Main container start -->
        <main class="ttr-wrapper">
            <div id="test"></div>
            <audio id="myAudio">
                <!--<source src="assestsAdmin/audio/magicaudio.ogg" type="audio/ogg">-->
                <source src="assestsAdmin/audio/magicaudio.mp3" type="audio/mpeg">
                <!--Your browser does not support the audio element.-->
            </audio>  
            <div class="container-fluid">
                <div class="db-breadcrumb">
                    <!-- Nav tabs -->
                    <!-- Tab panes -->
                    <h4 class="breadcrumb-title">Sliders List</h4>
                    <ul class="db-breadcrumb-list">
                        <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                        <li><a href="#">Sliders Management</a></li>
                        <li>Sliders List</li>
                    </ul>
                </div>

                <div class="row">
                    <!-- Your Profile Views Chart -->
                    <div class="col-lg-12 m-b30">
                        <div class="widget-box">
                            <div class="tab-content">
                                <div id="home1" class="container tab-pane fade"><br>
                                </div>
                                <div id="menu1" class="container tab-pane fade"><br>
                                </div>
                                <div id="menu2" class="container tab-pane active"><br>
                                    <div id="notification">Delete successfully!</div>
                                    <div id="addMess">
                                        ${requestScope.addMessage}
                                    </div>                                    
                                    <div class="col-12">
                                        <div class="ml-auto">
                                            <h2>Sliders List</h2>
                                        </div>
                                    </div>
                                    <div class="form-group row" style="">
                                        <div class="filter col-md-6">
                                            <h5>Filter result:</h5>
                                            <span for="sliderstatus">Slider status:</span
                                            <!--.-->
                                            <!--.-->
                                            <select name="sliderstatus" 
                                                    class="chosen-select"
                                                    onchange="setSliderStatus(this.value)">
                                                <option value="2" ${requestScope.status == "2" ? "selected" : ""}>--Status--</option>
                                                <option value="1" ${requestScope.status == "1" ? "selected" : ""}>Published</option>
                                                <option value="0" ${requestScope.status == "0" ? "selected" : ""}>Hidden</option>
                                            </select>
                                            <br/>
                                            <!--.-->

                                            <!--.-->                                            
                                            <br/>
                                            <input type="checkbox" 
                                                   ${requestScope.paging == "true" ? "checked" : ""}
                                                   value="true" 
                                                   onchange="setpagingCheckbox(this.checked)" 
                                                   id="paging-cbx"> Paginate list
                                            <button onclick="autoSubmitForm(1)">Do filter</button>
                                            <button onclick="location.href = 'sliderslist'">Clear filter</button>
                                            <br/>                                            
                                        </div>
                                        <div class="searchform col-md-6">
                                            <h5>Search for title/back link:</h5>
                                            <input class="form-control" 
                                                   oninput="searchSlider(this, ${requestScope.curunpet})" 
                                                   placeholder="Type something">
                                            <i class="fa fa-search" 
                                               style="position: relative; left: 400px; bottom: 30px" 
                                               aria-hidden="true">
                                            </i>
                                        </div>                                                    
                                        <div class="sort col-md-6">
                                            <h6>Sort result:</h6>
                                            <button onclick="sortTable1(5)">Sort by update date</button>
                                            <!--<button onclick="sortTable1(5)">Sort by number question</button>-->
                                            <button id="reversebtn">Reverse order</button>
                                        </div>
                                    </div>
                                    <!--Unnecessary form start-->  
                                    <form id="addForm" 
                                          style="display: none" 
                                          action="addslider" 
                                          method="post"
                                          enctype="multipart/form-data"
                                          >
                                        <div class="row">
                                            <div class="form-group col-12">
                                                <div class="container">
                                                    <div style="text-align: left;" class="row">
                                                        <div class="col-md-12">
                                                            <div class="table-wrap">
                                                                <table class="table table-striped">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Title</th>
                                                                            <th>Image</th>
                                                                            <th>Back link</th>
                                                                            <th>Action</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>
                                                                                <input required="" 
                                                                                       name="slidertitle" 
                                                                                       type="text" style="width: 100px">
                                                                            </td>
                                                                            <td>
                                                                                <div>
                                                                                    <img id="previewImage1" alt="Preview Image">
                                                                                    <input class="form-control" 
                                                                                           name="filesliderimg" 
                                                                                           type="file" 
                                                                                           id="imageInput1">
                                                                                    <!--onchange="previewImage('imageInput1', 'previewImage1')"-->
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <input required="" 
                                                                                       name="sliderbacklink" 
                                                                                       type="text" style="width: 100px">                                                                                
                                                                            </td>
                                                                            <td>
                                                                                <button class="btn">
                                                                                    Cancel
                                                                                </button>    
                                                                                <button class="btn gradient green"
                                                                                        type="submit"
                                                                                        value="Submit">
                                                                                    Add
                                                                                </button>    
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!--Unnecessary form end-->  
                                    <center>
                                        <h3>Sliders list</h3>
                                        <h4 class="message">${requestScope.message}</h4>
                                    </center>
                                    <div class="row">
                                        <div class="form-group col-12">
                                            <div class="container">

                                                <a href="#" 
                                                   style="position: relative; left: 820px; width: 150px" 
                                                   class="btn gradient green" 
                                                   onclick="openAdd()">Add New</a>

                                                <div style="text-align: left;" class="row">
                                                    <div class="col-md-12">
                                                        <div class="table-wrap">
                                                            <table class="table table-striped" id="myTable">
                                                                <thead>
                                                                    <tr>
                                                                        <th id="">ID</th>
                                                                        <th>Title</th>
                                                                        <th>Image</th>
                                                                        <th>Back link</th>
                                                                        <th>Status</th>
                                                                        <th>Latest modified on</th>
                                                                        <th>Created by</th>
                                                                        <th>Action</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="qlist-f">
                                                                    <c:set var="listSlider" value="${requestScope.listSlider}"/>
                                                                    <c:set var="listUN" value="${requestScope.listUN}"/>
                                                                    <c:set var="i" value="0"/>
                                                                    <c:forEach items="${listSlider}" var="slider">
                                                                        <tr class="revertable">
                                                                            <td>${slider.id_Slider}</td>
                                                                            <td>${slider.title_Slider}</td>
                                                                            <td><img class="sliderimg" src="${slider.thumbnail_Image}"></td>
                                                                            <td><a href="${slider.backlink}">${slider.backlink}</a></td>
                                                                            <td>
                                                                                <button id="chg-btn${slider.id_Slider}" onclick="changeSliderStatus(${slider.id_Slider})">
                                                                                    ${slider.status ? "published" : "hidden"}
                                                                                </button>
                                                                            </td>
                                                                            <td>${slider.update_Date}</td>
                                                                            <td>${listUN.get(i)}</td>
                                                                            <td>
                                                                                <button onclick="location.href = 'sliderdetailservlet?idslider=${slider.id_Slider}'" 
                                                                                        class="btn">
                                                                                    Details
                                                                                </button>    
                                                                                <!--                                                                                <button class="btn gradient brown" 
                                                                                                                                                                        onclick="deleteQuiz(${quiz.id_Quiz}, ${requestScope.curunpet})">
                                                                                                                                                                    Delete
                                                                                                                                                                </button>-->
                                                                            </td>
                                                                        </tr>
                                                                        <c:set var="i" value="${i + 1}"/>
                                                                    </c:forEach>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row pagination-bx rounded-sm gray clearfix">
                                            <c:set var="numofpage" value="${requestScope.numpage}"/>
                                            <c:set var="curunpet" value="${requestScope.curunpet}"/>
                                            <form action="sliderslist#myTable" method="get" id="pagination-f">
                                                <input type="hidden" value="${i}" name="page" id="pginput">
                                                <input type="hidden" value="${requestScope.status}" name="status" id="slstatus">
                                                <input type="hidden" value="${requestScope.paging}" name="paging" id="paging">
                                                <ul class="pagination align-content-end">
                                                    <c:if  test="${curunpet > 1}">
                                                        <li class="previous">
                                                            <button onclick="autoSubmitForm(${curunpet - 1})">
                                                                <i class="ti-arrow-left"></i> Prev
                                                            </button>
                                                        </li>
                                                    </c:if>
                                                    <c:forEach begin="1" end="${numofpage}" var="i">
                                                        <li class="${i == curunpet?"active":""}">
                                                            <button onclick="autoSubmitForm(${i})">${i}</button>
                                                        </li>
                                                    </c:forEach>
                                                    <c:if test="${curunpet < numofpage}">
                                                        <li class="next">
                                                            <button onclick="autoSubmitForm(${curunpet + 1})">Next 
                                                                <i class="ti-arrow-right"></i>
                                                            </button>
                                                        </li>
                                                    </c:if>
                                                </ul>
                                            </form>
                                        </div>
                                    </div>
                                    <!--</form>-->
                                </div>
                            </div>
                            <!--</div>-->
                        </div>
                    </div>
                    <!-- Your Profile Views Chart END-->
                </div>

            </div>
        </main>

        <div class="ttr-overlay"></div>



        <script>

        </script>

        <!-- External JavaScripts -->
        <script src="assestsAdmin/js/jquery.min.js"></script>
        <script src="assestsAdmin/vendors/bootstrap/js/popper.min.js"></script>
        <script src="assestsAdmin/vendors/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/vendors/chosen/chosen.jquery.js"></script>
        <script src="assets/vendors/chosen/prism.js"></script>
        <script src="assets/vendors/chosen/init.js"></script>
        <!--<script src="assets/vendors/chosen/jquery-3.2.1.min.js"></script>-->
        <!--<script src="assestsAdmin/vendors/bootstrap-select/bootstrap-select.min.js"></script>-->
        <script src="assestsAdmin/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
        <script src="assestsAdmin/vendors/magnific-popup/magnific-popup.js"></script>
        <!--<script src="assestsAdmin/vendors/counter/waypoints-min.js"></script>-->
        <script src="assestsAdmin/vendors/counter/counterup.min.js"></script>
        <script src="assestsAdmin/vendors/imagesloaded/imagesloaded.js"></script>
        <script src="assestsAdmin/vendors/masonry/masonry.js"></script>
        <script src="assestsAdmin/vendors/masonry/filter.js"></script>
        <script src="assestsAdmin/vendors/owl-carousel/owl.carousel.js"></script>
        <script src='assestsAdmin/vendors/scroll/scrollbar.min.js'></script>
        <!--<script src="assestsAdmin/js/functions.js"></script>-->
        <!--<script src="assestsAdmin/vendors/chart/chart.min.js"></script>-->
        <script src="assestsAdmin/js/admin.js"></script>
        <script src="assestsAdmin/js/quizzeslist.js"></script>
    </body>

    <!-- Mirrored from educhamp.themetrades.com/demo/admin/courses.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:11:35 GMT -->
</html>
