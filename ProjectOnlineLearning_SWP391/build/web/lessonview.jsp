<%-- 
    Document   : lessonview
    Created on : May 31, 2023, 8:59:14 PM
    Author     : GangsterCao
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <script src="assets/js/script.js"></script>
        <script src="assets/js/PopUp.js"></script>
        <link rel="stylesheet" type="text/css" href="assets/css/lessonview.css">
        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="EduChamp : Education HTML Template" />

        <!-- OG -->
        <meta property="og:title" content="EduChamp : Education HTML Template" />
        <meta property="og:description" content="EduChamp : Education HTML Template" />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no">

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />

        <!-- PAGE TITLE HERE ============================================= -->
        <title>EduChamp : Education HTML Template </title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/assets.css">

        <!-- TYPOGRAPHY ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/typography.css">

        <!-- SHORTCODES ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/shortcodes/shortcodes.css">
        <!-- STYLESHEETS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <link class="skin" rel="stylesheet" type="text/css" href="assets/css/color/color-1.css">
    </head>

    <c:if test="${sessionScope.user ==null}">
        <c:redirect url="home"/>
    </c:if>
    <c:if test="${sessionScope.user !=null}">
        <c:set value="${sessionScope.user}" var="u"/>
        <body id="bg">
            <div id="overlay" class="overlay">
            </div>
            <div class="page-wraper" style="position: relative">
                <div id="loading-icon-bx"></div>
                <!-- Header Top ==== -->
                <%@ include file="header_1.jsp" %>
                <!-- header END ==== -->
                <!-- Content -->
                <div class="page-content bg-white">
                    <!-- inner page banner -->
                    <div class="page-banner ovbl-dark" style="background-image:url(assets/images/banner/banner2.jpg); height: 150px">
                        <div class="container">
                            <div class="page-banner-entry">
                                <h1 class="text-white" style="height: 120px">Lesson</h1>
                            </div>
                        </div>
                    </div>
                    <!-- Breadcrumb row -->
                    <div class="breadcrumb-row">
                        <div class="container">
                            <ul class="list-inline">
                                <li><a href="home">Home</a></li>
                                <li><a href="#">My Course</a></li>
                                <li>Lesson View</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Breadcrumb row END -->                    
                    <c:set var="count" value="0" />
                    <c:set var="listlesson" value="${requestScope.listlesson}"/>
                    <c:set var="listboolean" value="${requestScope.listboolean}"/>
                    <c:set var="size" value="${requestScope.listboolean.size()}"/>
                    <c:set value="${requestScope.subjectName}" var="s"/>
                    <c:if test="${param.idlesson != null}">
                        <c:set value="${requestScope.lesson}" var="l"/>
                        <div class="content-block">
                            <div class="section-area section-sp1">
                                <div class="container">
                                    <div class="row">
                                        <c:if test="${l.id_Type == 3}">
                                            <video controls id="video${l.id_Lesson}" class="videoCenter" ontimeupdate="checkVideoProgress(${l.id_Lesson})">
                                                <source src="${l.video_Link}">
                                            </video>
                                        </c:if>
                                        <c:if test="${l.id_Type == 2}">
                                            <p style="margin-top: 50px">${l.video_Link}</p>

                                        </c:if>
                                        <h4 class="label" id="nameLesson">${l.name_Lesson}</h4>
                                        <p class="descriptioninName">${s.description}</p>

                                        <button onclick="showRectangle()" class="btn gradient green"
                                                id="buttonShowRectangleLessonView">
                                            List Lesson</button>                               
                                        <!--------------------- Information in rectangle here ---------------------->

                                        <div id="rectangle" class="rectangle">
                                            <button onclick="hideRectangle()" class="buttonhideRectangle">X</button>
                                            <h4 class="recTitle">${s.subject_Name}</h4>

                                            <a href="lessonview?idsubject=${requestScope.subjectid}">Before you start</a></br>
                                            <i class="fa fa-play-circle" aria-hidden="true">1min</i> 
                                            <div class="divider"></div>
                                            <div id="info" class="content">

                                                <c:forEach begin="1" end="${size}" var="i">
                                                    <c:set var="countbutton" value="${countbutton + 1}" />
                                                    <c:if test="${i != size}">                                  
                                                        <form id="FormLessonCheckBox${l.id_Lesson}" action="lessonview" method="post">
                                                            <input id="t1" type="hidden" name="id_User" value="${u.id_User}"/>
                                                            <input id="t2" type="hidden" name="Id_Lesson" value="${l.id_Lesson}"/>
                                                            <input id="t3" type="hidden" name="Id_Subject" value="${requestScope.subjectid}"/>
                                                            <input type="hidden" id="t4" name="redirectValue" value="${param.redirectValue}" />
                                                            <input hidden type="checkbox" name="Check_Status" value="true" id="checktocheckbox${l.id_Lesson}">
                                                            <button onclick="submitFormAndRedirect(${listlesson.get(i - 1).id_Lesson}, ${l.id_Lesson})" type="button" class="buttontosubmitform">
                                                                ${listlesson.get(i - 1).name_Lesson}</button>
                                                        </form>
                                                        <c:if test="${listlesson.get(i - 1).id_Type == 3}">
                                                            <button id="showButton" style="border: 0px" onclick="toggleHiddenElement(${listlesson.get(i - 1).id_Lesson})">Details</button>
                                                            <div id="hiddenElement${listlesson.get(i - 1).id_Lesson}" style="display: none;">
                                                                <p>${listlesson.get(i - 1).content_Lesson}</p>
                                                            </div>

                                                            <p><i class="fa fa-play-circle" aria-hidden="true"><span id="videoDuration${listlesson.get(i - 1).id_Lesson}">
                                                                    </span></i>    
                                                                <video id="videoID${listlesson.get(i - 1).id_Lesson}" type="video/mp4" hidden="" src="${listlesson.get(i - 1).video_Link}" 
                                                                       onloadedmetadata="getVideoDuration(${listlesson.get(i - 1).id_Lesson})" />
                                                            </p>
                                                        </c:if>
                                                        <c:if test="${listlesson.get(i - 1).id_Type == 2}">
                                                            <button id="showButton" style="border: 0px" onclick="toggleHiddenElement(${listlesson.get(i - 1).id_Lesson})">Details</button>
                                                            <div id="hiddenElement${listlesson.get(i - 1).id_Lesson}" style="display: none;">
                                                                <p>${listlesson.get(i - 1).content_Lesson}</p>
                                                            </div>
                                                        </c:if>
                                                        <div>
                                                            <c:if test="${listboolean.get(i-1).booleanValue() == true}">
                                                                <c:set var="count" value="${count + 1}" />
                                                                <input type="checkbox" disabled="" id="showcheckboxstatus${listlesson.get(i - 1).id_Lesson}" checked>
                                                                <p id="checkboxStatus${listlesson.get(i - 1).id_Lesson}" class="cblocation">Done</p>
                                                            </c:if>
                                                            <c:if test="${listboolean.get(i-1).booleanValue() != true}">
                                                                <input type="checkbox" disabled="" id="showcheckboxstatus${listlesson.get(i - 1).id_Lesson}">
                                                                <p id="checkboxStatus${listlesson.get(i - 1).id_Lesson}" class="cblocation">Not Yet</p>
                                                            </c:if>
                                                        </div>
                                                        <div class="divider"></div>
                                                    </c:if>
                                                    <!-- UPDATE LAST CODE ITER3 HERE -->
                                                    <c:if test="${i == size}">
                                                        <a href="quizlesson?id_lesson=${listlesson.get(i - 1).id_Lesson}">${listlesson.get(i - 1).name_Lesson}</a>
                                                        <p>${listlesson.get(i - 1).content_Lesson}</p>
                                                        <p><i class="fa fa-play-circle" aria-hidden="true"></i>${requestScope.countQuiz} Question</p>
                                                        <div>
                                                            <c:if test="${listboolean.get(i-1).booleanValue() == true}">
                                                                <c:set var="count" value="${count + 1}" />
                                                                <input type="checkbox" disabled="" id="showcheckboxstatus${listlesson.get(i - 1).id_Lesson}" checked>
                                                                <p class="cblocation">Done</p>
                                                                <p id="checkboxStatus${listlesson.get(i - 1).id_Lesson}"></p>
                                                            </c:if>
                                                            <c:if test="${listboolean.get(i-1).booleanValue() == false}">
                                                                <input type="checkbox" disabled="" id="showcheckboxstatus${listlesson.get(i - 1).id_Lesson}">
                                                                <p class="cblocation">Not Yet</p>
                                                                <p id="checkboxStatus${listlesson.get(i - 1).id_Lesson}"></p>
                                                            </c:if>   
                                                        </div>

                                                        <p class="progresstracking">Progress: (${count}/${requestScope.listlesson.size()} Total)| <span id="dura"></span></p>

                                                    </c:if>
                                                    <!-- UPDATE LAST CODE ITER3 HERE -->
                                                </c:forEach>
                                            </div>
                                        </div>
                                        <!----------------------------- End rectangle ----------------------------------------------->

                                        <c:forEach begin="1" end="${size}" var="i">
                                            <c:if test="${i > 1 && l.id_Lesson == listlesson.get(i - 1).id_Lesson}">
                                                <form id="checkForm" action="lessonview" method="post">
                                                    <input id="t11" type="hidden" name="id_User" value="${u.id_User}"/>
                                                    <input id="t21" type="hidden" name="Id_Lesson" value="${l.id_Lesson}"/>
                                                    <input id="t31" type="hidden" name="Id_Subject" value="${requestScope.subjectid}"/>
                                                    <input type="hidden" id="t41" name="redirectValue" value="${l.id_Lesson - 1}" />
                                                    <input hidden type="checkbox" name="Check_Status" value="true" id="checkcf${l.id_Lesson}">
                                                    <button class="btn" onclick="submitFormAndRedirect1(${l.id_Lesson-1}, 'checkForm')" type="submit">< Previous Lesson</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </form>
                                            </c:if>
                                            <c:if test="${i < size - 1 && l.id_Lesson == listlesson.get(i - 1).id_Lesson}">
                                                <form id="checkForm1" action="lessonview" method="post">
                                                    <input id="t11" type="hidden" name="id_User" value="${u.id_User}"/>
                                                    <input id="t21" type="hidden" name="Id_Lesson" value="${l.id_Lesson}"/>
                                                    <input id="t31" type="hidden" name="Id_Subject" value="${requestScope.subjectid}"/>
                                                    <input type="hidden" id="t41" name="redirectValue" value="${l.id_Lesson + 1}" />
                                                    <input hidden type="checkbox" name="Check_Status" value="true" id="checkcf${l.id_Lesson}">
                                                    <button class="btn" onclick="submitFormAndRedirect1(${l.id_Lesson+1}, 'checkForm1')" type="submit">Next Lesson ></button>
                                                </form>
                                            </c:if>
                                            <c:if test="${i == size && l.id_Type == 2}">
                                                <form id="checkForm2" action="lessonview" method="post">
                                                    <input id="t11" type="hidden" name="id_User" value="${u.id_User}"/>
                                                    <input id="t21" type="hidden" name="Id_Lesson" value="${l.id_Lesson}"/>
                                                    <input id="t31" type="hidden" name="Id_Subject" value="${requestScope.subjectid}"/>
                                                    <input type="hidden" id="t41" name="redirectValue" value="${l.id_Lesson}" />
                                                    <input hidden type="checkbox" name="Check_Status" value="true" checked="">
                                                    <button class="btn" onclick="submitFormAndRedirect1(${l.id_Lesson}, 'checkForm2')"  style="position: relative; left: 800px;">Mark As Completed</button>
                                                </form>
                                            </c:if>
                                        </c:forEach>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:if>
                    <c:if test="${param.idlesson == null}">
                        <div class="content-block">
                            <div class="section-area section-sp1">
                                <div class="container">
                                    <div class="row">
                                        <video id="video0" controls class="videoCenter" ontimeupdate="checkVideoProgress(0)">
                                            <source src="assets/video/IntroForAllSubject.mp4">
                                        </video>

                                        <h4 class="label" id="nameLesson">Welcome to learning</h4>
                                        <button onclick="showRectangle()" class="btn gradient green" id="buttonShowRectangleLessonView">
                                            List Lesson</button>
                                        <!--------------------- Information in rectangle here ---------------------->

                                        <div id="rectangle" class="rectangle">
                                            <button onclick="hideRectangle()" class="buttonhideRectangle">X</button>
                                            <h4 class="recTitle">${s.subject_Name}</h4>
                                            <a href="lessonview?idsubject=${requestScope.subjectid}">Before you start</a></br>
                                            <i class="fa fa-play-circle" aria-hidden="true">1min3sec</i> 
                                            <div class="divider"></div>
                                            <div id="info" class="content">
                                                <c:forEach begin="1" end="${size}" var="i">
                                                    <c:if test="${i != size}">
                                                        <button onclick="redirect(${requestScope.subjectid}, ${listlesson.get(i - 1).id_Lesson})" type="button" class="buttontosubmitform">
                                                            ${listlesson.get(i - 1).name_Lesson}</button>

                                                        <button id="showButton" style="border: 0px" onclick="toggleHiddenElement(${listlesson.get(i - 1).id_Lesson})">Details</button>
                                                        <div id="hiddenElement${listlesson.get(i - 1).id_Lesson}" style="display: none;">
                                                            <p>${listlesson.get(i - 1).content_Lesson}</p>
                                                        </div>
                                                        <p><i class="fa fa-play-circle" aria-hidden="true"><span id="videoDuration${listlesson.get(i - 1).id_Lesson}">
                                                                </span></i>    
                                                            <video id="videoID${listlesson.get(i - 1).id_Lesson}" type="video/mp4" hidden="" src="${listlesson.get(i - 1).video_Link}" 
                                                                   onloadedmetadata="getVideoDuration(${listlesson.get(i - 1).id_Lesson})" />
                                                        </p>
                                                        <div>

                                                            <c:if test="${listboolean.get(i-1).booleanValue() == true}">
                                                                <c:set var="count" value="${count + 1}" />
                                                                <input type="checkbox" disabled="" id="showcheckboxstatus${listlesson.get(i - 1).id_Lesson}" checked>
                                                                <p class="cblocation">Done</p>
                                                                <p id="checkboxStatus${listlesson.get(i - 1).id_Lesson}"></p>
                                                            </c:if>
                                                            <c:if test="${listboolean.get(i-1).booleanValue() == false}">
                                                                <input type="checkbox" disabled="" id="showcheckboxstatus${listlesson.get(i - 1).id_Lesson}">
                                                                <p class="cblocation">Not Yet</p>
                                                                <p id="checkboxStatus${listlesson.get(i - 1).id_Lesson}"></p>
                                                            </c:if>
                                                        </div>
                                                        <div class="divider"></div>
                                                    </c:if>
                                                    <!-- UPDATE LAST CODE ITER3 HERE -->
                                                    <c:if test="${i == size}">
                                                        <a href="quizlesson?id_lesson=${listlesson.get(i - 1).id_Lesson}">${listlesson.get(i - 1).name_Lesson}</a>
                                                        <p>${listlesson.get(i - 1).content_Lesson}</p>
                                                        <p><i class="fa fa-play-circle" aria-hidden="true"></i>${requestScope.countQuiz} Question</p>
                                                        <div>
                                                            <c:if test="${listboolean.get(i-1).booleanValue() == true}">
                                                                <c:set var="count" value="${count + 1}" />
                                                                <input type="checkbox" disabled="" id="showcheckboxstatus${listlesson.get(i - 1).id_Lesson}" checked>
                                                                <p class="cblocation">Done</p>
                                                                <p id="checkboxStatus${listlesson.get(i - 1).id_Lesson}"></p>
                                                            </c:if>
                                                            <c:if test="${listboolean.get(i-1).booleanValue() == false}">
                                                                <input type="checkbox" disabled="" id="showcheckboxstatus${listlesson.get(i - 1).id_Lesson}">
                                                                <p class="cblocation">Not Yet</p>
                                                                <p id="checkboxStatus${listlesson.get(i - 1).id_Lesson}"></p>
                                                            </c:if>   
                                                        </div>

                                                        <p class="progresstracking">Progress: (${count}/${requestScope.listlesson.size()} Total)| <span id="dura"></span></p>

                                                    </c:if>
                                                    <!-- UPDATE LAST CODE ITER3 HERE -->
                                                </c:forEach>
                                            </div>
                                        </div>
                                        <!----------------------------- End rectangle ----------------------------------------------->                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:if>
                    <!-- Content END-->
                    <!-- Footer ==== -->
                    <p class="phone-icon">
                        <a href="https://www.youtube.com/@giaolang" class="fa fa-external-link">Contact link</a>
                    </p>
                    <%@ include file="footer.jsp" %>
                    <!-- Footer END ==== -->
                    <!-- scroll top button -->
                    <button class="back-to-top fa fa-chevron-up" ></button>
                </div>
                <!-- External JavaScripts -->
                <script src="assets/js/jquery.min.js"></script>
                <script src="assets/vendors/bootstrap/js/popper.min.js"></script>
                <script src="assets/vendors/bootstrap/js/bootstrap.min.js"></script>
                <script src="assets/vendors/bootstrap-select/bootstrap-select.min.js"></script>
                <script src="assets/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
                <script src="assets/vendors/magnific-popup/magnific-popup.js"></script>
                <script src="assets/vendors/counter/waypoints-min.js"></script>
                <script src="assets/vendors/counter/counterup.min.js"></script>
                <script src="assets/vendors/imagesloaded/imagesloaded.js"></script>
                <script src="assets/vendors/masonry/masonry.js"></script>
                <script src="assets/vendors/masonry/filter.js"></script>
                <script src="assets/vendors/owl-carousel/owl.carousel.js"></script>
                <script src="assets/js/functions.js"></script>
                <script src="assets/js/contact.js"></script>
        </body>
    </c:if>
</html>
