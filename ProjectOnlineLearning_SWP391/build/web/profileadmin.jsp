<%-- 
    Document   : addcourse
    Created on : Jun 13, 2023, 7:19:56 PM
    Author     : PhanQuangHuy59
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <!-- Mirrored from educhamp.themetrades.com/demo/admin/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:08:15 GMT -->
    <head>
        <!-- META ============================================= -->
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="EduChamp : Education HTML Template" />

        <!-- OG -->
        <meta property="og:title" content="EduChamp : Education HTML Template" />
        <meta
            property="og:description"
            content="EduChamp : Education HTML Template"
            />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no" />

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="../error-404.html" type="image/x-icon" />
        <link
            rel="shortcut icon"
            type="image/x-icon"
            href="assets/images/favicon.png"
            />

        <!-- PAGE TITLE HERE ============================================= -->
        <title>EduChamp : Education HTML Template</title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <!--[if lt IE 9]>
          <script src="assets/js/html5shiv.min.js"></script>
          <script src="assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/assets.css" />
        <link
            rel="stylesheet"
            type="text/css"
            href="assets/vendors/calendar/fullcalendar.css"
            />

        <!-- TYPOGRAPHY ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/typography.css" />

        <!-- SHORTCODES ============================================= -->
        <link
            rel="stylesheet"
            type="text/css"
            href="assets/css/shortcodes/shortcodes.css"
            />

        <!-- STYLESHEETS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/style.css" />
        <link rel="stylesheet" type="text/css" href="assets/css/dashboard.css" />
        <link
            class="skin"
            rel="stylesheet"
            type="text/css"
            href="assets/css/color/color-1.css"
            />

        <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
         <script src="assets/js/PopUp.js"></script>
        <style>
            a:hover{
                text-decoration:none;
                color: orange;
            }

            .messagesuccess {
                display: none;
                position: fixed;
                top: 5%;
                left: 50%;
                transform: translate(-50%, -50%);
                background-color: green;
                padding:  20px 50px;
                border: 1px solid #ccc;
                border-radius: 4px;
                text-align: center;
                color: white;
                border: 1px solid orange;
                z-index: 100;
            }
            .messagefail {
                display: none;
                position: fixed;
                top: 5%;
                left: 50%;
                transform: translate(-50%, -50%);
                background-color: orange;
                padding:  20px 50px;
                border: 1px solid #ccc;
                border-radius: 4px;
                text-align: center;
                color: white;
                border: 1px solid orange;
                z-index: 100;
            }
            #active{
                color: orange;
            }
        </style>
    </head>
    <body class="ttr-opened-sidebar ttr-pinned-sidebar">
        <div id="myDiv" class="${requestScope.classmes}" style="display: none;">
            ${requestScope.mesaddcousrse}
        </div>
        <!-- header start -->
       
        <!-- header end -->
        <!-- Left sidebar menu start -->
        <div class="ttr-sidebar">
            <div class="ttr-sidebar-wrapper content-scroll">
                <!-- side menu logo start -->
                <div class="ttr-sidebar-logo">
                    <a href="#"
                       ><img alt="" src="assets/images/logo.png" width="122" height="27"
                          /></a>
                    <!-- <div class="ttr-sidebar-pin-button" title="Pin/Unpin Menu">
                                                  <i class="material-icons ttr-fixed-icon">gps_fixed</i>
                                                  <i class="material-icons ttr-not-fixed-icon">gps_not_fixed</i>
                                          </div> -->
                    <div class="ttr-sidebar-toggle-button">
                        <i class="ti-arrow-left"></i>
                    </div>
                </div>
                <!-- side menu logo end -->
                <!-- sidebar menu start -->
                <nav class="ttr-sidebar-navi">
                    <ul>

                        <li>
                            <a href="admincourselist" class="ttr-material-button">
                                <span  class="ttr-icon"><i class="ti-book"></i></span>
                                <span   class="ttr-label">Management Course</span>
                            </a>
                        </li>
                        <li>
                            <a href="adminsubject" class="ttr-material-button">
                                <span  class="ttr-icon"><i class="ti-book"></i></span>
                                <span   class="ttr-label">Management Subject</span>
                            </a>
                        </li>
                        <li>
                            <a href="useradmin" class="ttr-material-button">
                                <span class="ttr-icon"
                                      ><i class="ti-layout-list-post"></i
                                    ></span>
                                <span class="ttr-label">Management User</span>
                            </a>
                        </li>


                        <li>
                            <a href="profileadmin" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-user"></i></span>
                                <span id="active" class="ttr-label">My Profile</span>
                                <span class="ttr-arrow-icon"
                                      ><i class="fa fa-angle-down"></i
                                    ></span>
                            </a>
                            <ul>
                                <li>
                                    <a href="profileadmin" class="ttr-material-button"
                                       ><span id="active" class="ttr-label">User Profile</span></a
                                    >
                                </li>
                                <li>
                                    <a href="logout" class="ttr-material-button"
                                       ><span class="ttr-label">Logout</span></a
                                    >
                                </li>
                            </ul>
                        </li>
                        <li class="ttr-seperate"></li>
                    </ul>
                    <!-- sidebar menu end -->
                </nav>
                <!-- sidebar menu end -->
            </div>
        </div>
        <!-- Left sidebar menu end -->

        <!--Main container start -->

        <main class="ttr-wrapper">
            <c:set var="user1" value="${requestScope.user}"></c:set>
                <div class="container-fluid">
                    <div class="db-breadcrumb">
                        <h4 class="breadcrumb-title">Admin</h4>
                        <ul class="db-breadcrumb-list">
                            <li><a href="profilemarketing"><i class="ti-book"></i>Profile of ${user1.full_Name}</a></li>

                    </ul>
                </div>	
                <div class="row">
                    <!-- Your Profile Views Chart -->
                    <div class="col-lg-12 m-b30">
                        <div class="widget-box">
                            <div class="wc-title">
                                <h4>Infomation Profile</h4>
                            </div>
                            <div class="widget-inner">
                                <form action="profileadmin" method="post" class="edit-profile m-b30" enctype="multipart/form-data">

                                    <div class="row">
                                        <div class="col-12">
                                            <div class="ml-auto">
                                                <h3>1. Detail Profile</h3>
                                            </div>
                                        </div>
                                        <c:set var="user" value="${requestScope.user}"></c:set>
                                        <input type="hidden" value="${user.id_User}" name="iduser"/>
                                        <input type="hidden" value="1" name="check"/>
                                        <div class="form-group col-6">
                                            <label class="col-form-label">Display Name</label>
                                            <div>
                                                <input  name="fullname" required class="form-control" type="text" value="${user.full_Name}">

                                            </div>
                                        </div>

                                        <div class="form-group col-6">
                                            <label class="col-form-label">Email</label>
                                            <div>
                                                <input readonly name="email" class="form-control" type="text" value="${user.email}">
                                            </div>
                                        </div>
                                        <div class="form-group col-6">
                                            <label class="col-form-label">Phone Number</label>
                                            <div>
                                                <input  name="phone" pattern="0\d{9}" class="form-control" type="text" value="${user.phone_Number}">
                                            </div>
                                        </div>
                                        <div class="form-group col-6">
                                            <label class="col-form-label">Gender</label>
                                            <div style="display: flex;justify-content: space-around">
                                                <div>
                                                    <input ${user.gender == true ?"checked":""} type="radio" name="gender" value="1"/><span>Male</span>
                                                </div>
                                                <div>
                                                    <input ${user.gender == false ?"checked":""} type="radio" name="gender" value="0"/><span>Female</span>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group col-6">
                                            <label class="col-form-label">Address</label>
                                            <div>
                                                <input  name="address" class="form-control" type="text" value="${user.address}">
                                            </div>
                                        </div>
                                        <div class="form-group col-6">
                                            <label class="col-form-label">Create Date Account</label>
                                            <div>
                                                <input  name="createdate"  readonly="" class="form-control" type="date" value="${user.create_Date}">
                                            </div>
                                        </div>

                                        <div class="form-group col-6">
                                            <label required class="col-form-label">Image Course</label>
                                            <div>
                                                <img id="previewImage1" src="${user.avatar}" alt="Preview Image">
                                                <input class="form-control" name="avatar" type="file" id="imageInput1" onchange="previewImage('imageInput1', 'previewImage1')" value="${requestScope.imageCourse}">
                                                <p style="color: red">${requestScope.mes1}</p>
                                            </div>

                                        </div>
                                        <div class="seperator"></div>


                                        <div class="col-12">
                                            <input class="btn-secondry add-item m-r5" type="submit" value="Update Profile"/>
                                            <button type="button" class="btn" onclick="closeWindowAndPreventSubmit(event)"> Cancel Update Profile</button>
                                        </div>
                                    </div>


                            </div>
                        </div>
                    </div>
                    <!-- Your Profile Views Chart END-->
                </div>
            </div>
            <!-- Card -->







        </main>

        <div class="ttr-overlay"></div>

        <script>
            function deletefunction(idpost, namepost) {
                if (window.confirm("Are you sure you want to delete this " + namepost + " ?")) {
                    window.location = "deletepost?idpost=" + idpost;
                }
            }
            function previewImage(inputId, imageId) {
                var input = document.getElementById(inputId);
                var previewImage = document.getElementById(imageId);

                if (input.files && input.files[0]) {
                    var file = input.files[0];
                    var fileExtension = file.name.substring(file.name.lastIndexOf('.')).toLowerCase();
                    var allowedExtensions = ['.jpg', '.png'];

                    if (allowedExtensions.includes(fileExtension)) {
                        var reader = new FileReader();

                        reader.onload = function (e) {
                            previewImage.src = e.target.result;
                        };

                        reader.readAsDataURL(file);
                    } else {
                        // Đuôi tệp tin không hợp lệ, xử lý theo nhu cầu của bạn
                        console.log('Chỉ cho phép chọn tệp tin có đuôi .jpg hoặc .png.');
                    }
                }
            }
            window.onload = function () {
                var id = 1;

                if (id == 1) {
                    var myDiv = document.getElementById("myDiv");

                    setTimeout(function () {
                        myDiv.style.display = "block";

                        setTimeout(function () {
                            myDiv.style.display = "none";
                        }, 3000);
                    }, 0);
                }
            };
        </script>


        <!-- External JavaScripts -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/vendors/bootstrap/js/popper.min.js"></script>
        <script src="assets/vendors/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/vendors/bootstrap-select/bootstrap-select.min.js"></script>
        <script src="assets/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
        <script src="assets/vendors/magnific-popup/magnific-popup.js"></script>
        <script src="assets/vendors/counter/waypoints-min.js"></script>
        <script src="assets/vendors/counter/counterup.min.js"></script>
        <script src="assets/vendors/imagesloaded/imagesloaded.js"></script>
        <script src="assets/vendors/masonry/masonry.js"></script>
        <script src="assets/vendors/masonry/filter.js"></script>
        <script src="assets/vendors/owl-carousel/owl.carousel.js"></script>
        <script src="assets/vendors/scroll/scrollbar.min.js"></script>
        <script src="assets/js/functions.js"></script>
        <script src="assets/vendors/chart/chart.min.js"></script>
        <script src="assets/js/admin.js"></script>
        <script src="assets/vendors/calendar/moment.min.js"></script>
        <script src="assets/vendors/calendar/fullcalendar.js"></script>

    </body>
</html>

