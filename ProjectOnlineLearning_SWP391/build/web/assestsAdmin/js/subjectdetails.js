/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/ClientSide/javascript.js to edit this template
 */


function checkAccess(session_user_id, userid_in_subject, suid) {
    // Kiểm tra quyền truy cập ở đây
    if (session_user_id === userid_in_subject) {
        window.location.href = "subjectdetails?subjectid=" + suid;
    } else {
        alert("You dont have permission!");
    }
}

function showLessonNumber() {

    var lessonNumberDiv = document.getElementById("addfunction");
    if (lessonNumberDiv.style.display !== "block") {
        lessonNumberDiv.style.display = "block";
    }
}

function editCourseNumber(number) {

    var lessonNumberDiv = document.getElementById("editfunction" + number);
    if (lessonNumberDiv.style.display !== "block") {
        lessonNumberDiv.style.display = "block";
    }
}

function searchByName(param) {
    var txtSearch = param.value;
    $.ajax({
        url: "/projectswp391/searchsubjectdetailsajax",
        type: "get",
        data: {
            txt: txtSearch
        },
        success: function (data) {
            var row = document.getElementById("content");
            row.innerHTML = data;
        },
        error: function (xhr) {

        }
    });
}


function searchAuthorName(thisisavalue) {
    var txtSearch = thisisavalue.value;
    $.ajax({
        url: "/projectswp391/searchsubjectdetailsajaxteachername",
        type: "get",
        data: {
            txt: txtSearch
        },
        success: function (data3) {
            var row = document.getElementById("authorname");
            row.innerHTML = data3;
        },
        error: function (xhr) {

        }
    });
}

function changeSubjectName(option, id3) {
    var dropdownButton = document.querySelector('.element-to-take-5');
    dropdownButton.innerHTML = option;
    var authorNameVal = document.getElementById("isthisnotnull");
    authorNameVal.value = id3;
    event.preventDefault();
}

function changeAuthorName(option, id3) {
    var dropdownButton = document.querySelector('.element-to-take-4');
    dropdownButton.innerHTML = option;
    var authorNameVal = document.getElementById('authorinput');
    authorNameVal.value = id3;
    event.preventDefault();
}

function searchCourseCate(text) {
    var txtSearch = text.value;
    $.ajax({
        url: "/projectswp391/searchsubjectdetailsajaxcoursecate",
        type: "get",
        data: {
            txt: txtSearch
        },
        success: function (data1) {
            var row = document.getElementById("coursecontent");
            row.innerHTML = data1;
        },
        error: function (xhr) {

        }
    });
}

function searchPackagePriceSubjectName(text) {
    var txtSearch = text.value;
    $.ajax({
        url: "/projectswp391/searchpricepackagesubjectnameajax",
        type: "get",
        data: {
            txt: txtSearch
        },
        success: function (data1) {
            var row = document.getElementById("packagecontent");
            row.innerHTML = data1;
        },
        error: function (xhr) {

        }
    });
}

function validateFormAdd(event) {
    var addvalue = document.getElementById('addfunctionforpp');
    var selectButton = document.getElementById("selectButtonSubjectName");
    var selectedValue = document.getElementById("pricepackageinputname").value;
    var selectedValueStatus = document.getElementById("pricePackageStatus").value;
    var selectButtonStatus = document.getElementById("selectedStatus");
    if (selectedValue === "" || selectedValueStatus === "") {
        if (selectedValue === "") {
            event.preventDefault();
            selectButton.style.backgroundColor = "red";
        } else {
            selectButton.style.backgroundColor = "";
        }
        if (selectedValueStatus === "") {
            event.preventDefault();
            selectButtonStatus.style.backgroundColor = "red";
        } else {
            selectButtonStatus.style.backgroundColor = ""; // Đặt lại màu nền về mặc định
        }
    } else {
        addvalue.value = "add";
        selectButton.style.backgroundColor = "";
        selectButtonStatus.style.backgroundColor = "";
    }

}

function validateFormEdit(id) {
    var form = document.getElementById("PackagePriceForm" + id);
    var editvalue = document.getElementById('editfunctionforpp' + id);
    form.submit();
    editvalue.value = "edit";
}

function validateFormStatus(event) {

    var selectedValueStatus = document.getElementById("pricepackageinputname").value;
    var selectButtonStatus = document.getElementById("selectedStatus");
    if (selectedValueStatus === "") {
        event.preventDefault();
        selectButtonStatus.style.backgroundColor = "red";
    } else {
        selectButtonStatus.style.backgroundColor = ""; // Đặt lại màu nền về mặc định
    }
}

function AddStatusPP(status) {
    const button = document.querySelector('.element-to-take-status');
    const input = document.getElementById('pricePackageStatus');
    if (status) {
        button.textContent = 'Active';
        input.value = 'true';
    } else {
        button.textContent = 'Deactive';
        input.value = 'false';
    }
}

function editStatusPP(status1, bu, inp) {
    const button = document.querySelector('.element-to-take-edit' + bu);
    const input = document.getElementById('editPPStatus' + inp);
    if (status1) {
        button.textContent = 'Active';
        input.value = 'true';
    } else {
        button.textContent = 'Deactive';
        input.value = 'false';
    }
}

function updateStatus(status) {
    const button = document.querySelector('.element-to-take-1');
    const input = document.getElementById('statusInput');
    if (status) {
        button.textContent = 'Published';
        input.value = 'true';
    } else {
        button.textContent = 'Unpublished';
        input.value = 'false';
    }
}

function updateStatusCourse(status1) {
    const button = document.querySelector('.element-to-take-10');
    const input = document.getElementById('statusInputCourse');
    if (status) {
        button.textContent = 'Published';
        input.value = 'true';
    } else {
        button.textContent = 'Unpublished';
        input.value = 'false';
    }
}

function cancel(id1, id2) {
    window.location.href = "packageprice?subjectid=" + id1 + "&&pplistid=" + id2;
    event.preventDefault();
}

function cancelNoId(id2) {
    window.location.href = "packageprice?pplistid=" + id2;
    event.preventDefault();
}

function back() {
    window.location.href = "expertsubjectlistservlet";
    event.preventDefault();
}

function backAdmin() {
    window.location.href = "adminsubject";
    event.preventDefault();
}

function changeCourse(option, id2) {
    var dropdownButton = document.querySelector('.element-to-take-3');
    dropdownButton.innerHTML = option;
    var selectedValueInput = document.getElementById('courseValue');
    selectedValueInput.value = id2;
    event.preventDefault();
}

function changeDurationPrice(option, id2, id3, id4, id5) {
    var dropdownButton = document.querySelector('.element-to-take-5');
    dropdownButton.innerHTML = option;
    var selectedValueInput = document.getElementById('priceId');
    selectedValueInput.value = id2;
    var selectedValueInput2 = document.getElementById('statuspricepackage');
    selectedValueInput2.value = id3;
    var selectedValueInput3 = document.getElementById('listPricePricePackage');
    selectedValueInput3.value = id4;
    var selectedValueInput4 = document.getElementById('salePricePricePackage');
    selectedValueInput4.value = id5;
    if (id3) {
        selectedValueInput2.innerHTML = "Published";
    } else {
        selectedValueInput2.innerHTML = "Unpublished";
    }
    event.preventDefault();
}

function updateStatuspackagePrice(option, id3) {
    var dropdownButton = document.querySelector('.element-to-take-11');
    dropdownButton.innerHTML = option;
    var selectedValueInput2 = document.getElementById('statuspricepackage');
    selectedValueInput2.value = id3;
    if (option) {
        selectedValueInput2.innerHTML = "Published";
        document.getElementById('statusValue2').value = true;
    } else {
        selectedValueInput2.innerHTML = "Unpublished";
        document.getElementById('statusValue2').value = false;
    }
    event.preventDefault();
}
function changeText(option, id) {
    var dropdownButton = document.querySelector('.element-to-take-2');
    dropdownButton.innerHTML = option;
    var selectedValueInput = document.getElementById('selectedValue2');
    selectedValueInput.value = id;
    event.preventDefault();
}

function updateInputDescription() {
    var textarea = document.getElementById('myTextarea');
    var input = document.getElementById('myInput');

    input.value = textarea.value;
}

function updateInputDescription1() {
    var textarea = document.getElementById('myTextarea1');
    var input = document.getElementById('myInput1');

    input.value = textarea.value;
}

function previewImage(inputId, imageId) {
    var input = document.getElementById(inputId);
    var previewImage = document.getElementById(imageId);

    if (input.files && input.files[0]) {
        var file = input.files[0];
        var fileExtension = file.name.substring(file.name.lastIndexOf('.')).toLowerCase();
        var allowedExtensions = ['.jpg', '.png'];

        if (allowedExtensions.includes(fileExtension)) {
            var reader = new FileReader();

            reader.onload = function (e) {
                previewImage.src = e.target.result;
            };

            reader.readAsDataURL(file);
        } else {
            // Đuôi tệp tin không hợp lệ, xử lý theo nhu cầu của bạn
            console.log('Chỉ cho phép chọn tệp tin có đuôi .jpg hoặc .png.');
        }
    }
}

function previewImageCourse(inputId2, imageId2) {
    var input2 = document.getElementById(inputId2);
    var previewImage2 = document.getElementById(imageId2);

    if (input2.files && input2.files[0]) {
        var file = input2.files[0];
        var fileExtension = file.name.substring(file.name.lastIndexOf('.')).toLowerCase();
        var allowedExtensions = ['.jpg', '.png'];

        if (allowedExtensions.includes(fileExtension)) {
            var reader = new FileReader();

            reader.onload = function (e) {
                previewImage2.src = e.target.result;
            };

            reader.readAsDataURL(file);
        } else {
            // Đuôi tệp tin không hợp lệ, xử lý theo nhu cầu của bạn
            console.log('Chỉ cho phép chọn tệp tin có đuôi .jpg hoặc .png.');
        }
    }
}

function check(event) {
    var v16 = document.getElementById("statusInputCourse").value; // CourseStatus
    event.preventDefault();
    alert("v16: " + v16);
}

function doDeletePackagePrice(id) {
    if (confirm("Are you sure to delete Price Package With ID: " + id)) {
        window.location = "packagepricedelete?id=" + id;
    }
}

function doDeletePackagePriceAdmin(id) {
    if (confirm("Are you sure to delete Price Package With ID: " + id)) {
        window.location = "packagepriceadmindelete?id=" + id;
    }
}

function searchPackagePriceAtFirst(text) {
    var txtSearch = text.value;
    $.ajax({
        url: "/projectswp391/searchpackagepricecoursenameajax",
        type: "get",
        data: {
            txt: txtSearch
        },
        success: function (data1) {
            var row = document.getElementById("searchcontent");
            row.innerHTML = data1;
        },
        error: function (xhr) {

        }
    });
}
//update hungct 11/7/2023
function warningUser() {
    alert("You don't have permission");
}

function showAddPackage() {
    var lessonNumberDiv = document.getElementById("addpackagefunction");
    var lessonAdd = document.getElementById("editPackageFunction");
    if (lessonNumberDiv.style.display !== "block") {
        lessonNumberDiv.style.display = "block";
        if (lessonAdd.style.display === "block") {
            lessonAdd.style.display = "none";
        }
    }
}

function showEditPackage() {
    var lessonNumberDiv = document.getElementById("editPackageFunction");
    var lessonAdd = document.getElementById("addpackagefunction");
    if (lessonNumberDiv.style.display !== "block") {
        lessonNumberDiv.style.display = "block";
        if (lessonAdd.style.display === "block") {
            lessonAdd.style.display = "none";
        }
    }
}

function closePricePackage() {
    var lessonNumberDiv = document.getElementById("addpackagefunction");
    var clo = document.getElementById("closeNoAdd");
    clo.value = "No";
    if (lessonNumberDiv.style.display === "block") {
        lessonNumberDiv.style.display = "none";
    }
}
//update hungct 11/7/2023