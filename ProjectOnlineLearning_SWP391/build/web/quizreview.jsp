<%-- 
    Document   : reviewprogress_popup
    Created on : Jun 4, 2023, 2:43:40 PM
    Author     : Naviank
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>

        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="Online Learning System - Group 2 SE1714" />

        <!-- OG -->
        <meta property="og:title" content="Online Learning System - Group 2 SE1714" />
        <meta property="og:description" content="Online Learning System - Group 2 SE1714" />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no">

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />

        <!-- PAGE TITLE HERE ============================================= -->
        <!--<title>Online Learning System - Group 2 SE1714</title>-->
        <title>Review quiz</title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/assets.css">
        <link rel="stylesheet" type="text/css" href="assets/css/pkvstyle.css">

        <!-- TYPOGRAPHY ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/typography.css">

        <!-- SHORTCODES ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/shortcodes/shortcodes.css">

        <!-- STYLESHEETS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <link rel="stylesheet" type="text/css" href="assets/css/addition_style.css">
        <link class="skin" rel="stylesheet" type="text/css" href="assets/css/color/color-1.css">

        <!-- REVOLUTION SLIDER CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/vendors/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="assets/vendors/revolution/css/layers.css">
        <link rel="stylesheet" type="text/css" href="assets/vendors/revolution/css/settings.css">
        <link rel="stylesheet" type="text/css" href="assets/vendors/revolution/css/navigation.css">
        <!-- REVOLUTION SLIDER END -->	
        <link rel="stylesheet" type="text/css" href="assets/css/vanpk_style.css">

    </head>
    <body onload="createDaySelect()">
        <div class="page-wraper">
            <jsp:include page="header_1.jsp"/>
            <div class="page-banner ovbl-dark" style="background-image:url(assets/images/banner/banner1.jpg);height: 120px">
                <div class="container">
                </div>
            </div>
            <c:set var="quiz" value="${requestScope.quiz}" />
            <c:set var="listres" value="${sessionScope.listres}" />
            <c:set var="listmark" value="${requestScope.listmark}" />
            <c:set var="count" value="${1}" />
            <c:set var="order" value="${false}" />
            <div class="breadcrumb-row">
                <div class="container">
                    <ul class="list-inline">
                        <ul class="list-inline">
                            <div class="row">
                                <i class="ti-arrow-left"></i> &nbsp;
                                <li style=""><a href="quizlesson?id_lesson=${quiz.id_Lession}">Quiz lesson</a></li>
                            </div>
                            </li>
                        </ul>
                    </ul>
                </div>
            </div>
            <div class="review-title">
                <span>Subject: ${requestScope.subjname}</span>
                <span>Lesson: ${requestScope.less.name_Lesson}</span>
                <span>Review for: ${quiz.name_Quiz}</span>
            </div>
            <div class="review-body">
                <div class="review-filter text-center">
                    <form action="reviewquiz" method="post">
                        <label for="month">Filter by date:</label>&nbsp;&nbsp;
                        <label for="month">Select month:</label>
                        <select id="month" name="month">
                            <option value="">--Select month--</option>
                            <c:set var="i" value="1"/>
                            <option ${i == requestScope.month ? "selected" : ""} value="01" >January</option>
                            <c:set var="i" value="${i + 1}"/>
                            <option ${i == requestScope.month ? "selected" : ""} value="02">February</option>
                            <c:set var="i" value="${i + 1}"/>
                            <option ${i == requestScope.month ? "selected" : ""} value="03">March</option>
                            <c:set var="i" value="${i + 1}"/>
                            <option ${i == requestScope.month ? "selected" : ""} value="04">April</option>
                            <c:set var="i" value="${i + 1}"/>
                            <option ${i == requestScope.month ? "selected" : ""} value="05">May</option>
                            <c:set var="i" value="${i + 1}"/>
                            <option ${i == requestScope.month ? "selected" : ""} value="06">June</option>
                            <c:set var="i" value="${i + 1}"/>
                            <option ${i == requestScope.month ? "selected" : ""} value="07">July</option>
                            <c:set var="i" value="${i + 1}"/>
                            <option ${i == requestScope.month ? "selected" : ""} value="08">August</option>
                            <c:set var="i" value="${i + 1}"/>
                            <option ${i == requestScope.month ? "selected" : ""} value="09">September</option>
                            <c:set var="i" value="${i + 1}"/>
                            <option ${i == requestScope.month ? "selected" : ""} value="10">October</option>
                            <c:set var="i" value="${i + 1}"/>
                            <option ${i == requestScope.month ? "selected" : ""} value="11">November</option>
                            <c:set var="i" value="${i + 1}"/>
                            <option ${i == requestScope.month ? "selected" : ""} value="12">December</option>
                        </select>&nbsp;&nbsp;&nbsp;&nbsp;

                        <label for="day">From day:</label>
                        <select id="day" name="day">
                            <option value="">--Select day--</option>
                        </select>
                        <span>&nbsp;&nbsp;to now&nbsp;&nbsp;</span>
                        <!--<input type="hidden" name="quiz_id" value="${quiz.id_Quiz}">-->
                        <button type="submit">Filter</button>
                        <br/>
                        <label>Type of quiz:&nbsp;&nbsp;</label>
                        <select id="quizid-sl" name="quiz_id" onchange="this.form.submit()">
                            <c:forEach items="${requestScope.listquiz}" var="qi">
                                <option value="${qi.id_Quiz}" ${quiz.id_Quiz == qi.id_Quiz ? "selected" : ""}>${qi.name_Quiz}</option>
                            </c:forEach>
                        </select>
                    </form>
                    <hr/>
                    <div class="order-filt">
                        <label>Sort table:&nbsp;&nbsp;</label>
                        <button id="reversebtn">Reverse order</button>
                        <button onclick="sortTable1()">Sort by Mark</button>
                        <!--<button onclick="location.reload()">Revert to original</button>-->
                        <button onclick="location.href = 'reviewquiz?quiz_id=${quiz.id_Quiz}'">Clear filter</button>
                    </div>
                    <div class="quiz-filt">
                    </div>
                </div>

                <div class="review-board">
                    <div class="container">
                        <div style="text-align: left;" class="row">
                            <div class="col-md-12">
                                <div class="table-wrap">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th class="rv-od">#</th>
                                                <th class="rv-tm">Start time</th>
                                                <th class="rv-dr">Duration</th>
                                                <th class="rv-gr">Marks</th>
                                                <th class="rv-gr">Grade</th>
                                                <th class="rv-at">Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="container">
                        <div style="text-align: left;" class="row">
                            <div class="col-md-12">
                                <div class="table-wrap">
                                    <table class="table table-striped" id="myTable">
                                        <thead></thead>
                                        <tbody>
                                            <c:forEach items="${listres}" var="cr">
                                                <c:set var="tmp" value="${cr.duration}"/>
                                                <tr class="revertable">
                                                    <td  class="rv-od">${count}</td>
                                                    <td  class="rv-tm">${cr.start_Time.toLocaleString()}</td>
                                                    <td  class="rv-dr">
                                                        ${(tmp / 3600).intValue()}h:${((tmp % 3600) / 60).intValue()}m:${(tmp % 3600) % 60}s
                                                    </td>
                                                    <td  class="rv-gr">${listmark[count - 1]}/${quiz.number_Question}</td>
                                                    <td  class="rv-gr"><fmt:formatNumber value="${(10 / quiz.number_Question) * listmark[count - 1]}" maxFractionDigits="1"/>/10</td>
                                                    <td  class="rv-at">
                                                        <a class="btn" href="reviewdetails?id_Result=${cr.id_Result}">Review</a>
                                                    </td>
                                                </tr> 
                                                <c:set var="count" value="${count + 1}"/>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="btn-attempt text-center">
                    <button onclick="startQuiz(${quiz.id_Quiz})">Attempt quiz</button>
                </div>
            </div>

        </div>

        <script 
            src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js">
        </script>
        <script>
            function sortTable1() {
                var table, rows, switching, i, x, y, shouldSwitch;
                table = document.getElementById("myTable");
                switching = true;
                /*Make a loop that will continue until
                 no switching has been done:*/
                while (switching) {
                    //start by saying: no switching is done:
                    switching = false;
                    rows = table.rows;
                    /*Loop through all table rows (except the
                     first, which contains table headers):*/
                    for (i = 0; i < (rows.length - 1); i++) {
                        //start by saying there should be no switching:
                        shouldSwitch = false;
                        /*Get the two elements you want to compare,
                         one from current row and one from the next:*/
                        x = rows[i].getElementsByTagName("TD")[3];
                        y = rows[i + 1].getElementsByTagName("TD")[3];
                        //check if the two rows should switch place:
                        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                            //if so, mark as a switch and break the loop:
                            shouldSwitch = true;
                            break;
                        }
                    }
                    if (shouldSwitch) {
                        /*If a switch has been marked, make the switch
                         and mark that a switch has been done:*/
                        rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                        switching = true;
                    }
                }
            }
        </script>
        <script>
            function sortTable2() {
                var table, rows, switching, i, x, y, shouldSwitch;
                table = document.getElementById("myTable");
                switching = true;
                /*Make a loop that will continue until
                 no switching has been done:*/
                while (switching) {
                    //start by saying: no switching is done:
                    switching = false;
                    rows = table.rows;
                    /*Loop through all table rows (except the
                     first, which contains table headers):*/
                    for (i = 0; i < (rows.length - 1); i++) {
                        //start by saying there should be no switching:
                        shouldSwitch = false;
                        /*Get the two elements you want to compare,
                         one from current row and one from the next:*/
                        x = rows[i].getElementsByTagName("TD")[3];
                        y = rows[i + 1].getElementsByTagName("TD")[3];
                        //check if the two rows should switch place:
                        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                            //if so, mark as a switch and break the loop:
                            shouldSwitch = true;
                            break;
                        }
                    }
                    if (shouldSwitch) {
                        /*If a switch has been marked, make the switch
                         and mark that a switch has been done:*/
                        rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                        switching = true;
                    }
                }
            }
        </script>
        <script src="assets/js/quizattempt.js"></script>
        <script src="assets/js/quizreview.js"></script>
        <script src="assets/js/PopUp.js"></script>
    </body>
</html>
