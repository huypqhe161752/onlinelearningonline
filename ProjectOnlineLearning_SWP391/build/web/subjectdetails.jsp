<%-- 
    Document   : PageCourse
    Created on : May 17, 2023, 3:51:55 PM
    Author     : PCT
--%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from educhamp.themetrades.com/demo/admin/courses.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:10:19 GMT -->
    <head>
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/subjectlist.css">
        <script src="assestsAdmin/js/subjectdetails.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link href="assetsAdmin/select2/select2.min.css" rel="stylesheet" />
        <script src="assetsAdmin/select2/select2.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
        <script src="assets/js/PopUp.js"></script>
        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="EduChamp : Education HTML Template" />

        <!-- OG -->
        <meta property="og:title" content="EduChamp : Education HTML Template" />
        <meta property="og:description" content="EduChamp : Education HTML Template" />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no">

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="../error-404.html" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />

        <!-- PAGE TITLE HERE ============================================= -->
        <title>EduChamp : Education HTML Template </title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/assets.css">
        <link rel="stylesheet" type="text/css" href="assestsAdmin/vendors/calendar/fullcalendar.css">

        <!-- TYPOGRAPHY ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/typography.css">

        <!-- SHORTCODES ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/shortcodes/shortcodes.css">

        <!-- STYLESHEETS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/style.css">
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/dashboard.css">
        <link class="skin" rel="stylesheet" type="text/css" href="assets/css/color/color-1.css">
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/SubjectAdmin.css">
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/page.css">
    </head>
</head>
<body class="ttr-opened-sidebar ttr-pinned-sidebar">
    <!-- header start -->
    <header class="ttr-header">
        <div class="ttr-header-wrapper">
            <!--sidebar menu toggler start -->
            <div class="ttr-toggle-sidebar ttr-material-button">
                <i class="ti-close ttr-open-icon"></i>
                <i class="ti-menu ttr-close-icon"></i>
            </div>
            <!--sidebar menu toggler end -->
            <!--logo start -->
            <div class="ttr-logo-box">
                <div>
                    <a href="index.html" class="ttr-logo">
                        <img alt="" class="ttr-logo-mobile" src="assets/images/logo-mobile.png" width="30" height="30">
                        <img alt="" class="ttr-logo-desktop" src="assets/images/logo-white.png" width="160" height="27">
                    </a>
                </div>
            </div>
            <!--logo end -->
            <div class="ttr-header-menu">
                <!-- header left menu start -->
                <!-- header left menu end -->
            </div>
            <div class="ttr-header-right ttr-with-seperator">
                <!-- header right menu start -->
                <ul class="ttr-header-navigation">

                    <c:if test="${sessionScope.user!=null}">

                        <li> 
                            <a href="userprofile">
                                <img 
                                    src="${sessionScope.user.avatar}" 
                                    style="height: 48px; width: 48px; overflow: hidden; border-radius: 50%;"
                                    alt="${sessionScope.user.full_Name}"/>
                            </a>
                        </li>
                        <li><a href="userprofile">${user.full_Name}</a></li>
                        <li><a href="logout">Logout</a></li>
                        </c:if> 
                </ul>
                <!-- header right menu end -->
            </div>
            <!--header search panel start -->
            <div class="ttr-search-bar">
                <form class="ttr-search-form">
                    <div class="ttr-search-input-wrapper">
                        <input type="text" name="qq" placeholder="search something..." class="ttr-search-input">
                        <button type="submit" name="search" class="ttr-search-submit"><i class="ti-arrow-right"></i></button>
                    </div>
                    <span class="ttr-search-close ttr-search-toggle">
                        <i class="ti-close"></i>
                    </span>
                </form>
            </div>
            <!--header search panel end -->
        </div>
    </header>
    <!-- header end -->
    <!-- Left sidebar menu start -->
    <c:set value="${sessionScope.user}" var="su"/>
    <div class="ttr-sidebar">
        <div class="ttr-sidebar-wrapper content-scroll">
            <!-- side menu logo start -->
            <div class="ttr-sidebar-logo">
                <a href="#"><img alt="" src="assets/images/logo.png" width="122" height="27"></a>
                <!-- <div class="ttr-sidebar-pin-button" title="Pin/Unpin Menu">
                        <i class="material-icons ttr-fixed-icon">gps_fixed</i>
                        <i class="material-icons ttr-not-fixed-icon">gps_not_fixed</i>
                </div> -->
                <div class="ttr-sidebar-toggle-button">
                    <i class="ti-arrow-left"></i>
                </div>
            </div>
            <!-- side menu logo end -->
            <!-- sidebar menu start -->
            <nav class="ttr-sidebar-navi">
                <c:set var="currentpage" value="${requestScope.xxx}"/>
                <c:set var="currentpageDetails" value="${requestScope.xxxDetails}"/>
                <ul>

                    <li>
                        <a href="expertsubjectlistservlet" class="ttr-material-button">
                            <span class="ttr-icon"><i class="ti-book"></i></span>
                            <span class="ttr-label ${currentpage eq "SubjectManagment"? 'actived':""}">Subject Management</span>
                        </a>
                    </li>
                    <li style="margin-left: 60px">
                        <a href="#" class="ttr-material-button">
                            <span class="ttr-icon"><i class="ti-pencil-alt"></i></span>
                            <span class="ttr-label ${currentpageDetails eq "SubjectDetails"? 'actived':""}">Subject Detail</span>
                        </a>
                    </li>
                    <li style="margin-left: 60px">
                        <a href="subjectdimension?subjectid=${requestScope.asubjectwithID.id_Subject}" class="ttr-material-button">
                            <span class="ttr-icon"><i class="ti-pencil-alt"></i></span>
                            <span class="ttr-label ${currentpage eq "subjectdimension"? 'actived':""}">Subject Dimension</span>
                        </a>
                    </li>
                    <li style="margin-left: 60px">
                        <a href="packageprice?subjectid=${requestScope.asubjectwithID.id_Subject}&&pplistid=1" class="ttr-material-button">
                            <span class="ttr-icon"><i class="ti-pencil-alt"></i></span>
                            <span class="ttr-label ${currentpage eq "packageprice"? 'actived':""}">Price Package</span>
                        </a>
                    </li>
                    <li>
                        <a href="quizzeslist" class="ttr-material-button">
                            <span class="ttr-icon"><i class="ti-book"></i></span>
                            <span class="ttr-label">Quizzes Management</span>
                        </a>
                    </li>
                    <li>
                        <a href="listquestions" class="ttr-material-button">
                            <span class="ttr-icon"><i class="ti-book"></i></span>
                            <span class="ttr-label">Questions Management</span>
                        </a>
                    </li>
                    <li>
                        <a href="profileexpert" class="ttr-material-button">
                            <span class="ttr-icon"><i class="ti-user"></i></span>
                            <span id="active" class="ttr-label">My Profile</span>
                            <span class="ttr-arrow-icon"
                                  ><i class="fa fa-angle-down"></i
                                ></span>
                        </a>
                        <ul>
                            <li>
                                <a href="profileexpert" class="ttr-material-button"
                                   ><span id="active" class="ttr-label">User Profile</span></a
                                >
                            </li>
                            <li>
                                <a href="logout" class="ttr-material-button"
                                   ><span class="ttr-label">Logout</span></a
                                >
                            </li>
                        </ul>
                    </li>

                    <li class="ttr-seperate"></li>
                </ul>
            </nav>
            <!-- sidebar menu end -->
        </div>
    </div>
    <!-- Left sidebar menu end -->

    <!--Main container start -->
    <main class="ttr-wrapper">
        <div class="container-fluid">
            <div class="db-breadcrumb">
                <!-- Nav tabs -->


                <!-- Tab panes -->

                <h4 class="breadcrumb-title">Subject Management</h4>
                <c:set value="${requestScope.ppofcourse}" var="ppcourse"/>
                <ul class="db-breadcrumb-list">
                    <li>Subject Detail</li>
                </ul>
            </div>
            <div class="row">
                <!-- Your Profile Views Chart -->
                <div class="col-lg-12 m-b30">
                    <div class="widget-box">
                        <div class="wc-title">
                            <ul class="nav nav-tabs" role="tablist">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#home1">Subject Detail</a></li>
                                    <li><a href="packageprice?subjectid=${requestScope.asubjectwithID.id_Subject}&&pplistid=1">Package Price</a></li>
                                    <li><a href="subjectdimension?subjectid=${requestScope.asubjectwithID.id_Subject}">Dimension</a></li>
                                </ul>
                            </ul>
                        </div>
                        <c:set value="${requestScope.userID}" var="u"/>
                        <c:set value="${requestScope.categorySubjectName}" var="csn"/>
                        <c:set value="${requestScope.asubjectwithID}" var="aswid"/>
                        <c:set value="${requestScope.courseID}" var="cid"/>
                        <div class="widget-inner">
                            <div class="tab-content">
                                <div id="home1" class="container tab-pane active"><br>

                                    <form class="edit-profile m-b30" action="subjectdetails" method="post" enctype="multipart/form-data">
                                        <input hidden type="text" id="t9" name="subjectIDinput" value="${aswid.id_Subject}">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="ml-auto">
                                                    <h2>Subject Detail</h2>
                                                </div>
                                            </div>
                                            <div class="form-group col-6">
                                                <label class="col-form-label">Create Date</label>
                                                <div>
                                                    <input readonly="" class="form-control" id="cds" name="createDateSubject" type="date" value="${aswid.create_Date}">
                                                </div>
                                            </div>
                                            <div class="form-group col-6">
                                                <label class="col-form-label">Recent Update</label>
                                                <div>
                                                    <input readonly="" id="t7" name="updateDateSubject" class="form-control" type="date" value="${aswid.update_Date}">
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="ml-auto">
                                                    <h3>1. Subject information</h3>
                                                </div>
                                            </div>

                                            <div class="form-group col-6">
                                                <label class="col-form-label">Subject Name</label>
                                                <div>
                                                    <input required="" class="form-control" id="t1" name="subjectName" type="text" value="${aswid.subject_Name}">
                                                </div>
                                            </div>
                                            <div class="form-group col-6">
                                                <label class="col-form-label">Subject Status</label>
                                                <c:if test="${su.id_role == 1}">
                                                    <div class="dropdown">
                                                        <button class="btn-success element-to-take-1" type="button" id="subjectButtonstyl" data-toggle="dropdown">
                                                            ${aswid.status == "true" ? "Published" : "Unpublished"}<i class="ti-arrow-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="#" onclick="updateStatus(false)">Unpublished</a></li>
                                                            <li><a href="#" onclick="updateStatus(true)">Published</a></li>
                                                        </ul>
                                                        <input type="hidden" id="statusInput" name="subjectStatus" value="${aswid.status}">
                                                    </div>
                                                </c:if>
                                                <c:if test="${su.id_role == 2}">
                                                    <div class="dropdown">
                                                        <button class="btn gradient gray" class="element-to-take-1" onclick="warningUser()" type="button" id="subjectButtonstyl" data-toggle="dropdown">
                                                            ${aswid.status == "true" ? "Published" : "Unpublished"}
                                                        </button>
                                                        <input type="hidden" id="statusInput" name="subjectStatus" value="${aswid.status}">
                                                    </div>
                                                </c:if>
                                            </div>

                                            <div class="form-group col-6">
                                                <label class="col-form-label">Teacher Name</label>
                                                <c:if test="${su.id_role == 1}">
                                                    <div class="dropdown">
                                                        <button class="btn-success element-to-take-4" type="button" id="subjectButtonstyl" data-toggle="dropdown">
                                                            ${u.full_Name}</button>
                                                        <ul class="dropdown-menu scrollable">
                                                            <li class="input-container-todata">
                                                                <input oninput="searchAuthorName(this)" id="teacherNamestyl" type="text" style="width: 450px" value="">
                                                                <i class="fa fa-search" aria-hidden="true"></i>
                                                            </li>
                                                            <li id="authorname">
                                                                <c:forEach items="${requestScope.listAuthoer}" var="listDauthor">

                                                                    <a href="#" onclick="changeAuthorName('${listDauthor.full_Name}', '${listDauthor.id_User}')">${listDauthor.full_Name}</a>

                                                                </c:forEach>
                                                            </li>
                                                        </ul>
                                                        <input type="hidden" id="authorinput" name="teacherName" value="${u.id_User}">
                                                    </div>
                                                </c:if>
                                                <c:if test="${su.id_role == 2}">
                                                    <div class="dropdown">
                                                        <button class="btn gradient gray" class="element-to-take-4" onclick="warningUser()" type="button" id="subjectButtonstyl" data-toggle="dropdown">
                                                            ${u.full_Name}</button>
                                                        <input type="hidden" id="authorinput" name="teacherName" value="${u.id_User}">
                                                    </div>
                                                </c:if>
                                            </div>
                                            <div class="form-group col-6">
                                                <label class="col-form-label">Number Of Lesson</label>
                                                <div>
                                                    <input required="" class="form-control" id="t6" name="lessonNumber" type="text" value="${aswid.numberLesson}">
                                                </div>
                                            </div>
                                            <div class="form-group col-6">
                                                <label class="col-form-label">Subject Image</label>
                                                <div>
                                                    <img id="previewImage1" src="${aswid.image_subject}" alt="Preview Image">
                                                    <input class="form-control" name="fileInputSubject" type="file" id="imageInput1" onchange="previewImage('imageInput1', 'previewImage1')">
                                                </div>
                                            </div>
                                            <div class="form-group col-6">
                                                <label class="col-form-label">Category Subject</label>
                                                <div class="dropdown">
                                                    <button class="btn-success element-to-take-2" type="button" id="subjectButtonstyl" data-toggle="dropdown">
                                                        ${csn.getName_Catergory_Subject()}<i class="ti-arrow-down"></i></button>
                                                    <ul class="dropdown-menu scrollable">
                                                        <li class="input-container-todata">
                                                            <input oninput="searchByName(this)" type="text" id="teacherNamestyl" value="">
                                                            <i class="fa fa-search" aria-hidden="true"></i>
                                                        </li>
                                                        <li id="content">
                                                            <c:forEach items="${requestScope.listcatesubject}" var="lcs">

                                                                <a href="#" onclick="changeText('${lcs.name_Catergory_Subject}', '${lcs.id_Catergory_Subject}')">${lcs.name_Catergory_Subject}</a>

                                                            </c:forEach>
                                                        </li>
                                                    </ul>
                                                    <input type="hidden" id="selectedValue2" name="subjectCategory" value="${csn.id_Catergory_Subject}">
                                                </div>
                                            </div>
                                            <div class="seperator"></div>
                                            <div class="col-12 m-t20">
                                                <div class="ml-auto m-b5">
                                                    <h3>2. Subject Description</h3>
                                                </div>
                                            </div>
                                            <div class="form-group col-12">
                                                <div>
                                                    <textarea required="" class="form-control" id="myTextarea" onchange="updateInputDescription()">${aswid.description}</textarea>
                                                    <input type="text" hidden id="myInput" name="subjectDescription" value="${aswid.description}">
                                                </div>
                                            </div>
                                            <div class="col-12 m-t20">
                                                <div class="ml-auto m-b5">
                                                    <h3>3. Course General Information</h3>
                                                </div>
                                            </div>
                                            <div class="form-group col-6">
                                                <label class="col-form-label">Create Date</label>
                                                <div>
                                                    <input readonly="" id="coursecreateDate" name="courseToCreateDate" class="form-control" type="date" value="${cid.create_Date}">
                                                </div>
                                            </div>
                                            <div class="form-group col-6">
                                                <label class="col-form-label">Recent Update</label>
                                                <div>
                                                    <input readonly="" id="courseupdateDate" name="courseToUpdateDate" class="form-control" type="date" value="${cid.update_Date}">
                                                </div>
                                            </div>
                                            <div class="form-group col-6">
                                                <label class="col-form-label">Course Name</label>
                                                <div>
                                                    <input required="" class="form-control" id="c1" name="courseName" type="text" value="${cid.course_Name}">
                                                </div>
                                            </div>
                                            <c:set value="${requestScope.catecourse}" var="catecorse"/>
                                            <div class="form-group col-6">
                                                <label class="col-form-label">Category Course</label>
                                                <div class="dropdown">
                                                    <button class="btn-success element-to-take-3" type="button" id="subjectButtonstyl" data-toggle="dropdown">${catecorse.name_Catergory_Course}
                                                        <i class="ti-arrow-down"></i></button>
                                                    <ul class="dropdown-menu scrollable">
                                                        <li class="input-container-todata">
                                                            <input oninput="searchCourseCate(this)" type="text" id="teacherNamestyl" value="">
                                                            <i class="fa fa-search" aria-hidden="true"></i>
                                                        </li>
                                                        <li id="coursecontent">
                                                            <c:forEach items="${requestScope.listcc}" var="lcc">
                                                                <a href="#" onclick="changeCourse('${lcc.name_Catergory_Course}', '${lcc.id_Catergory_Course}')">${lcc.name_Catergory_Course}</a>
                                                            </c:forEach>
                                                        </li>

                                                    </ul>

                                                    <input type="hidden" id="courseValue" name="courseCategory" value="${catecorse.id_Catergory_Course}">
                                                </div>
                                            </div>
                                            <div class="form-group col-6">
                                                <label class="col-form-label">Course Image</label>
                                                <div>
                                                    <img id="previewImage2" src="${cid.image}" alt="Preview Image2">
                                                    <input class="form-control" name="fileInputCourse" type="file" id="imageInput2"
                                                           onchange="previewImageCourse('imageInput2', 'previewImage2')">
                                                </div>
                                            </div>
                                            <div class="form-group col-6">
                                                <label class="col-form-label">Course Status</label>
                                                <c:if test="${su.id_role == 1}">
                                                    <div class="dropdown">
                                                        <button class="btn-success element-to-take-10" type="button" id="subjectButtonstyl" data-toggle="dropdown">
                                                            ${cid.status == "true" ? "Published" : "Unpublished"} <i class="ti-arrow-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="#" onclick="updateStatusCourse(false)">Unpublished</a></li>
                                                            <li><a href="#" onclick="updateStatusCourse(true)">Published</a></li>
                                                        </ul>
                                                        <input type="hidden" id="statusInputCourse" name="CourseStatus" value="${cid.status}">
                                                    </div>
                                                </c:if>
                                                <c:if test="${su.id_role == 2}">
                                                    <div class="dropdown">
                                                        <button class="btn gradient gray" class="element-to-take-10" onclick="warningUser()" type="button" id="subjectButtonstyl" data-toggle="dropdown">
                                                            ${cid.status == "true" ? "Published" : "Unpublished"}
                                                        </button>
                                                        <input type="hidden" id="statusInputCourse" name="CourseStatus" value="${cid.status}">
                                                    </div>
                                                </c:if>
                                            </div>
                                            <div class="col-12 m-t20">
                                                <div class="ml-auto m-b5">
                                                    <h3>4. Course Description</h3>
                                                </div>
                                            </div>
                                            <div class="form-group col-12">
                                                <div>
                                                    <textarea required="" class="form-control" id="myTextarea1" onchange="updateInputDescription1()">${cid.description_Course}</textarea>
                                                    <input type="text" hidden id="myInput1" name="courseDescription" value="${cid.description_Course}">

                                                </div>
                                            </div>
                                            <!-- Update hungct 11/7/2023 -->
                                            <c:if test="${su.id_role == 1}">
                                                <c:set value="${requestScope.listppcourse}" var="listppcourse"/>
                                                <div class="col-12 m-t20">
                                                    <div class="ml-auto m-b5">
                                                        <h3>5. Price Package</h3>
                                                        <button class="btn" id="editbutton" type="button" onclick="showEditPackage()">Edit Price Package</button>
                                                        <button class="btn" type="button" onclick="showAddPackage()">Add New Price Package</button>
                                                    </div>
                                                </div>
                                                <div class="form-group col-12" id="addpackagefunction" style="display: none">
                                                    <h6>Add a package price</h6>
                                                    <table>
                                                        <tr>
                                                            <th id="thinpricepackageNumber">Number</th>
                                                            <th id="thinpricepackageDuration">Duration</th>
                                                            <th id="thinpricepackageDuration">List Price</th>
                                                            <th id="thinpricepackageDuration">Sale Price</th>
                                                            <th id="thinpricepackageDuration">Status</th>
                                                            <th id="thinpricepackagePackage">Action</th>        
                                                        </tr>
                                                        <tr>
                                                            <td id="tdinpricepackage">1
                                                            </td>
                                                            <td id="tdinpricepackage">
                                                                <input required="" type="text" style="width: 80px" value="">
                                                            </td>
                                                            <td id="tdinpricepackage">
                                                                <input required="" type="text" style="width: 80px" value="">
                                                            </td>
                                                            <td id="tdinpricepackage">
                                                                <input required="" type="text" style="width: 80px" value="">
                                                            </td>
                                                            <td id="tdinpricepackage">
                                                                <div class="dropdown">
                                                                    <button id="selectedStatus" class="btn" type="button" data-toggle="dropdown">
                                                                        Select
                                                                    </button>
                                                                    <ul class="dropdown-menu">
                                                                        <li><button class="btn" type="button">Deactive</button></li>
                                                                        <br>
                                                                        <li><button class="btn" type="button">Active</button></li>
                                                                    </ul>
                                                                    <input hidden="" value="">
                                                                </div>
                                                            </td>
                                                            <td id="tdinpricepackage">
                                                                <input hidden="" id="addfunctionforpp" name="addfunctionpp" value="">
                                                                <button type="submit" class="btn gradient green" >Add</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <c:if test="${requestScope.suId != null}">
                                                                    <button class="btn gradient red" onclick="closePricePackage()">Cancel</button>
                                                                </c:if>
                                                                <c:if test="${requestScope.suId == null}">
                                                                    <button class="btn gradient red" onclick="closePricePackage()">Cancel</button>

                                                                </c:if>
                                                            </td>

                                                        <input hidden="" type="text" name="tagvalue" value="${requestScope.tag}">
                                                        <input hidden="" type="text" name="suIdvalue" value="${requestScope.suId}">
                                                        </tr>
                                                    </table>
                                                </div>
                                                <input hidden="" id="closeNoAdd" value="Add">
                                                <div class="form-group col-12" id="editPackageFunction" style="display: none">  
                                                    <div class="form-group col-6">
                                                        <label class="col-form-label">Package Price Duration</label>
                                                        <div class="dropdown">
                                                            <button class="btn-success element-to-take-5" type="button" id="subjectButtonstyl" data-toggle="dropdown">
                                                                ${ppcourse.duration == 1 ? "Expired after 1 month"
                                                                  : ppcourse.duration == 2 ? "Expired after 2 month"
                                                                  : ppcourse.duration == 3 ? "Expired after 3 month"
                                                                  : ppcourse.duration == 4 ? "Expired after 4 month"
                                                                  : ppcourse.duration == 5 ? "Expired after 5 month"
                                                                  : ppcourse.duration == 6 ? "Expired after 6 month"
                                                                  : ppcourse.duration == 7 ? "Expired after 7 month"
                                                                  : ppcourse.duration == 8 ? "Expired after 8 month"
                                                                  : ppcourse.duration == 9 ? "Expired after 9 month"
                                                                  : ppcourse.duration == 10 ? "Expired after 10 month"
                                                                  : ppcourse.duration == 11 ? "Expired after 11 month"
                                                                  : ppcourse.duration == 12 ? "Expired after 12 month" : "Unlimited" }
                                                                <i class="ti-arrow-down"></i></button>
                                                            <ul class="dropdown-menu scrollable">
                                                                <li class="input-container-todata">
                                                                    <input oninput="searchCourseCate(this)" type="text" id="teacherNamestyl" value="">
                                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                                </li>
                                                                <li id="coursecontent">
                                                                    <c:forEach items="${listppcourse}" var="ppc">
                                                                        <a href="#" onclick="changeDurationPrice('${ppc.duration == 1 ? "Expired after 1 month"
                                                                                                                    : ppc.duration == 2 ? "Expired after 2 month"
                                                                                                                    : ppc.duration == 3 ? "Expired after 3 month"
                                                                                                                    :ppc.duration == 4 ? "Expired after 4 month"
                                                                                                                    :ppc.duration == 5 ? "Expired after 5 month"
                                                                                                                    :ppc.duration == 6 ? "Expired after 6 month"
                                                                                                                    :ppc.duration == 7 ? "Expired after 7 month"
                                                                                                                    :ppc.duration == 8 ? "Expired after 8 month"
                                                                                                                    :ppc.duration == 9 ? "Expired after 9 month"
                                                                                                                    :ppc.duration == 10 ? "Expired after 10 month"
                                                                                                                    :ppc.duration == 11 ? "Expired after 11 month"
                                                                                                                    :ppc.duration == 12 ? "Expired after 12 month" : "Unlimited"}', '${ppc.id_Package}', ${ppc.status}, ${ppc.list_Price}, ${ppc.sale_Price})">
                                                                               ${ppc.duration == 1 ? "Expired after 1 month"
                                                                                 : ppc.duration == 2 ? "Expired after 2 month"
                                                                                 : ppc.duration == 3 ? "Expired after 3 month"
                                                                                 :ppc.duration == 4 ? "Expired after 4 month"
                                                                                 :ppc.duration == 5 ? "Expired after 5 month"
                                                                                 :ppc.duration == 6 ? "Expired after 6 month"
                                                                                 :ppc.duration == 7 ? "Expired after 7 month"
                                                                                 :ppc.duration == 8 ? "Expired after 8 month"
                                                                                 :ppc.duration == 9 ? "Expired after 9 month"
                                                                                 :ppc.duration == 10 ? "Expired after 10 month"
                                                                                 :ppc.duration == 11 ? "Expired after 11 month"
                                                                                 :ppc.duration == 12 ? "Expired after 12 month" : "Unlimited"}</a>
                                                                           </c:forEach>
                                                                    </li>
                                                                </ul>
                                                                <input id="priceId" value="${ppcourse.id_Package}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-6">
                                                            <label class="col-form-label">Status</label>
                                                            <div class="dropdown" >
                                                                <button class="btn-success element-to-take-11" type="button" id="statuspricepackage" data-toggle="dropdown">
                                                                    ${ppcourse.status == "true" ? "Published" : "Unpublished"} 
                                                                </button>
                                                                <ul class="dropdown-menu">
                                                                    <li><a href="#" onclick="updateStatuspackagePrice(false, '${ppcourse.status}')">Unpublished</a></li>
                                                                    <li><a href="#" onclick="updateStatuspackagePrice(true, '${ppcourse.status}')">Published</a></li>
                                                                </ul>
                                                                <input id="statusValue2" value="zero">
                                                                <input type="hidden" id="statuspricepackage" value="${ppcourse.status}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-6">
                                                            <label class="col-form-label">List Price</label>
                                                            <div>
                                                                <input required="" class="form-control" id="listPricePricePackage" type="text" value="${ppcourse.list_Price}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-6">
                                                            <label class="col-form-label">Sale Price</label>
                                                            <div>
                                                                <input required="" class="form-control" id="salePricePricePackage" type="text" value="${ppcourse.sale_Price}">
                                                            </div>
                                                        </div>
                                                    </c:if>
                                                </div>
                                                <div style="position: relative; top: 90px">
                                                    <div class="col-12">
                                                        <button type="submit" value="Submit" style="background-color: #33cc00" class="btn">Save changes</button>
                                                    </div>
                                                    <div class="col-12">
                                                        <button onclick="back()" id="butonbackinlast" style="background-color: #999999" class="btn">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Update hungct 11/7/2023 -->
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Your Profile Views Chart END-->
                </div>

            </div>
        </main>

        <div class="ttr-overlay"></div>

        <!-- External JavaScripts -->
        <script src="assestsAdmin/js/jquery.min.js"></script>
        <script src="assestsAdmin/vendors/bootstrap/js/popper.min.js"></script>
        <script src="assestsAdmin/vendors/bootstrap/js/bootstrap.min.js"></script>
        <script src="assestsAdmin/vendors/bootstrap-select/bootstrap-select.min.js"></script>
        <script src="assestsAdmin/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
        <script src="assestsAdmin/vendors/magnific-popup/magnific-popup.js"></script>
        <script src="assestsAdmin/vendors/counter/waypoints-min.js"></script>
        <script src="assestsAdmin/vendors/counter/counterup.min.js"></script>
        <script src="assestsAdmin/vendors/imagesloaded/imagesloaded.js"></script>
        <script src="assestsAdmin/vendors/masonry/masonry.js"></script>
        <script src="assestsAdmin/vendors/masonry/filter.js"></script>
        <script src="assestsAdmin/vendors/owl-carousel/owl.carousel.js"></script>
        <script src='assestsAdmin/vendors/scroll/scrollbar.min.js'></script>
        <script src="assestsAdmin/js/functions.js"></script>
        <script src="assestsAdmin/vendors/chart/chart.min.js"></script>
        <script src="assestsAdmin/js/admin.js"></script>

    </body>

    <!-- Mirrored from educhamp.themetrades.com/demo/admin/courses.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:11:35 GMT -->
</html>