<%-- 
    Document   : PageCourse
    Created on : May 17, 2023, 3:51:55 PM
    Author     : PCT
--%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from educhamp.themetrades.com/demo/admin/courses.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:10:19 GMT -->
    <head>

        <script src="assets/js/PopUp.js"></script>
        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="EduChamp : Education HTML Template" />

        <!-- OG -->
        <meta property="og:title" content="EduChamp : Education HTML Template" />
        <meta property="og:description" content="EduChamp : Education HTML Template" />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no">

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="../error-404.html" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />

        <!-- PAGE TITLE HERE ============================================= -->
        <title>EduChamp : Education HTML Template </title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/assets.css">
        <link rel="stylesheet" type="text/css" href="assestsAdmin/vendors/calendar/fullcalendar.css">

        <!-- TYPOGRAPHY ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/typography.css">

        <!-- SHORTCODES ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/shortcodes/shortcodes.css">

        <!-- STYLESHEETS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/style.css">
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/dashboard.css">
        <link class="skin" rel="stylesheet" type="text/css" href="assets/css/color/color-1.css">
        <link rel="stylesheet" type="text/css" href="assets/css/SubjectAdmin.css">
        <link rel="stylesheet" type="text/css" href="assets/css/page.css">
        <style>
            .user-detail {
                display: flex;
                flex-direction: column;
                align-items: center;
                margin: 50px;
            }

            .user-avatar {
                width: 200px;
                height: 200px;
                overflow: hidden;
                border-radius: 50%;
                margin-bottom: 30px;
            }

            .user-avatar img {
                width: 100%;
                height: 100%;
                object-fit: cover;
            }

            .user-info {
                max-width: 600px;
            }

            .edit-button {
                margin-top: 30px;
                text-align: center;
            }

            button {
                background-color: #ffc107;
                color: #fff;
                font-size: 16px;
                border: none;
                padding: 10px 20px;
                border-radius: 5px;
                cursor: pointer;
            }

            button:hover {
                background-color: #ffca2b;
            }

            button:focus {
                outline: none;
            }

            table {
                border-collapse: collapse;
                width: 100%;
            }

            table td {
                padding: 10px;
                border-bottom: 1px solid #eee;
            }

            table td:first-child {
                font-weight: bold;
                width: 150px;
            }

            table tr:last-child td {
                border-bottom: none;
            }

          
            .message1 {
                position: fixed;
                color: white;
                background-color: #f5f5f5;
                height: 100vh;
                width: 100vw;
                z-index: 999;
                padding: 10px;
                background: rgba(0, 0, 0, 0.6);
                border: 1px solid #ddd;
                border-radius: 4px;
                box-shadow: 0px 2px 8px rgba(0, 0, 0, 0.15);
                font-size: 16px;
                text-align: center;
            }
            .message1.show {
                display: block;
            }
            #active{
                color: orange;
            }

        </style>
    </head>
    <body class="ttr-opened-sidebar ttr-pinned-sidebar">

        <!-- header start -->
        <header class="ttr-header">
            <c:if test="${not empty sessionScope.message}">

                <div id="message1" class="message1" style="">${sessionScope.message}</div>
                <% session.removeAttribute("message"); %>
            </c:if>
            <div class="ttr-header-wrapper">
                <!--sidebar menu toggler start -->
                <div class="ttr-toggle-sidebar ttr-material-button">
                    <i class="ti-close ttr-open-icon"></i>
                    <i class="ti-menu ttr-close-icon"></i>
                </div>
                <!--sidebar menu toggler end -->
                <!--logo start -->
                <div class="ttr-logo-box">
                    <div>
                        <a href="index.html" class="ttr-logo">
                            <img alt="" class="ttr-logo-mobile" src="assets/images/logo-mobile.png" width="30" height="30">
                            <img alt="" class="ttr-logo-desktop" src="assets/images/logo-white.png" width="160" height="27">
                        </a>
                    </div>
                </div>
                <!--logo end -->
                <div class="ttr-header-menu">
                    <!-- header left menu start -->
                    <ul class="ttr-header-navigation">
                        <li>
                            <a href="../index.html" class="ttr-material-button ttr-submenu-toggle">HOME</a>
                        </li>
                    </ul>
                    <!-- header left menu end -->
                </div>
                 <div class="ttr-header-right ttr-with-seperator">
                    <!-- header right menu start -->
                    <ul class="ttr-header-navigation">
                        <li>
                            <a href="#" class="ttr-material-button">${sessionScope.user.full_Name}</a>
                        </li>

                        <li>
                            <a href="#" class="ttr-material-button ttr-submenu-toggle"
                               ><span class="ttr-user-avatar"
                                   ><img
                                        alt=""
                                        src="${sessionScope.user.avatar}"
                                        width="32"
                                        height="32" /></span
                                ></a>
                            <div class="ttr-header-submenu">
                                <ul>
                                    <li><a href="#">My profile</a></li>
                                    <li><a href="#">Logout</a></li>
                                </ul>
                            </div>
                        </li>
                        <li style="background-color: orange">
                            <a href="logout" class="ttr-material-button">Logout</a>
                        </li>
                    </ul>
                    <!-- header right menu end -->
                </div>
                <!--header search panel start -->
                <div class="ttr-search-bar">
                    <form class="ttr-search-form">
                        <div class="ttr-search-input-wrapper">
                            <input type="text" name="qq" placeholder="search something..." class="ttr-search-input">
                            <button type="submit" name="search" class="ttr-search-submit"><i class="ti-arrow-right"></i></button>
                        </div>
                        <span class="ttr-search-close ttr-search-toggle">
                            <i class="ti-close"></i>
                        </span>
                    </form>
                </div>
                <!--header search panel end -->
            </div>
        </header>
        <!-- header end -->
        <!-- Left sidebar menu start -->
        <div class="ttr-sidebar">
            <div class="ttr-sidebar-wrapper content-scroll">
                <!-- side menu logo start -->
                <div class="ttr-sidebar-logo">
                    <a href="#"><img alt="" src="assets/images/logo.png" width="122" height="27"></a>
                    <!-- <div class="ttr-sidebar-pin-button" title="Pin/Unpin Menu">
                            <i class="material-icons ttr-fixed-icon">gps_fixed</i>
                            <i class="material-icons ttr-not-fixed-icon">gps_not_fixed</i>
                    </div> -->
                    <div class="ttr-sidebar-toggle-button">
                        <i class="ti-arrow-left"></i>
                    </div>
                </div>
                <!-- side menu logo end -->
                <!-- sidebar menu start -->
                <nav class="ttr-sidebar-navi">
                    <c:set var="currentpage" value="${requestScope.xxx}"/>
                    <ul>
                    <li>
                        <a href="dashboardmarketing" class="ttr-material-button">
                            <span class="ttr-icon"><i class="ti-home"></i></span>
                            <span class="ttr-label">Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a href="admincourselist" class="ttr-material-button">
                            <span class="ttr-icon"><i class="ti-book"></i></span>
                            <span class="ttr-label">Management Course</span>
                        </a>
                    </li>
                    <li>
                            <a href="expertsubjectlistservlet" class="ttr-material-button">
                                <span  class="ttr-icon"><i class="ti-book"></i></span>
                                <span   class="ttr-label">Management Subject</span>
                            </a>
                        </li>
                    <li>
                        <a id="active" href="useradmin" class="ttr-material-button">
                            <span class="ttr-icon"
                                      ><i class="ti-layout-list-post"></i
                                    ></span>
                            <span id="active" class="ttr-label ${currentpage eq "user"? 'active':""}">Management User</span>
                        </a>
                    </li>
                    <li>
                            <a href="#" class="ttr-material-button">
                                <span class="ttr-icon"
                                      ><i class="ti-layout-list-post"></i
                                    ></span>
                                <span class="ttr-label">Management Setting</span>
                            </a>
                        </li>

                        <li>
                            <a href="#" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-user"></i></span>
                                <span class="ttr-label">My Profile</span>
                                <span class="ttr-arrow-icon"
                                      ><i class="fa fa-angle-down"></i
                                    ></span>
                            </a>
                            <ul>
                                <li>
                                    <a href="#" class="ttr-material-button"
                                       ><span class="ttr-label">User Profile</span></a
                                    >
                                </li>
                                <li>
                                    <a href="#" class="ttr-material-button"
                                       ><span class="ttr-label">Logout</span></a
                                    >
                                </li>
                            </ul>
                        </li>

                    <li class="ttr-seperate"></li>
                </ul>
                </nav>
                <!-- sidebar menu end -->
            </div>
        </div>
        <!-- Left sidebar menu end -->

        <!--Main container start -->
        <main class="ttr-wrapper">
            <div class="container-fluid">
                <div class="db-breadcrumb">
                    <h4 class="breadcrumb-title">User</h4>
                    <ul class="db-breadcrumb-list">
                        <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                        <a href = "useradmin"><li>User Management</li></a>
                    </ul>
                </div>	
                <div class="row">
                    <!-- Your Profile Views Chart -->
                    <div class="col-lg-12 m-b30">
                        <div class="widget-box">
                            <div class="search-bar">
                                <h4>User Management</h4>                          
                            </div>
                            <div class="search-bar">
                                <form action="useradmin" method="get">
                                    <input type="text" name="searchName" value="${requestScope.key}" placeholder="Subject Name...">                            
                                    <button type="submit"><i>Search</i></button>
                                </form>                           
                                 <button class="add-subject"><a href = "adduseradmin">Add new User</a></button>
                            </div>
                            <div class="content-block">
                                <div class="row">
                                    <!-- Left part start -->
                                    <div class="col-lg-8 col-xl-8">
                                        <!-- blog start -->
                                        <div class="recent-news blog-lg">

                                            <div class="user-detail">
                                                <div class="user-avatar">
                                                    <img src="${requestScope.userjoin.getAvatar()}" alt="Avatar">
                                                </div>
                                                <div class="user-info">
                                                    <table>
                                                        <tr>
                                                            <td>Full Name:</td>
                                                            <td>${requestScope.userjoin.getFull_Name()}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Email:</td>
                                                            <td>${requestScope.userjoin.getEmail()}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Gender:</td>
                                                            <td>${userjoin.isGender() eq true? 'male':'female'}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Mobile:</td>
                                                            <td>${userjoin.getPhone_Number() }</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Create Date:</td>
                                                            <td>${userjoin.getCreate_Date() }</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Address:</td>
                                                            <td>${userjoin.getAddress() }</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Role:</td>
                                                            <td>${userjoin.getNameRole() }</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Status:</td>
                                                            <td>${userjoin.isStatus() eq true? 'active':'deactive'}</td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="edit-button">
                                                    <button>  <a href="updateuser?iduser=${userjoin.id_User}">Edit</a></button>                                             
                                                </div>
                                            </div>
                                        </div>
                                        <!-- blog END -->
                                    </div>
                                    <!-- Left part END -->
                                    <!-- Side bar start -->
                                    <div class="col-lg-4 col-xl-4">
                                        <aside  class="side-bar sticky-top">
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <br>

                                            <div class="widget recent-posts-entry">
                                                <h6 class="widget-title">Recent Users</h6>
                                                <div class="widget-post-bx">
                                                    <div class="widget-post clearfix">
                                                        <c:forEach items="${requestScope.list}" var="d">
                                                            <div class="ttr-post-media"> 
                                                                <a href="userdetailadmin?iduser=${d.getId_User()}"><img src="${d.getAvatar()}" width="200" height="143" alt="">  </a>
                                                            </div>
                                                            <div class="ttr-post-info">
                                                                <div class="ttr-post-header">
                                                                    <h6 class="post-title">${d.getFull_Name()}</h6>
                                                                </div>
                                                                <ul class="media-post">
                                                                    <li><i class="fa fa-calendar"></i>${d.isStatus()eq true? 'active':'deactive'}</li>
                                                                    <li><i class="fa fa-eye"></i>${d.getNameRole()}</li>
                                                                </ul>
                                                            </div>
                                                            </br>
                                                        </c:forEach>                                                    
                                                    </div>
                                                </div>
                                            </div> 
                                        </aside>
                                    </div>
                                    <!-- Side bar END -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </main>
        <!-- Your Profile Views Chart END--
        </main>
        <div class="ttr-overlay"></div>
        
        <!-- External JavaScripts -->
        <div style="height: 100px;width: 100%;background-color: rgb(93, 4, 176);"></div>
        <script src="assestsAdmin/js/jquery.min.js"></script>
        <script src="assestsAdmin/vendors/bootstrap/js/popper.min.js"></script>
        <script src="assestsAdmin/vendors/bootstrap/js/bootstrap.min.js"></script>
        <script src="assestsAdmin/vendors/bootstrap-select/bootstrap-select.min.js"></script>
        <script src="assestsAdmin/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
        <script src="assestsAdmin/vendors/magnific-popup/magnific-popup.js"></script>
        <script src="assestsAdmin/vendors/counter/waypoints-min.js"></script>
        <script src="assestsAdmin/vendors/counter/counterup.min.js"></script>
        <script src="assestsAdmin/vendors/imagesloaded/imagesloaded.js"></script>
        <script src="assestsAdmin/vendors/masonry/masonry.js"></script>
        <script src="assestsAdmin/vendors/masonry/filter.js"></script>
        <script src="assestsAdmin/vendors/owl-carousel/owl.carousel.js"></script>
        <script src='assestsAdmin/vendors/scroll/scrollbar.min.js'></script>
        <script src="assestsAdmin/js/functions.js"></script>
        <script src="assestsAdmin/vendors/chart/chart.min.js"></script>
        <script src="assestsAdmin/js/admin.js"></script>
        <script>
          
                    var messageBox = document.getElementById("message1");
            messageBox.style.display = "none"; // Ẩn thông báo ban đầu

            function showMessage() {
                // Hiển thị thông báo
                messageBox.style.display = "block";
                messageBox.style.top = (window.innerHeight - messageBox.offsetHeight) / 2 + "px"; // Căn giữa theo chiều dọc
                messageBox.style.left = (window.innerWidth - messageBox.offsetWidth) / 2 + "px"; // Căn giữa theo chiều ngang

                // Tự động tắt thông báo sau 3 giây
                setTimeout(function () {
                    messageBox.style.display = "none";
                }, 1000);
            }

            // Gọi hàm hiển thị thông báo
            showMessage();
        </script>
    </body>

    <!-- Mirrored from educhamp.themetrades.com/demo/admin/courses.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:11:35 GMT -->
</html>