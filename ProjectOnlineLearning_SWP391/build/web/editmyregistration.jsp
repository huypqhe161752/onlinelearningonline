<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">

    <head>
        <script src="assets/js/PopUp.js"></script>
        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="EduChamp : Education HTML Template" />

        <!-- OG -->
        <meta property="og:title" content="EduChamp : Education HTML Template" />
        <meta property="og:description" content="EduChamp : Education HTML Template" />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no">

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />

        <!-- PAGE TITLE HERE ============================================= -->
        <title>EduChamp : Education HTML Template </title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/assets.css">

        <!-- TYPOGRAPHY ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/typography.css">

        <!-- SHORTCODES ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/shortcodes/shortcodes.css">

        <!-- STYLESHEETS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <link class="skin" rel="stylesheet" type="text/css" href="assets/css/color/color-1.css">

    </head>
    <body id="bg">
        <div class="modal fade" id="change-myregister" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top: 100px">
            <div class="modal-dialog" role="document">
                <div class="modal-content clearfix">
                    <right><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><div class="form-control" style="background-color: red; color: black">X</div></span></button></right>
                    <div class="account-form-inner col-lg-7  col-md-7 col-sm-12 ">
                        <div class="account-container">
                            <div class="heading-bx left">
                                <h2 class="title-head">Course Register</h2>
                                <h2 style="text-align: center;">${c.course_Name}</h2>
                            </div>	
                            <form class="contact-bx" action="courseregister" method="post" >
                                <c:set value="${sessionScope.user}" var="u"/>
                                <div class="row placeani">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <p>Your Email Address</p>
                                                <c:if test="${sessionScope.user == null}">
                                                    <input id="n1" name="email" type="email" required="" class="form-control">
                                                </c:if>

                                                <c:if test="${sessionScope.user != null}">
                                                    <input name="email" type="email" readonly="" class="form-control" value="${u.getEmail()}">
                                                </c:if>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <div class="input-group"> 
                                                <p>Your Full Name</p>
                                                <c:if test="${sessionScope.user == null}">
                                                    <input name="userFullName" type="text" required="" class="form-control">
                                                </c:if>

                                                <c:if test="${sessionScope.user != null}">
                                                    <input name="userFullName" type="text" readonly="" required="" class="form-control" value="${u.getFull_Name()}">
                                                </c:if>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12" style="margin-left: -12px">
                                        <div class="form-group">
                                            <div class="input-group"> 
                                                <p>Your Phone Number</p>
                                                <c:if test="${sessionScope.user == null}">
                                                    <input name="userPhoneNumber" type="text" class="form-control" pattern="[0-9]{10,11}" required="" >
                                                </c:if>

                                                <c:if test="${sessionScope.user != null}">
                                                    <input name="userPhoneNumber" type="text" readonly="" class="form-control" pattern="[0-9]{10,11}" required="" value="${u.phone_Number}">
                                                </c:if>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12" style="margin-left: -12px">
                                        <div>
                                            <div> 
                                                <p>Select Price</p>
                                                <c:if test="${p == null}">
                                                    <select name="idpackageprice" id="1">
                                                        <c:forEach items="${requestScope.listprice}" var="pp2" varStatus="s">
                                                            <option value="pp2">Subject price ${s.count} - ${pp2.sale_Price}$ </option>
                                                        </c:forEach>
                                                    </select>
                                                </c:if>
                                                <c:if test="${p != null}">
                                                    <select name="idpackageprice" id="2">
                                                        <option value="${p.id_Package}">Subject price choose - ${p.sale_Price}$</option>
                                                        <c:forEach items="${requestScope.listprice}" var="pp" varStatus="status1">
                                                            <c:if test="${p.id_Package != pp.id_Package }">        
                                                                <option value="${pp.id_Package}">Subject price ${status1.count} - ${pp.sale_Price}$ </option>
                                                            </c:if>
                                                        </c:forEach>
                                                    </select>
                                                </c:if>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 m-b30" style="margin-top: 120px">

                                    <button  onclick="alertSave()" name="submit" type="submit" value="Submit" class="btn button-md" style="margin-left: -50px ">Save</button> 
                                    <button class="btn button-md" style="margin-left: 10px " data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- External JavaScripts -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/vendors/bootstrap/js/popper.min.js"></script>
        <script src="assets/vendors/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/vendors/bootstrap-select/bootstrap-select.min.js"></script>
        <script src="assets/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
        <script src="assets/vendors/magnific-popup/magnific-popup.js"></script>
        <script src="assets/vendors/counter/waypoints-min.js"></script>
        <script src="assets/vendors/counter/counterup.min.js"></script>
        <script src="assets/vendors/imagesloaded/imagesloaded.js"></script>
        <script src="assets/vendors/masonry/masonry.js"></script>
        <script src="assets/vendors/masonry/filter.js"></script>
        <script src="assets/vendors/owl-carousel/owl.carousel.js"></script>
        <script src="assets/js/functions.js"></script>
        <script src="assets/js/contact.js"></script>
        <script src="assets/checkinputpassword.js" type="text/javascript"></script>
    </body>
</html>
