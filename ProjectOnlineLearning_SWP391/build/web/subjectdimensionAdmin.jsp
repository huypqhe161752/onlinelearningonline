<%-- 
    Document   : PageCourse
    Created on : May 17, 2023, 3:51:55 PM
    Author     : PCT
--%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from educhamp.themetrades.com/demo/admin/courses.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:10:19 GMT -->
    <head>
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/subjectlist.css">
        <script src="assestsAdmin/js/subjectdetails.js"></script>
        <script src="assestsAdmin/js/subjectDimension.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link href="assetsAdmin/select2/select2.min.css" rel="stylesheet" />
        <script src="assetsAdmin/select2/select2.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
        <script src="assets/js/PopUp.js"></script>
        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="EduChamp : Education HTML Template" />

        <!-- OG -->
        <meta property="og:title" content="EduChamp : Education HTML Template" />
        <meta property="og:description" content="EduChamp : Education HTML Template" />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no">

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="../error-404.html" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />

        <!-- PAGE TITLE HERE ============================================= -->
        <title>EduChamp : Education HTML Template </title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/assets.css">
        <link rel="stylesheet" type="text/css" href="assestsAdmin/vendors/calendar/fullcalendar.css">

        <!-- TYPOGRAPHY ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/typography.css">

        <!-- SHORTCODES ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/shortcodes/shortcodes.css">

        <!-- STYLESHEETS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/style.css">
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/dashboard.css">
        <link class="skin" rel="stylesheet" type="text/css" href="assets/css/color/color-1.css">
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/SubjectAdmin.css">
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/page.css">
        <style>
            .active{
                color: black;
            }
            .actived{
                color:orange;
            }
        </style>
    </head>
</head>
<body class="ttr-opened-sidebar ttr-pinned-sidebar">
    <!-- header start -->
    <header class="ttr-header">
        <div class="ttr-header-wrapper">
            <!--sidebar menu toggler start -->
            <div class="ttr-toggle-sidebar ttr-material-button">
                <i class="ti-close ttr-open-icon"></i>
                <i class="ti-menu ttr-close-icon"></i>
            </div>
            <!--sidebar menu toggler end -->
            <!--logo start -->
            <div class="ttr-logo-box">
                <div>
                    <a href="index.html" class="ttr-logo">
                        <img alt="" class="ttr-logo-mobile" src="assets/images/logo-mobile.png" width="30" height="30">
                        <img alt="" class="ttr-logo-desktop" src="assets/images/logo-white.png" width="160" height="27">
                    </a>
                </div>
            </div>
            <!--logo end -->
            <div class="ttr-header-menu">
                <!-- header left menu start -->
                <!-- header left menu end -->
            </div>
            <div class="ttr-header-right ttr-with-seperator">
                <!-- header right menu start -->
                <ul class="ttr-header-navigation">

                    <c:if test="${sessionScope.user!=null}">


                        <li> 
                            <a href="#" class="ttr-material-button ttr-submenu-toggle"
                               ><span class="ttr-user-avatar"
                                   ><img
                                        alt=""
                                        src="${sessionScope.user.avatar}"
                                        width="32"
                                        height="32" /></span
                                ></a>
                            <div class="ttr-header-submenu">
                                <ul>
                                    <li><a onclick="openPopup1('profileexpert')">My profile</a></li>
                                    <li><a onclick="openPopup1('profileexpertchangepass')">Change Password</a></li>
                                    <li><a href="logout">Logout</a></li>
                                </ul>
                            </div>
                        </li>
                        <li><a href="#">${user.full_Name}</a></li>
                        <li><a href="logout">Logout</a></li>
                    </ul>
                </c:if>
                <!-- header right menu end -->
            </div>
            <!--header search panel start -->
            <div class="ttr-search-bar">
                <form class="ttr-search-form">
                    <div class="ttr-search-input-wrapper">
                        <input type="text" name="qq" placeholder="search something..." class="ttr-search-input">
                        <button type="submit" name="search" class="ttr-search-submit"><i class="ti-arrow-right"></i></button>
                    </div>
                    <span class="ttr-search-close ttr-search-toggle">
                        <i class="ti-close"></i>
                    </span>
                </form>
            </div>
            <!--header search panel end -->
        </div>
    </header>
    <!-- header end -->
    <!-- Left sidebar menu start -->
    <c:set value="${sessionScope.user}" var="su"/>
    <div class="ttr-sidebar">
        <div class="ttr-sidebar-wrapper content-scroll">
            <!-- side menu logo start -->
            <div class="ttr-sidebar-logo">
                <a href="#"><img alt="" src="assets/images/logo.png" width="122" height="27"></a>
                <!-- <div class="ttr-sidebar-pin-button" title="Pin/Unpin Menu">
                        <i class="material-icons ttr-fixed-icon">gps_fixed</i>
                        <i class="material-icons ttr-not-fixed-icon">gps_not_fixed</i>
                </div> -->
                <div class="ttr-sidebar-toggle-button">
                    <i class="ti-arrow-left"></i>
                </div>
            </div>
            <!-- side menu logo end -->
            <!-- sidebar menu start -->
            <nav class="ttr-sidebar-navi">
                <c:set var="currentpage" value="${requestScope.xxx}"/>
                <c:set var="currentpageDetails" value="${requestScope.xxxDetails}"/>
                <ul>
                    <li>
                        <a href="admincourselist" class="ttr-material-button">
                            <span id="active"  class="ttr-icon"><i class="ti-book"></i></span>
                            <span  id="active" class="ttr-label">Course Management</span>
                        </a>
                    </li>
                    <li>
                        <a href="adminsubject" class="ttr-material-button">
                            <span class="ttr-icon"><i class="ti-book"></i></span>
                            <span class="ttr-label ${currentpage eq "SubjectManagment"? 'actived':""}">Subject Management</span>
                        </a>
                    </li>

                    <c:if test="${requestScope.suId != null}">
                        <li style="margin-left: 60px">
                            <a href="subjectdetailsadmin?subjectid=${requestScope.suId}" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-pencil-alt"></i></span>
                                <span class="ttr-label ${currentpageDetails eq "SubjectDetails"? 'actived':""}">Subject Detail</span>
                            </a>
                        </li>
                    </c:if>
                    <li style="margin-left: 60px">
                        <a href="#" class="ttr-material-button">
                            <span class="ttr-icon"><i class="ti-pencil-alt"></i></span>
                            <span class="ttr-label ${currentpageDetails eq "subjectdimension"? 'actived':""}">Subject Dimension</span>
                        </a>
                    </li>
                    <c:if test="${requestScope.suId != null}">
                        <li style="margin-left: 60px">
                            <a href="packagepriceadmin?subjectid=${requestScope.suId}&&pplistid=1" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-pencil-alt"></i></span>
                                <span class="ttr-label ${currentpageDetails eq "packageprice"? 'actived':""}">Price Package</span>
                            </a>
                        </li>
                    </c:if>
                    <c:if test="${requestScope.suId == null}">
                        <li style="margin-left: 60px">
                            <a href="packagepriceadmin?pplistid=1" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-pencil-alt"></i></span>
                                <span class="ttr-label ${currentpageDetails eq "packageprice"? 'actived':""}">Price Package</span>
                            </a>
                        </li>
                    </c:if>
                    <li>
                        <a href="useradmin" class="ttr-material-button">
                            <span class="ttr-icon"
                                  ><i class="ti-layout-list-post"></i
                                ></span>
                            <span class="ttr-label">User Management</span>
                        </a>
                    </li>
                    <li>
                        <a href="quizzeslist" class="ttr-material-button">
                            <span class="ttr-icon"><i class="ti-book"></i></span>
                            <span class="ttr-label">Quizzes Management</span>
                        </a>
                    </li>
                    <li>
                        <a href="listquestions" class="ttr-material-button">
                            <span class="ttr-icon"><i class="ti-book"></i></span>
                            <span class="ttr-label">Questions Management</span>
                        </a>
                    </li>
                    <li>
                        <a href="profileexpert" class="ttr-material-button">
                            <span class="ttr-icon"><i class="ti-user"></i></span>
                            <span class="ttr-label">My Profile</span>
                            <span class="ttr-arrow-icon"
                                  ><i class="fa fa-angle-down"></i
                                ></span>
                        </a>
                        <ul>

                            <li>
                                <a onclick="openPopup1('profileexpert')" class="ttr-material-button"
                                   ><span class="ttr-label">User Profile</span></a
                                >
                            </li>
                            <li>
                                <a onclick="openPopup1('profileexpertchangepass')" class="ttr-material-button"
                                   ><span class="ttr-label">Change Password</span></a
                                >
                            </li>
                            <li>
                                <a href="logout" class="ttr-material-button"
                                   ><span class="ttr-label">Logout</span></a
                                >
                            </li>
                        </ul>
                    </li>
                    <li class="ttr-seperate"></li>
                </ul>
            </nav>
            <!-- sidebar menu end -->
        </div>
    </div>
    <!-- Left sidebar menu end -->

    <!--Main container start -->
    <main class="ttr-wrapper">
        <div class="container-fluid">
            <div class="db-breadcrumb">
                <!-- Nav tabs -->


                <!-- Tab panes -->

                <h4 class="breadcrumb-title">Subject Management</h4>
                <ul class="db-breadcrumb-list">
                    <li><a href="expertsubjectlistservlet">Subject Detail</a></li>
                    <li>Subject Dimension</li>
                </ul>
            </div>

            <div class="row">
                <!-- Your Profile Views Chart -->
                <div class="col-lg-12 m-b30">
                    <div class="widget-box">
                        <div class="wc-title">
                            <ul class="nav nav-tabs" role="tablist">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#menu2">Dimension</a></li>
                                        <c:if test="${requestScope.suId != null}">
                                        <li><a href="subjectdetailsadmin?subjectid=${requestScope.suId}">Subject Detail</a></li>
                                        </c:if>
                                        <c:if test="${requestScope.suId != null}">
                                        <li><a href="packagepriceadmin?subjectid=${requestScope.suId}&&pplistid=1">Package Price</a></li>
                                        </c:if>
                                        <c:if test="${requestScope.suId == null}">
                                        <li><a href="packagepriceadmin?pplistid=1">Package Price</a></li>
                                        </c:if>

                                </ul>
                            </ul>
                        </div>
                        <c:set value="${requestScope.listTyCaSu}" var="tcs"/>
                        <c:set value="${requestScope.listCateSubj}" var="cs"/>

                        <div class="widget-inner">
                            <div class="tab-content">
                                <div id="home1" class="container tab-pane fade"><br>
                                </div>
                                <div id="menu1" class="container tab-pane fade"><br>
                                </div>
                                <div id="menu2" class="container tab-pane active"><br>
                                    <div class="col-12">
                                        <div class="ml-auto">
                                            <h2>Subject Dimension</h2>
                                        </div>
                                    </div>
                                    <div class="form-group col-6" style="margin-bottom: 50px; margin-top: 30px">
                                        <h6>Filter</h6>
                                        <input class="form-control" 
                                               oninput="searchSubjecetDimensionName(this)" 
                                               placeholder="Enter dimension name">
                                        <i class="fa fa-search" 
                                           style="position: relative; left: 500px; bottom: 30px" 
                                           aria-hidden="true">
                                        </i>
                                    </div>
                                    <form id="addForm" style="display: none" action="subjectdimensionadmin" method="post">
                                        <div class="row">
                                            <div class="form-group col-12">
                                                <div class="container">
                                                    <div style="text-align: left;" class="row">
                                                        <div class="col-md-12">
                                                            <div class="table-wrap">
                                                                <table class="table table-striped">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Number</th>
                                                                            <th>Dimension</th>
                                                                            <th>Name</th>
                                                                            <th>Description</th>
                                                                            <th>Action</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    <td>${requestScope.sizeL + 1}</td>
                                                                    <td>
                                                                        <input required="" name="type_cater_Name_Dimension" type="text" style="width: 100px">
                                                                    </td>
                                                                    <td>
                                                                        <textarea required="" name="name_cater_Subject" style="width: 150px; height: 100px"></textarea>
                                                                    </td>
                                                                    <td>
                                                                        <textarea required="" name="description_cater_Subject" style="width: 400px; height: 100px"></textarea>
                                                                    </td>
                                                                    <td>
                                                                        <input hidden="" type="text" name="addsubjectdimension" value="add">
                                                                        <button type="submit" class="btn gradient green" >Save</button>&nbsp;&nbsp;<button type="button" class="btn gradient red" onclick="cancel()">Cancel</button>
                                                                    </td>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <c:forEach begin="1" end="${requestScope.sizeL}" var="i">
                                        <form id="editForm${i}" style="display: none" action="subjectdimensionadmin" method="post">
                                            <div class="row">
                                                <div class="form-group col-12">
                                                    <div class="container">
                                                        <div style="text-align: left;" class="row">
                                                            <div class="col-md-12">
                                                                <div class="table-wrap">
                                                                    <table class="table table-striped">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>Number</th>
                                                                                <th>Dimension</th>
                                                                                <th>Name</th>
                                                                                <th>Description</th>
                                                                                <th>Action</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td scope="row">${i}</td>
                                                                                <td>
                                                                                    <input readonly="" style="width: 100px" type="text" id="myInput${i}" name="dimensionTable${i}" value="${tcs[i-1].description}" onclick="showValue(${i})">
                                                                                    <div class="dropdown-content-1" id="myDropdown${i}">
                                                                                        <select id="mySelect${i}" onchange="changeValue(${i})">
                                                                                            <option value="${tcs[i-1].description}">${tcs[i-1].description}</option>
                                                                                            <c:forEach items="${requestScope.listDomain}" var="c">
                                                                                                <c:if test="${tcs[i-1].description != c.name_Catergory_Subject}">
                                                                                                    <option value="${c.name_Catergory_Subject}">${c.name_Catergory_Subject}</option>
                                                                                                </c:if>    
                                                                                            </c:forEach>
                                                                                        </select>
                                                                                    </div>
                                                                                </td>
                                                                                <td>
                                                                                    <textarea required="" name="nameTable${i}" style="width: 150px; height: 100px">
                                                                                        ${tcs[i-1].name_Catergory_Subject}
                                                                                    </textarea>

                                                                                </td>
                                                                                <td>
                                                                                    <textarea required="" name="descriptionTable${i}" style="width: 400px; height: 100px">
                                                                                        ${cs[i-1].description_Catergory_Subject}
                                                                                    </textarea> 
                                                                                </td>
                                                                                <td style="width: 20%">
                                                                                    <input hidden="" type="text" value="${i}" name="iValue">
                                                                                    <input hidden="" value="${cs[i-1].id_Catergory_Subject}" name="catergorySubjectID${i}" type="text">
                                                                                    <input hidden="" type="text" name="dimensionId${i}" value="${tcs[i-1].id_Type_Catergory_Subjec}">
                                                                                    <input hidden="" type="text" name="editsubjectdimension" value="edit">
                                                                                    <button type="submit" class="btn gradient green" >Save</button>&nbsp;&nbsp;<button type="button" class="btn gradient red" onclick="cancel()">Cancel</button>
                                                                                </td>
                                                                            </tr>

                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form> 
                                    </c:forEach>

                                    <form class="edit-profile m-b30" action="packageprice" method="post">
                                        <a href="#" style="position: relative; left: 820px; width: 150px" class="btn gradient green" onclick="openAdd()">Add New</a>
                                        <div class="row">
                                            <div class="form-group col-12">
                                                <div class="container">
                                                    <div style="text-align: left;" class="row">
                                                        <div class="col-md-12">
                                                            <div class="table-wrap">
                                                                <table class="table table-striped">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Number</th>
                                                                            <th>Dimension</th>
                                                                            <th>Name</th>
                                                                            <th>Description</th>
                                                                            <th>Action</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="subjectnamesearch">
                                                                        <c:forEach begin="1" end="${requestScope.sizeL}" var="i">
                                                                            <tr>
                                                                                <td scope="row">${i}
                                                                                </td>
                                                                                <td>${tcs[i-1].description}</td>
                                                                                <td>${tcs[i-1].name_Catergory_Subject}</td>
                                                                                <td>${cs[i-1].description_Catergory_Subject}</td>
                                                                                <td style="width: 20%">
                                                                                    <button type="button" onclick="openEdit(${i})" class="btn">Edit</button>    
                                                                                    <button type="button" class="btn gradient brown" onclick="deleteEditAdmin(${cs[i-1].id_Catergory_Subject})">Delete</button>
                                                                                </td>
                                                                            </tr>
                                                                        </c:forEach>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Your Profile Views Chart END-->
            </div>

        </div>
    </main>

    <div class="ttr-overlay"></div>

    <!-- External JavaScripts -->
    <script src="assestsAdmin/js/jquery.min.js"></script>
    <script src="assestsAdmin/vendors/bootstrap/js/popper.min.js"></script>
    <script src="assestsAdmin/vendors/bootstrap/js/bootstrap.min.js"></script>
    <script src="assestsAdmin/vendors/bootstrap-select/bootstrap-select.min.js"></script>
    <script src="assestsAdmin/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
    <script src="assestsAdmin/vendors/magnific-popup/magnific-popup.js"></script>
    <script src="assestsAdmin/vendors/counter/waypoints-min.js"></script>
    <script src="assestsAdmin/vendors/counter/counterup.min.js"></script>
    <script src="assestsAdmin/vendors/imagesloaded/imagesloaded.js"></script>
    <script src="assestsAdmin/vendors/masonry/masonry.js"></script>
    <script src="assestsAdmin/vendors/masonry/filter.js"></script>
    <script src="assestsAdmin/vendors/owl-carousel/owl.carousel.js"></script>
    <script src='assestsAdmin/vendors/scroll/scrollbar.min.js'></script>
    <script src="assestsAdmin/js/functions.js"></script>
    <script src="assestsAdmin/vendors/chart/chart.min.js"></script>
    <script src="assestsAdmin/js/admin.js"></script>

</body>

<!-- Mirrored from educhamp.themetrades.com/demo/admin/courses.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:11:35 GMT -->
</html>