/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.AnswerDAO;
import dal.LessonDAO;
import dal.QuestionDAO;
import dal.QuizDAO;
import dal.SubjectDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import model.Answer;
import model.Question;
import model.Quiz;

/**
 *
 * @author Naviank
 */
@WebServlet(name = "AttemptQuizServlet", urlPatterns = {"/attemptquiz"})
public class QuizAttemptServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AttemptQuizServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AttemptQuizServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //handle request variable
        String cur_number_raw = request.getParameter("cur_number");
        String cur_answer_string = (String) request.getSession().getAttribute("cur_answer_string");
        Quiz quiz = (Quiz) request.getSession().getAttribute("cur_quiz");

        //set up quiz & question value
        int cur_number;
        try {
            cur_number = Integer.parseInt(cur_number_raw);
        } catch (NumberFormatException e) {
            cur_number = 1;
        }
        
        QuizDAO qid = new QuizDAO();
        QuestionDAO qed = new QuestionDAO();
        AnswerDAO ad = new AnswerDAO();
        LessonDAO ld = new LessonDAO();

        SubjectDAO sd = new SubjectDAO();
        //handle quiz-time
//        Date start_time = (Date) request.getSession().getAttribute("start_time");
//        Date limit_time = (Date) request.getSession().getAttribute("limit_time");
//        if (start_time == null) {
//            start_time = new Date();
//        }
//        if (limit_time == null) {
//            limit_time = new Date(start_time.getTime() + 1000 * quiz.getTime_Limit());
//        } else {
//            long distance = limit_time.getTime() - (new Date()).getTime();
//            if (distance <= 0) {
//                request.getRequestDispatcher("quizsubmit").forward(request, response);
//            }
//        }


        //declare variables        
        int numberQuestion = quiz.getNumber_Question();
        if (cur_number > numberQuestion) {
            cur_number = numberQuestion;
        }
        if (cur_number <= 0) {
            cur_number = 1;
        }
        if (cur_answer_string == null) {
            cur_answer_string = "";
        }
        if ("".equals(cur_answer_string) || cur_answer_string.length() < numberQuestion) {
            cur_answer_string = qid.getCorrectStringAnswerOfQuiz(quiz.getId_Quiz()).replaceAll("[A-Z]", "*");
        }
        request.getSession().setAttribute("cur_answer_string", cur_answer_string);
        boolean[] barr = ad.classifyAnsweredQuestion(cur_answer_string);

        Question cur_question = qed.getQuestionOfQuizByOrder(cur_number, quiz.getId_Quiz());
        List<Answer> cur_answerlist = ad.getAllAnswerOfQuestion(cur_question.getId_Question());

        request.getSession().setAttribute("cur_quiz", quiz);
        request.getSession().setAttribute("cur_less", ld.getLessonByIdLesson(quiz.getId_Lession()));
        request.setAttribute("cur_subject", sd.getSubjectByIdLesson(quiz.getId_Lession()));
        request.setAttribute("cur_number", cur_number);
        request.setAttribute("cur_question", cur_question);
        request.setAttribute("cur_ncorrect", ad.getNumOfCorrectAnswerOfQuestion(cur_question.getId_Question()));
        request.setAttribute("cur_apos", ad.getPositionInAnswerString(cur_number, cur_answer_string));
        request.setAttribute("cur_answerlist", cur_answerlist);
        request.setAttribute("boolarr", barr);
        request.getRequestDispatcher("quizattempt.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

