/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.BlogDAO;
import dal.CatergoryBlogDAO;
import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import model.Blog;
import model.CatergoryBlog;
import model.User;

/**
 *
 * @author PhanQuangHuy59
 */
@MultipartConfig()
@WebServlet(name = "UpdateOrAddPostMarketing", urlPatterns = {"/updatepost"})
public class UpdatePostMarketing extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        BlogDAO blogDao = new BlogDAO();
        UserDAO userDao = new UserDAO();
        CatergoryBlogDAO cateDao = new CatergoryBlogDAO();

        ArrayList<CatergoryBlog> listCateBlog = new ArrayList<>();
        ArrayList<User> listMarketing = new ArrayList<>();

        listCateBlog = (ArrayList<CatergoryBlog>) cateDao.getAllBlogCate();
        listMarketing = userDao.getMarketing();

        String idpost = request.getParameter("idpost");
        Blog b = blogDao.getBlogByIdH(Integer.parseInt(idpost));
        request.setAttribute("post", b);
        request.setAttribute("listmarketing", listMarketing);
        request.setAttribute("listcate", listCateBlog);
        request.getRequestDispatcher("postdetailmarketing.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String check = request.getParameter("check");
        BlogDAO blogDao = new BlogDAO();
        UserDAO userDao = new UserDAO();
        CatergoryBlogDAO cateDao = new CatergoryBlogDAO();

        ArrayList<CatergoryBlog> listCateBlog = new ArrayList<>();
        ArrayList<User> listMarketing = new ArrayList<>();

        listCateBlog = (ArrayList<CatergoryBlog>) cateDao.getAllBlogCate();
        listMarketing = userDao.getMarketing();
        // lay ra id post
        String idpost = request.getParameter("idpost");
        Blog b = blogDao.getBlogByIdH(Integer.parseInt(idpost));

        String titlePost = request.getParameter("titlePost");
        String createdatepost = request.getParameter("createdatepost");
        String updatepost = request.getParameter("updatepost");
        String viewpost = request.getParameter("viewpost");
        String idcatepost = request.getParameter("catepost");
        String statuspost = request.getParameter("statupost");
        String iduser = request.getParameter("ownpost");
        String brief_infor = request.getParameter("briefinfor");
        String content = request.getParameter("content");

        Part filePart = request.getPart("imagepost");
        Part filePart1 = request.getPart("imagepost");

        Blog blogcheck = blogDao.checkExistBlog(titlePost, Integer.parseInt(idpost));
        if (blogcheck == null) {

            String thumbnail = "";
            boolean check1 = false;
            if (filePart != null && filePart.getSize() > 0) {
                String originalFileName = filePart.getSubmittedFileName();
                String fileExtension = originalFileName.substring(originalFileName.lastIndexOf("."));
                thumbnail = System.currentTimeMillis() + fileExtension;

               String uploadPath = "B:\\FPT_Uni\\SU23\\SWP391\\PROJECT_SWP391\\Final\\14-07-2023 clone\\summer2023-swp391.se1714-g2\\ProjectOnlineLearning_SWP391\\web\\assets\\images\\post\\" + thumbnail;
                    String uploadBuildPath = "B:\\FPT_Uni\\SU23\\SWP391\\PROJECT_SWP391\\Final\\14-07-2023 clone\\summer2023-swp391.se1714-g2\\ProjectOnlineLearning_SWP391\\build\\web\\assets\\images\\post\\" + thumbnail;
                try {
                    FileOutputStream fos = new FileOutputStream(uploadPath);
                    FileOutputStream fos1 = new FileOutputStream(uploadBuildPath);
                    InputStream is = filePart.getInputStream();
                    byte[] data = new byte[is.available()];
                    is.read(data);
                    fos.write(data);
                    fos1.write(data);
                    fos.close();
                    fos1.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                thumbnail = b.getThumbnail_Blog();
                check1 = true;
            }
            if(check1 == false){
                thumbnail = "assets/images/post/"+ thumbnail;
            }
            b.setTitle(titlePost);
            b.setContent_Blog(content);
            b.setBrief_Infor_Blog(brief_infor);
            b.setId_User(Integer.parseInt(iduser));
            b.setId_CatergoryBlog(Integer.parseInt(idcatepost));
            b.setStatus(statuspost.equals("1"));
            b.setThumbnail_Blog(thumbnail);
            blogDao.updateBlog(b);
            Blog reblog = blogDao.getBlogByIdH(Integer.parseInt(idpost));
            request.setAttribute("post", reblog);
            request.setAttribute("mesaddcousrse", "Successfully update the post to the EduChamp system");
            request.setAttribute("classmes", " messagesuccess");

        } else {
            b.setTitle(titlePost);
            b.setContent_Blog(content);
            b.setBrief_Infor_Blog(brief_infor);
            b.setId_User(Integer.parseInt(iduser));
            b.setId_CatergoryBlog(Integer.parseInt(idcatepost));
            b.setStatus(statuspost.equals("1"));
            request.setAttribute("post", b);
            request.setAttribute("mesaddcousrse", "Update Post failed, Try again!");
            request.setAttribute("classmes", " messagefail");
            request.setAttribute("mes", "The title of the post already exists, try another title tape.");
        }
        request.setAttribute("listmarketing", listMarketing);
        request.setAttribute("listcate", listCateBlog);
        request.getRequestDispatcher("postdetailmarketing.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
