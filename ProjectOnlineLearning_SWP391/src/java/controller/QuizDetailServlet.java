/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.LessonDAO;
import dal.QuizDAO;
import dal.SubjectDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.Date;
import model.Lesson;
import model.Quiz;
import model.Subject;

/**
 *
 * @author Admin
 */
@WebServlet(name = "QuizDetailServlet", urlPatterns = {"/quizdetail"})
public class QuizDetailServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet QuizDetailServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet QuizDetailServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int qid = Integer.parseInt(request.getParameter("qid"));
        QuizDAO qDAO = new QuizDAO();
        Quiz q = qDAO.getQuizByID(qid);
        request.setAttribute("quiz", q);
        
        LessonDAO lDAO= new LessonDAO();
        String l = lDAO.getLessonNameByQuizID(qid);
        request.setAttribute("lesson", l);
        
        SubjectDAO sDAO = new SubjectDAO();
        Subject s = sDAO.getSubjectByQuizID(qid);
        request.setAttribute("subject", s);
        //response.getWriter().print(q.getCreateDate());
        request.getRequestDispatcher("expert/quizdetail.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int qid = Integer.parseInt(request.getParameter("qid"));
        Date date = new Date();
        Date udate = new Date();
        java.sql.Date uDate = new java.sql.Date(udate.getTime());
        String name = request.getParameter("name");
        int timelimit = Integer.parseInt(request.getParameter("dur"));
        float passrate = Float.valueOf(request.getParameter("rate"));
        QuizDAO qDAO = new QuizDAO();
        qDAO.updateQuiz(name,timelimit,passrate,qid,uDate);
        response.sendRedirect("quizdetail?qid="+qid);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
