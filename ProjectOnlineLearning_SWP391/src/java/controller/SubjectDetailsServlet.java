/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.CatergorySubjectDAO;
import dal.CourseCategoryDAO;
import dal.CourseDAO;
import dal.PackagePriceDAO;
import dal.SubjectDAO;
import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import model.CatergoryCourse;
import model.CatergorySubject;
import model.Course;
import model.PackagePrice;
import model.Subject;
import model.User;

/**
 *
 * @author GangsterCao
 */
@MultipartConfig()//1
@WebServlet(name = "SubjectDetailsServlet", urlPatterns = {"/subjectdetails"})
public class SubjectDetailsServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SubjectDetailsServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SubjectDetailsServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String subject_id_raw = request.getParameter("subjectid");
        SubjectDAO sdao = new SubjectDAO();
        CatergorySubjectDAO csdao = new CatergorySubjectDAO();
        UserDAO udao = new UserDAO();
        CourseDAO cdao = new CourseDAO();
        CourseCategoryDAO ccdao = new CourseCategoryDAO();
        PackagePriceDAO ppdao = new PackagePriceDAO();
        ArrayList<CatergoryCourse> listcc = ccdao.getAllCatergory();

        Subject s = sdao.getSubjectById(Integer.parseInt(subject_id_raw));

        CatergorySubject cs = csdao.getCatergorySubject(s.getId_Catergory_Subject());
        User u = udao.getUserByIdUser(s.getId_User());
        Course c = cdao.getCourseByIDToUpdate(s.getId_Course());
        List<CatergorySubject> listcatesubject = csdao.getAllCatergorySubject();
        List<Course> clist = cdao.getAllCourses();
        List<User> listU = udao.getListUserRoleExpert();
        CatergoryCourse cc = ccdao.getCategoryCourseWithID(c.getId_Category());
        List<PackagePrice> listppcourse = ppdao.getPackagePriceOfCourseUnpublic(c.getId_Course());
        PackagePrice p = ppdao.getFirstPackagePriceByIdCourse(c.getId_Course());

        request.setAttribute("listppcourse", listppcourse);
        request.setAttribute("ppofcourse", p);
        request.setAttribute("listAuthoer", listU);
        request.setAttribute("catecourse", cc);
        request.setAttribute("listcc", listcc);
        request.setAttribute("clist", clist);
        request.setAttribute("listcatesubject", listcatesubject);
        request.setAttribute("courseID", c);
        request.setAttribute("userID", u);
        request.setAttribute("categorySubjectName", cs);
        request.setAttribute("asubjectwithID", s);
        request.setAttribute("xxx", "SubjectManagment");
        request.setAttribute("xxxDetails", "SubjectDetails");
        request.getRequestDispatcher("subjectdetails.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String subjectName_raw = request.getParameter("subjectName");
        String subjectStatus_raw = request.getParameter("subjectStatus");
        String teacherName_raw = request.getParameter("teacherName");
        String lessonNumber_raw = request.getParameter("lessonNumber");
        String subjectCategory_raw = request.getParameter("subjectCategory");
        String subjectDescription_raw = request.getParameter("subjectDescription");
        String createDateSubject_raw = request.getParameter("createDateSubject");
        String updateSubjectRecent_raw = request.getParameter("updateDateSubject");

        //-----------------------------------------
        String courseStatus = request.getParameter("CourseStatus");
        String createDateCourse_raw = request.getParameter("courseToCreateDate");
        String courseToUpdateDate_raw = request.getParameter("courseToUpdateDate");
        String courseName_raw = request.getParameter("courseName");
        String courseCategory_raw = request.getParameter("courseCategory");
        String courseDescription_raw = request.getParameter("courseDescription");
        String subjectIDinput_raw = request.getParameter("subjectIDinput");
        //-----------------------------------------
        SubjectDAO sdao = new SubjectDAO();
        CourseDAO cdao = new CourseDAO();
        UserDAO udao = new UserDAO();
        //-----------------------------------------
        Subject s = sdao.getSubjectById(Integer.parseInt(subjectIDinput_raw));
        //-----------------------------------------
        Part filePart = request.getPart("fileInputSubject");
        if (filePart.getSize() > 0) {

            String realPath = request.getServletContext().getRealPath("/");
            File parentDir = new File(realPath).getParentFile();
            File grandParentDir = parentDir.getParentFile();
            String fileName = new File(filePart.getSubmittedFileName()).getName();
            String fileExtension = fileName.substring(fileName.lastIndexOf("."));
            String thubnail = "Subject"+System.currentTimeMillis() + fileExtension;
            String fileWeb = grandParentDir + "\\web\\assestsAdmin\\images\\Subject/" + thubnail;
            try {

                FileOutputStream fos = new FileOutputStream(fileWeb);
                InputStream is = filePart.getInputStream();
                byte[] data = new byte[is.available()];
                is.read(data);
                fos.write(data);
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            File file = new File(fileWeb);
            if (!file.exists()) {
                filePart.write(fileWeb);
                s.setImage_subject("assestsAdmin/images/Subject/" + thubnail);//7
                // File doesn't exist, write the file
                try {
                    Thread.sleep(1000); // Đợi 0.5 giây (500 milliseconds)
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                s.setImage_subject("assestsAdmin/images/Subject/" + thubnail);
                try {
                    Thread.sleep(1000); // Đợi 0.5 giây (500 milliseconds)
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                // File already exists, do not write
            }

        }
//        //-----------------------------------------
        s.setSubject_Name(subjectName_raw);//3
        s.setStatus(Boolean.valueOf(subjectStatus_raw));//4
        s.setNumberLesson(Integer.parseInt(lessonNumber_raw));//6
        s.setId_Catergory_Subject(Integer.parseInt(subjectCategory_raw));//8
        s.setDescription(subjectDescription_raw);//9
        s.setId_User(Integer.parseInt(teacherName_raw));
        s.setCreate_Date(Date.valueOf(createDateSubject_raw));//1
        s.setUpdate_Date(Date.valueOf(updateSubjectRecent_raw));//2

        sdao.updateIntoSubjectWithID(s);
        //-----------------------------------------       
        //-----------------------------------------
        Course c = cdao.getCourseByIDToUpdate(s.getId_Course());
        Part filePart2 = request.getPart("fileInputCourse");
        if (filePart2.getSize() > 0) {

            String realPath2 = request.getServletContext().getRealPath("/");
            File parentDir2 = new File(realPath2).getParentFile();
            File grandParentDir2 = parentDir2.getParentFile();
            String fileName = new File(filePart2.getSubmittedFileName()).getName();
            String fileExtension = fileName.substring(fileName.lastIndexOf("."));
            String thubnail = "Course"+System.currentTimeMillis() + fileExtension;
            String fileWeb = grandParentDir2 + "\\web\\assestsAdmin\\images\\CourseUpload/" + thubnail;
            try {
                FileOutputStream fos = new FileOutputStream(fileWeb);
                InputStream is = filePart2.getInputStream();
                byte[] data = new byte[is.available()];
                is.read(data);
                fos.write(data);
                fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            File file1 = new File(fileWeb);
            if (!file1.exists()) {
                filePart2.write(fileWeb);
                c.setImage("assestsAdmin/images/CourseUpload/" + thubnail);//7
                try {
                    Thread.sleep(1000); // Đợi 0.5 giây (500 milliseconds)
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                // File doesn't exist, write the file
            } else {
                c.setImage("assestsAdmin/images/CourseUpload/" + thubnail);
                // File already exists, do not write
                try {
                    Thread.sleep(1000); // Đợi 0.5 giây (500 milliseconds)
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }

        c.setCourse_Name(courseName_raw);//3     
        c.setDescription_Course(courseDescription_raw);//7
        c.setId_Category(Integer.parseInt(courseCategory_raw));//4
        c.setStatus(Boolean.valueOf(courseStatus));//6
        c.setCreate_Date(Date.valueOf(createDateCourse_raw));//1
        c.setUpdate_Date(Date.valueOf(courseToUpdateDate_raw));//
        cdao.updateCourseWithIdSubject(c);
        try {
            Thread.sleep(500); // Đợi 0.5 giây (500 milliseconds)
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        HttpSession session = request.getSession();
        session.setAttribute("val", 1);
        request.getRequestDispatcher("expertsubjectlistservlet").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
