/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.SliderDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Slider;

/**
 *
 * @author PCT
 */
@WebServlet(name = "UpdateSlider", urlPatterns = {"/updateslider"})
public class UpdateSlider extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            SliderDAO dao = new SliderDAO();
            int id = Integer.parseInt(request.getParameter("idslider"));
            Slider slider = dao.getSliderByID(id);
            List<Slider> list = dao.getAllSliders();
            System.out.println("List" + list);
            request.setAttribute("listSlider", list);
            request.setAttribute("sliderdetail", slider);
            request.getRequestDispatcher("updateslider.jsp").forward(request, response);
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id_Slider = request.getParameter("id_Slider");
        String title_Slider = request.getParameter("title_Slider");

        String thumbnail_Image = request.getParameter("thumbnail_Image");
        String create_Date = request.getParameter("create_Date");

        String update_Date = request.getParameter("update_Date");
        String status = request.getParameter("status");
        String notes = request.getParameter("notes");
        String blacklink = request.getParameter("backlink");
        String content_Slider = request.getParameter("content_Slider");
        String id_User = request.getParameter("id_user");
        SliderDAO s = new SliderDAO();

        s.updateSliderById(id_Slider, title_Slider, thumbnail_Image, create_Date, update_Date, status, notes, blacklink, content_Slider, id_User);


        request.getRequestDispatcher("castout.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
