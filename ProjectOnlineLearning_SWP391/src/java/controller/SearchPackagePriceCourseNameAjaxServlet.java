/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.CourseDAO;
import dal.PackagePriceDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import model.Course;
import model.PackagePrice;
import model.User;

/**
 *
 * @author GangsterCao
 */
@WebServlet(name = "SearchPackagePriceCourseNameAjaxServlet", urlPatterns = {"/searchpackagepricecoursenameajax"})
public class SearchPackagePriceCourseNameAjaxServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String text = request.getParameter("txt");
        CourseDAO cdao = new CourseDAO();
        List<Course> listcourse = cdao.searchCourse(text);
        PackagePriceDAO pdao = new PackagePriceDAO();
        List<PackagePrice> pnormal = pdao.getAllpackagePriceNormal();
        List<PackagePrice> pupdate1 = new ArrayList<>();
        List<Course> nameCourse = new ArrayList<>();
        for (int i = 0; i < pnormal.size(); i++) {
            for (int j = 0; j < listcourse.size(); j++) {
                if (pnormal.get(i).getId_Course() == listcourse.get(j).getId_Course()) {
                    PackagePrice p = pdao.getPackagePriceByIdCourse(listcourse.get(j).getId_Course());
                    pupdate1.add(p);
                }
            }
        }
        for (PackagePrice pu : pupdate1) {
            Course c = cdao.getCourseByIDToUpdate(pu.getId_Course());
            nameCourse.add(c);
        }
        List<String> Status = new ArrayList<>();
        for (int i = 0; i < pupdate1.size(); i++) {
            if (pupdate1.get(i).isStatus()) {
                Status.add("Active");
            } else {
                Status.add("Deactive");
            }
        }

        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();

        User u = (User) session.getAttribute("user");
        if (u.getId_role() == 1) {
            for (int i = 0; i < pupdate1.size(); i++) {
                out.println("<tr>\n"
                        + "                                                                                <th style=\"width: 5%\" scope=\"row\">" + (i + 1) + "</th>\n"
                        + "                                                                                <td style=\"width: 20%\">" + nameCourse.get(i).getCourse_Name() + "</td>\n"
                        + "                                                                                <td style=\"width: 10%\">" + pupdate1.get(i).getDuration() + "</td>\n"
                        + "                                                                                <td>" + pupdate1.get(i).getList_Price() + "&nbsp;$</td>\n"
                        + "                                                                                <td>" + pupdate1.get(i).getSale_Price() + "&nbsp;$</td>\n"
                        + "                                                                                <td>" + Status.get(i) + "</td>\n"
                        + "                                                                                \n"
                        + "                                                                                    <td style=\"width: 20%\">\n"
                        + "                                                                                        <button value=\"edit\" class=\"btn\" onclick=\"editCourseNumber(" + i + ")\">Edit</button>\n"
                        + "                                                                                        <button type=\"button\" class=\"btn gradient brown\" onclick=\"doDeletePackagePrice(" + pupdate1.get(i).getId_Package() + ")\">Delete</button>\n"
                        + "                                                                                    </td>\n"
                        + "                                                                                \n"
                        + "                                                                            </tr>");
            }
        } else {
            for (int i = 0; i < pupdate1.size(); i++) {
                out.println("<tr>\n"
                        + "                                                                                <th style=\"width: 5%\" scope=\"row\">" + (i + 1) + "</th>\n"
                        + "                                                                                <td style=\"width: 20%\">" + nameCourse.get(i).getCourse_Name() + "</td>\n"
                        + "                                                                                <td style=\"width: 10%\">" + pupdate1.get(i).getDuration() + "</td>\n"
                        + "                                                                                <td>" + pupdate1.get(i).getList_Price() + "&nbsp;$</td>\n"
                        + "                                                                                <td>" + pupdate1.get(i).getSale_Price() + "&nbsp;$</td>\n"
                        + "                                                                                <td>" + Status.get(i) + "</td>\n"
                        + "                                                                            </tr>");
            }
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
