/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import dal.CatergoryCourseDAO;
import dal.CourseCategoryDAO;
import dal.CourseDAO;
import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import model.CatergoryCourse;
import model.Course;
import model.User;

/**
 *
 * @author PhanQuangHuy59
 */
@WebServlet(name="AdminCourseList", urlPatterns={"/admincourselist"})
public class AdminCourseList extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        doGet(request, response);
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        CourseDAO couDao = new CourseDAO();
        UserDAO userDao = new UserDAO();
        CourseCategoryDAO cateDao = new CourseCategoryDAO();
        
        
        ArrayList<Course> listcour = new ArrayList<>();
        ArrayList<User> listteacher = new ArrayList<>();
        ArrayList<CatergoryCourse> listcategory = new ArrayList<>();
        
        ArrayList<CatergoryCourse> listcategory1 = cateDao.getAllCatergory();
        ArrayList<User> listexpert =userDao.getExpert();
        
        String check = request.getParameter("check");
        User u = new User();
        u.setId_role(1);
        if(check == null || check.length() == 0){
            listcour = couDao.getFullCourseHuy(u);
        }else if(check.equals("1")){
            String key = request.getParameter("name");
            request.setAttribute("key", key);
            listcour = couDao.getCourseByName(key);
        }else if(check.equals("2")){
            String idcard = request.getParameter("categoryID");
            request.setAttribute("categoryID", idcard);
            listcour = couDao.getFullCourseByCategoryHuy(u, Integer.parseInt(idcard));
        }
        else if(check.equals("3")){
            String idUser = request.getParameter("expert");
            User user = userDao.getUserByIdUser(Integer.parseInt(idUser));
            request.setAttribute("expert", idUser);
            listcour = couDao.getFullCourseHuy(user);
        }else if(check.equals("4")){
            String status = request.getParameter("status");
            request.setAttribute("status", Integer.parseInt(status));
            listcour = couDao.getFullCourseByStatusHuy(u, Integer.parseInt(status));
        }
        
         String tpage = request.getParameter("page");
         int numPs = listcour.size();
            int numpage = numPs / 15 + (numPs % 15 == 0 ? 0 : 1);
            listcour = phantrangcourse(listcour, tpage);
            int page;
            try {
                page = Integer.parseInt(tpage);
            } catch (NumberFormatException e) {
                page = 1;
            }
            request.setAttribute("page", page);
            request.setAttribute("num1", numpage);
        
        if(listcour.size() > 0){
            for (Course course : listcour) {
                System.out.println(course.getCourse_Name());  
                User u1 = userDao.getUserByIdUser(course.getId_User());
                CatergoryCourse ca = cateDao.getCatergoryById(course.getId_Category());
                listteacher.add(u1);
                listcategory.add(ca);
            }
        }
       
         request.setAttribute("check", check);
        request.setAttribute("listcourse", listcour);
        request.setAttribute("listteacher", listteacher);
        request.setAttribute("listcategory", listcategory);
        
        request.setAttribute("listexpert", listexpert);
        request.setAttribute("listca", listcategory1);
        request.getRequestDispatcher("adminlistcourse.jsp").forward(request, response);
    }
     public ArrayList<Course> phantrangcourse(ArrayList<Course> list, String tpage) {
        int numPs = list.size();
        int numperPage = 15; 

        int numpage = numPs / numperPage + (numPs % numperPage == 0 ? 0 : 1);
        int start, end;

        int page;

        try {
            page = Integer.parseInt(tpage);
        } catch (NumberFormatException e) {
            page = 1;
        }
        start = (page - 1) * numperPage;
        if (page * numperPage > numPs) {
            end = numPs;
        } else {
            end = page * numperPage;
        }

        return getListByPage(list, start, end);
    }

    public ArrayList<Course> getListByPage(ArrayList<Course> list,
            int start, int end) {
        ArrayList<Course> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
