/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.CourseCategoryDAO;
import dal.CourseDAO;
import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import model.CatergoryCourse;
import model.Course;
import model.User;

/**
 *
 * @author PhanQuangHuy59
 */
@MultipartConfig()
@WebServlet(name = "AdminUpdateCourse", urlPatterns = {"/updatecourse"})
public class AdminUpdateCourse extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AdminUpdateCourse</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AdminUpdateCourse at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        UserDAO userDao = new UserDAO();
        CourseCategoryDAO caDao = new CourseCategoryDAO();
        CourseDAO courDao = new CourseDAO();
        ArrayList<User> listExpert = new ArrayList<>();
        ArrayList<CatergoryCourse> listCa = new ArrayList<>();
        listExpert = userDao.getExpert();
        listCa = caDao.getAllCatergory();
        String idcourse = request.getParameter("idcourse");
        Course cour = new Course();
        cour = courDao.getCourseByIDH(Integer.parseInt(idcourse));
        request.setAttribute("cour", cour);
        request.setAttribute("liste", listExpert);
        request.setAttribute("listc", listCa);
        request.getRequestDispatcher("updatecourse.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        UserDAO userDao = new UserDAO();
        CourseCategoryDAO caDao = new CourseCategoryDAO();
        CourseDAO courDao = new CourseDAO();

        String idCourse = request.getParameter("idcourse");
        String courseName = request.getParameter("nameCourse");
        String statusCourse = request.getParameter("statuCourse");
        String categoryCourse = request.getParameter("categoryCourse");
        String expertCourse = request.getParameter("expert");
        String description = request.getParameter("description");
        Part filePart = request.getPart("imageCourse");
        Part filePart1 = request.getPart("imageCourse");

        Course check = courDao.getCourseNameandid(courseName,Integer.parseInt(idCourse));

        if (check.getCourse_Name() == null) {

            Course updateCourse = new Course();
            updateCourse.setId_Course(Integer.parseInt(idCourse));
            updateCourse.setCourse_Name(courseName);
            updateCourse.setStatus(statusCourse.equals("1"));
            updateCourse.setId_Category(Integer.parseInt(categoryCourse));
            updateCourse.setId_User(Integer.parseInt(expertCourse));
            updateCourse.setDescription_Course(description);
            String thumbnail = "";
            boolean check1 = false;
            if (filePart != null && filePart.getSize() > 0) {
                if (filePart.getSubmittedFileName().endsWith(".jpg") || filePart.getSubmittedFileName().endsWith(".png")) {
                    String originalFileName = filePart.getSubmittedFileName();
                    String fileExtension = originalFileName.substring(originalFileName.lastIndexOf("."));
                    thumbnail = System.currentTimeMillis() + fileExtension;

                   String uploadPath = "B:\\FPT_Uni\\SU23\\SWP391\\PROJECT_SWP391\\Final\\14-07-2023 clone\\summer2023-swp391.se1714-g2\\ProjectOnlineLearning_SWP391\\web\\assets\\images\\courses\\" + thumbnail;
                    String uploadBuildPath = "B:\\FPT_Uni\\SU23\\SWP391\\PROJECT_SWP391\\Final\\14-07-2023 clone\\summer2023-swp391.se1714-g2\\ProjectOnlineLearning_SWP391\\build\\web\\assets\\images\\courses\\" + thumbnail;
                    try {
                        FileOutputStream fos = new FileOutputStream(uploadPath);
                        FileOutputStream fos1 = new FileOutputStream(uploadBuildPath);
                        InputStream is = filePart.getInputStream();
                        byte[] data = new byte[is.available()];
                        is.read(data);
                        fos.write(data);
                        fos1.write(data);
                        fos.close();
                        fos1.close();
                        thumbnail = "assets/images/courses/" + thumbnail;

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    check1 = true;
                }

            } else {
                thumbnail = request.getParameter("image");
            }
            updateCourse.setImage(thumbnail);
            updateCourse.setCreate_Date(check.getCreate_Date());
            updateCourse.setUpdate_Date(check.getUpdate_Date());
            if (check1 == false) {
                courDao.UpdateCouseAdminH(updateCourse);
                Course cDisplay = courDao.getCourseByIDH(Integer.parseInt(idCourse));
                request.setAttribute("mesaddcousrse", "Successfully update the course to the EduChamp system");
                request.setAttribute("classmes", " messagesuccess");
                request.setAttribute("cour", cDisplay);
            } else {
                 request.setAttribute("cour", check);
                request.setAttribute("cour", updateCourse);
                request.setAttribute("mesaddcousrse", "Add courses failed, Try again");
                request.setAttribute("mes1", "The image must be in .jpg or .png format");
            }

        } else {
            System.out.println("else");
            Course updateCourse = new Course();
            updateCourse.setId_Course(Integer.parseInt(idCourse));
            updateCourse.setCourse_Name(courseName);
            updateCourse.setStatus(statusCourse.equals("1"));
            updateCourse.setId_Category(Integer.parseInt(categoryCourse));
            updateCourse.setId_User(Integer.parseInt(expertCourse));
            updateCourse.setDescription_Course(description);
            updateCourse.setImage(check.getImage());
            updateCourse.setCreate_Date(check.getCreate_Date());
            updateCourse.setUpdate_Date(check.getUpdate_Date());

            request.setAttribute("cour", updateCourse);
            request.setAttribute("mesaddcousrse", "Add courses failed, Try again");
            request.setAttribute("classmes", " messagefail");
            if (check.getCourse_Name() != null) {
                request.setAttribute("mes", "The name of the course already exists try another name!");
            }
            if (filePart != null && filePart.getSize() > 0) {
                if (!filePart.getSubmittedFileName().endsWith(".jpg") && !filePart.getSubmittedFileName().endsWith(".png")) {
                    request.setAttribute("mes1", "The image must be in .jpg or .png format");
                }
            }

        }

        ArrayList<User> listExpert = new ArrayList<>();
        ArrayList<CatergoryCourse> listCa = new ArrayList<>();
        listExpert = userDao.getExpert();
        listCa = caDao.getAllCatergory();

//        request.setAttribute("cour", cour);
        request.setAttribute("liste", listExpert);
        request.setAttribute("listc", listCa);
        request.getRequestDispatcher("updatecourse.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
