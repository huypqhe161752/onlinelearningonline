/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.LessonDAO;
import dal.TypeLessonDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model1.Lessons;

/**
 *
 * @author PCT
 */
@WebServlet(name = "SubjectLesson", urlPatterns = {"/subjectlesson"})
public class SubjectLesson extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            LessonDAO dao = new LessonDAO();
            TypeLessonDAO dao1 = new TypeLessonDAO();
            String idsubject = request.getParameter("id_Subject");
            String id_Lesson = request.getParameter("id_Lesson");
            boolean status = "1".equals(request.getParameter("status"));
            // Cập nhật trạng thái bài học
            // Cập nhật trạng thái bài học
            if (id_Lesson != null) {
                dao.updateStatusLesson(id_Lesson, status);
            }

// Lấy danh sách bài học mới sau khi cập nhật trạng thái
            List<Lessons> list = dao.getAllLessonByIdSubject(idsubject);
            int numPs = list.size();
            int numperPage = 6;
            int numpage = numPs / numperPage + (numPs % numperPage == 0 ? 0 : 1);
            int start, end;

            String tpage = request.getParameter("page");

            int page;

            try {
                page = Integer.parseInt(tpage);
            } catch (NumberFormatException e) {
                page = 1;
            }
            start = (page - 1) * numperPage;
            if (page * numperPage > numPs) {
                end = numPs;
            } else {
                end = page * numperPage;
            }

            List<Lessons> arrsubject = dao.getListByPage1(list, start, end);
            request.setAttribute("num1", numpage);
            request.setAttribute("page", page);
            request.setAttribute("typelesson", dao1.getAll());
            request.setAttribute("list", arrsubject);
            request.setAttribute("id_Subject", idsubject);
            request.setAttribute("xxx", "SubjectManagment");

// Chuyển hướng đến trang danh sách bài học
            request.getRequestDispatcher("subjectlesson.jsp").forward(request, response);
        }

// Lấy danh sách bài học mới
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
