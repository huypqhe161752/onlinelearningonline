/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.CheckBoxLearnDAO;
import dal.CourseDAO;
import dal.SubjectDAO;
import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import model.CheckBoxLearn;
import model.Course;
import model.Subject;
import model.User;

/**
 *
 * @author PhanQuangHuy59
 */
@WebServlet(name = "MyCourseDetail", urlPatterns = {"/mycoursedetail"})
public class MyCourseDetailServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MyCourseDetail</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MyCourseDetail at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User u = (User) session.getAttribute("user");
        CourseDAO coDao = new CourseDAO();
        SubjectDAO subDao = new SubjectDAO();
        UserDAO usDao = new UserDAO();
        CheckBoxLearnDAO chDao = new CheckBoxLearnDAO();
        ArrayList<User> listUser = new ArrayList<>();
        String idcourse = request.getParameter("idcourse");
        String tpage = request.getParameter("page");
        Course cour = coDao.getCourseByID(Integer.parseInt(idcourse));
        String searchKey = request.getParameter("subjectname");
      
        ArrayList<Subject> listsubject = null;
        ArrayList<Double> process = new ArrayList<>();
        if (searchKey == null) {
            listsubject = subDao.getSubjectByIdCourse(Integer.parseInt(idcourse), 1);
        } else {
            listsubject = subDao.getSubjectByNameSubjectOrNameTeacher(searchKey, Integer.parseInt(idcourse));
        }
        
        for (Subject subject : listsubject) {
            double numberLession, numberLearn, process1;
            numberLession = subDao.getNumberLesson(subject.getId_Subject());
            
            numberLearn = chDao.getNumberLearn(subject.getId_Subject(), u.getId_User());
           
            
            try {
                process1 = Math.ceil(100 *(numberLearn / numberLession));
                if(Double.isNaN(process1)){
                    process1 = 0;
                }
                        
               
            } catch (ArithmeticException e) {
                process1 = 0;
            }

            process.add(process1);

        }
        int numPs = listsubject.size();
        int numpage = numPs / 6 + (numPs % 6 == 0 ? 0 : 1);
        listsubject = phantrang(listsubject, tpage);
        int page;
        try {
            page = Integer.parseInt(tpage);
        } catch (NumberFormatException e) {
            page = 1;
        }
        for (Subject subject : listsubject) {
            listUser.add(usDao.getUserByIdUser(subject.getId_User()));
        }
        request.setAttribute("process", process);
        request.setAttribute("key", searchKey);
        request.setAttribute("page", page);
        request.setAttribute("num1", numpage);
        request.setAttribute("listauthor", listUser);
        request.setAttribute("idcourse", idcourse);
        request.setAttribute("course", cour);
        request.setAttribute("listsubject", listsubject);
        request.setAttribute("xxx", "mycourse");
        request.getRequestDispatcher("mycoursedetail.jsp").forward(request, response);
    }

    public ArrayList<Subject> phantrang(ArrayList<Subject> list, String tpage) {
        int numPs = list.size();
        int numperPage = 6;

        int numpage = numPs / numperPage + (numPs % numperPage == 0 ? 0 : 1);
        int start, end;

        int page;

        try {
            page = Integer.parseInt(tpage);
        } catch (NumberFormatException e) {
            page = 1;
        }
        start = (page - 1) * numperPage;
        if (page * numperPage > numPs) {
            end = numPs;
        } else {
            end = page * numperPage;
        }

        return getListByPage(list, start, end);
    }

    public ArrayList<Subject> getListByPage(ArrayList<Subject> list,
            int start, int end) {
        ArrayList<Subject> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
