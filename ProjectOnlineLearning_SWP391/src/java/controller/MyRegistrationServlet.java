/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.PackagePriceDAO;
import dal.RegistrationDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import model.Blog;
import model.MyRegistration;
import model.PackagePrice;
import model.User;

/**
 *
 * @author Admin
 */
@WebServlet(name = "MyRegistrationServlet", urlPatterns = {"/myregistrationservlet"})
public class MyRegistrationServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MyRegistrationServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MyRegistrationServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User u = (User) session.getAttribute("user");
        if (u == null) {
            response.sendRedirect("login.jsp");
        } else {
            RegistrationDAO reDAO = new RegistrationDAO();
            ArrayList<MyRegistration> listMyRe = reDAO.getMyRegistration(u.getEmail());
            session.setAttribute("MyRes", listMyRe);
            int numPs = listMyRe.size();
            int numperPage = 6;
            int numpage = numPs / numperPage + (numPs % numperPage == 0 ? 0 : 1);
            int start, end;
            String tpage = request.getParameter("page");
            int page;
            try {
                page = Integer.parseInt(tpage);
            } catch (NumberFormatException e) {
                page = 1;
            }
            start = (page - 1) * numperPage;
//        if (page * numperPage > numPs) {
//            end = numPs;
//        } else {
//            end = page * numperPage;
//        }
            end = Math.min(page * numperPage, numPs);
            ArrayList<MyRegistration> mainlist = reDAO.getListByPage(listMyRe, start, end);
            session.setAttribute("listMyRe", mainlist);

            ArrayList<String> arS = new ArrayList<>();
            for (int i = 0; i < listMyRe.size(); i++) {
                if (arS.contains(listMyRe.get(i).getName_Category())) {
                } else {
                    arS.add(listMyRe.get(i).getName_Category());
                }
            }
            PackagePriceDAO pDAO = new PackagePriceDAO();
            ArrayList<PackagePrice> pp = new ArrayList<>();
            pp = pDAO.getAll();
            request.setAttribute("listprice", pp);
            session.setAttribute("listCate", arS);
            request.setAttribute("numofpage", numpage);
            request.setAttribute("currentpage", page);
            //session.setAttribute("listMyRe", listMyRe);
            request.getRequestDispatcher("myregistration.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User u = (User) session.getAttribute("user");
        if (u == null) {
            response.sendRedirect("login.jsp");
        } else {
            ArrayList<MyRegistration> myRe = (ArrayList<MyRegistration>) session.getAttribute("listMyRe");
            String content = request.getParameter("searchcontent");
            int ch = Integer.parseInt(request.getParameter("typeofsearch"));
            if (ch == 1) {
                RegistrationDAO reDAO = new RegistrationDAO();
                ArrayList<MyRegistration> m = reDAO.getMyRegistrationByNameOfCourse(myRe,u.getEmail(), content);

                int numPs = m.size();
                int numperPage = 6;
                int numpage = numPs / numperPage + (numPs % numperPage == 0 ? 0 : 1);
                int start, end;
                String tpage = request.getParameter("page");
                int page;
                try {
                    page = Integer.parseInt(tpage);
                } catch (NumberFormatException e) {
                    page = 1;
                }
                start = (page - 1) * numperPage;
//        if (page * numperPage > numPs) {
//            end = numPs;
//        } else {
//            end = page * numperPage;
//        }
                end = Math.min(page * numperPage, numPs);
                ArrayList<MyRegistration> mainlist = reDAO.getListByPage(m, start, end);
                session.setAttribute("listMyRe", mainlist);
                request.setAttribute("numofpage", numpage);
                request.setAttribute("currentpage", page);

                ArrayList<String> arS = new ArrayList<>();
                for (int i = 0; i < m.size(); i++) {
                    if (arS.contains(m.get(i).getName_Category())) {

                    } else {
                        arS.add(m.get(i).getName_Category());
                    }
                }
                request.setAttribute("listCate", arS);

                request.setAttribute("contentsearch", content);
                request.getRequestDispatcher("myregistration.jsp").forward(request, response);
            } else if (ch == 2) {
                RegistrationDAO reDAO = new RegistrationDAO();
                String str = request.getParameter("categorysearch");
                if (str.equals("All")) {
                    response.sendRedirect("myregistrationservlet");
                } else {
                    ArrayList<MyRegistration> m = reDAO.getMyRegistrationByNameOfCourseCategory(u.getEmail(), str);

                    int numPs = m.size();
                    int numperPage = 6;
                    int numpage = numPs / numperPage + (numPs % numperPage == 0 ? 0 : 1);
                    int start, end;
                    String tpage = request.getParameter("page");
                    int page;
                    try {
                        page = Integer.parseInt(tpage);
                    } catch (NumberFormatException e) {
                        page = 1;
                    }
                    start = (page - 1) * numperPage;
//        if (page * numperPage > numPs) {
//            end = numPs;
//        } else {
//            end = page * numperPage;
//        }
                    end = Math.min(page * numperPage, numPs);
                    ArrayList<MyRegistration> mainlist = reDAO.getListByPage(m, start, end);
                    session.setAttribute("listMyRe", mainlist);
                    request.setAttribute("numofpage", numpage);
                    request.setAttribute("currentpage", page);

                    ArrayList<String> arS = new ArrayList<>();
                    arS = (ArrayList<String>) session.getAttribute("listCate");
                    session.setAttribute("listCate", arS);
                    request.setAttribute("type", str);
                    session.setAttribute("listMyRe", m);
                    request.getRequestDispatcher("myregistration.jsp").forward(request, response);
                }
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
