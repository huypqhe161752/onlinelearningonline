/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.RegistrationDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model1.RegistrationJoin;

/**
 *
 * @author PCT
 */
@WebServlet(name = "searchregistration", urlPatterns = {"/searchregistration"})
public class searchregistration extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            RegistrationDAO dao = new RegistrationDAO();
            String from = request.getParameter("startDate");
            String to = request.getParameter("endDate");

// Kiểm tra điều kiện startDate không được lớn hơn endDate
            if (from.compareTo(to) > 0) {
                List<RegistrationJoin> list = dao.getAll1();
                int numPs = list.size();
            int numperPage = 6;
            int numpage = numPs / numperPage + (numPs % numperPage == 0 ? 0 : 1);
            int start, end;

            String tpage = request.getParameter("page");

            int page;

            try {
                page = Integer.parseInt(tpage);
            } catch (NumberFormatException e) {
                page = 1;
            }
            start = (page - 1) * numperPage;
            if (page * numperPage > numPs) {
                end = numPs;
            } else {
                end = page * numperPage;
            }

            List<RegistrationJoin> arrsubject = dao.getListByPage1(list, start, end);
            request.setAttribute("num1", numpage);
            request.setAttribute("page", page);
              request.setAttribute("list", arrsubject);
                request.setAttribute("startDate", from);
                request.setAttribute("xxx", "user");
                request.setAttribute("endDate", to);
                request.setAttribute("message", "Start date cannot be later than end date");

                request.getRequestDispatcher("registrationlist.jsp").forward(request, response);

            }

            List<RegistrationJoin> list = dao.getSearchDate(from, to);
            int numPs = list.size();
            int numperPage = 6;
            int numpage = numPs / numperPage + (numPs % numperPage == 0 ? 0 : 1);
            int start, end;

            String tpage = request.getParameter("page");

            int page;

            try {
                page = Integer.parseInt(tpage);
            } catch (NumberFormatException e) {
                page = 1;
            }
            start = (page - 1) * numperPage;
            if (page * numperPage > numPs) {
                end = numPs;
            } else {
                end = page * numperPage;
            }

            List<RegistrationJoin> arrsubject = dao.getListByPage1(list, start, end);
            request.setAttribute("num1", numpage);
            request.setAttribute("page", page);
              request.setAttribute("list", arrsubject);
            request.setAttribute("startDate", from);
            request.setAttribute("endDate", to);
            request.setAttribute("xxx", "user");
            request.getRequestDispatcher("registrationlist.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
