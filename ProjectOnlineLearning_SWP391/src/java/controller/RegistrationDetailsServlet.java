/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.CourseDAO;
import dal.PackagePriceDAO;
import dal.RegistrationDAO;
import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Course;
import model.PackagePrice;
import model.Registration;
import model.User;

/**
 *
 * @author GangsterCao
 */
@WebServlet(name = "RegistrationDetailsServlet", urlPatterns = {"/registrationdetails"})
public class RegistrationDetailsServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet RegistrationDetailsServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet RegistrationDetailsServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String registrationID_raw = request.getParameter("idregistration");
        int registrationID = Integer.parseInt(registrationID_raw);
        RegistrationDAO rdao = new RegistrationDAO();
        UserDAO udao = new UserDAO();
        CourseDAO cdao = new CourseDAO();
        PackagePriceDAO ppdao = new PackagePriceDAO();

        Registration r = rdao.getRegistrationByID(registrationID);
        User u = udao.getUserByIdUser(r.getId_User());
        Course c = cdao.getCourseByIDToUpdate(r.getId_Course());
        PackagePrice pp = ppdao.getPackagePriceByIdcuaHuy(r.getId_packagePrice());
        Boolean statusRegistration;
        if (r.isStatus() == null) {
            statusRegistration = null;
        } else {
            statusRegistration = r.isStatus();
        }

        List<Course> lcourse = cdao.getAllNormalCOurse();
        List<PackagePrice> lpp = ppdao.getAllpackagePriceNormal();

        request.setAttribute("listpackageprice", lpp);
        request.setAttribute("listCourse", lcourse);
        request.setAttribute("statusRegistration", statusRegistration);
        request.setAttribute("registration", r);
        request.setAttribute("user", u);
        request.setAttribute("course", c);
        request.setAttribute("packageprice", pp);
        request.setAttribute("xxx", "RegistrationDetails");
        request.getRequestDispatcher("registrationdetails.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String checkForm_raw = request.getParameter("checkFormValue");
        int checkFormValue = Integer.parseInt(checkForm_raw);
        if (checkFormValue == 1) {
            String registrationID_raw = request.getParameter("idregistration");
            String registrationStatus_raw = request.getParameter("registrationStatus");
            String editSubjectName_raw = request.getParameter("editSubjectName");
            String editListPrice_raw = request.getParameter("editListPrice");
            String editSalePrice_raw = request.getParameter("editSalePrice");
            RegistrationDAO rdao = new RegistrationDAO();
            CourseDAO cdao = new CourseDAO();
            PackagePriceDAO ppdao = new PackagePriceDAO();

            Boolean registrationStatus;
            if (request.getParameter("registrationStatus").equals("true")) {
                registrationStatus = true;
                //System.out.println("true");
            } else if (request.getParameter("registrationStatus").equals("false")) {
                //System.out.println("false");
                registrationStatus = false;
            } else {
                registrationStatus = null;
                //System.out.println("Not null but have space");
            }
            int registrationID = Integer.parseInt(registrationID_raw);

            Registration r = rdao.getRegistrationByID(registrationID);
            PackagePrice pp = ppdao.getPackagePriceByIdcuaHuy(r.getId_packagePrice());
            Course c = cdao.getCourseByIDToUpdate(r.getId_Course());
            int editListPrice = Integer.parseInt(editListPrice_raw);
            int editSalePrice = Integer.parseInt(editSalePrice_raw);

            if (registrationStatus == null) {
                rdao.updateStatusToNull(registrationID);
                cdao.updateCourseName(editSubjectName_raw, c.getId_Course());
                ppdao.updateListPriceSalePriceByID(editListPrice, editSalePrice, pp.getId_Package());
            } else {
                rdao.updateIntoRegistrationWithId(registrationStatus, registrationID);
                cdao.updateCourseName(editSubjectName_raw, c.getId_Course());
                ppdao.updateListPriceSalePriceByID(editListPrice, editSalePrice, pp.getId_Package());
            }

            response.sendRedirect("registrationlist");
        } else if (checkFormValue == 2) {
            UserDAO udao = new UserDAO();
            RegistrationDAO rdao = new RegistrationDAO();
            CourseDAO cdao = new CourseDAO();
            PackagePriceDAO ppdao = new PackagePriceDAO();
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
