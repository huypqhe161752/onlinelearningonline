/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.SliderDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.File;
import java.sql.Date;
import model.Slider;
import model.User;

/**
 *
 * @author Naviank
 */
@MultipartConfig()
@WebServlet(name = "AddSliderServlet", urlPatterns = {"/addslider"})
public class AddSliderServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddSliderServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddSliderServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String slidertitle = request.getParameter("slidertitle");
        String sliderbacklink = request.getParameter("sliderbacklink");
        SliderDAO sd = new SliderDAO();
        Slider s = new Slider();
        s.setTitle_Slider(slidertitle);
        s.setBacklink(sliderbacklink);
        Date d = new Date((new java.util.Date()).getTime());
        User u = (User) request.getSession().getAttribute("user");

        s.setCreate_Date(d);
        s.setUpdate_Date(d);
        s.setStatus(false);
        s.setId_User(u.getId_User());
        Part filePart = request.getPart("filesliderimg");
        if (filePart.getSize() > 0) {
            String realPath = request.getServletContext().getRealPath("/");
            File parentDir = new File(realPath).getParentFile();
            File grandParentDir = parentDir.getParentFile();
            String fileName = new File(filePart.getSubmittedFileName()).getName();
            String fileWeb = grandParentDir + "\\web\\assets\\images\\slider/" + fileName;
            File file = new File(fileWeb);
            if (!file.exists()) {
                filePart.write(fileWeb);
                while (true) {
                    try {
                        Thread.sleep(1500); // Đợi 0.5 giây (500 milliseconds)
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (file.exists() == true) {
                        break;
                    }
                }
                s.setThumbnail_Image("assets/images/slider/" + fileName);
//                s.setImage_subject("assestsAdmin/images/Subject/" + fileName);//7
                // File doesn't exist, write the file
            } else {
                s.setThumbnail_Image("assets/images/slider/" + fileName);
//                s.setImage_subject("assestsAdmin/images/Subject/" + fileName);
                // File already exists, do not write
            }

        }
        int tmp = sd.addNewSlider(s);
        String message;
        if (tmp == 0) {
            message = "Add failed!";
        } else {
            message = "Add successfully!";
        }
//        request.setAttribute("addMessage", message);
        response.sendRedirect("sliderslist?page=1000&addMessage=" + message + "#myTable");
//        request.getRequestDispatcher("sliderslist?page=1000#myTable").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
