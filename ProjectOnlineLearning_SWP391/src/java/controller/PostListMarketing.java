/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import dal.BlogDAO;
import dal.CatergoryBlogDAO;
import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Set;
import model.Blog;
import model.CatergoryBlog;
import model.User;

/**
 *
 * @author PhanQuangHuy59
 */
@WebServlet(name="PostListMarketing", urlPatterns={"/postlistmarketing"})
public class PostListMarketing extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet PostListMarketing</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet PostListMarketing at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        CatergoryBlogDAO cateDao = new CatergoryBlogDAO();
        BlogDAO blogDao = new BlogDAO();
        UserDAO userDao = new UserDAO();
        ArrayList<User> listMarketing = new ArrayList<>();
        ArrayList<CatergoryBlog> listCateBlog = new ArrayList<>();
        ArrayList<Blog> listBlog = new ArrayList<>();
        ArrayList<User> listOwnBlog = new ArrayList<>();
        ArrayList<CatergoryBlog> listCateOfBlog = new ArrayList<>();
        
        
        String check = request.getParameter("check");
        System.out.println(check);
        if(check ==null || check.equals("10")){
            check = "10";
             listBlog = blogDao.getAllBlogH();
        }else if(check.equals("1")){
            String keysearch = request.getParameter("name");
            listBlog = (ArrayList<Blog>)blogDao.searchByStringInput(keysearch);
            request.setAttribute("key", keysearch);
        }else if(check.equals("2")){
            String idcate = request.getParameter("categoryID");
            listBlog = (ArrayList<Blog>)blogDao.getBlogFolowCategory(Integer.parseInt(idcate));
           request.setAttribute("categoryID", idcate);
            
        }else if(check.equals("3")){
            String iduser = request.getParameter("marketing");
            listBlog = (ArrayList<Blog>) blogDao.getBlogFolowUser(Integer.parseInt(iduser));
            request.setAttribute("marketing", iduser);
        }else if(check.equals("4")){
            String sort = request.getParameter("sort");
            listBlog = (ArrayList<Blog>)blogDao.SortBlog(Integer.parseInt(sort));
            request.setAttribute("sort", sort);
        }
        listMarketing = userDao.getMarketing();
        listCateBlog = (ArrayList<CatergoryBlog>)cateDao.getAllBlogCate();
        if(listBlog.size() != 0){
            
            String tpage = request.getParameter("page");
            int numPs = listBlog.size();
            int numpage = numPs / 10 + (numPs % 10 == 0 ? 0 : 1);
            listBlog = phantrangcourse(listBlog, tpage);
            int page;
            try {
                page = Integer.parseInt(tpage);
            } catch (NumberFormatException e) {
                page = 1;
            }
            request.setAttribute("page", page);
            request.setAttribute("num1", numpage);
            for (Blog blog : listBlog) {
                User u = userDao.getUserByIdUser(blog.getId_User());
                CatergoryBlog c = cateDao.getCategoryOfId(blog.getId_CatergoryBlog());
                listOwnBlog.add(u);
                listCateOfBlog.add(c);
            }
        }
        request.setAttribute("check", check);
       request.setAttribute("listblog", listBlog);
       request.setAttribute("listown", listOwnBlog);
       request.setAttribute("listmarketing", listMarketing);
       request.setAttribute("listcate", listCateBlog);
       request.setAttribute("listcateofblog", listCateOfBlog);
       request.setAttribute("active", check);
       
       request.getRequestDispatcher("postlistmarketing.jsp").forward(request, response);
        
        
        
        
        
    } 

    public ArrayList<Blog> phantrangcourse(ArrayList<Blog> list, String tpage) {
        int numPs = list.size();
        int numperPage = 10; 

        int numpage = numPs / numperPage + (numPs % numperPage == 0 ? 0 : 1);
        int start, end;

        int page;

        try {
            page = Integer.parseInt(tpage);
        } catch (NumberFormatException e) {
            page = 1;
        }
        start = (page - 1) * numperPage;
        if (page * numperPage > numPs) {
            end = numPs;
        } else {
            end = page * numperPage;
        }

        return getListByPage(list, start, end);
    }

    public ArrayList<Blog> getListByPage(ArrayList<Blog> list,
            int start, int end) {
        ArrayList<Blog> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
