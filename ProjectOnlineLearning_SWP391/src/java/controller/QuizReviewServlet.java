/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.LessonDAO;
import dal.QuizDAO;
import dal.QuizResultDAO;
import dal.SubjectDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Lesson;
import model.Quiz;
import model.Result;
import model.User;

/**
 *
 * @author Naviank
 */
@WebServlet(name = "QuizReviewProgressServlet", urlPatterns = {"/reviewquiz"})
public class QuizReviewServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet QuizReviewProgressServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet QuizReviewProgressServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int quiz_id = Integer.parseInt(request.getParameter("quiz_id"));
        String month = (request.getParameter("month"));
        String day = (request.getParameter("day"));
        if (month == null) {
            month = "";
        }
        if (day == null) {
            day = "";
        }

        User u = (User) request.getSession().getAttribute("user");
        QuizDAO qid = new QuizDAO();
        LessonDAO ld = new LessonDAO();
        SubjectDAO sd = new SubjectDAO();
        QuizResultDAO qrd = new QuizResultDAO();

        List<Result> listres = qrd.getAllResultOfQuizOfUserByDate(quiz_id, u.getId_User(), month, day);
        Quiz quiz = qid.selectQuizByID(quiz_id);
        Lesson less = ld.getLessonByIdLesson(quiz.getId_Lession());
        List<Quiz> qarr = qid.selectQuizOfLesson(quiz.getId_Lession());
        String subjname = sd.getSubjectByIdQuestion(quiz_id);
        float[] arr = new float[listres.size()];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = qrd.gradeAResultByIdResult(listres.get(i).getId_Result());
        }

        request.setAttribute("month", month);
        request.setAttribute("day", day);
        request.setAttribute("quiz", quiz);
        request.setAttribute("less", less);
        request.setAttribute("subjname", subjname);
        request.getSession().setAttribute("listres", listres);
        request.setAttribute("listmark", arr);
        request.setAttribute("listquiz", qarr);
        request.getRequestDispatcher("quizreview.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int quiz_id = Integer.parseInt(request.getParameter("quiz_id"));
        String month = (request.getParameter("month"));
        String day = (request.getParameter("day"));
        if (month == null) {
            month = "";
        }
        if (day == null) {
            day = "";
        }

        User u = (User) request.getSession().getAttribute("user");
        QuizDAO qid = new QuizDAO();
        LessonDAO ld = new LessonDAO();
        SubjectDAO sd = new SubjectDAO();
        QuizResultDAO qrd = new QuizResultDAO();

        List<Result> listres = qrd.getAllResultOfQuizOfUserByDate(quiz_id, u.getId_User(), month, day);
        Quiz quiz = qid.selectQuizByID(quiz_id);
        Lesson less = ld.getLessonByIdLesson(quiz.getId_Lession());
        List<Quiz> qarr = qid.selectQuizOfLesson(quiz.getId_Lession());
        String subjname = sd.getSubjectByIdQuestion(quiz_id);
        float[] arr = new float[listres.size()];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = qrd.gradeAResultByIdResult(listres.get(i).getId_Result());
        }

        request.setAttribute("month", month);
        request.setAttribute("day", day);
        request.setAttribute("quiz", quiz);
        request.setAttribute("less", less);
        request.setAttribute("subjname", subjname);
        request.getSession().setAttribute("listres", listres);
        request.setAttribute("listmark", arr);
        request.setAttribute("listquiz", qarr);
        request.getRequestDispatcher("quizreview.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
