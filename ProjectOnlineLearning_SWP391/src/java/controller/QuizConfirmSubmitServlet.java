/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.AnswerDAO;
import dal.LessonDAO;
import dal.QuestionDAO;
import dal.QuizDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Quiz;

/**
 *
 * @author Naviank
 */
@WebServlet(name = "TestServlet", urlPatterns = {"/confirm"})
public class QuizConfirmSubmitServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet TestServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet TestServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Quiz quiz = (Quiz) request.getSession().getAttribute("cur_quiz");
        String cur_answer_string = (String) request.getSession().getAttribute("cur_answer_string");
        
        AnswerDAO ad = new AnswerDAO();
        String message;
        boolean arr[] = ad.classifyAnsweredQuestion(cur_answer_string);
        int count = 0;
        for (boolean b : arr) {
            if(b) count++;
        }
        int numq = quiz.getNumber_Question();
        if (count == 0) {
            message = "You have answered 0 question. By clicking on the "
                    + "[Score Exam] button below, you will "
                    + "complete your current exam and receive your score. "
                    + "You will not be able to change any answers after this point";
        } else if (count == numq) {
            message = "You have answered all " + count + " questions. By clicking on the "
                    + "[Score Exam] button below, you will "
                    + "complete your current exam and receive your score. "
                    + "You will not be able to change any answers after this point";
        } else {
            message = "You have answered only " + count + " questions. By clicking on the "
                    + "[Score Exam] button below, you will "
                    + "complete your current exam and receive your score. "
                    + "You will not be able to change any answers after this point";
        }

        request.setAttribute("message", message);
        request.getRequestDispatcher("quizcf_popup.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
