/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.AnswerDAO;
import dal.LessonDAO;
import dal.QuestionDAO;
import dal.QuizDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.Date;
import model.Quiz;

/**
 *
 * @author Naviank
 */
@WebServlet(name = "SetSessionServlet", urlPatterns = {"/setSession"})
public class QuizSetSessionServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String quiz_id_raw = request.getParameter("quiz_id");

        QuizDAO qid = new QuizDAO();

        int quiz_id = Integer.parseInt(quiz_id_raw);
        Quiz quiz = qid.selectQuizByID(quiz_id);
        //handle quiz-time
        Date start_time_date = (Date) request.getSession().getAttribute("start_time");
        Date limit_time_date = (Date) request.getSession().getAttribute("limit_time");

        if (start_time_date == null) {
            start_time_date = new Date();
        }
        if (limit_time_date == null) {
            limit_time_date = new Date(start_time_date.getTime() + 1000 * quiz.getTime_Limit());
        }
        long start_time = start_time_date.getTime();
        long limit_time = limit_time_date.getTime();

        //declare variables        
        String cur_answer_string = qid.getCorrectStringAnswerOfQuiz(quiz_id).replaceAll("[A-Z]", "*");
        int peek_time = 0;
        if (quiz.getNumber_Question() < 5) {
            peek_time = 1;
        } else {
            peek_time = 2;
        }

        HttpSession session = request.getSession();
        session.setAttribute("cur_answer_string", cur_answer_string);
        session.setAttribute("start_time", start_time);
        session.setAttribute("limit_time", limit_time);
        session.setAttribute("cur_quiz", quiz);
        session.setAttribute("peek_time", peek_time);
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
