/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.LessonDAO;
import dal.SubjectDAO;
import dal.TypeLessonDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Lesson;

/**
 *
 * @author PCT
 */
@WebServlet(name = "updatelessson", urlPatterns = {"/updatelessson"})
public class updatelessson extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            LessonDAO dao = new LessonDAO();
            TypeLessonDAO dao1 = new TypeLessonDAO();
            SubjectDAO dao2 = new SubjectDAO();
            int id = Integer.parseInt(request.getParameter("idlesson"));
            Lesson less = dao.getLessonByIdLesson(id);
            request.setAttribute("lesson", less);
            request.setAttribute("type", dao1.getAll());
            request.setAttribute("subject", dao2.getAllSubject());
            request.setAttribute("xxx", "SubjectManagment");
            request.getRequestDispatcher("updatelesson.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id_lesson");
        String name = request.getParameter("namelessson");
        String content = request.getParameter("content");
        String video = request.getParameter("video");
        boolean status = request.getParameter("status").equals("1"); // Chuyển đổi giá trị của tham số status từ chuỗi sang boolean
        String id_subject = request.getParameter("id_subject");
        String id_type = request.getParameter("id_type");
        String create_Date = request.getParameter("create_Date");
        String update_Date = request.getParameter("update_Date");
        LessonDAO dao = new LessonDAO();
        dao.updateLesson(id, name, content, video, status, id_subject, id_type, create_Date, update_Date);
        String message = "Update Successful";
         request.getSession().setAttribute("message", message);
        response.sendRedirect("lesondetailexpert?idlesson=" + id);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
