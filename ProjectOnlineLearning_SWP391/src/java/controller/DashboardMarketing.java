/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import com.oracle.wls.shaded.org.apache.bcel.generic.AALOAD;
import dal.CatergoryCourseDAO;
import dal.CourseCategoryDAO;
import dal.CourseDAO;
import dal.PackagePriceDAO;
import dal.RegistrationDAO;
import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import model.CatergoryCourse;
import model.Course;
import model.PackagePrice;
import model.Registration;
import model.User;

/**
 *
 * @author PhanQuangHuy59
 */
@WebServlet(name = "DashboardMarketing", urlPatterns = {"/dashboardmarketing"})
public class DashboardMarketing extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String[] listMonth = new String[]{"Month 1", "Month 2", "Month 3", "Month 4", "Month 5", "Month 6", "Month 7", "Month 8", "Month 9",
            "Month 10", "Month 11", "Month 12"};
        String[] listColor = new String[]{"rgb(255, 99, 132)", "rgb(54, 162, 235)", "rgb(255, 206, 86)",
            "rgb(75, 192, 192)", "rgb(153, 102, 255)", "rgb(255, 159, 64)", "rgb(151, 13, 43)", "rgb(11, 151, 18)",
            "rgb(194, 87, 0)", "rgb(57, 20, 221)", "rgb(189, 195, 18)", "rgb(51, 13, 133)"};
        CourseDAO courseDao = new CourseDAO();
        RegistrationDAO regisDao = new RegistrationDAO();
        PackagePriceDAO packDao = new PackagePriceDAO();
        CourseCategoryDAO caDao = new CourseCategoryDAO();
        CourseCategoryDAO cateDao = new CourseCategoryDAO();
        UserDAO userDao = new UserDAO();
        // lay ra danh sach Course Category

        String flag = request.getParameter("flag");
        ArrayList<Course> listcourse = new ArrayList<>();
        ArrayList<Course> listcourse1 = new ArrayList<>();
        ArrayList<PackagePrice> listpack = new ArrayList<>();
        ArrayList<Registration> listregistraion = new ArrayList<>();
        ArrayList<Boolean> liststatusregister = new ArrayList<>();
        ArrayList<User> listuser = new ArrayList<>();
        ArrayList<Long> listrevenuecourse = new ArrayList<>();
        ArrayList<CatergoryCourse> listcater = cateDao.getAllCatergory();
        ArrayList<Integer> listnumberRegistraion = new ArrayList<>();

        String dateFromFilter = request.getParameter("dateFrom1");
        String dateToFilter = request.getParameter("dateTo1");

        long totalRevenue = 0;
        int totalRegistration = 0;
        int totalRegistrationsuccess = 0;
        int totalRegistrationprocess = 0;
        String tpage = request.getParameter("page");

        if (flag != null && (flag.equals("1") || flag.equals("2"))) {
            if (flag.equals("1")) {
                listcourse = courseDao.getNewCourse(1);
            } else if (flag.equals("2")) {
                listcourse = courseDao.getNewCourse(2);
            }

            int numPs = listcourse.size();
            int numpage = numPs / 10 + (numPs % 10 == 0 ? 0 : 1);
            listcourse = phantrangcourse(listcourse, tpage);
            int page;
            try {
                page = Integer.parseInt(tpage);
            } catch (NumberFormatException e) {
                page = 1;
            }
            request.setAttribute("page", page);
            request.setAttribute("num1", numpage);

            if (listcourse.size() > 0 && (flag.equals("1") || flag.equals("2"))) {
                for (Course course : listcourse) {
                    User u = userDao.getUserByIdUser(course.getId_User());
                    long revenue = regisDao.getRevenueByCourse(course.getId_Course());
                    listrevenuecourse.add(revenue);
                    listuser.add(u);
                    listnumberRegistraion.add(regisDao.numberRegistrationOfCouorse(course.getId_Course()));
                }
                totalRevenue = regisDao.getTotalRevenueCourse(Integer.parseInt(flag));
                totalRegistration = regisDao.getTotalRegistraionCourse(Integer.parseInt(flag), -1);
                totalRegistrationsuccess = regisDao.getTotalRegistraionCourse(Integer.parseInt(flag), 1);
                totalRegistrationprocess = regisDao.getTotalRegistraionCourse(Integer.parseInt(flag), 0);

                request.setAttribute("totalregistration", totalRegistration);
                request.setAttribute("totalregistrationsuccess", totalRegistrationsuccess);
                request.setAttribute("totalregistrationprocess", totalRegistrationprocess);
                request.setAttribute("totalRevenue", totalRevenue);

                request.setAttribute("listrevenuecourse", listrevenuecourse);
                request.setAttribute("listcourse", listcourse);
                request.setAttribute("listexpert", listuser);
                request.setAttribute("listnumberregistration", listnumberRegistraion);

            }
        }

        if (dateFromFilter != null && dateToFilter != null) {
            if (flag != null && (!flag.equals("3") && !flag.equals("4") && !flag.equals("5") && !flag.equals("6"))) {
                request.setAttribute("mess1", "You have not selected a filter");
                request.setAttribute("dateFrom1", dateFromFilter);
                request.setAttribute("dateTo1", dateToFilter);
            }
        }

        if (flag != null && (flag.equals("3") || flag.equals("4") || flag.equals("5") || flag.equals("6") || flag.equals("7") || flag.equals("8") || flag.equals("9"))) {

            if (!flag.equals("8") && !flag.equals("9")) {
                if (dateFromFilter != null && dateToFilter != null) {
                    listregistraion = regisDao.getRegistration(Integer.parseInt(flag), Date.valueOf(dateFromFilter), Date.valueOf(dateToFilter));
                    request.setAttribute("dateFrom1", dateFromFilter);
                    request.setAttribute("dateTo1", dateToFilter);
                } else {
                    listregistraion = regisDao.getRegistration(Integer.parseInt(flag), null, null);
                }

            }
            String from = request.getParameter("dateFrom");
            String to = request.getParameter("dateTo");
            if (from != null && to != null) {
                if (flag.equals("8")) {
                    listregistraion = regisDao.getRegistrationFormTo(Date.valueOf(from), Date.valueOf(to));
                    request.setAttribute("from", Date.valueOf(from));
                    request.setAttribute("to", Date.valueOf(to));
                }
            }

            if (flag.equals("9")) {
                String idCa = request.getParameter("idcategory");
                listregistraion = regisDao.getRegistrationByCategory(Integer.parseInt(idCa));
                request.setAttribute("idcate", idCa);
            }
            if (listregistraion != null && listregistraion.size() != 0) {
                int numPs = listregistraion.size();
                int numpage = numPs / 10 + (numPs % 10 == 0 ? 0 : 1);
                listregistraion = phantrang(listregistraion, tpage);
                int page;
                try {
                    page = Integer.parseInt(tpage);
                } catch (NumberFormatException e) {
                    page = 1;
                }
                request.setAttribute("page", page);
                request.setAttribute("num1", numpage);
            }

            if (listregistraion != null && listregistraion.size() != 0) {
                LocalDate now = LocalDate.now();

                for (Registration registration : listregistraion) {
                    User u = userDao.getUserByIdUser(registration.getId_User());
                    Course c = courseDao.getCourseByID(registration.getId_Course());
                    PackagePrice p = packDao.getPackagePriceByIdcuaHuy(registration.getId_packagePrice());
                    listuser.add(u);
                    listcourse1.add(c);
                    listpack.add(p);
                    if (Date.valueOf(now).after(registration.getValid_To())) {
                        liststatusregister.add(Boolean.TRUE);
                    } else {
                        liststatusregister.add(Boolean.FALSE);
                    }
                }
                for (int i = 0; i < listregistraion.size(); i++) {
                    totalRegistration += 1;
                    if (listregistraion.get(i).isStatus() != null) {
                        if (listregistraion.get(i).isStatus()) {
                            totalRegistrationsuccess += 1;
                            totalRevenue += listpack.get(i).getSale_Price();
                        } else {
                            totalRegistrationprocess += 1;
                        }
                    }

                }

            }
            request.setAttribute("totalregistration", totalRegistration);
            request.setAttribute("totalregistrationsuccess", totalRegistrationsuccess);
            request.setAttribute("totalregistrationprocess", totalRegistrationprocess);
            request.setAttribute("totalRevenue", totalRevenue);

            request.setAttribute("listregistration", listregistraion);
            request.setAttribute("listuser", listuser);
            request.setAttribute("listcourse1", listcourse1);
            request.setAttribute("listpackage", listpack);
            request.setAttribute("liststatus", liststatusregister);

        }

        if (flag == null || flag.equals("10")) {
            LocalDate now1 = LocalDate.now();
            LocalDate befor = now1.plusDays(-7);
            flag = "10";
            listregistraion = regisDao.getRegistrationFormTo(Date.valueOf(befor), Date.valueOf(now1));
            if (listregistraion.size() != 0) {
                int numPs = listregistraion.size();
                int numpage = numPs / 10 + (numPs % 10 == 0 ? 0 : 1);
                listregistraion = phantrang(listregistraion, tpage);
                int page;
                try {
                    page = Integer.parseInt(tpage);
                } catch (NumberFormatException e) {
                    page = 1;
                }
                request.setAttribute("page", page);
                request.setAttribute("num1", numpage);
            }

            if (listregistraion.size() != 0) {
                LocalDate now = LocalDate.now();

                for (Registration registration : listregistraion) {
                    User u = userDao.getUserByIdUser(registration.getId_User());
                    Course c = courseDao.getCourseByID(registration.getId_Course());
                    PackagePrice p = packDao.getPackagePriceByIdcuaHuy(registration.getId_packagePrice());
                    listuser.add(u);
                    listcourse1.add(c);
                    listpack.add(p);
                    if (Date.valueOf(now).after(registration.getValid_To())) {
                        liststatusregister.add(Boolean.TRUE);
                    } else {
                        liststatusregister.add(Boolean.FALSE);
                    }
                }
                for (int i = 0; i < listregistraion.size(); i++) {
                    totalRegistration += 1;
                    if (listregistraion.get(i).isStatus()) {
                        totalRegistrationsuccess += 1;
                        totalRevenue += listpack.get(i).getSale_Price();
                    } else {
                        totalRegistrationprocess += 1;
                    }
                }

            }
            request.setAttribute("from", befor);
            request.setAttribute("to", now1);
            request.setAttribute("totalregistration", totalRegistration);
            request.setAttribute("totalregistrationsuccess", totalRegistrationsuccess);
            request.setAttribute("totalregistrationprocess", totalRegistrationprocess);
            request.setAttribute("totalRevenue", totalRevenue);

            request.setAttribute("listregistration", listregistraion);
            request.setAttribute("listuser", listuser);
            request.setAttribute("listcourse1", listcourse1);
            request.setAttribute("listpackage", listpack);
            request.setAttribute("liststatus", liststatusregister);

        }

        request.setAttribute("flag", flag);

        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        ArrayList<CatergoryCourse> listCa = caDao.getAllCatergory();

        int numberCourse = courseDao.countCountActiveandUnActive();
        int numberRegister = regisDao.countRegisStrationAll();
        int numberRegisSucces = regisDao.countRegisStrationSuccess();
        long revenue = regisDao.getRevenueAll();

        // tao mang de lay ra cac gia tri tháng
        long[] revenueMonth = new long[12];
        long[] revenueMonthTron = new long[12];
        long[] revenueCategory = new long[listCa.size()];
        long[] revenueCategoryTron = new long[listCa.size()];
        long sum1 = 0;
        long sum2 = 0;
        // lay ra danh sach
        for (int i = 0; i < revenueMonth.length; i++) {
            revenueMonth[i] = regisDao.getRevenueByMonth(i);
            sum1 += regisDao.getRevenueByMonth(i);
        }
        int count = 0;
        for (CatergoryCourse l : listCa) {
            revenueCategory[count] = regisDao.getRevenueByCategory(l.getId_Catergory_Course());
            sum2 += regisDao.getRevenueByCategory(l.getId_Catergory_Course());
            count++;
        }
        // for month circle chart
        for (int i = 0; i < revenueMonthTron.length; i++) {
            long a = revenueMonth[i];
            double ratio = (double) a / sum1;
            double percent = ratio * 100;
            revenueMonthTron[i] = Math.round(percent);
        }
        //for category circle chart
        for (int i = 0; i < revenueCategory.length; i++) {
            long a = revenueCategory[i];
            double ratio = (double) a / sum2;
            double percent = ratio * 100;

            revenueCategoryTron[i] = Math.round(percent);
        }
        ////
        String labletroncate = "";
        String colorcategory = "";
        String solieu = "";
        String solieucot = "";
        for (int i = 0; i < revenueCategoryTron.length; i++) {

            if (i == revenueCategoryTron.length - 1) {
                labletroncate += "\"" + listCa.get(i).getName_Catergory_Course() + "\"";
                colorcategory += "\"" + listColor[i] + "\"";
                solieucot += "\"" + revenueCategory[i] + "\"";
                solieu += revenueCategoryTron[i] + "";
            } else {
                labletroncate += "\"" + listCa.get(i).getName_Catergory_Course() + "\",";
                colorcategory += "\"" + listColor[i] + "\",";
                solieucot += "\"" + revenueCategory[i] + "\",";
                solieu += revenueCategoryTron[i] + ",";
            }
        }
        ////////////////////////////////////////////////////////////////////////////////////////////

        request.setAttribute("chartmonth", revenueMonth);
        //request.setAttribute("chartcategory", revenueCategory);
        request.setAttribute("chartmonthcircle", revenueMonthTron);
        // request.setAttribute("chartcategorycircle", revenueCategoryTron);
        request.setAttribute("titlemonth", listMonth);
        //request.setAttribute("titlecate", listCa);
        request.setAttribute("listcolor", listColor);
        request.setAttribute("lablecate", labletroncate);
        request.setAttribute("colorcate", colorcategory);
        request.setAttribute("solieu", solieu);
        request.setAttribute("solieucot", solieucot);

        request.setAttribute("listcater", listcater);
        request.setAttribute("numbercourse", numberCourse);
        request.setAttribute("numberregister", numberRegister);
        request.setAttribute("numberregissucces", numberRegisSucces);
        request.setAttribute("revenue", revenue);
        request.getRequestDispatcher("dashboardmarketing.jsp").forward(request, response);
    }

    public ArrayList<Registration> phantrang(ArrayList<Registration> list, String tpage) {
        int numPs = list.size();
        int numperPage = 10;

        int numpage = numPs / numperPage + (numPs % numperPage == 0 ? 0 : 1);
        int start, end;

        int page;

        try {
            page = Integer.parseInt(tpage);
        } catch (NumberFormatException e) {
            page = 1;
        }
        start = (page - 1) * numperPage;
        if (page * numperPage > numPs) {
            end = numPs;
        } else {
            end = page * numperPage;
        }

        return getListByPage(list, start, end);
    }

    public ArrayList<Registration> getListByPage(ArrayList<Registration> list,
            int start, int end) {
        ArrayList<Registration> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }

    public ArrayList<Course> phantrangcourse(ArrayList<Course> list, String tpage) {
        int numPs = list.size();
        int numperPage = 10;

        int numpage = numPs / numperPage + (numPs % numperPage == 0 ? 0 : 1);
        int start, end;

        int page;

        try {
            page = Integer.parseInt(tpage);
        } catch (NumberFormatException e) {
            page = 1;
        }
        start = (page - 1) * numperPage;
        if (page * numperPage > numPs) {
            end = numPs;
        } else {
            end = page * numperPage;
        }

        return getListByPageCourse(list, start, end);
    }

    public ArrayList<Course> getListByPageCourse(ArrayList<Course> list,
            int start, int end) {
        ArrayList<Course> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
