/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.CatergorySubjectDAO;
import dal.CourseDAO;
import dal.SubjectDAO;
import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import model.CatergorySubject;
import model.Course;
import model.Subject;
import model.User;

/**
 *
 * @author PhanQuangHuy59
 */
@WebServlet(name = "AdminSubjectInCourse", urlPatterns = {"/adminsubjectcourse"})
public class AdminSubjectInCourse extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       request.getRequestDispatcher("adminlistsubjectofcourse.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String check = request.getParameter("check");
        String idCourse = request.getParameter("idcourse");
        SubjectDAO subDao = new SubjectDAO();
        CatergorySubjectDAO cateDao = new CatergorySubjectDAO();
        UserDAO userDao = new UserDAO();
        CourseDAO courDao = new CourseDAO();
        
         ArrayList<Subject> listSubject = new ArrayList<>();
         ArrayList<CatergorySubject> listca = new ArrayList<>();
         ArrayList<User> liste  = new ArrayList<>();
         Course c = courDao.getCourseByID(Integer.parseInt(idCourse));
        if (check == null) {
             listSubject = subDao.getAllSubjectByIdCourse(Integer.parseInt(idCourse), 4);
        }else if(check.equals("1")){
            String searchName = request.getParameter("nameSubject");
            listSubject = subDao.getAllSubjectByKeyName(searchName, Integer.parseInt(idCourse));
            request.setAttribute("key", searchName);
        }else if(check.equals("2")){
            String statuString = request.getParameter("status");
            listSubject = subDao.getSubjectByStatus(Integer.parseInt(statuString), Integer.parseInt(idCourse));
            request.setAttribute("status", statuString);
        }
        if(listSubject != null && listSubject.size() != 0){
            for (Subject subject : listSubject) {
                CatergorySubject  ca = cateDao.getCatergorySubjectById(subject.getId_Catergory_Subject());
                User user = userDao.getUserByIdUser(subject.getId_User());
                liste.add(user);
                listca.add(ca);
            }
        }
        request.setAttribute("cour", c);
        request.setAttribute("listsub", listSubject);
        request.setAttribute("idcourse", idCourse);
        request.setAttribute("listexpert", liste);
        request.setAttribute("listcate", listca);
        request.getRequestDispatcher("adminlistsubjectofcourse.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
