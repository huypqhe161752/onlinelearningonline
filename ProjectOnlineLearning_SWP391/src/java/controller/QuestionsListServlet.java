/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.QuestionDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Questions;
import model.User;

/**
 *
 * @author Admin
 */
@WebServlet(name = "QuestionsListServlet", urlPatterns = {"/listquestions"})
public class QuestionsListServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-s pecific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet QuestionsListServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet QuestionsListServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User u = (User) session.getAttribute("user");
        if (u == null) {
            response.sendRedirect("login.jsp");
        } else {
            ArrayList<Questions> list = new ArrayList<>();
            QuestionDAO qDAO = new QuestionDAO();
            if (u.getId_role() == 1) {
                list = qDAO.getAllQuest();
            } else if(u.getId_role()==2) {
                list = qDAO.getAllQuest2(u.getEmail());
            }
            ArrayList<String> nameSubjects = new ArrayList<>();
            for (int i = 0; i < list.size(); i++) {
                if (!nameSubjects.contains(list.get(i).getSubject_Name())) {
                    nameSubjects.add(list.get(i).getSubject_Name());
                }
            }
            request.setAttribute("sList", nameSubjects);

            ArrayList<String> nameLessons = new ArrayList<>();
            for (int i = 0; i < list.size(); i++) {
                if (!nameLessons.contains(list.get(i).getLesson_Name())) {
                    nameLessons.add(list.get(i).getLesson_Name());
                }
            }
            request.setAttribute("lList", nameLessons);

            ArrayList<String> dimensions = new ArrayList<>();
            for (int i = 0; i < list.size(); i++) {
                if (!dimensions.contains(list.get(i).getSubject_Category_Name())) {
                    dimensions.add(list.get(i).getSubject_Category_Name());
                }
            }
            int numPs = list.size();
            int numperPage = 8;
            int numpage = numPs / numperPage + (numPs % numperPage == 0 ? 0 : 1);
            int start, end;
            String tpage = request.getParameter("page");
            int page = 1;
            try {
                page = Integer.parseInt(tpage);
            } catch (NumberFormatException e) {
                page = 1;
            }
            start = (page - 1) * numperPage;
//        if (page * numperPage > numPs) {
//            end = numPs;
//        } else {
//            end = page * numperPage;
//        }
            end = Math.min(page * numperPage, numPs);
            ArrayList<Questions> mainlist = qDAO.getListByPage(list, start, end);
            request.setAttribute("list", mainlist);
            request.setAttribute("numofpage", numpage);
            request.setAttribute("currentpage", page);
            request.setAttribute("dimensions", dimensions);
            //request.setAttribute("list", list);
            request.getRequestDispatcher("expert/questionlist.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User u = (User) session.getAttribute("user");
        if (u == null) {
            response.sendRedirect("login.jsp");
        } else {
            String btn = request.getParameter("btn");
            if (btn.equals("1")) {
                String content = request.getParameter("searchkey");
                request.setAttribute("key", content);
                QuestionDAO qDAO = new QuestionDAO();
                ArrayList<Questions> list = qDAO.getQuestionByContent(content);

                ArrayList<String> nameSubjects = new ArrayList<>();
                for (int i = 0; i < list.size(); i++) {
                    if (!nameSubjects.contains(list.get(i).getSubject_Name())) {
                        nameSubjects.add(list.get(i).getSubject_Name());
                    }
                }
                request.setAttribute("sList", nameSubjects);

                ArrayList<String> nameLessons = new ArrayList<>();
                for (int i = 0; i < list.size(); i++) {
                    if (!nameLessons.contains(list.get(i).getLesson_Name())) {
                        nameLessons.add(list.get(i).getLesson_Name());
                    }
                }
                request.setAttribute("lList", nameLessons);

                ArrayList<String> dimensions = new ArrayList<>();
                for (int i = 0; i < list.size(); i++) {
                    if (!dimensions.contains(list.get(i).getSubject_Category_Name())) {
                        dimensions.add(list.get(i).getSubject_Category_Name());
                    }
                }
                request.setAttribute("dimensions", dimensions);
                request.setAttribute("sList", nameSubjects);
                request.setAttribute("list", list);
                request.getRequestDispatcher("expert/questionlist.jsp").forward(request, response);
            } else {
                String date = request.getParameter("datesearchkey");
                if (date == null) {
                    response.sendRedirect("listquestions");
                } else {
                    java.sql.Date searchdate = java.sql.Date.valueOf(date);
                    request.setAttribute("datekey", searchdate.toString());
                    //response.getWriter().print(searchdate);
                    QuestionDAO qDAO = new QuestionDAO();
                    ArrayList<Questions> qlist = qDAO.getQuestionsByDate(searchdate);
                    request.setAttribute("list", qlist);
                    ArrayList<String> nameSubjects = new ArrayList<>();
                    for (int i = 0; i < qlist.size(); i++) {
                        if (!nameSubjects.contains(qlist.get(i).getSubject_Name())) {
                            nameSubjects.add(qlist.get(i).getSubject_Name());
                        }
                    }
                    request.setAttribute("sList", nameSubjects);

                    ArrayList<String> nameLessons = new ArrayList<>();
                    for (int i = 0; i < qlist.size(); i++) {
                        if (!nameLessons.contains(qlist.get(i).getLesson_Name())) {
                            nameLessons.add(qlist.get(i).getLesson_Name());
                        }
                    }
                    request.setAttribute("lList", nameLessons);

                    ArrayList<String> dimensions = new ArrayList<>();
                    for (int i = 0; i < qlist.size(); i++) {
                        if (!dimensions.contains(qlist.get(i).getSubject_Category_Name())) {
                            dimensions.add(qlist.get(i).getSubject_Category_Name());
                        }
                    }
                    request.setAttribute("dimensions", dimensions);
                    request.setAttribute("sList", nameSubjects);

                    request.getRequestDispatcher("expert/questionlist.jsp").forward(request, response);
                }
            }
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
