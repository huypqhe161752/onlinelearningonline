/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.CourseDAO;
import dal.PackagePriceDAO;
import dal.SubjectDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import model.Course;
import model.PackagePrice;
import model.Subject;

/**
 *
 * @author GangsterCao
 */
@WebServlet(name = "PackagePriceServlet", urlPatterns = {"/packageprice"})
public class PackagePriceServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet PackagePriceServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet PackagePriceServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String subjectid = request.getParameter("subjectid");
        String pageNumber = request.getParameter("pplistid");
        int indexpageNumber = 0;
        if (pageNumber != null) {
            indexpageNumber = Integer.parseInt(pageNumber);
        }
        PackagePriceDAO ppdao = new PackagePriceDAO();
        CourseDAO cdao = new CourseDAO();

        List<PackagePrice> listnormalpp = ppdao.getAllpackagePriceNormal();
        List<PackagePrice> listpp = ppdao.getAllpackagePrice();
        int count = ppdao.getTotalPackagePriceList();//155
        int endPage = count / 5;
        if (count % 5 != 0) {
            endPage++;
        }
        List<PackagePrice> listPicking = ppdao.getPagingPackagePrice(indexpageNumber);
        List<Course> lcourse = cdao.getCourseNameWithPP(indexpageNumber);

        int ksize = endPage;
        int nlocation = indexpageNumber;
        int nextLo = nlocation + 1;
        int prevLo = nlocation - 1;

        
        int countter = lcourse.size();
        List<Course> listAllCourse = cdao.getAllNormalCOurse();

        request.setAttribute("nLocation", nlocation);
        request.setAttribute("PrevLoca", prevLo);
        request.setAttribute("nextLoca", nextLo);
        request.setAttribute("normalSList", listAllCourse);
        request.setAttribute("forForeach", countter);
        request.setAttribute("cPicking", lcourse);
        request.setAttribute("maxID", listnormalpp.size() + 1);
        request.setAttribute("pPicking", listPicking);
        request.setAttribute("endP", endPage);
        request.setAttribute("tag", indexpageNumber);
        request.setAttribute("listinpp", listpp);
        request.setAttribute("xxx", "SubjectManagment");
        request.setAttribute("xxxDetails", "packageprice");
        if(subjectid != null){
            request.setAttribute("suId", Integer.parseInt(subjectid));
        }
        request.getRequestDispatcher("packageprice.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String duration_raw = request.getParameter("pricePackageDuration");
        String listPrice_raw = request.getParameter("pricePackageListPrice");
        String salePrice_raw = request.getParameter("pricePackageSalePrice");
        String status_package_raw = request.getParameter("pricePackageStatus");
        String idCourse_value_raw = request.getParameter("coursewillnotnullagain");
        String add = request.getParameter("addfunctionpp");
        String edit = request.getParameter("editfunctionpp");
        String ivalue = request.getParameter("ivalue");
        String delete = request.getParameter("delete" + ivalue);
        if (add != null) {

            int duration = Integer.parseInt(duration_raw);
            int listPrice = Integer.parseInt(listPrice_raw);
            int salePrice = Integer.parseInt(salePrice_raw);
            Boolean status_package = Boolean.valueOf(status_package_raw);
            int idCourse_value = Integer.parseInt(idCourse_value_raw);
            PackagePriceDAO ppdao = new PackagePriceDAO();
            PackagePrice p = new PackagePrice();
            p.setDuration(duration);
            p.setList_Price(listPrice);
            p.setSale_Price(salePrice);
            p.setStatus(status_package);
            p.setId_Course(idCourse_value);
            ppdao.addNewPackagePrice(p);
            HttpSession session = request.getSession();
            session.setAttribute("val", 2);
            request.getRequestDispatcher("expertsubjectlistservlet").forward(request, response);
        }
        if (edit != null) {
            String editId_raw = request.getParameter("editId" + ivalue);
            String editName_raw = request.getParameter("editName" + ivalue);
            String editNameId_raw = request.getParameter("editNameId" + ivalue);
            String editDuration_raw = request.getParameter("editDuration" + ivalue);
            String editListPrice_raw = request.getParameter("editListPrice" + ivalue);
            String editSalePrice_raw = request.getParameter("editSalePrice" + ivalue);
            String editStatus_raw = request.getParameter("editStatus" + ivalue);
//            System.out.println(ivalue);
//            System.out.println(editId_raw);
//            System.out.println(editName_raw);
//            System.out.println(editNameId_raw);
//            System.out.println(editDuration_raw);
//            System.out.println(editListPrice_raw);
//            System.out.println(editSalePrice_raw);
//            System.out.println(editStatus_raw);
//            System.out.println("-------------------");
            PackagePriceDAO ppdao = new PackagePriceDAO();
            PackagePrice p = ppdao.getPackagePriceByIdToUpdate(Integer.parseInt(editId_raw));
            p.setDuration(Integer.parseInt(editDuration_raw));
            p.setList_Price(Integer.parseInt(editListPrice_raw));
            p.setSale_Price(Integer.parseInt(editSalePrice_raw));
            p.setStatus(Boolean.valueOf(editStatus_raw));
            p.setId_Course(Integer.parseInt(editNameId_raw));
            p.setId_Package(Integer.parseInt(editId_raw));
//            System.out.println(p.getDuration());
//            System.out.println(p.getList_Price());
//            System.out.println(p.getSale_Price());
//            System.out.println(p.isStatus());
//            System.out.println(p.getId_Course());
//            System.out.println(p.getId_Package());
            ppdao.editPackagePriceWithId(p);

            CourseDAO cdao = new CourseDAO();
            Course c = cdao.getCourseByIDToUpdate(Integer.parseInt(editNameId_raw));
            c.setCourse_Name(editName_raw);
            cdao.updateCourseWithIdSubject(c);

            HttpSession session = request.getSession();
            session.setAttribute("val", 3);
            request.getRequestDispatcher("expertsubjectlistservlet").forward(request, response);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
