/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.QuizDAO;
import dal.SubjectDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.LevelQuiz;
import model.Quiz;
import model.Subject;
import model.User;

/**
 *
 * @author Naviank
 */
@WebServlet(name = "QuizzesListServlet", urlPatterns = {"/quizzeslist"})
public class QuizzesListServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet QuizzesListServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet QuizzesListServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        QuizDAO qd = new QuizDAO();
        SubjectDAO sd = new SubjectDAO();

        List<LevelQuiz> levelQuiz = qd.getAllLevelOfQuiz();
        List<Subject> listSubject = sd.getALLNormalSubject();

        String quizlevelid = request.getParameter("quizlevelid");
        if (quizlevelid == null || "0".equals(quizlevelid)) {
            quizlevelid = "";
        }
        String subjectid = request.getParameter("subjectid");
        if (subjectid == null || "0".equals(subjectid)) {
            subjectid = "";
        }
        String message = "";
        User u = (User) request.getSession().getAttribute("user");
        List<Quiz> tmpList = qd.filterQuizzesByLevelAndSubject(quizlevelid, subjectid);
        List<Quiz> originalListQuiz = qd.filterListByIdUser(u.getId_User(), tmpList);
        //Calculate for paging
        int numPs = originalListQuiz.size();
        int numperPage = 5;
        int numpage = numPs / numperPage + (numPs % numperPage == 0 ? 0 : 1);
        int start;
        int end;
        String tpage = request.getParameter("page");
        String paging = request.getParameter("paging");
        if (paging == null) {
            paging = "true";
        }
        int page;
        try {
            page = Integer.parseInt(tpage);
        } catch (NumberFormatException e) {
            page = 1;
        }
        if (page <= 0) {
            page = 1;
        }
        if (page > numpage) {
            page = numpage;
        }
        if (originalListQuiz.isEmpty()) {
            message = "There is no quizzes available!!!";
            request.setAttribute("levelQuiz", levelQuiz);
            request.setAttribute("listSubject", listSubject);
            request.setAttribute("quizlevelid", quizlevelid);
            request.setAttribute("subjectid", subjectid);
            request.setAttribute("paging", paging);
            request.setAttribute("message", message);
            request.getRequestDispatcher("quizzeslist.jsp").forward(request, response);
        }
        start = (page - 1) * numperPage;
        end = Math.min(page * numperPage, numPs);
        List<Quiz> listQuiz = qd.getListByPage(originalListQuiz, start, end);
        if (paging.equals("false")) {
            page = 1;
            numpage = 1;
            listQuiz = originalListQuiz;
        }

        //End paging
        List<String> listlessonname = qd.getAllQuizLessonName(listQuiz);
        List<String> listsubjectname = qd.getAllQuizSubjectName(listQuiz);
        List<String> listlevelname = qd.getAllQuizLevelName(listQuiz);
        List<Float> listptratio = qd.getAllQuizPassTakeRatio(listQuiz);

        request.setAttribute("quizzeslist", listQuiz);
        request.setAttribute("listlname", listlessonname);
        request.setAttribute("listsname", listsubjectname);
        request.setAttribute("listlevelname", listlevelname);
        request.setAttribute("listptratio", listptratio);
        request.setAttribute("levelQuiz", levelQuiz);
        request.setAttribute("listSubject", listSubject);
        request.setAttribute("quizlevelid", quizlevelid);
        request.setAttribute("subjectid", subjectid);
        request.setAttribute("numpage", numpage);
        request.setAttribute("curunpet", page);
        request.setAttribute("paging", paging);
        request.setAttribute("message", message);
        if (u.getId_role() == 1) {
            request.getRequestDispatcher("quizzeslist_admin.jsp").forward(request, response);
        } else {
            request.getRequestDispatcher("quizzeslist.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        QuizDAO qd = new QuizDAO();
        SubjectDAO sd = new SubjectDAO();

        List<LevelQuiz> levelQuiz = qd.getAllLevelOfQuiz();
        List<Subject> listSubject = sd.getALLNormalSubject();

        String quizlevelid = request.getParameter("quizlevelid");
        if (quizlevelid == null || "0".equals(quizlevelid)) {
            quizlevelid = "";
        }
        String subjectid = request.getParameter("subjectid");
        if (subjectid == null || "0".equals(subjectid)) {
            subjectid = "";
        }
        String message = "";
        User u = (User) request.getSession().getAttribute("user");
        List<Quiz> tmpList = qd.filterQuizzesByLevelAndSubject(quizlevelid, subjectid);
        List<Quiz> originalListQuiz = qd.filterListByIdUser(u.getId_User(), tmpList);
        //Calculate for paging
        int numPs = originalListQuiz.size();
        int numperPage = 5;
        int numpage = numPs / numperPage + (numPs % numperPage == 0 ? 0 : 1);
        int start;
        int end;
        String tpage = request.getParameter("page");
        String paging = request.getParameter("paging");
        if (paging == null) {
            paging = "true";
        }
        int page;
        try {
            page = Integer.parseInt(tpage);
        } catch (NumberFormatException e) {
            page = 1;
        }
        if (page <= 0) {
            page = 1;
        }
        if (page > numpage) {
            page = numpage;
        }
        if (originalListQuiz.isEmpty()) {
            message = "There is no quizzes available!!!";
            request.setAttribute("levelQuiz", levelQuiz);
            request.setAttribute("listSubject", listSubject);
            request.setAttribute("quizlevelid", quizlevelid);
            request.setAttribute("subjectid", subjectid);
            request.setAttribute("paging", paging);
            request.setAttribute("message", message);
            request.getRequestDispatcher("quizzeslist.jsp").forward(request, response);
        }
        start = (page - 1) * numperPage;
        end = Math.min(page * numperPage, numPs);
        List<Quiz> listQuiz = qd.getListByPage(originalListQuiz, start, end);
        if (paging.equals("false")) {
            page = 1;
            numpage = 1;
            listQuiz = originalListQuiz;
        }

        //End paging
        List<String> listlessonname = qd.getAllQuizLessonName(listQuiz);
        List<String> listsubjectname = qd.getAllQuizSubjectName(listQuiz);
        List<String> listlevelname = qd.getAllQuizLevelName(listQuiz);
        List<Float> listptratio = qd.getAllQuizPassTakeRatio(listQuiz);

        request.setAttribute("quizzeslist", listQuiz);
        request.setAttribute("listlname", listlessonname);
        request.setAttribute("listsname", listsubjectname);
        request.setAttribute("listlevelname", listlevelname);
        request.setAttribute("listptratio", listptratio);
        request.setAttribute("levelQuiz", levelQuiz);
        request.setAttribute("listSubject", listSubject);
        request.setAttribute("quizlevelid", quizlevelid);
        request.setAttribute("subjectid", subjectid);
        request.setAttribute("numpage", numpage);
        request.setAttribute("curunpet", page);
        request.setAttribute("paging", paging);
        request.setAttribute("message", message);
        if (u.getId_role() == 1) {
            request.getRequestDispatcher("quizzeslist.jsp").forward(request, response);
        } else {
            request.getRequestDispatcher("quizzeslist_admin.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
