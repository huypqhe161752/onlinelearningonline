/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.QuestionDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import model.User;

@MultipartConfig
@WebServlet(name = "ImportQuestionServlet", urlPatterns = {"/addquestions"})
public class ImportQuestionServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ImportQuestionServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ImportQuestionServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        User u = (User) request.getSession().getAttribute("user");
        if (u == null) {
            response.sendRedirect("login.jsp");
        } else {
            Part filePart = request.getPart("file");
            String realPath = request.getServletContext().getRealPath("/");

            File parentDir = new File(realPath).getParentFile();
            File grandParentDir = parentDir.getParentFile();

            String fileName = new File(filePart.getSubmittedFileName()).getName();

            InputStream fileContent = filePart.getInputStream();

            String savePath = grandParentDir + "\\web\\expert\\data/" + fileName;
            File file = new File(savePath);
            if (!file.exists()) {
                filePart.write(savePath);
                while (true) {
                    try {
                        Thread.sleep(1000); // Đợi 0.5 giây (500 milliseconds)
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (file.exists() == true) {
                        break;
                    }
                }
            }
            try {
                BufferedReader br = new BufferedReader(new FileReader(file));
                String line;
                int count = 1;
                while ((line = br.readLine()) != null) {
                    if (count != 1) {
                        String[] fields = line.split("\\|");
                        String[] correctAnsers = fields[4].split("\\;");
                        String[] incorrectAnsers = fields[5].split("\\;");
                        count++;
                        for (int i = 0; i < fields.length; i++) {
                            System.out.println(fields[i]);
                        }
                        System.out.println("========");
                        for (int i = 0; i < correctAnsers.length; i++) {
                            System.out.println(correctAnsers[i]);
                        }
                        System.out.println("========");
                        for (int i = 0; i < incorrectAnsers.length; i++) {
                            System.out.println(incorrectAnsers[i]);
                        }
                        java.util.Date crDate = new java.util.Date();

                        java.sql.Date date = new java.sql.Date(crDate.getTime());
                        System.out.println(date);
                        QuestionDAO qdao = new QuestionDAO();
                        qdao.importQuestion(fields, correctAnsers, incorrectAnsers, u.getEmail(), date);
                    } else {
                        count++;
                    }
                }
                br.close();
                file.delete();
                response.sendRedirect("listquestions");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
