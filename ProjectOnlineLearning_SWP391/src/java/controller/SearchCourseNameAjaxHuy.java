/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.CourseCategoryDAO;
import dal.CourseDAO;
import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import model.CatergoryCourse;
import model.Course;
import model.User;

/**
 *
 * @author PhanQuangHuy59
 */
@WebServlet(name = "SearchCourseNameAjaxHuy", urlPatterns = {"/searchcoursenameajax"})
public class SearchCourseNameAjaxHuy extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        CourseDAO couDao = new CourseDAO();
        UserDAO userDao = new UserDAO();
        CourseCategoryDAO cateDao = new CourseCategoryDAO();

        ArrayList<Course> listcour = new ArrayList<>();
        ArrayList<User> listteacher = new ArrayList<>();
        ArrayList<CatergoryCourse> listcategory = new ArrayList<>();

        String name = request.getParameter("txt");
        listcour = couDao.getCourseByName(name);
        if (listcour.size() > 0) {
            for (Course course : listcour) {
                System.out.println(course.getCourse_Name());
                User u1 = userDao.getUserByIdUser(course.getId_User());
                CatergoryCourse ca = cateDao.getCatergoryById(course.getId_Category());
                listteacher.add(u1);
                listcategory.add(ca);
            }
            for (int i = 0;i < listcour.size();i++) {
                out.println(" <div class=\"card-courses-list admin-courses\">\n"
                        + "                                            <div class=\"card-courses-media\">\n"
                        + "                                                <img src=\""+listcour.get(i).getImage()+"\" alt=\"educhamp\"/>\n"
                        + "                                            </div>\n"
                        + "                                            <div class=\"card-courses-full-dec\">\n"
                        + "                                                <div class=\"card-courses-title\">\n"
                        + "                                                    <h4>"+listcour.get(i).getCourse_Name()+"</h4>\n"
                        + "                                                </div>\n"
                        + "                                                <div class=\"card-courses-list-bx\">\n"
                        + "                                                    <ul class=\"card-courses-view\">\n"
                        + "                                                        <li class=\"card-courses-user\">\n"
                        + "                                                            <div class=\"card-courses-user-pic\">\n"
                        + "                                                                <img src=\""+listteacher.get(i).getAvatar()+"\" alt=\"teacher\"/>\n"
                        + "                                                            </div>\n"
                        + "                                                            <div class=\"card-courses-user-info\">\n"
                        + "                                                                <h5>Teacher</h5>\n"
                        + "                                                                <h4>"+listteacher.get(i).getFull_Name()+"</h4>\n"
                        + "                                                            </div>\n"
                        + "                                                        </li>\n"
                        + "                                                        <li class=\"card-courses-categories\">\n"
                        + "                                                            <h5>Categories</h5>\n"
                        + "                                                            <h4>"+listcategory.get(i).getName_Catergory_Course()+"</h4>\n"
                        + "                                                        </li>\n"
                        + "                                                        <li class=\"card-courses-review\">\n"
                        + "                                                            <h5>Status Course</h5>\n"
                        + "\n"
                        + "                                                            <h4 href=\"#\" class=\"btn button-sm green radius-xl\">"+(listcour.get(i).isStatus()?"Public":"Unpulic")+"</h4>\n"
                        + "                                                        </li>\n"
                        + "\n"
                        + "\n"
                        + "\n"
                        + "                                                    </ul>\n"
                        + "                                                </div>\n"
                        + "                                                <div class=\"row card-courses-dec\">\n"
                        + "                                                    <div class=\"col-md-12\">\n"
                        + "                                                        <h6 class=\"m-b10\">Course Description</h6>\n"
                        + "                                                        <p>"+listcour.get(i).getDescription_Course()+"</p>	\n"
                        + "                                                    </div>\n"
                        + "                                                    <div class=\"col-md-12\">\n"
                        + "                                                        <a href=\"updatecourse?idcourse=${listcourse.get(i).id_Course}\" class=\"btn green radius-xl outline\">Update Course</a>\n"
                        + "                                                        <a href=\"deletecourse1?idcourse=${listcourse.get(i).id_Course}\" class=\"btn red outline radius-xl \">Delete Course</a>\n"
                        + "                                                    </div>\n"
                        + "                                                </div>\n"
                        + "\n"
                        + "                                            </div>\n"
                        + "                                        </div>");
            }
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
