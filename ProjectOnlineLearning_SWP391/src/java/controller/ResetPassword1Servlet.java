package controller;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */


import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.HashSet;

/**
 *
 * @author PhanQuangHuy59
 */
@WebServlet(urlPatterns={"/resetpassword1"})
public class ResetPassword1Servlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ResetPassword1</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ResetPassword1 at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String resetpass = request.getParameter("resetpass");
        String confimpass = request.getParameter("confirmpass");
        UserDAO dao = new UserDAO();
        
        HttpSession sesion = request.getSession();
        String email =  (String)sesion.getAttribute("email");
        if(email == null || email.length() == 0){
             request.getRequestDispatcher("forgetpassword.jsp").forward(request, response);
        }else{
             if (resetpass.equals(confimpass)) {
                 dao.ResetPassWord(email, resetpass);
                 request.setAttribute("mes", "You have successfully reset your password");
                 request.setAttribute("mes1", "You can click the button below to go to the login page");
                 request.setAttribute("action", "Click To Login");
                 request.setAttribute("path", "login.jsp");
                 sesion.removeAttribute("resetcode");
                 request.setAttribute("newpass", resetpass);
                 request.setAttribute("confirmpass", confimpass);
                 request.getRequestDispatcher("notification.jsp").forward(request, response);
             } else {
                 request.setAttribute("err", "Two passwords do not match ");
                 request.getRequestDispatcher("resetpassword.jsp").forward(request, response);
             }
        }
        
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
