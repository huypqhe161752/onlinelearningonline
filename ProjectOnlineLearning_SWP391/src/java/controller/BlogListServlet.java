/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.BlogDAO;
import dal.CatergoryBlogDAO;

import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Blog;
import model.CatergoryBlog;
import model.User;

/**
 *
 * @author Naviank
 */
@WebServlet(name = "BlogListServlet", urlPatterns = {"/bloglist"})
public class BlogListServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet BlogListServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet BlogListServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //Get request paramaters

        String torder = request.getParameter("order");
        boolean order;
        order = (torder == null || "".equals(torder) || "1".equals(torder));
        torder = (order ? "1" : "2");
        String searchvalue = request.getParameter("searchvalue");
        String ses_searchvalue = request.getParameter("searchvalue");
        if (searchvalue == null) {
            searchvalue = "";
        }
        String cate = request.getParameter("cate");
        if ("".equals(cate) || cate == null) {
            cate = "0";
        }

        String tfilter = request.getParameter("filter");
        int filter;
        try {
            filter = Integer.parseInt(tfilter);
        } catch (NumberFormatException e) {
            filter = 0;
        }
        //End of get request parameter

        //Init DAO
        BlogDAO bd = new BlogDAO();
        UserDAO ud = new UserDAO();
        CatergoryBlogDAO cbd = new CatergoryBlogDAO();
        //
        List<Blog> listBlog = bd.filterBlogsList(filter, order, Integer.parseInt(cate), searchvalue);
        List<User> listMarketer = ud.getListUserHasRole(2);
        List<CatergoryBlog> listCate = cbd.get3BlogCate(0);
        //Calculate for paging
        int numPs = listBlog.size();
        int numperPage = 4;
        int numpage = numPs / numperPage + (numPs % numperPage == 0 ? 0 : 1);
        int start, end;
        String tpage = request.getParameter("page");
        int page;
        try {
            page = Integer.parseInt(tpage);
        } catch (NumberFormatException e) {
            page = 1;
        }
        start = (page - 1) * numperPage;
        if (page * numperPage > numPs) {
            end = numPs;
        } else {
            end = page * numperPage;
        }
        end = Math.min(page * numperPage, numPs);
        List<Blog> mainlist = bd.getListByPage(listBlog, start, end);
        //End paging
        String mess = "";
        switch (filter) {
            case 1:
                mess = "Display the" + (order? " newest" : " oldest") + " post first!";
                break;
            case 2:
                String tmp = cbd.getCategoryOfId(Integer.parseInt(cate)).getName_Catergory_Blog();
                mess = "Display the posts of category: " + tmp;
                break;
            case 3:
                mess = "Display the" + (order? " most" : " least") + " views posts first!";
                break;
            case 4:
                mess = "Display search result for input: "  + searchvalue;
                break;
            default:
                mess = "";
                break;
        }
        //=====================
        //Set request attribute
        request.setAttribute("listMarketer", listMarketer); //list marketing user 
        request.setAttribute("listCate", listCate); //list category blog
        request.getSession().setAttribute("searchvalue", ses_searchvalue); //list category blog

        request.setAttribute("listBlog", mainlist); //list blog will be displayed
        request.setAttribute("recentBlog", bd.get8BlogsByUpdateDate(true).subList(0, 4)); //list recent blog for bloglist right sidebar

        request.setAttribute("numofpage", numpage); //so trang duoc phan cho listBlog
        request.setAttribute("currentpage", page); //vi tri trang hien tai
        request.setAttribute("currentcate", Integer.parseInt(cate)); //vi tri trang hien tai
        request.setAttribute("currentfilt", filter); //vi tri trang hien tai
        request.setAttribute("currentorder", torder); //vi tri trang hien tai
        request.setAttribute("corder", order); //vi tri trang hien tai
        request.setAttribute("mess", mess); //vi tri trang hien tai
        request.setAttribute("xxx", "bloglist"); //vi tri trang hien tai

        request.getRequestDispatcher("bloglist.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String searchvalue = request.getParameter("searchvalue");

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
