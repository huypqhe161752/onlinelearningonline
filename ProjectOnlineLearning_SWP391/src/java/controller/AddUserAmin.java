/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.RoleDAO;
import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import model.User;
import model1.UserJoin;

/**
 *
 * @author PCT
 */
@MultipartConfig()
@WebServlet(name = "AddUserAmin", urlPatterns = {"/adduseradmin"})
public class AddUserAmin extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            RoleDAO dao1 = new RoleDAO();
            request.setAttribute("RoleName", dao1.getAll());
            request.setAttribute("xxx", "user");
            request.getRequestDispatcher("adduseradmin.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        UserDAO dao = new UserDAO();
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String full_Name = request.getParameter("full_Name");
        String phone = request.getParameter("phone");
        boolean gender = "1".equals(request.getParameter("gender"));
        String address = request.getParameter("address");
        boolean status = "1".equals(request.getParameter("status"));
        String id_Role = request.getParameter("id_role");
        String createDate = request.getParameter("create_Date");
        String avatar = null;

// Kiểm tra xem các trường bắt buộc đã được điền đầy đủ hay không
        if (email == null || email.trim().isEmpty()
                || password == null || password.trim().isEmpty()
                || full_Name == null || full_Name.trim().isEmpty()
                || phone == null || phone.trim().isEmpty()
                 || address == null || address.trim().isEmpty()
                || id_Role == null || id_Role.trim().isEmpty()
                || createDate == null || createDate.trim().isEmpty()) {

            String message = "Please fill in all required fields.";
            request.getSession().setAttribute("message", message);
            request.getSession().setAttribute("email", email);
            request.getSession().setAttribute("pass", password);
            request.getSession().setAttribute("full_Name", full_Name);
            request.getSession().setAttribute("phone", phone);
            request.getSession().setAttribute("gender", gender);
            request.getSession().setAttribute("address", address);
            request.getSession().setAttribute("status", status);
            request.getSession().setAttribute("id_Role", id_Role);
             request.getSession().setAttribute("avatar", avatar);
            request.getSession().setAttribute("create_Date", createDate);
            response.sendRedirect("adduseradmin");
            return;
        }

// Validate phone number format
        if (!isValidPhoneNumber(phone)) {
            String message = "Phone number format is incorrect. Please enter a phone number that starts with 0 and has 10 digits.";
            request.getSession().setAttribute("message", message);
            request.getSession().setAttribute("email", email);
            request.getSession().setAttribute("pass", password);
            request.getSession().setAttribute("full_Name", full_Name);
            request.getSession().setAttribute("phone", phone);
            request.getSession().setAttribute("gender", gender);
            request.getSession().setAttribute("address", address);
            request.getSession().setAttribute("status", status);
            request.getSession().setAttribute("id_Role", id_Role);
                      request.getSession().setAttribute("avatar", avatar);
            request.getSession().setAttribute("create_Date", createDate);
            response.sendRedirect("adduseradmin");
            return;
        }

// Kiểm tra xem email đã tồn tại trong cơ sở dữ liệu hay chưa
        UserJoin existingEmail = dao.getEmail(email);
        if (existingEmail != null) {
            String message = "Email already exists.";
            request.getSession().setAttribute("message", message);
            request.getSession().setAttribute("email", email);
            request.getSession().setAttribute("pass", password);
            request.getSession().setAttribute("full_Name", full_Name);
            request.getSession().setAttribute("phone", phone);
            request.getSession().setAttribute("gender", gender);
            request.getSession().setAttribute("address", address);
            request.getSession().setAttribute("status", status);
            request.getSession().setAttribute("id_Role", id_Role);
                      request.getSession().setAttribute("avatar", avatar);
            request.getSession().setAttribute("create_Date", createDate);
            response.sendRedirect("adduseradmin");
            return;
        }

// Upload avatar file if it exists
        Part filePart = request.getPart("avatar");
        if (filePart.getSize() > 0) {

            String realPath = request.getServletContext().getRealPath("/");
            File parentDir = new File(realPath).getParentFile();
            File grandParentDir = parentDir.getParentFile();
            String fileName = new File(filePart.getSubmittedFileName()).getName();
            String fileExtension = fileName.substring(fileName.lastIndexOf("."));
            System.out.println(fileExtension);
            String thubnail = System.currentTimeMillis() + fileExtension;
            String fileWeb = grandParentDir + "\\web\\assets\\images\\courses/" + thubnail;
            try {

                FileOutputStream fos = new FileOutputStream(fileWeb);
                InputStream is = filePart.getInputStream();
                byte[] data = new byte[is.available()];
                is.read(data);
                fos.write(data);
                fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            File file = new File(fileWeb);
            if (!file.exists()) {
                filePart.write(fileWeb);
                 avatar = "assets/images/courses/" +thubnail;
                // File doesn't exist, write the file
                try {
                    Thread.sleep(1500); // Đợi 0.5 giây (500 milliseconds)
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                avatar = "assets/images/courses/" +thubnail;
                try {
                    Thread.sleep(1500); // Đợi 0.5 giây (500 milliseconds)
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                // File already exists, do not write
            }

        }

// Insert user into database
        dao.insertUser(email, password, full_Name, phone, gender, avatar, address, status, id_Role, createDate);
        sentMailForExpert(full_Name, email, password);
        String message = "Add User Successful";
        request.setAttribute("message", message);
        response.sendRedirect("useradmin");

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private boolean isValidPhoneNumber(String phone) {
        Pattern pattern = Pattern.compile("^0\\d{9}$");
        Matcher matcher = pattern.matcher(phone);
        return matcher.matches();
    }

    public void sentMailForExpert(String name, String email, String pas) {
        try {

            String from = "CustomerService94321@gmail.com"; // Thay đổi địa chỉ email người nhận
            String password = "lwrtmwkgshlqaycp"; // Thay đổi mật khẩu email của bạn
            Properties properties = new Properties();
            properties.put("mail.smtp.auth", "true");
            properties.put("mail.smtp.host", "smtp.gmail.com");
            properties.put("mail.smtp.port", 587);
            properties.put("mail.smtp.starttls.enable", true);
            properties.put("mail.transport.protocl", "smtp");
            Authenticator auth;

            auth = new Authenticator() {
                @Override
                protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                    return new javax.mail.PasswordAuthentication(from, password);
                }
            };

            Session session = Session.getInstance(properties, auth);
            Message message = new MimeMessage(session);
            message.setSubject("Email From my EduChamp");

            message.setContent("<div style=\"width: 90%; margin: 20px auto;\">\n"
                    + "  <div style=\"border: 2px solid black; border-radius: 2%; overflow: hidden;\">\n"
                    + "    <div style=\"display: flex;\">\n"
                    + "      <div style=\"width: 40%; text-align: center; margin: auto 10px;\">\n"
                    + "        <div style=\"width: 100%; text-align: center;\">\n"
                    + "          <h1 style=\"font-family: Helvetica Neue, Arial, sans-serif; font-size: 64px; font-weight: bold; color: #333; text-align: center; text-transform: uppercase; letter-spacing: 2px; margin: 20px 0; text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.2); background-image: linear-gradient(to right, #8f047c, #8f047c); -webkit-background-clip: text; -webkit-text-fill-color: transparent;\">EduChamp</h1>\n"
                    + "          <h4 style=\"font-family: Helvetica Neue, Arial, sans-serif; font-size: 24px; font-weight: bold; color: #333; text-align: center; text-transform: uppercase; letter-spacing: 2px; margin: 20px 0; text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.2); background-image: linear-gradient(to right, #8f047c, #8f047c); -webkit-background-clip: text; -webkit-text-fill-color: transparent;\">Education &amp; Courses</h4>\n"
                    + "        </div>\n"
                    + "      </div>\n"
                    + "      <div style=\"width: 60%; padding: 10px; background-color: rgb(148, 59, 231); overflow: hidden;\">\n"
                    + "        <div style=\"padding: 20px 10px; border: 2px solid black; border-radius: 5px; margin-bottom: 10px;\">\n"
                    + "          <p style=\"font-style: italic; font-size: 30px; color: white;\">Hello " + name + ",</p>\n"
                    + "          <p style=\"font-size: 24px; color: white;\">Thank you for registering with EduChamp! Your account has been created successfully.</p>\n"
                    + "          <p style=\"font-size: 24px; color: white;\">Please find below your login credentials:</p>\n"
                    + "          <ul style=\"font-size: 24px; color: white; margin: 20px 0; padding-left: 50px;\">\n"
                    + "            <li style=\"margin-bottom: 10px;\">User Name: " + email + "</li>\n"
                    + "            <li>Password: " + pas + "</li>\n"
                    + "          </ul>\n"
                    + "        </div>\n"
                    + "        <div style=\"padding: 20px 10px; border: 2px solid black; border-radius: 5px; margin-bottom: 10px;\">\n"
                    + "          <p style=\"font-style: italic; font-size: 30px; color: white;\">You can click below to login:</p>\n"
                    + "          <a href=\"http://localhost:9999/projectswp391/login.jsp\" style=\"background-color: rgb(43, 200, 43); color: white; padding: 20px; display: inline-block; margin-top: 20px; border: 1px solid black; font-size: 30px; font-weight: bold; text-decoration: none;\">Login</a>\n"
                    + "        </div>\n"
                    + "        <p style=\"font-size: 24px; color: white;\">Thank you for choosing EduChamp for your education and training needs.</p>\n"
                    + "        <p style=\"font-size: 24px; color: white;\">Best regards,</p>\n"
                    + "        <p style=\"font-size: 24px; color: white;\">The EduChamp Team</p>\n"
                    + "      </div>\n"
                    + "    </div>\n"
                    + "  </div>\n"
                    + "</div>", "text/html");

            Address addressTo = new InternetAddress(email);
            message.setRecipient(Message.RecipientType.TO, addressTo);

            Transport.send(message);

        } catch (MessagingException ex) {
            Logger.getLogger(AddCourseServlet.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

}