/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.CatergorySubjectDAO;
import dal.CourseDAO;
import dal.PackagePriceDAO;
import dal.SubjectDAO;
import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import model.CatergorySubject;
import model.Course;
import model.PackagePrice;
import model.Subject;
import model.User;

/**
 *
 * @author PhanQuangHuy59
 */
@WebServlet(name = "CourseDetail", urlPatterns = {"/coursedetail"})
public class CourseDetailServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CourseDetail</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CourseDetail at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        SubjectDAO dao = new SubjectDAO();
        CourseDAO dao1 = new CourseDAO();
        UserDAO userDao = new UserDAO();

        PackagePriceDAO dao3 = new PackagePriceDAO();
        CatergorySubjectDAO dao2 = new CatergorySubjectDAO();

        String id_course = request.getParameter("idcourse");

        String checksort = request.getParameter("check");

         // lay ra danh sách dã dduowoc sap xep 
        ArrayList<Subject> list = dao.getSubjectByIdCourse(Integer.parseInt(id_course),0);
 
        //lay 

        Course cour = dao1.getCourseByID(Integer.parseInt(id_course));
        // lay danh sach price of course
        ArrayList<PackagePrice> listPrice = dao3.getPackagePriceOfCourse(Integer.parseInt(id_course));

        //lay danh sachh catergory subject

        ArrayList<Integer> listidcategorySubject = dao.getCategorySubjectInCourse(Integer.parseInt(id_course));
        ArrayList<CatergorySubject> arr1CatergorySubject = new ArrayList<>();
        for (Integer integer : listidcategorySubject) {
            arr1CatergorySubject.add(dao2.getCatergorySubject(integer));
        }
        // phan trang 

        int numPs = list.size();
        int numperPage = 6;

        int numpage = numPs / numperPage + (numPs % numperPage == 0 ? 0 : 1);
        int start, end;

        String tpage = request.getParameter("page");

        int page;

        try {
            page = Integer.parseInt(tpage);
        } catch (NumberFormatException e) {
            page = 1;
        }
        start = (page - 1) * numperPage;
        if (page * numperPage > numPs) {
            end = numPs;
        } else {
            end = page * numperPage;
        }

        List<Subject> arrsubject = dao.getListByPage(list, start, end);
        List<User> userList = new ArrayList<>();
        for (Subject subject : arrsubject) {
            User u = userDao.getUserByIdUser(subject.getId_User());
            userList.add(u);
        }
        ///
        for (PackagePrice p : listPrice) {
            System.out.println(p.getList_Price());
        }
        SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
        
        request.setAttribute("idcourse", id_course);
        request.setAttribute("check", "0");
        request.setAttribute("num1", numpage);

        request.setAttribute("page", page);

        request.setAttribute("list", arrsubject);
        request.setAttribute("author", userList);
        request.setAttribute("list2", arr1CatergorySubject);

        request.setAttribute("listprice", listPrice);
        //request.setAttribute("xxx", "courseservlet");
        request.setAttribute("cour", cour);
        request.setAttribute("date", f.format(cour.getCreate_Date()));
        request.setAttribute("xxx", "courselist");
        
        request.getRequestDispatcher("course_detail.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
