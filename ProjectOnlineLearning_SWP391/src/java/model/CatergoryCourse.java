/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dal.CourseDAO;
import java.io.Serializable;

/**
 *
 * @author PhanQuangHuy59
 */
public class CatergoryCourse implements Serializable {

    private int id_Catergory_Course;
    private String name_Catergory_Course;

    public CatergoryCourse() {
    }

    public CatergoryCourse(int id_Catergory_Course, String name_Catergory_Course) {
        this.id_Catergory_Course = id_Catergory_Course;
        this.name_Catergory_Course = name_Catergory_Course;
    }

    public int getId_Catergory_Course() {
        return id_Catergory_Course;
    }

    public void setId_Catergory_Course(int id_Catergory_Course) {
        this.id_Catergory_Course = id_Catergory_Course;
        CourseDAO cd = new CourseDAO();
        this.name_Catergory_Course = cd.getCourseCateByID(id_Catergory_Course).getName_Catergory_Course();
    }

    public String getName_Catergory_Course() {
        return name_Catergory_Course;
    }

    public void setName_Catergory_Course(String name_Catergory_Course) {
        this.name_Catergory_Course = name_Catergory_Course;
    }

    @Override
    public String toString() {
        return "CatergoryCourse{" + "id_Catergory_Course=" + id_Catergory_Course + ", name_Catergory_Course=" + name_Catergory_Course + '}';
    }

    public static void main(String[] args) {
        CatergoryCourse cc = new CatergoryCourse();
        cc.setId_Catergory_Course(1);
        System.out.println(cc);
    }
}
