/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Admin
 */
public class Questions {
    private int id_Question;
    private String content_Question;
    private String subject_Name;
    private String lesson_Name;
    private String level_Name;
    private boolean status;
    private String subject_Category_Name;

    public Questions() {
    }

    public Questions(int id_Question, String content_Question, String subject_Name, String lesson_Name, String level_Name, boolean status, String subject_Category_Name) {
        this.id_Question = id_Question;
        this.content_Question = content_Question;
        this.subject_Name = subject_Name;
        this.lesson_Name = lesson_Name;
        this.level_Name = level_Name;
        this.status = status;
        this.subject_Category_Name = subject_Category_Name;
    }

    public int getId_Question() {
        return id_Question;
    }

    public void setId_Question(int id_Question) {
        this.id_Question = id_Question;
    }

    public String getContent_Question() {
        return content_Question;
    }

    public void setContent_Question(String content_Question) {
        this.content_Question = content_Question;
    }

    public String getSubject_Name() {
        return subject_Name;
    }

    public void setSubject_Name(String subject_Name) {
        this.subject_Name = subject_Name;
    }

    public String getLesson_Name() {
        return lesson_Name;
    }

    public void setLesson_Name(String lesson_Name) {
        this.lesson_Name = lesson_Name;
    }

    public String getLevel_Name() {
        return level_Name;
    }

    public void setLevel_Name(String level_Name) {
        this.level_Name = level_Name;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getSubject_Category_Name() {
        return subject_Category_Name;
    }

    public void setSubject_Category_Name(String subject_Category_Name) {
        this.subject_Category_Name = subject_Category_Name;
    }
    
}
