/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author PhanQuangHuy59
 */
public class LevelQuiz {
    private int id_Level;
    private String name_Level;

    public LevelQuiz() {
    }

    public LevelQuiz(int id_Level, String name_Level) {
        this.id_Level = id_Level;
        this.name_Level = name_Level;
    } 

    public int getId_Level() {
        return id_Level;
    }

    public void setId_Level(int id_Level) {
        this.id_Level = id_Level;
    }

    public String getName_Level() {
        return name_Level;
    }

    public void setName_Level(String name_Level) {
        this.name_Level = name_Level;
    }
    
}
