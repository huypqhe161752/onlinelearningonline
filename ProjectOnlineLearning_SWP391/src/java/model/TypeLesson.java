/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author PhanQuangHuy59
 */
public class TypeLesson {
    private int id_Type;
    private String name_TypeLesson;

    public TypeLesson() {
    }

    public TypeLesson(int id_Type, String name_TypeLesson) {
        this.id_Type = id_Type;
        this.name_TypeLesson = name_TypeLesson;
    }

    public int getId_Type() {
        return id_Type;
    }

    public void setId_Type(int id_Type) {
        this.id_Type = id_Type;
    }

    public String getName_TypeLesson() {
        return name_TypeLesson;
    }

    public void setName_TypeLesson(String name_TypeLesson) {
        this.name_TypeLesson = name_TypeLesson;
    }
    
}
