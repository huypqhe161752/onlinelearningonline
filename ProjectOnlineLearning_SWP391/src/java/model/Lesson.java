/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;

/**
 *
 * @author PhanQuangHuy59
 */
public class Lesson {
    private int id_Lesson;
    private String name_Lesson;
    private String content_Lesson;
    private String video_Link;
    private boolean status;
    private int id_Subject;
    private int id_Type;
    private Date create_Date;
    private Date update_Date;

    public Lesson() {
    }

    public Lesson(int id_Lesson, String name_Lesson, String content_Lesson, String video_Link, boolean status, int id_Subject, int id_Type, Date create_Date, Date update_Date) {
        this.id_Lesson = id_Lesson;
        this.name_Lesson = name_Lesson;
        this.content_Lesson = content_Lesson;
        this.video_Link = video_Link;
        this.status = status;
        this.id_Subject = id_Subject;
        this.id_Type = id_Type;
        this.create_Date = create_Date;
        this.update_Date = update_Date;
    }

    public Date getCreate_Date() {
        return create_Date;
    }

    public void setCreate_Date(Date create_Date) {
        this.create_Date = create_Date;
    }

    public Date getUpdate_Date() {
        return update_Date;
    }

    public void setUpdate_Date(Date update_Date) {
        this.update_Date = update_Date;
    }

    

    public int getId_Lesson() {
        return id_Lesson;
    }

    public void setId_Lesson(int id_Lesson) {
        this.id_Lesson = id_Lesson;
    }

    public String getName_Lesson() {
        return name_Lesson;
    }

    public void setName_Lesson(String name_Lesson) {
        this.name_Lesson = name_Lesson;
    }

    public String getContent_Lesson() {
        return content_Lesson;
    }

    public void setContent_Lesson(String content_Lesson) {
        this.content_Lesson = content_Lesson;
    }

    public String getVideo_Link() {
        return video_Link;
    }

    public void setVideo_Link(String video_Link) {
        this.video_Link = video_Link;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getId_Subject() {
        return id_Subject;
    }

    public void setId_Subject(int id_Subject) {
        this.id_Subject = id_Subject;
    }

    public int getId_Type() {
        return id_Type;
    }

    public void setId_Type(int id_Type) {
        this.id_Type = id_Type;
    }

    
    
}
