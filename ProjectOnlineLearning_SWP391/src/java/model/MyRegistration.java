/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;

/**
 *
 * @author Admin
 */
public class MyRegistration {
    private int id_Register;
    private int id_User;
    private int id_Course;
    private Date registration_Date;
    private int id_packagePrice;
    private boolean status;
    private Date valid_From;
    private Date valid_To;
    private String name_Sourse;
    private String name_Category;
    private int total_cost;
    private String image;

    public MyRegistration() {
    }

    public MyRegistration(int id_Register, int id_User, int id_Course, Date registration_Date, int id_packagePrice, boolean status, Date valid_From, Date valid_To, String name_Course, String name_Category, int total_cost, String image) {
        this.id_Register = id_Register;
        this.id_User = id_User;
        this.id_Course = id_Course;
        this.registration_Date = registration_Date;
        this.id_packagePrice = id_packagePrice;
        this.status = status;
        this.valid_From = valid_From;
        this.valid_To = valid_To;
        this.name_Sourse = name_Course;
        this.name_Category = name_Category;
        this.total_cost = total_cost;
    }

    public int getId_Register() {
        return id_Register;
    }

    public void setId_Register(int id_Register) {
        this.id_Register = id_Register;
    }

    public int getId_User() {
        return id_User;
    }

    public void setId_User(int id_User) {
        this.id_User = id_User;
    }

    public int getId_Course() {
        return id_Course;
    }

    public void setId_Course(int id_Course) {
        this.id_Course = id_Course;
    }

    public Date getRegistration_Date() {
        return registration_Date;
    }

    public void setRegistration_Date(Date registration_Date) {
        this.registration_Date = registration_Date;
    }

    public int getId_packagePrice() {
        return id_packagePrice;
    }

    public void setId_packagePrice(int id_packagePrice) {
        this.id_packagePrice = id_packagePrice;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Date getValid_From() {
        return valid_From;
    }

    public void setValid_From(Date valid_From) {
        this.valid_From = valid_From;
    }

    public Date getValid_To() {
        return valid_To;
    }

    public void setValid_To(Date valid_To) {
        this.valid_To = valid_To;
    }

    public String getName_Course() {
        return name_Sourse;
    }

    public void setName_Course(String name_Subject) {
        this.name_Sourse = name_Subject;
    }

    public String getName_Category() {
        return name_Category;
    }

    public void setName_Category(String name_Category) {
        this.name_Category = name_Category;
    }

    public int getTotal_cost() {
        return total_cost;
    }

    public void setTotal_cost(int total_cost) {
        this.total_cost = total_cost;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
    

}
