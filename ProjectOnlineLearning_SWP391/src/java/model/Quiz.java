/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;

/**
 *
 * @author PhanQuangHuy59
 */
public class Quiz {
    private int id_Quiz;
    private String name_Quiz;
    private int number_Question;
    private int time_Limit;
    private int id_Level;
    private String description_Quiz;
    private int id_Lession;
    private Date createDate;
    private Date update_Date;
    private int id_User;
    private float passpercent;

    public float getPasspercent() {
        return passpercent;
    }

    public void setPasspercent(float passpercent) {
        this.passpercent = passpercent;
    }

    public Quiz(int id_Quiz, String name_Quiz, int number_Question, int time_Limit, int id_Level, String description_Quiz, int id_Lession, Date createDate, Date update_Date, int id_User, float passpercent) {
        this.id_Quiz = id_Quiz;
        this.name_Quiz = name_Quiz;
        this.number_Question = number_Question;
        this.time_Limit = time_Limit;
        this.id_Level = id_Level;
        this.description_Quiz = description_Quiz;
        this.id_Lession = id_Lession;
        this.createDate = createDate;
        this.update_Date = update_Date;
        this.id_User = id_User;
        this.passpercent = passpercent;
    }

    public Quiz() {
    }

    public Quiz(int id_Quiz, String name_Quiz, int number_Question, int time_Limit, int id_Level, String description_Quiz, int id_Lession) {
        this.id_Quiz = id_Quiz;
        this.name_Quiz = name_Quiz;
        this.number_Question = number_Question;
        this.time_Limit = time_Limit;
        this.id_Level = id_Level;
        this.description_Quiz = description_Quiz;
        this.id_Lession = id_Lession;
    }

    public Quiz(int id_Quiz, String name_Quiz, int number_Question, int time_Limit, int id_Level, String description_Quiz, int id_Lession, Date createDate, Date update_Date, int id_User) {
        this.id_Quiz = id_Quiz;
        this.name_Quiz = name_Quiz;
        this.number_Question = number_Question;
        this.time_Limit = time_Limit;
        this.id_Level = id_Level;
        this.description_Quiz = description_Quiz;
        this.id_Lession = id_Lession;
        this.createDate = createDate;
        this.update_Date = update_Date;
        this.id_User = id_User;
    }

    public Quiz(int id_Quiz, String name_Quiz, int number_Question, int time_Limit, int id_Level, String description_Quiz, int id_Lession, Date createDate, Date update_Date, int id_User, int passpercent) {
        this.id_Quiz = id_Quiz;
        this.name_Quiz = name_Quiz;
        this.number_Question = number_Question;
        this.time_Limit = time_Limit;
        this.id_Level = id_Level;
        this.description_Quiz = description_Quiz;
        this.id_Lession = id_Lession;
        this.createDate = createDate;
        this.update_Date = update_Date;
        this.id_User = id_User;
        this.passpercent = passpercent;
    }


        
    public int getId_Quiz() {
        return id_Quiz;
    }

    public void setId_Quiz(int id_Quiz) {
        this.id_Quiz = id_Quiz;
    }

    public String getName_Quiz() {
        return name_Quiz;
    }

    public void setName_Quiz(String name_Quiz) {
        this.name_Quiz = name_Quiz;
    }

    public int getNumber_Question() {
        return number_Question;
    }

    public void setNumber_Question(int number_Question) {
        this.number_Question = number_Question;
    }

    public int getTime_Limit() {
        return time_Limit;
    }

    public void setTime_Limit(int time_Limit) {
        this.time_Limit = time_Limit;
    }

    public int getId_Level() {
        return id_Level;
    }

    public void setId_Level(int id_Level) {
        this.id_Level = id_Level;
    }

    public String getDescription_Quiz() {
        return description_Quiz;
    }

    public void setDescription_Quiz(String description_Quiz) {
        this.description_Quiz = description_Quiz;
    }

    public int getId_Lession() {
        return id_Lession;
    }

    public void setId_Lession(int id_Lession) {
        this.id_Lession = id_Lession;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public int getId_User() {
        return id_User;
    }

    public void setId_User(int id_User) {
        this.id_User = id_User;
    }

    public Date getUpdate_Date() {
        return update_Date;
    }

    public void setUpdate_Date(Date update_Date) {
        this.update_Date = update_Date;
    }

    public void setPasspercent(int passpercent) {
        this.passpercent = passpercent;
    }
    
    
}
