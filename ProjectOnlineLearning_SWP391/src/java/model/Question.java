/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;

/**
 *
 * @author PhanQuangHuy59
 */
public class Question implements Comparable{
    private int id_Question;
    private String content_Question;
    private int id_Quiz;
    private String description;
    private int id_Type;
    private Date createDate;
    private Date update_Date;

    public Question() {
    }

    public Question(int id_Question, String content_Question, int id_Quiz, String description) {
        this.id_Question = id_Question;
        this.content_Question = content_Question;
        this.id_Quiz = id_Quiz;
        this.description = description;
    }

    public Question(int id_Question, String content_Question, int id_Quiz, String description, int id_Type, Date createDate, Date update_Date) {
        this.id_Question = id_Question;
        this.content_Question = content_Question;
        this.id_Quiz = id_Quiz;
        this.description = description;
        this.id_Type = id_Type;
        this.createDate = createDate;
        this.update_Date = update_Date;
    }

    

    public int getId_Question() {
        return id_Question;
    }

    public void setId_Question(int id_Question) {
        this.id_Question = id_Question;
    }

    public String getContent_Question() {
        return content_Question;
    }

    public void setContent_Question(String content_Question) {
        this.content_Question = content_Question;
    }

    public int getId_Quiz() {
        return id_Quiz;
    }

    public void setId_Quiz(int id_Quiz) {
        this.id_Quiz = id_Quiz;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId_Type() {
        return id_Type;
    }

    public void setId_Type(int id_Type) {
        this.id_Type = id_Type;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdate_Date() {
        return update_Date;
    }

    public void setUpdate_Date(Date update_Date) {
        this.update_Date = update_Date;
    }


    @Override
    public int compareTo(Object o) {
        return this.id_Question - ((Question) o).id_Question;
    }

    
    
}
