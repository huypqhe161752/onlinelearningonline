/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author PhanQuangHuy59
 */
public class Answer {
    private int id_Answer;
    private String answer_Content;
    private String is_Corret;
    private int id_Question;

    public Answer() {
    }

    public Answer(int id_Answer, String answer_Content, String is_Corret, int id_Question) {
        this.id_Answer = id_Answer;
        this.answer_Content = answer_Content;
        this.is_Corret = is_Corret;
        this.id_Question = id_Question;
    }

    public int getId_Answer() {
        return id_Answer;
    }

    public void setId_Answer(int id_Answer) {
        this.id_Answer = id_Answer;
    }

    public String getAnswer_Content() {
        return answer_Content;
    }

    public void setAnswer_Content(String answer_Content) {
        this.answer_Content = answer_Content;
    }

    public String getIs_Corret() {
        return is_Corret;
    }

    public void setIs_Corret(String is_Corret) {
        this.is_Corret = is_Corret;
    }

    public int getId_Question() {
        return id_Question;
    }

    public void setId_Question(int id_Question) {
        this.id_Question = id_Question;
    }
    
}
