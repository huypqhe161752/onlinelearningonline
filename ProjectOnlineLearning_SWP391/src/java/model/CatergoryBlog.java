/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author PhanQuangHuy59
 */
public class CatergoryBlog {
    private int id_Catergory_Blog;
    private String name_Catergory_Blog;

    public CatergoryBlog() {
    }

    public CatergoryBlog(int id_Catergory_Blog, String name_Catergory_Blog) {
        this.id_Catergory_Blog = id_Catergory_Blog;
        this.name_Catergory_Blog = name_Catergory_Blog;
    }

    public int getId_Catergory_Blog() {
        return id_Catergory_Blog;
    }

    public void setId_Catergory_Blog(int id_Catergory_Blog) {
        this.id_Catergory_Blog = id_Catergory_Blog;
    }

    public String getName_Catergory_Blog() {
        return name_Catergory_Blog;
    }

    public void setName_Catergory_Blog(String name_Catergory_Blog) {
        this.name_Catergory_Blog = name_Catergory_Blog;
    }
    
}
