/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;

/**
 *
 * @author PhanQuangHuy59
 */
public class CheckBoxLearn {
    private int id_Lesson;
    private int id_User;
    private Date learn_Date;

    public CheckBoxLearn(int id_Lesson, int id_User, Date learn_Date) {
        this.id_Lesson = id_Lesson;
        this.id_User = id_User;
        this.learn_Date = learn_Date;
    }

    public CheckBoxLearn() {
    }

    public int getId_Lesson() {
        return id_Lesson;
    }

    public void setId_Lesson(int id_Lesson) {
        this.id_Lesson = id_Lesson;
    }

    public int getId_User() {
        return id_User;
    }

    public void setId_User(int id_User) {
        this.id_User = id_User;
    }

    public Date getLearn_Date() {
        return learn_Date;
    }

    public void setLearn_Date(Date learn_Date) {
        this.learn_Date = learn_Date;
    }

   
    
}
