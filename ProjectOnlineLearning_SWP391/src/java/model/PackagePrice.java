/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author PhanQuangHuy59
 */
public class PackagePrice {

    private int id_Package;
    private int duration;
    private int list_Price;
    private int sale_Price;
    private Boolean status;
    private int id_Course;

    public PackagePrice() {
    }

    public PackagePrice(int id_Course) {
        this.id_Course = id_Course;
    }

    public PackagePrice(int id_Package, int duration, int list_Price, int sale_Price, boolean status, int id_Course) {
        this.id_Package = id_Package;
        this.duration = duration;
        this.list_Price = list_Price;
        this.sale_Price = sale_Price;
        this.status = status;
        this.id_Course = id_Course;
    }

    public int getId_Package() {
        return id_Package;
    }

    public void setId_Package(int id_Package) {
        this.id_Package = id_Package;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getList_Price() {
        return list_Price;
    }

    public void setList_Price(int list_Price) {
        this.list_Price = list_Price;
    }

    public int getSale_Price() {
        return sale_Price;
    }

    public void setSale_Price(int sale_Price) {
        this.sale_Price = sale_Price;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getId_Course() {
        return id_Course;
    }

    public void setId_Course(int id_Course) {
        this.id_Course = id_Course;
//        PackagePriceDAO ppd = new PackagePriceDAO();
//        PackagePrice pp = ppd.getLowestPacketPriceOfCourse(id_Course);
//        this.id_Package = pp.id_Package;
//        this.duration = pp.getDuration();
//        this.list_Price = pp.getList_Price();
//        this.sale_Price = pp.getSale_Price();
//        this.status = true;
    }

    public static void main(String[] args) {
        CatergoryCourse cc = new CatergoryCourse();
        cc.setId_Catergory_Course(5);
        System.out.println(cc);
    }

    @Override
    public String toString() {
        return "PackagePrice{" + "id_Package=" + id_Package + ", duration=" + duration + ", list_Price=" + list_Price + ", sale_Price=" + sale_Price + ", status=" + status + ", id_Course=" + id_Course + '}';
    }

}
