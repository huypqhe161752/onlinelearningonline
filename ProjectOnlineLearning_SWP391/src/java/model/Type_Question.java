/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author PhanQuangHuy59
 */
public class Type_Question {

    private int id_Type;
    private String name_TypeQuestion;
    private String description;

    public Type_Question() {
    }

    public Type_Question(int id_Type, String name_TypeQuestion, String description) {
        this.id_Type = id_Type;
        this.name_TypeQuestion = name_TypeQuestion;
        this.description = description;
    }

    public int getId_Type() {
        return id_Type;
    }

    public void setId_Type(int id_Type) {
        this.id_Type = id_Type;
    }

    public String getName_TypeQuestion() {
        return name_TypeQuestion;
    }

    public void setName_TypeQuestion(String name_TypeQuestion) {
        this.name_TypeQuestion = name_TypeQuestion;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}
