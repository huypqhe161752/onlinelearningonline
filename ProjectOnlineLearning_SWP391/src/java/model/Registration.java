/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;
import java.text.SimpleDateFormat;

/**
 *
 * @author PhanQuangHuy59
 */
public class Registration {
    private int id_Register;
    private int id_User;
    private int id_Course;
    private Date registration_Date;
    private int id_packagePrice;
    private Boolean status;
    private Date valid_From;
    private Date valid_To;
    
    public Registration() {
    }

    public Registration(int id_Register, int id_User, int id_Course, Date registration_Date, int id_packagePrice, boolean status, Date valid_From, Date valid_To) {
        this.id_Register = id_Register;
        this.id_User = id_User;
        this.id_Course = id_Course;
        this.registration_Date = registration_Date;
        this.id_packagePrice = id_packagePrice;
        this.status = status;
        this.valid_From = valid_From;
        this.valid_To = valid_To;
    }

    public int getId_Register() {
        return id_Register;
    }

    public void setId_Register(int id_Register) {
        this.id_Register = id_Register;
    }

    public int getId_User() {
        return id_User;
    }

    public void setId_User(int id_User) {
        this.id_User = id_User;
    }

    public int getId_Course() {
        return id_Course;
    }

    public void setId_Course(int id_Course) {
        this.id_Course = id_Course;
    }

    public Date getRegistration_Date() {
        return registration_Date;
    }

    public void setRegistration_Date(Date registration_Date) {
        this.registration_Date = registration_Date;
    }

    public int getId_packagePrice() {
        return id_packagePrice;
    }

    public void setId_packagePrice(int id_packagePrice) {
        this.id_packagePrice = id_packagePrice;
    }

    public Boolean isStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Date getValid_From() {
        return valid_From;
    }

    public void setValid_From(Date valid_From) {
        this.valid_From = valid_From;
    }

    public Date getValid_To() {
        return valid_To;
    }

    public void setValid_To(Date valid_To) {
        this.valid_To = valid_To;
    }
     public String formatDateRegistration(){
        SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
        return f.format(registration_Date);
    }
     public String formatDateValidFrom(){
        SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
        return f.format(valid_From);
    }
    public String formatDateValidTo(){
        SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
        return f.format(valid_To);
    }
}
