/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author PhanQuangHuy59
 */
public class CatergorySubject {

    private int id_Catergory_Subject;
    private String name_Catergory_Subject;
    private String description_Catergory_Subject;
    private int id_Type_Catergory_Subjec;

    public CatergorySubject() {
    }

    public CatergorySubject(int id_Catergory_Subject, String name_Catergory_Subject, String description_Catergory_Subject, int id_Type_Catergory_Subjec) {
        this.id_Catergory_Subject = id_Catergory_Subject;
        this.name_Catergory_Subject = name_Catergory_Subject;
        this.description_Catergory_Subject = description_Catergory_Subject;
        this.id_Type_Catergory_Subjec = id_Type_Catergory_Subjec;
    }

    

    public int getId_Catergory_Subject() {
        return id_Catergory_Subject;
    }

    public void setId_Catergory_Subject(int id_Catergory_Subject) {
        this.id_Catergory_Subject = id_Catergory_Subject;
    }

    public String getName_Catergory_Subject() {
        return name_Catergory_Subject;
    }

    public void setName_Catergory_Subject(String name_Catergory_Subject) {
        this.name_Catergory_Subject = name_Catergory_Subject;
    }

    public String getDescription_Catergory_Subject() {
        return description_Catergory_Subject;
    }

    public void setDescription_Catergory_Subject(String description_Catergory_Subject) {
        this.description_Catergory_Subject = description_Catergory_Subject;
    }

    public int getId_Type_Catergory_Subjec() {
        return id_Type_Catergory_Subjec;
    }

    public void setId_Type_Catergory_Subjec(int id_Type_Catergory_Subjec) {
        this.id_Type_Catergory_Subjec = id_Type_Catergory_Subjec;
    }

}
