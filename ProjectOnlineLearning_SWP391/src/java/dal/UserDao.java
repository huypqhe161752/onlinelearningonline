/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.User;
import model1.UserJoin;

/**
 *
 * @author PhanQuangHuy59
 */
public class UserDAO extends DBContext {

    public String getUserByemail(String email) {
        String sql = "SELECT [id_User],[email],[password],[full_Name],[phone_Number],[gender]\n"
                + "      ,[avartar],[address],[status],[id_role]\n"
                + "  FROM [dbo].[User] where email =  ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, email);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {

                return rs.getString("email");
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public void updateUserById(User u) {
        String sql = "UPDATE [dbo].[User]\n"
                + "   SET [email] = ?\n"
                + "      ,[password] = ?\n"
                + "      ,[full_Name] = ?\n"
                + "      ,[phone_Number] = ?\n"
                + "      ,[gender] = ?\n"
                + "      ,[avartar] = ?\n"
                + "      ,[address] = ?\n"
                + "      ,[status] = ?\n"
                + "      ,[id_Role] = ?\n"
                + " WHERE [id_User] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, u.getEmail());
            st.setString(2, u.getPassword());
            st.setString(3, u.getFull_Name());
            st.setString(4, u.getPhone_Number());
            st.setBoolean(5, u.isGender());
            st.setString(6, u.getAvatar());
            st.setString(7, u.getAddress());
            st.setBoolean(8, u.isStatus());
            st.setInt(9, u.getId_role());
            st.setInt(10, u.getId_User());
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public List<User> getAllUser() {
        List<User> list = new ArrayList<>();
        String sql = "select * from [User]";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {

                list.add(new User(
                        rs.getInt("id_User"), 
                        rs.getString("email"), 
                        rs.getString("password"), 
                        rs.getString("full_Name"), 
                        rs.getString("phone_Number"), 
                        rs.getBoolean("gender"), 
                        rs.getString("avartar"), 
                        rs.getString("address"), 
                        rs.getBoolean("status"), 
                        rs.getInt("id_Role"), 
                        rs.getDate("create_Date"))
                );

            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public int getTopUserId() {
        //UserDAO udao = new UserDAO();
        int count = 0;
        String sql = "SELECT * FROM [User] WHERE id_User = (SELECT MAX(id_User) FROM [User])";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                count = rs.getInt(1);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return count;
    }

    public void insertNewUser(User u) {
        String sql = "INSERT INTO [dbo].[User]\n"
                + "           ([email]\n"
                + "           ,[password]\n"
                + "           ,[full_Name]\n"
                + "           ,[phone_Number]\n"
                + "           ,[gender]\n"
                + "           ,[avartar]\n"
                + "           ,[address]\n"
                + "           ,[status]\n"
                + "           ,[id_Role])\n"
                + "     VALUES\n"
                + "           (?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, u.getEmail());
            st.setString(2, u.getPassword());
            st.setString(3, u.getFull_Name());
            st.setString(4, u.getPhone_Number());
            st.setBoolean(5, u.isGender());
            st.setString(6, u.getAvatar());
            st.setString(7, u.getAddress());
            st.setBoolean(8, u.isStatus());
            st.setInt(9, u.getId_role());
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    /**
     * @author Naviank
     * @return number of customers who have account in database
     */
    public int countCustomer() {
        String sql = "select count (*) from [User]\n"
                + "where id_role = 3";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);

            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return 0;
    }

    public void ResetPassWord(String email, String pass) {
        String sql = "UPDATE [dbo].[User]\n"
                + "   SET [password] = ?\n"
                + "      \n"
                + " WHERE email = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, pass);
            st.setString(2, email);
            st.executeUpdate();
        } catch (SQLException e) {
        }

    }

    public User getUserByIdUser(int id) {
        String sql = "SELECT *\n"
                + "  FROM [OnlineLearningSystem].[dbo].[User] where id_User =  ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                User u = new User();
                u.setId_User(id);
                u.setEmail(rs.getString("email"));
                u.setPassword(rs.getString("password"));
                u.setFull_Name(rs.getString("full_Name"));
                u.setPhone_Number(rs.getString("phone_Number"));
                u.setGender(rs.getBoolean("gender"));
                u.setAvatar(rs.getString("avartar"));
                u.setAddress(rs.getString("address"));
                u.setStatus(rs.getBoolean("status"));
                u.setId_role(rs.getInt("id_role"));
                u.setCreate_Date(rs.getDate("create_Date"));
                return u;
            }
        } catch (SQLException e) {
        }
        return null;
    }

    /**
     * @author Naviank
     * @param id_role
     * @return list of customers who have the same id_role as the parameter
     */
    public List<User> getListUserHasRole(int id_role) {
        List<User> list = new ArrayList<>();
        String sql = "select * from [User]\n"
                + "where id_role = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id_role);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                 list.add(new User(
                        rs.getInt("id_User"), 
                        rs.getString("email"), 
                        rs.getString("password"), 
                        rs.getString("full_Name"), 
                        rs.getString("phone_Number"), 
                        rs.getBoolean("gender"), 
                        rs.getString("avartar"), 
                        rs.getString("address"), 
                        rs.getBoolean("status"), 
                        rs.getInt("id_Role"), 
                        rs.getDate("create_Date"))
                );
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public User getProfileByEmail(String email) {

        String sql = "SELECT *\n"
                + "FROM [dbo].[User] where email = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, email);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                User u1 = new User();
                u1.setEmail(email);
                u1.setPassword(rs.getString("password"));
                u1.setAddress(rs.getString("address"));
                u1.setAvatar(rs.getString("avartar"));
                u1.setFull_Name(rs.getString("full_Name"));
                u1.setId_role(rs.getInt("id_Role"));
                u1.setGender(rs.getBoolean("gender"));
                u1.setStatus(rs.getBoolean("status"));
                u1.setPhone_Number(rs.getString("phone_Number"));
                u1.setId_User(rs.getInt("id_User"));
                return u1;
            }
        } catch (SQLException e) {
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    public void changePassword(String newPassword, String email) {
        String sql = "UPDATE [User] \n"
                + "SET [password] = ?\n"
                + "where email = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, newPassword);
            st.setString(2, email);
            st.executeUpdate();
        } catch (SQLException e) {
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public List<User> getAuthorById_Course(int id) {
        // String author = null;
        User u = new User();
        List<User> list = new ArrayList<>();
        String sql = "SELECT [User].full_Name, [User].avartar\n"
                + "FROM [dbo].[User]\n"
                + "JOIN [dbo].[Subject] ON [User].id_User = Subject.id_User\n"
                + "JOIN [dbo].[Course] ON Subject.id_Course = Course.id_Course\n"
                + "WHERE Course.id_Course = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                u.setFull_Name(rs.getString("full_Name"));
                u.setAvatar(rs.getString("avartar"));
                list.add(u);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public void changeProfile(User u, String email) {
        String sql = "Update [User]\n"
                + "SET full_Name = ?,\n"
                + "phone_Number=?,\n"
                + "gender=?,\n"
                + "avartar=?,\n"
                + "[address]=?\n"
                + "Where email = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, u.getFull_Name());
            st.setString(2, u.getPhone_Number());
            if (u.isGender()) {
                st.setInt(3, 1);
            } else {
                st.setInt(3, 0);
            }
            st.setString(4, u.getAvatar());
            st.setString(5, u.getAddress());
            st.setString(6, email);
            st.executeUpdate();
        } catch (SQLException e) {
        } finally {
            try {
                connection.commit();
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public ArrayList<User> getExpert() {
        ArrayList<User> list = new ArrayList<>();
        String sql = "SELECT [id_User],[email],[password],[full_Name],[phone_Number]\n"
                + ",[gender],[avartar],[address],[status],[id_Role]\n"
                + "FROM [dbo].[User] where id_Role = 2";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                User u = new User();
                u.setId_User(rs.getInt("id_User"));
                u.setEmail(rs.getString("email"));
                u.setPassword(rs.getString("password"));
                u.setFull_Name(rs.getString("full_Name"));
                u.setPhone_Number(rs.getString("phone_Number"));
                u.setGender(rs.getBoolean("gender"));
                u.setAvatar(rs.getString("avartar"));
                u.setAddress(rs.getString("address"));
                u.setStatus(rs.getBoolean("status"));
                u.setId_role(rs.getInt("id_role"));
                list.add(u);
            }
            return list;
        } catch (SQLException e) {
        }
        return null;
    }
    public ArrayList<User> getExpertByName(String name) {
        ArrayList<User> list = new ArrayList<>();
        String sql = "SELECT [id_User],[email],[password],[full_Name],[phone_Number]\n"
                + ",[gender],[avartar],[address],[status],[id_Role]\n"
                + "FROM [dbo].[User] where id_Role = 2";
        sql = sql + " and full_Name like \'%"+name+"%\'";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                User u = new User();
                u.setId_User(rs.getInt("id_User"));
                u.setEmail(rs.getString("email"));
                u.setPassword(rs.getString("password"));
                u.setFull_Name(rs.getString("full_Name"));
                u.setPhone_Number(rs.getString("phone_Number"));
                u.setGender(rs.getBoolean("gender"));
                u.setAvatar(rs.getString("avartar"));
                u.setAddress(rs.getString("address"));
                u.setStatus(rs.getBoolean("status"));
                u.setId_role(rs.getInt("id_role"));
                list.add(u);
            }
            return list;
        } catch (SQLException e) {
        }
        return null;
    }

/// PCT
    public List<UserJoin> getAllUser1() {
        List<UserJoin> list = new ArrayList<>();
        String sql = "select a.id_User,a.email,a.password, a.full_Name, a.phone_Number,\n"
                + "a.gender, a.avartar, a.address, a.status,a.id_Role,a.create_Date,\n"
                + "b.nameRole from [dbo].[User] a join Role b on a.id_Role = b.id_Role";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {

                list.add(new UserJoin(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getBoolean(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getBoolean(9),
                        rs.getInt(10),
                        rs.getDate(11),
                        rs.getString(12)
                ));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<UserJoin> getAllUserTop6() {
        List<UserJoin> list = new ArrayList<>();
        String sql = "select Top 6 a.id_User,a.email,a.password, a.full_Name, a.phone_Number,\n"
                + "a.gender, a.avartar, a.address, a.status,a.id_Role,a.create_Date,\n"
                + "b.nameRole from [dbo].[User] a join Role b on a.id_Role = b.id_Role order by a.create_Date desc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new UserJoin(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getBoolean(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getBoolean(9),
                        rs.getInt(10),
                        rs.getDate(11),
                        rs.getString(12)
                ));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<UserJoin> getSearch(String search, String sortID) {
        List<UserJoin> list = new ArrayList<>();
        String sql = "select a.id_User,a.email,a.password, a.full_Name, a.phone_Number,\n"
                + "a.gender, a.avartar, a.address, a.status,a.id_Role,a.create_Date,\n"
                + "b.nameRole from [dbo].[User] a join Role b on a.id_Role = b.id_Role";
        if (search != null && !search.trim().isEmpty()) {
            sql = sql + " and a.full_Name like ?";
        }
        if (sortID != null && !sortID.isEmpty()) {
            if (sortID.equals("1")) {
                sql = sql + " order by full_Name";
            } else if (sortID.equals("2")) {
                sql = sql + " order by email ";
            } else if (sortID.equals("3")) {
                sql = sql + " order by phone_Number";
            } else if (sortID.equals("4")) {
                sql = sql + " order by nameRole";
            } else if (sortID.equals("0")) {
                sql = sql;
            }
        } else {
            sql = sql;
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            if (search != null && !search.trim().isEmpty()) {
                st.setString(1, "%" + search + "%");
            }
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new UserJoin(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getBoolean(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getBoolean(9),
                        rs.getInt(10),
                        rs.getDate(11),
                        rs.getString(12)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public UserJoin getIDUser(int id) {
        String sql = "select a.id_User,a.email,a.password, a.full_Name, a.phone_Number,\n"
                + "a.gender, a.avartar, a.address, a.status,a.id_Role,a.create_Date,\n"
                + "b.nameRole from [dbo].[User] a join Role b on a.id_Role = b.id_Role and a.id_User = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                UserJoin u = new UserJoin();
                u.setId_User(rs.getInt("id_User"));
                u.setEmail(rs.getString("email"));
                u.setPassword(rs.getString("password"));
                u.setFull_Name(rs.getString("full_Name"));
                u.setPhone_Number(rs.getString("phone_Number"));
                u.setGender(rs.getBoolean("gender"));
                u.setAvatar(rs.getString("avartar"));
                u.setAddress(rs.getString("address"));
                u.setStatus(rs.getBoolean("status"));
                u.setId_role(rs.getInt("id_Role"));
                u.setCreate_Date(rs.getDate("create_Date"));
                u.setNameRole(rs.getString("nameRole"));
                return u;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }


    public List<UserJoin> getUsersByRole(String id_Role) {
        List<UserJoin> list = new ArrayList<>();
        String sql = "select a.id_User,a.email,a.password, a.full_Name, a.phone_Number,\n"
                + "a.gender, a.avartar, a.address, a.status,a.id_Role,a.create_Date,\n"
                + "b.nameRole from [dbo].[User] a join Role b on a.id_Role = b.id_Role ";
        if (id_Role != null && !id_Role.isEmpty()) {
            sql = sql + " and a.id_Role=?";
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            if (id_Role != null && !id_Role.isEmpty()) {
                st.setString(1, id_Role);
            }
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new UserJoin(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getBoolean(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getBoolean(9),
                        rs.getInt(10),
                        rs.getDate(11),
                        rs.getString(12)
                ));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<UserJoin> getUsersByStatus(String status) {
        List<UserJoin> list = new ArrayList<>();
        String sql = "select a.id_User,a.email,a.password, a.full_Name, a.phone_Number,\n"
                + "a.gender, a.avartar, a.address, a.status,a.id_Role,a.create_Date,\n"
                + "b.nameRole from [dbo].[User] a join Role b on a.id_Role = b.id_Role";
        if (status != null && !status.isEmpty()) {
            sql = sql + " where status = ?";
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            if (status != null && !status.isEmpty()) {
                st.setString(1, status);
            }
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new UserJoin(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getBoolean(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getBoolean(9),
                        rs.getInt(10),
                        rs.getDate(11),
                        rs.getString(12)
                ));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<UserJoin> getUsersByGender(String gender) {
        List<UserJoin> list = new ArrayList<>();
        String sql = "select a.id_User,a.email,a.password, a.full_Name, a.phone_Number,\n"
                + "a.gender, a.avartar, a.address, a.status,a.id_Role,a.create_Date,\n"
                + "b.nameRole from [dbo].[User] a join Role b on a.id_Role = b.id_Role";
        if (gender != null && !gender.isEmpty()) {
            sql = sql + " where gender = ?";
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            if (gender != null && !gender.isEmpty()) {
                st.setString(1, gender);
            }
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new UserJoin(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getBoolean(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getBoolean(9),
                        rs.getInt(10),
                        rs.getDate(11),
                        rs.getString(12)
                ));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }
    public UserJoin getEmail(String email) {
        String sql = "select a.id_User,a.email,a.password, a.full_Name, a.phone_Number,\n"
                + "a.gender, a.avartar, a.address, a.status,a.id_Role,a.create_Date,\n"
                + "b.nameRole from [dbo].[User] a join Role b on a.id_Role = b.id_Role and a.email like ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, email);
            ResultSet rs = st.executeQuery();
           if (rs.next()) {
                 UserJoin u = new UserJoin();
                u.setId_User(rs.getInt("id_User"));
                u.setEmail(rs.getString("email"));
                u.setPassword(rs.getString("password"));
                u.setFull_Name(rs.getString("full_Name"));
                u.setPhone_Number(rs.getString("phone_Number"));
                u.setGender(rs.getBoolean("gender"));
                u.setAvatar(rs.getString("avartar"));
                u.setAddress(rs.getString("address"));
                u.setStatus(rs.getBoolean("status"));
                u.setId_role(rs.getInt("id_Role"));
                u.setCreate_Date(rs.getDate("create_Date"));
                u.setNameRole(rs.getString("nameRole"));
                return u;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

   public List<User> getListUserRoleExpert() {
        List<User> list = new ArrayList<>();
        String sql = "SELECT [id_User]\n"
                + "      ,[email]\n"
                + "      ,[password]\n"
                + "      ,[full_Name]\n"
                + "      ,[phone_Number]\n"
                + "      ,[gender]\n"
                + "      ,[avartar]\n"
                + "      ,[address]\n"
                + "      ,[status]\n"
                + "      ,[id_Role]\n"
                + "      ,[create_Date]\n"
                + "  FROM [OnlineLearningSystem].[dbo].[User]\n"
                + "  where id_Role = 2";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                User u = new User();
                u.setId_User(rs.getInt("id_User"));
                u.setEmail(rs.getString("email"));
                u.setPassword(rs.getString("password"));
                u.setFull_Name(rs.getString("full_Name"));
                u.setPhone_Number(rs.getString("phone_Number"));
                u.setGender(rs.getBoolean("gender"));
                u.setAvatar(rs.getString("avartar"));
                u.setAddress(rs.getString("address"));
                u.setStatus(rs.getBoolean("status"));
                u.setId_role(rs.getInt("id_role"));
                list.add(u);
            }
            return list;
        } catch (SQLException e) {
        }
        return null;
    }
    public void updateUser(String id_User, String email, String password, String full_Name, String phone, Boolean gender, String avartar, String address, Boolean status,
            String id_Role, String createDate) {
        String sql = "UPDATE [dbo].[User]\n"
                + "   SET [email] = ?\n"
                + "      ,[password] = ?\n"
                + "      ,[full_Name] = ?\n"
                + "      ,[phone_Number] = ?\n"
                + "      ,[gender] = ?\n"
                + "      ,[avartar] = ?\n"
                + "      ,[address] = ?\n"
                + "      ,[status] = ?\n"
                + "      ,[id_Role] = ?\n"
                + "      ,[create_Date] = ?\n"
                + " WHERE [id_User] = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, email);
            ps.setString(2, password);
            ps.setString(3, full_Name);
            ps.setString(4, phone);
            ps.setBoolean(5, gender);
            ps.setString(6, avartar);
            ps.setString(7, address);
            ps.setBoolean(8, status);
            ps.setString(9, id_Role);
            ps.setString(10, createDate);
            ps.setString(11, id_User);

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
      public void updateStatusUser(String id_User , Boolean status) {
        String sql = "UPDATE [dbo].[User]\n"
                + "   SET [status] = ?\n"
                + " WHERE [id_User] = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setBoolean(1, status);
            ps.setString(2, id_User);

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertUser(String email, String password, String full_Name, String phone, Boolean gender, String avartar, String address, Boolean status,
            String id_Role, String createDate) {
        String sql = "INSERT INTO [dbo].[User]\n"
                + "           ([email]\n"
                + "           ,[password]\n"
                + "           ,[full_Name]\n"
                + "           ,[phone_Number]\n"
                + "           ,[gender]\n"
                + "           ,[avartar]\n"
                + "           ,[address]\n"
                + "           ,[status]\n"
                + "           ,[id_Role]\n"
                + "           ,[create_Date])\n"
                + "     VALUES (?,?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, email);
            ps.setString(2, password);
            ps.setString(3, full_Name);
            ps.setString(4, phone);
            ps.setBoolean(5, gender);
            ps.setString(6, avartar);
            ps.setString(7, address);
            ps.setBoolean(8, status);
            ps.setString(9, id_Role);
            ps.setString(10, createDate);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public List<UserJoin> getListByPage1(List<UserJoin> list,
            int start, int end) {
        List<UserJoin> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }

    public static void main(String[] args) {
        UserDAO dao = new UserDAO();
        String id = "1";

        Boolean status = true;


        dao.updateStatusUser(id, status);
    }


    public List<User> searchUserExpert(String txt) {
        List<User> list = new ArrayList<>();
        String sql = "SELECT [id_User]\n"
                + "      ,[email]\n"
                + "      ,[password]\n"
                + "      ,[full_Name]\n"
                + "      ,[phone_Number]\n"
                + "      ,[gender]\n"
                + "      ,[avartar]\n"
                + "      ,[address]\n"
                + "      ,[status]\n"
                + "      ,[id_Role]\n"
                + "      ,[create_Date]\n"
                + "  FROM [OnlineLearningSystem].[dbo].[User]\n"
                + "  where full_Name like ? and id_Role = 2";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%"+txt+"%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                User u = new User();
                u.setId_User(rs.getInt("id_User"));
                u.setEmail(rs.getString("email"));
                u.setPassword(rs.getString("password"));
                u.setFull_Name(rs.getString("full_Name"));
                u.setPhone_Number(rs.getString("phone_Number"));
                u.setGender(rs.getBoolean("gender"));
                u.setAvatar(rs.getString("avartar"));
                u.setAddress(rs.getString("address"));
                u.setStatus(rs.getBoolean("status"));
                u.setId_role(rs.getInt("id_role"));
                u.setCreate_Date(rs.getDate("create_Date"));
                list.add(u);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public ArrayList<User> getMarketing() {
        ArrayList<User> list = new ArrayList<>();
        String sql = "SELECT [id_User],[email],[password],[full_Name],[phone_Number]\n"
                + ",[gender],[avartar],[address],[status],[id_Role]\n"
                + "FROM [dbo].[User] where id_Role = 4";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                User u = new User();
                u.setId_User(rs.getInt("id_User"));
                u.setEmail(rs.getString("email"));
                u.setPassword(rs.getString("password"));
                u.setFull_Name(rs.getString("full_Name"));
                u.setPhone_Number(rs.getString("phone_Number"));
                u.setGender(rs.getBoolean("gender"));
                u.setAvatar(rs.getString("avartar"));
                u.setAddress(rs.getString("address"));
                u.setStatus(rs.getBoolean("status"));
                u.setId_role(rs.getInt("id_role"));
                list.add(u);
            }
            return list;
        } catch (SQLException e) {
        }
        return null;
    }
}
