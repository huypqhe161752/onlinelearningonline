/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.CatergoryBlog;

/**
 *
 * @author PhanQuangHuy59
 */
public class CatergoryBlogDAO extends DBContext {

    public List<CatergoryBlog> getAllBlogCate() {
        List<CatergoryBlog> list = new ArrayList<>();
        String sql = "select * from CatergoryBlog";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new CatergoryBlog(rs.getInt(1), rs.getString(2)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<CatergoryBlog> get3BlogCate(int offset) {
        List<CatergoryBlog> list = new ArrayList<>();
        String sql = "select * \n"
                + "from CatergoryBlog\n"
                + "order by id_CatergoryBlog\n"
                + "offset ? rows\n"
                + "fetch next 3 ROWS ONLY";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, offset);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new CatergoryBlog(rs.getInt(1), rs.getString(2)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<CatergoryBlog> getAllCategoryBlog() {
        List<CatergoryBlog> list = new ArrayList<>();
        String sql = "SELECT [id_CatergoryBlog]\n"
                + "      ,[name_CatergoryBlog]\n"
                + "  FROM [dbo].[CatergoryBlog]";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                CatergoryBlog c = new CatergoryBlog();
                c.setId_Catergory_Blog(rs.getInt("id_CatergoryBlog"));
                c.setName_Catergory_Blog(rs.getString("name_CatergoryBlog"));
                list.add(c);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<CatergoryBlog> getCatergoryName() {
        List<CatergoryBlog> list = new ArrayList<>();
        String sql = "SELECT Blog.id_Blog, [CatergoryBlog].name_CatergoryBlog\n"
                + "FROM Blog\n"
                + "JOIN [CatergoryBlog] ON Blog.id_CatergoryBlog = [CatergoryBlog].id_CatergoryBlog;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                CatergoryBlog c = new CatergoryBlog(rs.getInt("id_Blog"), rs.getString("name_CatergoryBlog"));
                list.add(c);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    /**
     * @author Naviank
     * @param idBlog
     * @return
     */
    public CatergoryBlog getCategoryOfBlog(int idBlog) {
        String sql = "select * from CatergoryBlog where id_Blog = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, idBlog);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new CatergoryBlog(rs.getInt(1), rs.getString(2));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    /**
     * @author Naviank
     * @param idCate
     * @return
     */
    public CatergoryBlog getCategoryOfId(int idCate) {
        String sql = "select * from CatergoryBlog where id_CatergoryBlog = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, idCate);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new CatergoryBlog(rs.getInt(1), rs.getString(2));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public static void main(String[] args) {
        CatergoryBlogDAO c = new CatergoryBlogDAO();
        List<CatergoryBlog> cc = c.getCatergoryName();
        CatergoryBlog ctg = new CatergoryBlog();
        for (int i = 0; i < cc.size(); i++) {
            if (2 == cc.get(i).getId_Catergory_Blog()) {
                ctg.setId_Catergory_Blog(cc.get(i).getId_Catergory_Blog());
                ctg.setName_Catergory_Blog(cc.get(i).getName_Catergory_Blog());
            }
        }
        System.out.println(ctg.getName_Catergory_Blog());
        System.out.println(c.getAllBlogCate().size());
    }
}
