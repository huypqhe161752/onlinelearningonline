/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.TypeLesson;

/**
 *
 * @author PhanQuangHuy59
 */
public class TypeLessonDAO extends DBContext{
    public List<TypeLesson> getAll() {
    List<TypeLesson> list = new ArrayList<>();
    String sql = "SELECT * FROM TypeLesson";
    try {
        PreparedStatement st = connection.prepareStatement(sql);
        ResultSet rs = st.executeQuery();
        while (rs.next()) {
            list.add(new TypeLesson(rs.getInt(1), rs.getString(2)));
        }
    } catch (SQLException e) {
        e.printStackTrace();
    }
    return list;
}
}
