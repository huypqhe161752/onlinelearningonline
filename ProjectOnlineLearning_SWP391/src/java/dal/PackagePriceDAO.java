/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import model.PackagePrice;
import java.util.List;
import java.util.Scanner;
import model.PackagePrice;

/**
 *
 * @author PhanQuangHuy59
 */
public class PackagePriceDAO extends DBContext {

    public ArrayList<PackagePrice> getPackagePriceOfCourse(int id_course) {
        String sql = "SELECT [id_Package],[duration],[list_Price]\n"
                + "      ,[sale_Price],[status],[id_Course]\n"
                + " FROM [dbo].[PackagePrice] where status = 1 and id_Course= ? order by sale_Price,duration";

        ArrayList<PackagePrice> list = new ArrayList<>();

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id_course);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                PackagePrice pack = new PackagePrice();
                pack.setId_Package(rs.getInt("id_Package"));
                pack.setDuration(rs.getInt("duration"));
                pack.setList_Price(rs.getInt("list_Price"));
                pack.setSale_Price(rs.getInt("sale_Price"));
                pack.setStatus(rs.getBoolean("status"));
                pack.setId_Course(rs.getInt("id_Course"));
                list.add(pack);

            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    /**
     * @author Naviank
     * @return List of the lowest package price for each course existing in
     * database. If the course doesn't have any package price, return a
     * 0-package price
     */
    public List<PackagePrice> getLowestPakageOfEach() {
        List<PackagePrice> list = new ArrayList<>();

        int arr[] = {0};
        String tmp1 = "select id_Course from Course group by id_Course";
        String tmp2 = "select count(*) from Course";
        try {
            int num = 0;
            PreparedStatement st1 = connection.prepareStatement(tmp1);
            PreparedStatement st2 = connection.prepareStatement(tmp2);
            ResultSet rs1 = st1.executeQuery();
            ResultSet rs2 = st2.executeQuery();
            if (rs2.next()) {
                num = rs2.getInt(1);
            }
            arr = new int[num];
            num = 0;
            while (rs1.next()) {
                arr[num++] = rs1.getInt(1);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        for (int x : arr) {
            System.out.println(x);
            PackagePrice tmp = getLowestPacketPriceOfCourse(x);
            if (tmp != null) {
                list.add(tmp);
            } else {
                list.add(new PackagePrice(x));
            }
        }
        return list;
    }

    public Date calculateExpirationDate(Date valid_from, int duration) {
        LocalDate startDateObj = valid_from.toLocalDate();
        LocalDate expirationDate = startDateObj.plusMonths(duration);
        return Date.valueOf(expirationDate);
    }

    /**
     * @author Naviank
     * @param id_course
     * @return a package price corresponding to a specific course
     */
    public PackagePrice getLowestPacketPriceOfCourse(int id_course) {
        String sql = "select * from PackagePrice\n"
                + "where PackagePrice.status = 1 and PackagePrice.id_Course = ?\n"
                + "order by list_Price asc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id_course);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new PackagePrice(rs.getInt(1),
                        rs.getInt(2),
                        rs.getInt(3),
                        rs.getInt(4),
                        true,
                        rs.getInt(6));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public PackagePrice getPackagePriceById(int id) {
        String sql = "SELECT [id_Package]\n"
                + "      ,[duration]\n"
                + "      ,[list_Price]\n"
                + "      ,[sale_Price]\n"
                + "      ,[status]\n"
                + "      ,[id_Course]\n"
                + "  FROM [dbo].[PackagePrice]\n"
                + "  where id_Package = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new PackagePrice(rs.getInt(1),
                        rs.getInt(2),
                        rs.getInt(3),
                        rs.getInt(4),
                        true,
                        rs.getInt(6));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public PackagePrice getPackagePriceByIdcuaHuy(int id) {

        String sql = "SELECT [id_Package]\n"
                + "      ,[duration]\n"
                + "      ,[list_Price]\n"
                + "      ,[sale_Price]\n"
                + "      ,[status]\n"
                + "      ,[id_Course]\n"
                + "  FROM [dbo].[PackagePrice]\n"
                + "  where id_Package = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                PackagePrice p = new PackagePrice();
                p.setId_Package(rs.getInt("id_Package"));
                p.setDuration(rs.getInt("duration"));
                p.setList_Price(rs.getInt("list_Price"));
                p.setSale_Price(rs.getInt("sale_Price"));
                p.setStatus(rs.getBoolean("status"));
                p.setId_Course(rs.getInt("id_Course"));
                return p;
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public static void main(String[] args) {
        PackagePriceDAO ppdao = new PackagePriceDAO();
        List<PackagePrice> list = ppdao.getAll();
//        System.out.println(l1.size());
//        for (PackagePrice packagePrice : l1) {
//            System.out.println(packagePrice);
//        }
        int a = ppdao.getTotalPackagePriceList();
        System.out.println(a);
        PackagePrice p = ppdao.getPackagePriceByIdToUpdate(25);
        System.out.println(p.getId_Course());
//        Scanner sc = new Scanner(System.in);
//        int n;
//        while (true) {
//            System.out.print("Enter:");
//            n = sc.nextInt();
//            if (n == 0) {
//                break;
//            }
//            PackagePrice p = ppdao.getLowestPacketPriceOfCourse(n);
//            System.out.println("Price: " + p.getList_Price() + ", Sale:" + p.getSale_Price());
//        }
    }

    public List<PackagePrice> getAllpackagePrice() {//lay tat ca pacjage price
        List<PackagePrice> list = new ArrayList<>();
        String sql = "  SELECT p.[id_Package]\n"
                + "      ,p.[duration]\n"
                + "      ,p.[list_Price]\n"
                + "      ,p.[sale_Price]\n"
                + "      ,p.[status]\n"
                + "FROM [OnlineLearningSystem].[dbo].[PackagePrice] p\n"
                + "JOIN [OnlineLearningSystem].[dbo].[Subject] s ON p.[id_Course] = s.[id_Course]";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                PackagePrice pp = new PackagePrice();
                pp.setId_Package(rs.getInt("id_Package"));
                pp.setDuration(rs.getInt("duration"));
                pp.setList_Price(rs.getInt("list_Price"));
                pp.setSale_Price(rs.getInt("sale_Price"));
                pp.setStatus(rs.getBoolean("status"));
                list.add(pp);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<PackagePrice> getAllpackagePriceNormal() {//lay tat ca pacjage price
        List<PackagePrice> list = new ArrayList<>();
        String sql = "SELECT [id_Package]\n"
                + "      ,[duration]\n"
                + "      ,[list_Price]\n"
                + "      ,[sale_Price]\n"
                + "      ,[status]\n"
                + "      ,[id_Course]\n"
                + "  FROM [OnlineLearningSystem].[dbo].[PackagePrice]";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                PackagePrice pp = new PackagePrice();
                pp.setId_Package(rs.getInt("id_Package"));
                pp.setDuration(rs.getInt("duration"));
                pp.setList_Price(rs.getInt("list_Price"));
                pp.setSale_Price(rs.getInt("sale_Price"));
                pp.setStatus(rs.getBoolean("status"));
                pp.setId_Course(rs.getInt("id_Course"));
                list.add(pp);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public int getTotalPackagePriceList() {
        String sql = "SELECT count(*)\n"
                + "FROM [OnlineLearningSystem].[dbo].[PackagePrice] p\n";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return 0;
    }

    public List<PackagePrice> getPagingPackagePrice(int pageNumber) {
        List<PackagePrice> list = new ArrayList<>();
        String sql = "SELECT p.[id_Package],\n"
                + "       p.[duration],\n"
                + "       p.[list_Price],\n"
                + "       p.[sale_Price],\n"
                + "       p.[status],\n"
                + "	   p.id_Course\n"
                + "FROM [OnlineLearningSystem].[dbo].[PackagePrice] p\n"
                + "ORDER BY p.[id_Package]\n"
                + "OFFSET ? ROWS FETCH NEXT 5 ROWS ONLY;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, (pageNumber - 1) * 5);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                PackagePrice pp = new PackagePrice();
                pp.setId_Package(rs.getInt("id_Package"));
                pp.setDuration(rs.getInt("duration"));
                pp.setList_Price(rs.getInt("list_Price"));
                pp.setSale_Price(rs.getInt("sale_Price"));
                pp.setStatus(rs.getBoolean("status"));
                pp.setId_Course(rs.getInt("id_Course"));
                list.add(pp);
            }
        } catch (SQLException e) {

        }
        return list;
    }

    public void addNewPackagePrice(PackagePrice p) {
        String sql = "INSERT INTO [dbo].[PackagePrice]\n"
                + "           ([duration]\n"
                + "           ,[list_Price]\n"
                + "           ,[sale_Price]\n"
                + "           ,[status]\n"
                + "           ,[id_Course])\n"
                + "     VALUES\n"
                + "           (?,?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, p.getDuration());
            st.setInt(2, p.getList_Price());
            st.setInt(3, p.getSale_Price());
            st.setBoolean(4, p.isStatus());
            st.setInt(5, p.getId_Course());
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void editPackagePriceWithId(PackagePrice p) {
        String sql = "UPDATE [dbo].[PackagePrice]\n"
                + "   SET [duration] = ?\n"
                + "      ,[list_Price] = ?\n"
                + "      ,[sale_Price] = ?\n"
                + "      ,[status] = ?\n"
                + "      ,[id_Course] = ?\n"
                + " WHERE id_Package = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, p.getDuration());
            st.setInt(2, p.getList_Price());
            st.setInt(3, p.getSale_Price());
            st.setBoolean(4, p.isStatus());
            st.setInt(5, p.getId_Course());
            st.setInt(6, p.getId_Package());
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public PackagePrice getPackagePriceByIdToUpdate(int id) {
        String sql = "SELECT [id_Package]\n"
                + "      ,[duration]\n"
                + "      ,[list_Price]\n"
                + "      ,[sale_Price]\n"
                + "      ,[status]\n"
                + "      ,[id_Course]\n"
                + "  FROM [dbo].[PackagePrice]\n"
                + "  where id_Package = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new PackagePrice(rs.getInt(1),
                        rs.getInt(2),
                        rs.getInt(3),
                        rs.getInt(4),
                        rs.getBoolean(5),
                        rs.getInt(6));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public PackagePrice getPackagePriceByIdCourse(int id) {
        String sql = "SELECT [id_Package]\n"
                + "      ,[duration]\n"
                + "      ,[list_Price]\n"
                + "      ,[sale_Price]\n"
                + "      ,[status]\n"
                + "      ,[id_Course]\n"
                + "  FROM [dbo].[PackagePrice]\n"
                + "  where id_Course = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new PackagePrice(rs.getInt(1),
                        rs.getInt(2),
                        rs.getInt(3),
                        rs.getInt(4),
                        rs.getBoolean(5),
                        rs.getInt(6));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public void deletePackagePrice(int id) {
        String sql = "DELETE FROM [dbo].[PackagePrice]\n"
                + "      WHERE id_Package = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            st.executeQuery();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public ArrayList<PackagePrice> getAll() {
        ArrayList<PackagePrice> listPrice = new ArrayList<>();
        String sql = "select * from PackagePrice";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                PackagePrice p = new PackagePrice(rs.getInt(1),
                        rs.getInt(2),
                        rs.getInt(3),
                        rs.getInt(4),
                        true,
                        rs.getInt(6));
                listPrice.add(p);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return listPrice;
    }

    public void deletePackagePriceByIdCourse(int idCourse) {
        String sql = "DELETE FROM [dbo].[PackagePrice]\n"
                + "      WHERE id_Course = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, idCourse);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }
    //caohung

    public List<PackagePrice> getPackagePriceOfCourseUnpublic(int id_course) {
        String sql = "SELECT [id_Package],[duration],[list_Price]\n"
                + "      ,[sale_Price],[status],[id_Course]\n"
                + " FROM [dbo].[PackagePrice] where id_Course= ? order by duration";

        ArrayList<PackagePrice> list = new ArrayList<>();

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id_course);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                PackagePrice pack = new PackagePrice();
                pack.setId_Package(rs.getInt("id_Package"));
                pack.setDuration(rs.getInt("duration"));
                pack.setList_Price(rs.getInt("list_Price"));
                pack.setSale_Price(rs.getInt("sale_Price"));
                pack.setStatus(rs.getBoolean("status"));
                pack.setId_Course(rs.getInt("id_Course"));
                list.add(pack);

            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public PackagePrice getFirstPackagePriceByIdCourse(int id) {
        String sql = "SELECT TOP 1 [id_Package]\n"
                + "      ,[duration]\n"
                + "      ,[list_Price]\n"
                + "      ,[sale_Price]\n"
                + "      ,[status]\n"
                + "      ,[id_Course]\n"
                + "  FROM [dbo].[PackagePrice]\n"
                + "  where id_Course = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new PackagePrice(rs.getInt(1),
                        rs.getInt(2),
                        rs.getInt(3),
                        rs.getInt(4),
                        rs.getBoolean(5),
                        rs.getInt(6));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public void updateListPriceSalePriceByID(int listprice, int saleprice, int id) {
        String sql = "UPDATE [dbo].[PackagePrice]\n"
                + "   SET [list_Price] = ?\n"
                + "      ,[sale_Price] = ?\n"
                + " WHERE [id_Package] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, listprice);
            st.setInt(2, saleprice);
            st.setInt(3, id);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public List<PackagePrice> getPackagePriceOfCoursePublic(int id_course) {
        String sql = "SELECT [id_Package],[duration],[list_Price]\n"
                + "      ,[sale_Price],[status],[id_Course]\n"
                + " FROM [dbo].[PackagePrice] where id_Course= ? and status = 1 order by duration";

        ArrayList<PackagePrice> list = new ArrayList<>();

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id_course);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                PackagePrice pack = new PackagePrice();
                pack.setId_Package(rs.getInt("id_Package"));
                pack.setDuration(rs.getInt("duration"));
                pack.setList_Price(rs.getInt("list_Price"));
                pack.setSale_Price(rs.getInt("sale_Price"));
                pack.setStatus(rs.getBoolean("status"));
                pack.setId_Course(rs.getInt("id_Course"));
                list.add(pack);

            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }
    //caohung
    
    public void insertPackagePrice(int duration, int listprice, int saleprice, int idcourse) {
        String sql = "INSERT INTO [dbo].[PackagePrice]\n"
                + "           ([duration],[list_Price],[sale_Price],[status],[id_Course])\n"
                + "VALUES(?,?,?,1,?)";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, duration);
            st.setInt(2, listprice);
            st.setInt(3, saleprice);
            st.setInt(4, idcourse);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }
}

