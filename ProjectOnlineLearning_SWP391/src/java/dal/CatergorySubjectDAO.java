/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.CatergorySubject;

import model.Type_Catergory_Subject;
/**
 *
 * @author PhanQuangHuy59
 */
public class CatergorySubjectDAO extends DBContext {

    public CatergorySubject getCatergorySubject(int idca) {
        String sql = "SELECT  [id_Catergory_Subject]\n"
                + "      ,[name_Catergory_Subject]\n"
                + "      ,[description_Catergory_Subject]\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Catergory_Subject] where id_Catergory_Subject = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, idca);
            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                CatergorySubject ca = new CatergorySubject();
                ca.setId_Catergory_Subject(rs.getInt("id_Catergory_Subject"));
                ca.setName_Catergory_Subject(rs.getString("name_Catergory_Subject"));
                ca.setDescription_Catergory_Subject(rs.getString("description_Catergory_Subject"));

                return ca;
            }


        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public List<CatergorySubject> getAllCatergorySubject() {
        List<CatergorySubject> list = new ArrayList<>();
        String sql = "SELECT  [id_Catergory_Subject]\n"
                + "      ,[name_Catergory_Subject]\n"
                + "      ,[description_Catergory_Subject]\n"
                + "      ,[id_Type_Catergory_Subjec]\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Catergory_Subject]";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                CatergorySubject ca = new CatergorySubject();
                ca.setId_Catergory_Subject(rs.getInt("id_Catergory_Subject"));
                ca.setName_Catergory_Subject(rs.getString("name_Catergory_Subject"));
                ca.setDescription_Catergory_Subject(rs.getString("description_Catergory_Subject"));
                ca.setId_Type_Catergory_Subjec(rs.getInt("id_Type_Catergory_Subjec"));
                list.add(ca);
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public String getCategorySubjectByIdquestion(int id) {
        String cate_subject = null;
        String sql = "select cs.name_Catergory_Subject\n"
                + "from Catergory_Subject cs, [Subject] s, Lesson l, Quiz qi, Question q\n"
                + "where cs.id_Catergory_Subject=s.id_Catergory_Subject and s.id_Subject=l.id_Subject and l.id_Lesson=qi.id_Lesson and qi.id_Quiz=q.id_Quiz\n"
                + "and q.id_Question=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1,id);
            ResultSet rs = st.executeQuery();
            while (rs.next()){
                cate_subject = rs.getString("name_Catergory_Subject");
            }
            return cate_subject;
        } catch (Exception e) {
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
    
    public List<CatergorySubject> searchSubjectCate(String txt) {
        List<CatergorySubject> cslist = new ArrayList<>();
        String sql = "SELECT [id_Catergory_Subject]\n"
                + "      ,[name_Catergory_Subject]\n"
                + "      ,[description_Catergory_Subject]\n"
                + "      ,[id_Type_Catergory_Subjec]\n"
                + "  FROM [dbo].[Catergory_Subject]\n"
                + "  where [name_Catergory_Subject] like ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + txt + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                CatergorySubject ca = new CatergorySubject();
                ca.setId_Catergory_Subject(rs.getInt("id_Catergory_Subject"));
                ca.setName_Catergory_Subject(rs.getString("name_Catergory_Subject"));
                ca.setDescription_Catergory_Subject(rs.getString("description_Catergory_Subject"));
                ca.setId_Type_Catergory_Subjec(rs.getInt("id_Type_Catergory_Subjec"));
                cslist.add(ca);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return cslist;
    }

    public List<Type_Catergory_Subject> getTypeCaterSubjJoinWithCaterSubj() {
        List<Type_Catergory_Subject> list = new ArrayList<>();
        String sql = "Select tcs.id_Type_Catergory_Subjec as 'IdType',\n"
                + "cs.name_Catergory_Subject,\n"
                + "tcs.name_Catergory_Subject as 'NameType'\n"
                + "from Type_Catergory_Subject tcs \n"
                + "join Catergory_Subject cs ON cs.id_Type_Catergory_Subjec = tcs.id_Type_Catergory_Subjec";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Type_Catergory_Subject ty = new Type_Catergory_Subject();
                ty.setId_Type_Catergory_Subjec(rs.getInt("IdType"));
                ty.setName_Catergory_Subject(rs.getString("name_Catergory_Subject"));
                ty.setDescription(rs.getString("NameType"));
                list.add(ty);
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Type_Catergory_Subject> getTypeCaterSubj() {
        List<Type_Catergory_Subject> list = new ArrayList<>();
        String sql = "SELECT [id_Type_Catergory_Subjec]\n"
                + "      ,[name_Catergory_Subject]\n"
                + "      ,[description]\n"
                + "  FROM [dbo].[Type_Catergory_Subject]";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Type_Catergory_Subject ty = new Type_Catergory_Subject();
                ty.setId_Type_Catergory_Subjec(rs.getInt("id_Type_Catergory_Subjec"));
                ty.setName_Catergory_Subject(rs.getString("name_Catergory_Subject"));
                ty.setDescription(rs.getString("description"));
                list.add(ty);
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<CatergorySubject> getCatergorySubjectJoinTypeCaterSubj() {
        List<CatergorySubject> list = new ArrayList<>();
        String sql = " SELECT *\n"
                + "FROM Catergory_Subject";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                CatergorySubject c = new CatergorySubject(
                        rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4));
                list.add(c);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public static void main(String[] args) {
        CatergorySubjectDAO d = new CatergorySubjectDAO();
        Type_Catergory_Subject tcs = d.getTypeCateById(1);
        System.out.println(tcs.getName_Catergory_Subject());
        List<Type_Catergory_Subject> tcs1 = d.getTypeCaterSubj();
        for (Type_Catergory_Subject type_Catergory_Subject : tcs1) {
            System.out.println(type_Catergory_Subject);
        }
    }

    public void AddNewCatergorySubject(CatergorySubject cs) {
        String sql = "INSERT INTO [dbo].[Catergory_Subject]\n"
                + "           ([name_Catergory_Subject]\n"
                + "           ,[description_Catergory_Subject]\n"
                + "           ,[id_Type_Catergory_Subjec])\n"
                + "     VALUES\n"
                + "           (?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, cs.getName_Catergory_Subject());
            st.setString(2, cs.getDescription_Catergory_Subject());
            st.setInt(3, cs.getId_Type_Catergory_Subjec());
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void AddNewSubjectDimension(Type_Catergory_Subject tcs) {
        String sql = "INSERT INTO [dbo].[Type_Catergory_Subject]\n"
                + "           ([id_Type_Catergory_Subjec]\n"
                + "           ,[name_Catergory_Subject]\n"
                + "           ,[description])\n"
                + "     VALUES\n"
                + "           (?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, tcs.getId_Type_Catergory_Subjec());
            st.setString(2, tcs.getName_Catergory_Subject());
            st.setString(3, tcs.getDescription());
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void UpdateIntoCatergorySubject(CatergorySubject cs) {
        String sql = "UPDATE [dbo].[Catergory_Subject]\n"
                + "   SET [name_Catergory_Subject] = ?\n"
                + "      ,[description_Catergory_Subject] = ?\n"
                + "      ,[id_Type_Catergory_Subjec] = ?\n"
                + " WHERE id_Catergory_Subject = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, cs.getName_Catergory_Subject());
            st.setString(2, cs.getDescription_Catergory_Subject());
            st.setInt(3, cs.getId_Type_Catergory_Subjec());
            st.setInt(4, cs.getId_Catergory_Subject());
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public CatergorySubject getCatergorySubjectById(int idca) {
        String sql = "SELECT [id_Catergory_Subject]\n"
                + "      ,[name_Catergory_Subject]\n"
                + "      ,[description_Catergory_Subject]\n"
                + "      ,[id_Type_Catergory_Subjec]\n"
                + "  FROM [dbo].[Catergory_Subject]\n"
                + "  where id_Catergory_Subject = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, idca);
            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                CatergorySubject ca = new CatergorySubject();
                ca.setId_Catergory_Subject(rs.getInt("id_Catergory_Subject"));
                ca.setName_Catergory_Subject(rs.getString("name_Catergory_Subject"));
                ca.setDescription_Catergory_Subject(rs.getString("description_Catergory_Subject"));
                ca.setId_Type_Catergory_Subjec(rs.getInt("id_Type_Catergory_Subjec"));
                return ca;
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public void deleteCatergorySubject(int id) {
        String sql = "DELETE FROM [dbo].[Catergory_Subject]\n"
                + "      WHERE id_Catergory_Subject = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            st.executeQuery();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public Type_Catergory_Subject getTypeCateById(int id) {
        String sql = "  SELECT [id_Type_Catergory_Subjec]\n"
                + "      ,[name_Catergory_Subject]\n"
                + "      ,[description]\n"
                + "  FROM [dbo].[Type_Catergory_Subject]\n"
                + "  where [id_Type_Catergory_Subjec] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Type_Catergory_Subject tcs = new Type_Catergory_Subject();
                tcs.setId_Type_Catergory_Subjec(rs.getInt("id_Type_Catergory_Subjec"));
                tcs.setName_Catergory_Subject(rs.getString("name_Catergory_Subject"));
                tcs.setDescription(rs.getString("description"));
                return tcs;
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

}
