/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import jakarta.servlet.http.Part;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Lesson;
import model.Subject;
import model.SubjectDTO;

/**
 *
 * @author PhanQuangHuy59
 */
public class SubjectDAO extends DBContext {

    public ArrayList<Subject> getSubjectByIdCourse(int id, int key) {
        ArrayList<Subject> listsub = new ArrayList<>();
        String sql = "SELECT [id_Subject],[subject_Name],[description],[numberLession],[create_Date]\n"
                + "      ,[status],[image_subject],[id_Course],[id_Catergory_Subject],[id_User]\n"
                + "  FROM [dbo].[Subject] where id_Course = ? and status = 1 ";

        if (key == 1) {
            sql = sql + "order by subject_Name";
        } else if (key == 2) {
            sql = sql + "order by subject_Name desc";
        } else if (key == 3) {
            sql = sql + "order by create_Date";
        } else if (key == 4) {
            sql = sql + "order by create_Date desc";
        } else if (key == 0) {
            sql = sql;
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            Subject sub;
            while (rs.next()) {
                sub = new Subject();
                sub.setId_Subject(rs.getInt("id_Subject"));
                sub.setSubject_Name(rs.getString("subject_Name"));
                sub.setDescription(rs.getString("description"));
                sub.setNumberLesson(rs.getInt("numberLession"));
                sub.setCreate_Date(rs.getDate("create_Date"));
                sub.setStatus(rs.getBoolean("status"));
                sub.setImage_subject(rs.getString("image_subject"));
                sub.setId_Course(rs.getInt("id_Course"));
                sub.setId_Catergory_Subject(rs.getInt("id_Catergory_Subject"));
                sub.setId_User(rs.getInt("id_User"));
                listsub.add(sub);
            }
            return listsub;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public ArrayList<Subject> getSubjectByIdCatergoryCheckbox(String[] cater) {
        ArrayList<Subject> list = new ArrayList<>();
        String sql = "SELECT  [id_Subject],[subject_Name] ,[description],[numberLession]\n"
                + "      ,[create_Date],[status],[image_subject],[id_Course],[id_Catergory_Subject],[id_User]\n"
                + "FROM [OnlineLearningSystem].[dbo].[Subject] where status = 1";
        if (cater != null && cater.length != 0) {
            for (int i = 0; i < cater.length; i++) {
                if (i == 0) {
                    sql = sql + " and  id_Catergory_Subject in(" + cater[i] + ",";
                }
                if (i > 0 && i < cater.length - 1) {
                    sql = sql + cater[i] + ",";
                }
                if (i == cater.length - 1) {
                    sql = sql + cater[i] + ")";
                }
            }
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                Subject sub = new Subject();
                sub.setId_Subject(rs.getInt("id_Subject"));
                sub.setSubject_Name(rs.getString("subject_Name"));
                sub.setDescription(rs.getString("description"));
                sub.setNumberLesson(rs.getInt("numberLession"));
                sub.setCreate_Date(rs.getDate("create_Date"));
                sub.setStatus(rs.getBoolean("status"));
                sub.setImage_subject(rs.getString("image_subject"));
                sub.setId_Course(rs.getInt("id_Course"));
                sub.setId_Catergory_Subject(rs.getInt("id_Catergory_Subject"));
                sub.setId_User(rs.getInt("id_User"));
                list.add(sub);

            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public ArrayList<Subject> getSubjectByKeyName(String nameSubject, int idcourse) {
        ArrayList<Subject> list = new ArrayList<>();
        String sql = "SELECT  [id_Subject]\n"
                + "      ,[subject_Name]\n"
                + "      ,[description]\n"
                + "      ,[numberLession]\n"
                + "      ,[create_Date]\n"
                + "      ,[status]\n"
                + "      ,[image_subject]\n"
                + "      ,[id_Course]\n"
                + "      ,[id_Catergory_Subject]\n"
                + "      ,[id_User]\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Subject]\n"
                + "  where status = 1";
        if (nameSubject != null && nameSubject.length() != 0) {
            sql = sql + " and subject_Name like'%" + nameSubject + "%'";
        }
        sql = sql + " and id_Course = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, idcourse);

            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                Subject sub = new Subject();
                sub.setId_Subject(rs.getInt("id_Subject"));
                sub.setSubject_Name(rs.getString("subject_Name"));
                sub.setDescription(rs.getString("description"));
                sub.setNumberLesson(rs.getInt("numberLession"));
                sub.setCreate_Date(rs.getDate("create_Date"));
                sub.setStatus(rs.getBoolean("status"));
                sub.setImage_subject(rs.getString("image_subject"));
                sub.setId_Course(rs.getInt("id_Course"));
                sub.setId_Catergory_Subject(rs.getInt("id_Catergory_Subject"));
                sub.setId_User(rs.getInt("id_User"));
                list.add(sub);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public ArrayList<Subject> getSubjectByCateSubjectInCourse(int idcourse, int cater) {
        ArrayList<Subject> list = new ArrayList<>();
        String sql = "SELECT  [id_Subject]\n"
                + "      ,[subject_Name]\n"
                + "      ,[description]\n"
                + "      ,[numberLession]\n"
                + "      ,[create_Date]\n"
                + "      ,[status]\n"
                + "      ,[image_subject]\n"
                + "      ,[id_Course]\n"
                + "      ,[id_Catergory_Subject]\n"
                + "      ,[id_User]\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Subject]\n"
                + "  where status = 1 and id_Course = ? and id_Catergory_Subject = ?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, idcourse);
            st.setInt(2, cater);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                Subject sub = new Subject();
                sub.setId_Subject(rs.getInt("id_Subject"));
                sub.setSubject_Name(rs.getString("subject_Name"));
                sub.setDescription(rs.getString("description"));
                sub.setNumberLesson(rs.getInt("numberLession"));
                sub.setCreate_Date(rs.getDate("create_Date"));
                sub.setStatus(rs.getBoolean("status"));
                sub.setImage_subject(rs.getString("image_subject"));
                sub.setId_Course(rs.getInt("id_Course"));
                sub.setId_Catergory_Subject(rs.getInt("id_Catergory_Subject"));
                sub.setId_User(rs.getInt("id_User"));
                list.add(sub);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public ArrayList<Integer> getCategorySubjectInCourse(int idcourse) {
        ArrayList<Integer> list = new ArrayList<>();
        String sql = "select distinct id_Catergory_Subject from Subject\n"
                + " where id_Course = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, idcourse);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int idca = rs.getInt(1);
                list.add(idca);
            }
            return list;
        } catch (SQLException e) {
        }
        return null;
    }

    public List<Subject> getListByPage(List<Subject> list,
            int start, int end) {
        List<Subject> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }

    public ArrayList<Subject> getSubjectByNameSubjectOrNameTeacher(String nameSubject, int idcourse) {
        ArrayList<Subject> list = new ArrayList<>();
        String sql = "SELECT [id_Subject],[subject_Name],[description],[numberLession],[create_Date],s.[status]\n"
                + ",[image_Subject],[id_Course],[id_Catergory_Subject],s.[id_User]\n"
                + " FROM [Subject] as s , [User] as u\n"
                + " where s.id_Course = ? and s.status = 1 and  s.id_User = u.id_User\n";
        if (nameSubject.length() != 0) {
            sql = sql + " and (u.full_Name like '%" + nameSubject + "%' or s.subject_Name like '%" + nameSubject + "%') order by subject_Name asc";
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, idcourse);

            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                Subject sub = new Subject();
                sub.setId_Subject(rs.getInt("id_Subject"));
                sub.setSubject_Name(rs.getString("subject_Name"));
                sub.setDescription(rs.getString("description"));
                sub.setNumberLesson(rs.getInt("numberLession"));
                sub.setCreate_Date(rs.getDate("create_Date"));
                sub.setStatus(rs.getBoolean("status"));
                sub.setImage_subject(rs.getString("image_subject"));
                sub.setId_Course(rs.getInt("id_Course"));
                sub.setId_Catergory_Subject(rs.getInt("id_Catergory_Subject"));
                sub.setId_User(rs.getInt("id_User"));
                list.add(sub);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public int getNumberLesson(int idSubjct) {
        String sql = "SELECT count(id_Lesson) as numberLession\n"
                + "FROM [OnlineLearningSystem].[dbo].[Lesson] where id_Subject = ?\n"
                + "group by id_Subject";
        int numberLes = 0;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, idSubjct);
            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                numberLes = rs.getInt("numberLession");
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return numberLes;
    }

    public Subject getSubjectByID(int idSubject) {

        String sql = "SELECT  [id_Subject],[subject_Name],[description],[numberLession]\n"
                + ",[create_Date],[status],[image_Subject],[id_Course],[id_Catergory_Subject],[id_User]\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Subject] where id_Subject = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, idSubject);

            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                Subject sub = new Subject();
                sub.setId_Subject(rs.getInt("id_Subject"));
                sub.setSubject_Name(rs.getString("subject_Name"));
                sub.setDescription(rs.getString("description"));
                sub.setNumberLesson(rs.getInt("numberLession"));
                sub.setCreate_Date(rs.getDate("create_Date"));
                sub.setStatus(rs.getBoolean("status"));
                sub.setImage_subject(rs.getString("image_subject"));
                sub.setId_Course(rs.getInt("id_Course"));
                sub.setId_Catergory_Subject(rs.getInt("id_Catergory_Subject"));
                sub.setId_User(rs.getInt("id_User"));
                return sub;
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public List<SubjectDTO> getListByPage1(List<SubjectDTO> list,
            int start, int end) {
        List<SubjectDTO> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }

    public ArrayList<SubjectDTO> getAllSubject(String id_User) {
        ArrayList<SubjectDTO> list = new ArrayList<>();
        String sql = "SELECT [id_Subject]\n"
                + "      ,[subject_Name]\n"
                + "      ,[description]\n"
                + "      ,[numberLession]\n"
                + "      ,s.[create_Date]\n"
                + "      ,s.[status]\n"
                + "      ,[image_Subject]\n"
                + "      ,c.course_Name,c.id_User\n"
                + "      ,cs.name_Catergory_Subject\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Subject] s join Course c on s.id_Course = c.id_Course\n"
                + "  join Catergory_Subject cs on s.id_Catergory_Subject = cs.id_Catergory_Subject and c.id_User = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id_User);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                SubjectDTO sub = new SubjectDTO();
                sub.setId_Subject(rs.getInt("id_Subject"));
                sub.setSubject_Name(rs.getString("subject_Name"));
                sub.setDescription(rs.getString("description"));
                sub.setNumberLesson(rs.getInt("numberLession"));
                sub.setCreate_Date(rs.getDate("create_Date"));
                sub.setStatus(rs.getBoolean("status"));
                sub.setImage_subject(rs.getString("image_subject"));
                sub.setCourseName(rs.getString("course_Name"));
                sub.setId_User(rs.getInt("id_User"));
                sub.setName_Catergory_Subject(rs.getString("name_Catergory_Subject"));
                list.add(sub);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }
     public ArrayList<SubjectDTO> getAllSubject() {
        ArrayList<SubjectDTO> list = new ArrayList<>();
        String sql = "SELECT [id_Subject]\n"
                + "      ,[subject_Name]\n"
                + "      ,[description]\n"
                + "      ,[numberLession]\n"
                + "      ,s.[create_Date]\n"
                + "      ,s.[status]\n"
                + "      ,[image_Subject]\n"
                + "      ,c.course_Name,c.id_User\n"
                + "      ,cs.name_Catergory_Subject\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Subject] s join Course c on s.id_Course = c.id_Course\n"
                + "  join Catergory_Subject cs on s.id_Catergory_Subject = cs.id_Catergory_Subject ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                SubjectDTO sub = new SubjectDTO();
                sub.setId_Subject(rs.getInt("id_Subject"));
                sub.setSubject_Name(rs.getString("subject_Name"));
                sub.setDescription(rs.getString("description"));
                sub.setNumberLesson(rs.getInt("numberLession"));
                sub.setCreate_Date(rs.getDate("create_Date"));
                sub.setStatus(rs.getBoolean("status"));
                sub.setImage_subject(rs.getString("image_subject"));
                sub.setCourseName(rs.getString("course_Name"));
                sub.setId_User(rs.getInt("id_User"));
                sub.setName_Catergory_Subject(rs.getString("name_Catergory_Subject"));
                list.add(sub);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public SubjectDTO getSubjectById1(int id) {
        String sql = "SELECT [id_Subject]\n"
                + "      ,[subject_Name]\n"
                + "      ,[description]\n"
                + "      ,[numberLession]\n"
                + "      ,s.[create_Date]\n"
                + "      ,s.[status]\n"
                + "      ,[image_Subject]\n"
                + "      ,c.course_Name,c.id_User\n"
                + "      ,cs.name_Catergory_Subject\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Subject] s join Course c on s.id_Course = c.id_Course\n"
                + "  join Catergory_Subject cs on s.id_Catergory_Subject = cs.id_Catergory_Subject and [id_Subject] = ?\n";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                SubjectDTO sub = new SubjectDTO();
                sub.setId_Subject(rs.getInt("id_Subject"));
                sub.setSubject_Name(rs.getString("subject_Name"));
                sub.setDescription(rs.getString("description"));
                sub.setNumberLesson(rs.getInt("numberLession"));
                sub.setCreate_Date(rs.getDate("create_Date"));
                sub.setStatus(rs.getBoolean("status"));
                sub.setImage_subject(rs.getString("image_subject"));
                sub.setCourseName(rs.getString("course_Name"));
                sub.setId_User(rs.getInt("id_User"));
                sub.setName_Catergory_Subject(rs.getString("name_Catergory_Subject"));
                return sub;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public Subject getSubjectById(int id) {
        String sql = "SELECT  [id_Subject]\n"
                + "      ,[subject_Name]\n"
                + "      ,[description]\n"
                + "      ,[numberLession]\n"
                + "      ,[create_Date]"
                + "      ,update_Date\n"
                + "      ,[status]\n"
                + "      ,[image_subject]\n"
                + "      ,[id_Course]\n"
                + "      ,[id_Catergory_Subject]\n"
                + "      ,[id_User]\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Subject] where [id_Subject] = ?\n";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Subject sub = new Subject();
                sub.setId_Subject(rs.getInt("id_Subject"));
                sub.setSubject_Name(rs.getString("subject_Name"));
                sub.setDescription(rs.getString("description"));
                sub.setNumberLesson(rs.getInt("numberLession"));
                sub.setCreate_Date(rs.getDate("create_Date"));
                sub.setUpdate_Date(rs.getDate("update_Date"));
                sub.setStatus(rs.getBoolean("status"));
                sub.setImage_subject(rs.getString("image_subject"));
                sub.setId_Course(rs.getInt("id_Course"));
                sub.setId_Catergory_Subject(rs.getInt("id_Catergory_Subject"));
                sub.setId_User(rs.getInt("id_User"));
                return sub;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public List<SubjectDTO> getSubjectByStatus(String status) {
        ArrayList<SubjectDTO> list = new ArrayList<>();
        String sql = "SELECT [id_Subject]\n"
                + "      ,[subject_Name]\n"
                + "      ,[description]\n"
                + "      ,[numberLession]\n"
                + "      ,s.[create_Date]\n"
                + "      ,s.[status]\n"
                + "      ,[image_Subject]\n"
                + "      ,c.course_Name,c.id_User\n"
                + "      ,cs.name_Catergory_Subject\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Subject] s join Course c on s.id_Course = c.id_Course\n"
                + "  join Catergory_Subject cs on s.id_Catergory_Subject = cs.id_Catergory_Subject and s.status like ? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + status + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                SubjectDTO sub = new SubjectDTO();
                sub.setId_Subject(rs.getInt("id_Subject"));
                sub.setSubject_Name(rs.getString("subject_Name"));
                sub.setDescription(rs.getString("description"));
                sub.setNumberLesson(rs.getInt("numberLession"));
                sub.setCreate_Date(rs.getDate("create_Date"));
                sub.setStatus(rs.getBoolean("status"));
                sub.setImage_subject(rs.getString("image_subject"));
                sub.setCourseName(rs.getString("course_Name"));
                sub.setId_User(rs.getInt("id_User"));
                sub.setName_Catergory_Subject(rs.getString("name_Catergory_Subject"));
                list.add(sub);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<SubjectDTO> getSubjectByCatergoryId(int id) {
        ArrayList<SubjectDTO> list = new ArrayList<>();
        String sql = "SELECT [id_Subject]\n"
                + "      ,[subject_Name]\n"
                + "      ,[description]\n"
                + "      ,[numberLession]\n"
                + "      ,s.[create_Date]\n"
                + "      ,s.[status]\n"
                + "      ,[image_Subject]\n"
                + "      ,c.course_Name,c.id_User\n"
                + "      ,cs.name_Catergory_Subject\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Subject] s join Course c on s.id_Course = c.id_Course\n"
                + "  join Catergory_Subject cs on s.id_Catergory_Subject = cs.id_Catergory_Subject and s.id_Catergory_Subject = ? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                SubjectDTO sub = new SubjectDTO();
                sub.setId_Subject(rs.getInt("id_Subject"));
                sub.setSubject_Name(rs.getString("subject_Name"));
                sub.setDescription(rs.getString("description"));
                sub.setNumberLesson(rs.getInt("numberLession"));
                sub.setCreate_Date(rs.getDate("create_Date"));
                sub.setStatus(rs.getBoolean("status"));
                sub.setImage_subject(rs.getString("image_subject"));
                sub.setCourseName(rs.getString("course_Name"));
                sub.setId_User(rs.getInt("id_User"));
                sub.setName_Catergory_Subject(rs.getString("name_Catergory_Subject"));
                list.add(sub);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public void updateSubject(String subjectID, String subjectName, String description, String numberLesson, String createdDate, String status, String courseID, String categoryID, String image) {
        String sql = "UPDATE [dbo].[Subject]\n"
                + "   SET [subject_Name] = ?\n"
                + "      ,[description] = ?\n"
                + "      ,[numberLession] = ?\n"
                + "      ,[create_Date] = ?\n"
                + "      ,[status] = ?\n"
                + "      ,[image_subject] = ?\n"
                + "      ,[id_Course] = ?\n"
                + "      ,[id_Catergory_Subject] = ?\n"
                + " WHERE id_Subject = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, subjectName);
            st.setString(2, description);
            st.setInt(3, Integer.parseInt(numberLesson));
            st.setString(4, createdDate);
            st.setBoolean(5, status.equals("1"));
            st.setString(6, image);
            st.setInt(7, Integer.parseInt(courseID));
            st.setInt(8, Integer.parseInt(categoryID));
            st.setInt(9, Integer.parseInt(subjectID));
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void deleteSubject(String id) {
        String sql = "DELETE FROM [dbo].[Subject]\n"
                + "      WHERE id_Subject = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public ArrayList<SubjectDTO> getSubjectByName(String nameSubject) {
        ArrayList<SubjectDTO> list = new ArrayList<>();
        String sql = "SELECT [id_Subject]\n"
                + "      ,[subject_Name]\n"
                + "      ,[description]\n"
                + "      ,[numberLession]\n"
                + "      ,s.[create_Date]\n"
                + "      ,s.[status]\n"
                + "      ,[image_Subject]\n"
                + "      ,c.course_Name,c.id_User\n"
                + "      ,cs.name_Catergory_Subject\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Subject] s join Course c on s.id_Course = c.id_Course\n"
                + "  join Catergory_Subject cs on s.id_Catergory_Subject = cs.id_Catergory_Subject and subject_Name like ?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + nameSubject + "%");

            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                SubjectDTO sub = new SubjectDTO();
                sub.setId_Subject(rs.getInt("id_Subject"));
                sub.setSubject_Name(rs.getString("subject_Name"));
                sub.setDescription(rs.getString("description"));
                sub.setNumberLesson(rs.getInt("numberLession"));
                sub.setCreate_Date(rs.getDate("create_Date"));
                sub.setStatus(rs.getBoolean("status"));
                sub.setImage_subject(rs.getString("image_subject"));
                sub.setCourseName(rs.getString("course_Name"));
                sub.setId_User(rs.getInt("id_User"));
                sub.setName_Catergory_Subject(rs.getString("name_Catergory_Subject"));
                list.add(sub);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public void updateIntoSubjectWithID(Subject s) {
        String sql = "UPDATE [dbo].[Subject]\n"
                + "   SET [subject_Name] = ?\n"
                + "      ,[description] = ?\n"
                + "      ,[numberLession] = ?\n"
                + "      ,[create_Date] = ?\n"
                + "      ,[update_Date] = ?\n"
                + "      ,[status] = ?\n"
                + "      ,[image_Subject] = ?\n"
                + "      ,[id_Catergory_Subject] = ?\n"
                + " WHERE [id_Subject] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, s.getSubject_Name());
            st.setString(2, s.getDescription());
            st.setInt(3, s.getNumberLesson());
            st.setDate(4, s.getCreate_Date());
            st.setDate(5, s.getUpdate_Date());
            st.setBoolean(6, s.isStatus());
            st.setString(7, s.getImage_subject());
            st.setInt(8, s.getId_Catergory_Subject());
            st.setInt(9, s.getId_Subject());
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public Date getCurrentDate() {
        LocalDate currentDate = LocalDate.now();
        return Date.valueOf(currentDate);
    }

    public List<Subject> listsubjectWithPackagePrice() {
        List<Subject> list = new ArrayList<>();
        String sql = "SELECT s.[subject_Name],\n"
                + "		p.id_Package\n"
                + "FROM [OnlineLearningSystem].[dbo].[Subject] s\n"
                + "JOIN [OnlineLearningSystem].[dbo].[PackagePrice] p ON p.[id_Course] = s.[id_Course]";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Subject s = new Subject();
                s.setSubject_Name(rs.getString("subject_Name"));
                s.setId_Course(rs.getInt("id_Package"));
                list.add(s);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Subject> listSjPPByPageNumber(int pageNumber) {
        List<Subject> list = new ArrayList<>();
        String sql = "SELECT	ROW_NUMBER() OVER (ORDER BY p.[id_Package]) AS row1,\n"
                + "		s.[subject_Name],\n"
                + "		p.id_Package,\n"
                + "		p.[id_Course]\n"
                + "FROM [OnlineLearningSystem].[dbo].[Subject] s\n"
                + "JOIN [OnlineLearningSystem].[dbo].[PackagePrice] p ON p.[id_Course] = s.[id_Course]\n"
                + "Order by s.[subject_Name]\n"
                + "OFFSET ? ROWS FETCH NEXT 5 ROWS ONLY;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, (pageNumber - 1) * 5);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Subject s = new Subject();
                s.setSubject_Name(rs.getString("subject_Name"));
                s.setId_Course(rs.getInt("id_Package"));
                s.setId_User(rs.getInt("id_Course"));
                list.add(s);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Subject> searchSubjectNameAndID(String text) {
        List<Subject> list = new ArrayList<>();
        String sql = "SELECT [id_Subject]\n"
                + "      ,[subject_Name]\n"
                + "      ,[description]\n"
                + "      ,[numberLession]\n"
                + "      ,[create_Date]\n"
                + "      ,[update_Date]\n"
                + "      ,[status]\n"
                + "      ,[image_Subject]\n"
                + "      ,[id_Course]\n"
                + "      ,[id_Catergory_Subject]\n"
                + "      ,[id_User]\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Subject]\n"
                + "  where subject_Name like ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + text + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Subject s = new Subject();
                s.setId_Subject(rs.getInt("id_Subject"));
                s.setSubject_Name(rs.getString("subject_Name"));
                s.setId_Course(rs.getInt("id_Course"));
                list.add(s);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Subject> getALLNormalSubject() {
        List<Subject> list = new ArrayList<>();
        String sql = "SELECT [id_Subject]\n"
                + "      ,[subject_Name]\n"
                + "      ,[description]\n"
                + "      ,[numberLession]\n"
                + "      ,[create_Date]\n"
                + "      ,[update_Date]\n"
                + "      ,[status]\n"
                + "      ,[image_Subject]\n"
                + "      ,[id_Course]\n"
                + "      ,[id_Catergory_Subject]\n"
                + "      ,[id_User]\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Subject]";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Subject s = new Subject();
                s.setId_Subject(rs.getInt("id_Subject"));
                s.setSubject_Name(rs.getString("subject_Name"));
                s.setDescription(rs.getString("description"));
                s.setNumberLesson(rs.getInt("numberLession"));
                s.setCreate_Date(rs.getDate("create_Date"));
                s.setUpdate_Date(rs.getDate("update_Date"));
                s.setStatus(rs.getBoolean("status"));
                s.setImage_subject(rs.getString("image_Subject"));
                s.setId_Course(rs.getInt("id_Course"));
                s.setId_Catergory_Subject(rs.getInt("id_Catergory_Subject"));
                s.setId_User(rs.getInt("id_User"));
                list.add(s);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public static void main(String[] args) {
        SubjectDAO sdao = new SubjectDAO();
        List<Subject> list = sdao.listSjPPByPageNumber(1);
        System.out.println(list.size());
        for (Subject subject : list) {
            System.out.println(subject.getId_User());
        }
    }

    public String getSubjectByIdQuestion(int id) {
        String s = new String();
        String sql = "select s.subject_Name\n"
                + "from [Subject] s, Lesson l, Quiz qi, Question q\n"
                + "where s.id_Subject=l.id_Subject and l.id_Lesson=qi.id_Lesson and qi.id_Quiz=q.id_Quiz\n"
                + "and q.id_Question=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                s = rs.getString("subject_Name");
            }
            return s;
        } catch (Exception e) {
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    public Subject getSubjectByQuizID(int qid) {
        Subject s = new Subject();
        String sql = "select s.subject_Name,s.image_Subject from [Subject] s,lesson l, Quiz q where s.id_Subject=l.id_Subject and l.id_Lesson=q.id_Lesson and q.id_Quiz=?";
        try{
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1,qid);
            ResultSet rs = st.executeQuery();
            while (rs.next()){
                s.setCreate_Date(null);
                s.setDescription("");
                s.setId_Catergory_Subject(1);
                s.setId_Course(1);
                s.setId_Subject(1);
                s.setId_User(1);
                s.setImage_subject(rs.getString("image_Subject"));
                s.setNumberLesson(3);
                s.setStatus(true);
                s.setSubject_Name(rs.getString("subject_Name"));
                s.setUpdate_Date(null);
            }
            return s;
        } catch (Exception e) {
        } finally{
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
    public Subject getSubjectByKeyNameForAddSubject(String nameSubject) {
        ArrayList<Subject> list = new ArrayList<>();
        String sql = "SELECT [id_Subject],[subject_Name],[description],[numberLession]\n"
                + "      ,[create_Date] ,[update_Date],[status],[image_Subject]\n"
                + "      ,[id_Course],[id_Catergory_Subject],[id_User]\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Subject] where subject_Name = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, nameSubject);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Subject sub = new Subject();
                sub.setId_Subject(rs.getInt("id_Subject"));
                sub.setSubject_Name(rs.getString("subject_Name"));
                sub.setDescription(rs.getString("description"));
                sub.setNumberLesson(rs.getInt("numberLession"));
                sub.setCreate_Date(rs.getDate("create_Date"));
                sub.setUpdate_Date(rs.getDate("update_Date"));
                sub.setStatus(rs.getBoolean("status"));
                sub.setImage_subject(rs.getString("image_subject"));
                sub.setId_Course(rs.getInt("id_Course"));
                sub.setId_Catergory_Subject(rs.getInt("id_Catergory_Subject"));
                sub.setId_User(rs.getInt("id_User"));
                return sub;
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public void InsertSubject(Subject c) {
        String sql = "INSERT INTO [dbo].[Subject]([subject_Name],[description],[numberLession]\n"
                + ",[create_Date],[update_Date],[status],[image_Subject],[id_Course]\n"
                + ",[id_Catergory_Subject],[id_User])\n"
                + " VALUES(?,?,0,GETDATE(),GETDATE(),?,?,?,?,?)";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, c.getSubject_Name());
            st.setString(2, c.getDescription());
            st.setBoolean(3, c.isStatus());
            st.setString(4, c.getImage_subject());
            st.setInt(5, c.getId_Course());
            st.setInt(6, c.getId_Catergory_Subject());
            st.setInt(7, c.getId_User());
            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e);
        }
    }


    /**
     * @author Naviank
     * @param id_Lesson
     * @return
     */
    public Subject getSubjectByIdLesson(int id_Lesson) {
        LessonDAO ld = new LessonDAO();
        Lesson les = ld.getLessonByIdLesson(id_Lesson);
        Subject subj = getSubjectByID(les.getId_Subject());
        return subj;
    }

    public ArrayList<Subject> getSubjectByKeyNameAdmin(String nameSubject, int idcourse) {
        ArrayList<Subject> list = new ArrayList<>();
        String sql = "SELECT  [id_Subject]\n"
                + "      ,[subject_Name]\n"
                + "      ,[description]\n"
                + "      ,[numberLession]\n"
                + "      ,[create_Date],[update_Date]\n"
                + "      ,[status]\n"
                + "      ,[image_subject]\n"
                + "      ,[id_Course]\n"
                + "      ,[id_Catergory_Subject]\n"
                + "      ,[id_User]\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Subject]\n"
                + "  where status in  (1,0)";
        if (nameSubject != null && nameSubject.length() != 0) {
            sql = sql + " and subject_Name like'%" + nameSubject + "%'";
        }
        sql = sql + " and id_Course = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, idcourse);

            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                Subject sub = new Subject();
                sub.setId_Subject(rs.getInt("id_Subject"));
                sub.setSubject_Name(rs.getString("subject_Name"));
                sub.setDescription(rs.getString("description"));
                sub.setNumberLesson(rs.getInt("numberLession"));
                sub.setCreate_Date(rs.getDate("create_Date"));
                sub.setUpdate_Date(rs.getDate("update_Date"));
                sub.setStatus(rs.getBoolean("status"));
                sub.setImage_subject(rs.getString("image_subject"));
                sub.setId_Course(rs.getInt("id_Course"));
                sub.setId_Catergory_Subject(rs.getInt("id_Catergory_Subject"));
                sub.setId_User(rs.getInt("id_User"));
                list.add(sub);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public ArrayList<Subject> getSubjectByStatus(int status, int idcourse) {
        ArrayList<Subject> list = new ArrayList<>();
        String sql = "SELECT  [id_Subject]\n"
                + "      ,[subject_Name]\n"
                + "      ,[description]\n"
                + "      ,[numberLession]\n"
                + "      ,[create_Date],[update_Date]\n"
                + "      ,[status]\n"
                + "      ,[image_subject]\n"
                + "      ,[id_Course]\n"
                + "      ,[id_Catergory_Subject]\n"
                + "      ,[id_User]\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Subject]\n"
                + "  where status = ? and id_Course = ?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, status);
            st.setInt(2, idcourse);

            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                Subject sub = new Subject();
                sub.setId_Subject(rs.getInt("id_Subject"));
                sub.setSubject_Name(rs.getString("subject_Name"));
                sub.setDescription(rs.getString("description"));
                sub.setNumberLesson(rs.getInt("numberLession"));
                sub.setCreate_Date(rs.getDate("create_Date"));
                sub.setUpdate_Date(rs.getDate("update_Date"));
                sub.setStatus(rs.getBoolean("status"));
                sub.setImage_subject(rs.getString("image_subject"));
                sub.setId_Course(rs.getInt("id_Course"));
                sub.setId_Catergory_Subject(rs.getInt("id_Catergory_Subject"));
                sub.setId_User(rs.getInt("id_User"));
                list.add(sub);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public ArrayList<Subject> getAllSubjectByIdCourse(int id, int key) {
        ArrayList<Subject> listsub = new ArrayList<>();
        String sql = "SELECT [id_Subject],[subject_Name],[description],[numberLession],[create_Date]\n"
                + "      ,[status],[image_subject],[id_Course],[id_Catergory_Subject],[id_User]\n"
                + "  FROM [dbo].[Subject] where id_Course=? ";

        if (key == 1) {
            sql = sql + "order by subject_Name";
        } else if (key == 2) {
            sql = sql + "order by subject_Name desc";
        } else if (key == 3) {
            sql = sql + "order by create_Date";
        } else if (key == 4) {
            sql = sql + "order by create_Date desc";
        } else if (key == 0) {
            sql = sql;
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            Subject sub;
            while (rs.next()) {
                sub = new Subject();
                sub.setId_Subject(rs.getInt("id_Subject"));
                sub.setSubject_Name(rs.getString("subject_Name"));
                sub.setDescription(rs.getString("description"));
                sub.setNumberLesson(rs.getInt("numberLession"));
                sub.setCreate_Date(rs.getDate("create_Date"));
                sub.setStatus(rs.getBoolean("status"));
                sub.setImage_subject(rs.getString("image_subject"));
                sub.setId_Course(rs.getInt("id_Course"));
                sub.setId_Catergory_Subject(rs.getInt("id_Catergory_Subject"));
                sub.setId_User(rs.getInt("id_User"));
                listsub.add(sub);
            }
            return listsub;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public ArrayList<Subject> getAllSubjectByKeyName(String nameSubject, int idcourse) {
        ArrayList<Subject> list = new ArrayList<>();
        String sql = "SELECT  [id_Subject]\n"
                + "      ,[subject_Name]\n"
                + "      ,[description]\n"
                + "      ,[numberLession]\n"
                + "      ,[create_Date]\n"
                + "      ,[status]\n"
                + "      ,[image_subject]\n"
                + "      ,[id_Course]\n"
                + "      ,[id_Catergory_Subject]\n"
                + "      ,[id_User]\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Subject]\n"
                + "  where status in(0,1)";
        if (nameSubject != null && nameSubject.length() != 0) {
            sql = sql + " and subject_Name like'%" + nameSubject + "%'";
        }
        sql = sql + " and id_Course = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, idcourse);

            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                Subject sub = new Subject();
                sub.setId_Subject(rs.getInt("id_Subject"));
                sub.setSubject_Name(rs.getString("subject_Name"));
                sub.setDescription(rs.getString("description"));
                sub.setNumberLesson(rs.getInt("numberLession"));
                sub.setCreate_Date(rs.getDate("create_Date"));
                sub.setStatus(rs.getBoolean("status"));
                sub.setImage_subject(rs.getString("image_subject"));
                sub.setId_Course(rs.getInt("id_Course"));
                sub.setId_Catergory_Subject(rs.getInt("id_Catergory_Subject"));
                sub.setId_User(rs.getInt("id_User"));
                list.add(sub);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public void DeleteRangeSubjectH(ArrayList<Subject> list) {
        String sql = "DELETE FROM [dbo].[Subject]\n"
                + "      WHERE id_Subject in";
        String id_Subject = "";
        for (int i = 0; i < list.size(); i++) {
            if (i == 0) {
                id_Subject = id_Subject + "(" + list.get(i).getId_Subject() + ",";
            }
            if (i != 0 && i != list.size() - 1) {
                id_Subject = id_Subject + list.get(i).getId_Subject() + ",";
            }
            if (i == (list.size() - 1)) {
                id_Subject = id_Subject + list.get(i).getId_Subject() + ")";
            }
        }
        sql += id_Subject;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }


}
