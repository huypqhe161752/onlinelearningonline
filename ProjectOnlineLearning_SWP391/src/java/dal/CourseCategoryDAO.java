/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.CatergoryCourse;
import model.Course;

/**
 *
 * @author PhanQuangHuy59
 */
public class CourseCategoryDAO extends DBContext {

    public ArrayList<CatergoryCourse> getAllCatergory() {
        ArrayList<CatergoryCourse> list = new ArrayList<>();
        String sql = "SELECT  [id_Catergory]\n"
                + "      ,[name_Catergory]\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Course_Catergory]";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                CatergoryCourse c = new CatergoryCourse();
                c.setId_Catergory_Course(rs.getInt("id_Catergory"));
                c.setName_Catergory_Course(rs.getString("name_Catergory"));
                list.add(c);
            }

            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;

    }

    public CatergoryCourse getCatergoryById(int id) {

        String sql = "SELECT  [id_Catergory]\n"
                + "      ,[name_Catergory]\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Course_Catergory] where id_Catergory = ? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                CatergoryCourse c = new CatergoryCourse();
                c.setId_Catergory_Course(rs.getInt("id_Catergory"));
                c.setName_Catergory_Course(rs.getString("name_Catergory"));
                return c;
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;

    }

    public List<CatergoryCourse> searchCategoryCourse(String txt) {
        List<CatergoryCourse> list = new ArrayList<>();
        String sql = "SELECT [id_Catergory]\n"
                + "      ,[name_Catergory]\n"
                + "  FROM [dbo].[Course_Catergory]\n"
                + "  where name_Catergory like ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + txt + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                CatergoryCourse c = new CatergoryCourse();
                c.setId_Catergory_Course(rs.getInt("id_Catergory"));
                c.setName_Catergory_Course(rs.getString("name_Catergory"));
                list.add(c);
            }
        } catch (SQLException e) {
        }
        return list;
    }

    public CatergoryCourse getCategoryCourseWithID(int id) {
        String sql = "SELECT [id_Catergory]\n"
                + "      ,[name_Catergory]\n"
                + "  FROM [dbo].[Course_Catergory]\n"
                + "  where id_Catergory = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                CatergoryCourse c = new CatergoryCourse();
                c.setId_Catergory_Course(id);
                c.setName_Catergory_Course(rs.getString("name_Catergory"));
                return c;
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public static void main(String[] args) {
        CourseCategoryDAO cdao = new CourseCategoryDAO();
        CatergoryCourse c = cdao.getCategoryCourseWithID(12);
        System.out.println(c.getName_Catergory_Course());
        ArrayList<CatergoryCourse> list = cdao.getAllCatergory();
        for (CatergoryCourse c1 : list) {
            System.out.println(c1.getId_Catergory_Course() + "  :  " + c1.getName_Catergory_Course());
        }
    }
}
