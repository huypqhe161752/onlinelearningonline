/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import model.CheckBoxLearn;
import model.Quiz;
import model.Result;

/**
 *
 * @author Naviank
 */
public class QuizResultDAO extends DBContext {

    /**
     * @author Naviank
     * @param id_User
     * @param id_Quiz
     * @return the newest result of a quiz of a user
     */
    public Result getNewestResultOfUser(int id_User, int id_Quiz) {
        String sql = "SELECT [id_Result]\n"
                + "      ,[start_Time]\n"
                + "      ,[duration]\n"
                + "      ,[doResult]\n"
                + "      ,[correctAnwser]\n"
                + "      ,[id_User]\n"
                + "      ,[id_Quiz]\n"
                + "  FROM [dbo].[Result]"
                + "  WHERE id_User = ? and id_Quiz = ?"
                + "  ORDER BY id_Result desc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id_User);
            st.setInt(2, id_Quiz);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new Result(rs.getInt("id_Result"),
                        rs.getTimestamp("start_Time"),
                        rs.getInt("duration"),
                        rs.getString("doResult"),
                        rs.getString("correctAnwser"),
                        id_User,
                        id_Quiz);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    /**
     * @author Naviank
     * @param res
     * @return
     */
    public int addNewResult(Result res) {
        String sql = "INSERT INTO [dbo].[Result]\n"
                + "           ([start_Time]\n"
                + "           ,[duration]\n"
                + "           ,[doResult]\n"
                + "           ,[correctAnwser]\n"
                + "           ,[id_User]\n"
                + "           ,[id_Quiz])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setTimestamp(1, res.getStart_Time());
            st.setInt(2, res.getDuration());
            st.setString(3, res.getDoResult());
            st.setString(4, res.getCorrectAnwser());
            st.setInt(5, res.getId_User());
            st.setInt(6, res.getId_Quiz());
            st.executeUpdate();
            CheckBoxLearnDAO cbld = new CheckBoxLearnDAO();
            QuizDAO qd = new QuizDAO();
            Quiz q = qd.selectQuizByID(res.getId_Quiz());
            System.out.println(q.getName_Quiz());
            CheckBoxLearn cbl = cbld.getIdUserAndIdLearn(res.getId_User(), q.getId_Lession());
            if (cbl == null) {
                System.out.println("null");
                float grade = gradeAResult(res);
                System.out.println(grade);
                if (grade > (q.getPasspercent() * q.getNumber_Question())) {
                    cbld.insertIntoCheckBoxOfUser(res.getId_User(), q.getId_Lession());
                }
            } else {
                System.out.println("not null");
                System.out.println(cbl.getId_User() + ", " + cbl.getId_Lesson());
            }
            return getNewestResultOfUser(res.getId_User(), res.getId_Quiz()).getId_Result();
        } catch (SQLException e) {
            System.out.println(e);
        }
        return 0;
    }

    public int addNewResultTest(Result res) {
        String sql = "INSERT INTO [dbo].[Result]\n"
                + "           ([start_Time]\n"
                + "           ,[duration]\n"
                + "           ,[doResult]\n"
                + "           ,[correctAnwser]\n"
                + "           ,[id_User]\n"
                + "           ,[id_Quiz])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setTimestamp(1, res.getStart_Time());
            st.setInt(2, res.getDuration());
            st.setString(3, res.getDoResult());
            st.setString(4, res.getCorrectAnwser());
            st.setInt(5, res.getId_User());
            st.setInt(6, res.getId_Quiz());
            st.executeUpdate();
            CheckBoxLearnDAO cbld = new CheckBoxLearnDAO();
            QuizDAO qd = new QuizDAO();
            Quiz q = qd.selectQuizByID(res.getId_Quiz());
            System.out.println(q.getName_Quiz());
            CheckBoxLearn cbl = cbld.getIdUserAndIdLearn(res.getId_User(), q.getId_Lession());
            if (cbl == null) {
                System.out.println("null");
                float grade = gradeAResult(res);
                System.out.println(grade);
                if (grade > (0.8 * q.getNumber_Question())) {
                    cbld.insertIntoCheckBoxOfUser(res.getId_User(), q.getId_Lession());
                }
            } else {
                System.out.println("not null");
                System.out.println(cbl.getId_User() + ", " + cbl.getId_Lesson());
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return 0;
    }

    /**
     * @author Naviank
     * @param quiz_id
     * @return
     */
    public List<Result> getAllResultOfQuiz(int quiz_id) {
        List<Result> list = new ArrayList<>();
        String sql = "select * from Result where id_Quiz = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, quiz_id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Result(rs.getInt("id_Result"),
                        rs.getTimestamp("start_Time"),
                        rs.getInt("duration"),
                        rs.getString("doResult"),
                        rs.getString("correctAnwser"),
                        rs.getInt("id_User"),
                        quiz_id));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    /**
     * @author Naviank
     * @param quiz_id
     * @param id_User
     * @return
     */
    public List<Result> getAllResultOfQuizOfUser(int quiz_id, int id_User) {
        List<Result> list = new ArrayList<>();
        String sql = "select * from Result where id_Quiz = ? and id_User = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, quiz_id);
            st.setInt(2, id_User);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Result res = (new Result(rs.getInt("id_Result"),
                        rs.getTimestamp("start_Time"),
                        rs.getInt("duration"),
                        rs.getString("doResult"),
                        rs.getString("correctAnwser"),
                        id_User,
                        quiz_id));
                list.add(res);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    /**
     * @author Naviank
     * @param quiz_id
     * @param id_User
     * @param month
     * @param day
     * @return
     */
    public List<Result> getAllResultOfQuizOfUserByDate(int quiz_id, int id_User, String month, String day) {
        List<Result> list = new ArrayList<>();
        String sql = "select * from Result where id_Quiz = ? and id_User = ? and DAY(start_Time) >= ? and Month(start_Time) >= ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, quiz_id);
            st.setInt(2, id_User);
            st.setInt(4, month.equals("") ? 0 : Integer.parseInt(month));
            st.setInt(3, day.equals("") ? 0 : Integer.parseInt(day));
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Result res = (new Result(rs.getInt("id_Result"),
                        rs.getTimestamp("start_Time"),
                        rs.getInt("duration"),
                        rs.getString("doResult"),
                        rs.getString("correctAnwser"),
                        id_User,
                        quiz_id));
                list.add(res);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    /**
     * @author Naviank
     * @param quiz_id
     * @param id_User
     * @param bymark
     * @param order
     * @return
     */
    public List<Result> getAllResultOfQuizOfUser(int quiz_id, int id_User, int bymark, boolean order) {
        List<Result> list = new ArrayList<>();
        String sql = "select * from Result where id_Quiz = ? and id_User = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, quiz_id);
            st.setInt(2, id_User);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Result res = (new Result(rs.getInt("id_Result"),
                        rs.getTimestamp("start_Time"),
                        rs.getInt("duration"),
                        rs.getString("doResult"),
                        rs.getString("correctAnwser"),
                        id_User,
                        quiz_id));
                list.add(res);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        if (order) {
            return list;
        } else {
            return reverseResList(list);
        }
    }

    /**
     * @author Naviank
     * @param list
     * @return
     */
    public List<Result> reverseResList(List<Result> list) {
        List<Result> newlist = new ArrayList<>();
        for (Result r : list) {
            newlist.add(r);
        }
        return newlist;
    }

    /**
     * @author Naviank
     * @param res_id
     * @return
     */
    public Result getResultByID(int res_id) {
        String sql = "SELECT [id_Result]\n"
                + "      ,[start_Time]\n"
                + "      ,[duration]\n"
                + "      ,[doResult]\n"
                + "      ,[correctAnwser]\n"
                + "      ,[id_User]\n"
                + "      ,[id_Quiz]\n"
                + "  FROM [dbo].[Result]"
                + "  WHERE id_Result = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, res_id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new Result(res_id,
                        rs.getTimestamp("start_Time"),
                        rs.getInt("duration"),
                        rs.getString("doResult"),
                        rs.getString("correctAnwser"),
                        rs.getInt("id_User"), rs.getInt("id_Quiz"));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    /**
     * @author Naviank
     * @param res_id
     * @return
     */
    public float gradeAResultByIdResult(int res_id) {
        float grade = 0;
        Result r = getResultByID(res_id);
        String dstr = r.getDoResult();
        String cstr = r.getCorrectAnwser();
        String[] darr = dstr.split(",");
        String[] carr = cstr.split(",");
        for (int i = 0; i < darr.length; i++) {
            grade += Utils.gradeAQuestion(darr[i], carr[i]);
        }
        return grade;
    }

    /**
     * @author Naviank
     * @param r
     * @return
     */
    public float gradeAResult(Result r) {
        float grade = 0;
        String dstr = r.getDoResult();
        String cstr = r.getCorrectAnwser();
        String[] darr = dstr.split(",");
        String[] carr = cstr.split(",");
        for (int i = 0; i < darr.length; i++) {
            grade += Utils.gradeAQuestion(darr[i], carr[i]);
        }
        return grade;
    }

    /**
     * @author Naviank
     * @param quiz_id
     * @return
     */
    public float getBestResultOfQuiz(int quiz_id) {
        String sql = "select * from Result where id_Quiz = ?";
        float max = 0;
//        Result maxr = new Result();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, quiz_id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                float tmp_grade = gradeAResultByIdResult(rs.getInt("id_Result"));
                if (tmp_grade > max) {
                    max = tmp_grade;
//                    maxr = new Result(rs.getInt("id_Result"),
//                            rs.getTime("start_Time"),
//                            rs.getDate("do_Date"),
//                            rs.getInt("duration"),
//                            rs.getString("doResult"),
//                            rs.getString("correctAnwser"),
//                            rs.getInt("id_User"),
//                            quiz_id);
                }
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return max;
    }

    /**
     * @author Naviank
     * @param quiz_id
     * @param id_User
     * @return
     */
    public float getBestResultOfQuizOfUser(int quiz_id, int id_User) {
        String sql = "select * from Result where id_Quiz = ? and id_User = ?";
        float max = 0;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, quiz_id);
            st.setInt(2, id_User);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                float tmp_grade = gradeAResultByIdResult(rs.getInt("id_Result"));
                if (tmp_grade > max) {
                    max = tmp_grade;
                }
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return max;
    }

    /**
     * @author Naviank
     * @param id_quiz
     * @return
     */
    public int countAttemptTimeOfQuiz(int id_quiz) {
        int res = 0;
        String sql = "SELECT count(*)\n"
                + "FROM [dbo].[Result]\n"
                + "WHERE id_Quiz = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id_quiz);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                res = rs.getInt(1);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return res;
    }

    /**
     * @author Naviank
     * @param id_quiz
     * @param id_User
     * @return
     */
    public int countAttemptTimeOfQuizOfUser(int id_quiz, int id_User) {
        int res = 0;
        String sql = "SELECT count(*)\n"
                + "FROM [dbo].[Result]\n"
                + "WHERE id_Quiz = ? and id_User = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id_quiz);
            st.setInt(2, id_User);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                res = rs.getInt(1);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return res;
    }

    /**
     * @author Naviank
     * @param id_quiz
     * @return
     */
    public float calcTakePassRatioOfQuiz(int id_quiz) {
        QuizDAO qd = new QuizDAO();
        Quiz quiz = qd.selectQuizByID(id_quiz);
        List<Result> listres = getAllResultOfQuiz(id_quiz);
        if (listres.isEmpty()) {
            return 0;
        }
        int take = listres.size();
        int pass = 0;
        float grade = (float) quiz.getPasspercent() * quiz.getNumber_Question() / 100;
        for (Result r : listres) {
            float tmp = gradeAResult(r);
            if (tmp > grade) {
                pass += 1;
            }
        }
        return (float) pass / take;
    }

    /**
     *
     * @param res_id
     * @return either (1) the row count for SQL Data Manipulation Language (DML)
     * statements or (2) 0 for SQL statements that return nothing
     */
    public int deleteResultByID(int res_id) {
        String sql = "DELETE FROM [dbo].[Result]\n"
                + "      WHERE id_Result = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, res_id);
            return st.executeUpdate();
        } catch (SQLException e) {
            e.getErrorCode();
        }
        return 0;
    }

    /**
     * @author Naviank
     * @param id_Quiz
     * @return the number of records deleted from table Question in database
     *
     */
    public int deleteResultOfQuiz(int id_Quiz) {
        int count = 0;
        List<Result> list = getAllResultOfQuiz(id_Quiz);
        for (Result q : list) {
            deleteResultByID(q.getId_Result());
            count++;
        }
        return count;
    }

    public ArrayList<Result> getResultByRangeIdQuiz(ArrayList<Quiz> list) {
        String id_Subject = "";
        ArrayList<Result> listResult = new ArrayList<>();
        String sql = "SELECT  [id_Result],[start_Time],[duration]\n"
                + ",[doResult],[correctAnwser],[id_User],[id_Quiz]\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Result] where id_Quiz in";
        for (int i = 0; i < list.size(); i++) {
            if (i == 0) {
                id_Subject = id_Subject + "(" + list.get(i).getId_Quiz() + ",";
            }
            if (i != 0 && i != list.size() - 1) {
                id_Subject = id_Subject + list.get(i).getId_Quiz() + ",";
            }
            if (i == (list.size() - 1)) {
                id_Subject = id_Subject + list.get(i).getId_Quiz() + ")";
            }
        }
        sql += id_Subject;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Result r = new Result();
                r.setId_Result(rs.getInt("id_Result"));
                r.setStart_Time(rs.getTimestamp("start_Time"));
                r.setDuration(rs.getInt("duration"));
                r.setDoResult(rs.getString("doResult"));
                r.setCorrectAnwser(rs.getString("correctAnwser"));
                r.setId_User(rs.getInt("id_User"));
                r.setId_Quiz(rs.getInt("id_Quiz"));
                listResult.add(r);
            }
            return listResult;
        } catch (SQLException e) {
            System.out.println(e);

        }
        return null;
    }

    public void DeleteQuizRangeH(ArrayList<Quiz> list) {
        String id_Subject = "";

        String sql = "DELETE FROM [dbo].[Result]\n"
                + "      WHERE id_Quiz in";
        for (int i = 0; i < list.size(); i++) {
            if (i == 0) {
                id_Subject = id_Subject + "(" + list.get(i).getId_Quiz() + ",";
            }
            if (i != 0 && i != list.size() - 1) {
                id_Subject = id_Subject + list.get(i).getId_Quiz() + ",";
            }
            if (i == (list.size() - 1)) {
                id_Subject = id_Subject + list.get(i).getId_Quiz() + ")";
            }
        }
        sql += id_Subject;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }

    }

    public static void main(String[] args) {
        QuizResultDAO qrd = new QuizResultDAO();
//        Result r = qrd.getNewestResultOfUser(1, 3);
//        System.out.println(r.getStart_Time());
//        QuizDAO qd = new QuizDAO();
//        Quiz q = qd.selectQuizByID(3);
//        Result r = new Result(new Timestamp(new java.util.Date().getTime()), 10,
//                "A***,AB**,*BC*,**C*,***D,A***,*B**,*B**,***D",
//                "A***,AB**,*BC*,**C*,***D,A***,*B**,*B**,***D", 1, 3);
//        System.out.println(qrd.addNewResult(r));
//        List<Result> listres = qrd.getAllResultOfQuizOfUserByDate(2, 8, "", "");
//
//        float arr[] = new float[listres.size()];
//        for (int i = 0; i < arr.length; i++) {
//            arr[i] = qrd.gradeAResultByIdResult(listres.get(i).getId_Result());
//        }
//        System.out.println(listres.size());
        System.out.println(qrd.calcTakePassRatioOfQuiz(5));
    }
}
