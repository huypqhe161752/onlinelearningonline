/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Lesson;
import model.Quiz;
import model.Subject;
import model1.Lessons;

/**
 *
 * @author PhanQuangHuy59
 */
public class LessonDAO extends DBContext {

    public List<Lesson> getLessonBySubjectId(int id) {
        List<Lesson> list = new ArrayList<>();
        String sql = "SELECT [id_Lesson]\n"
                + "      ,[name_Lesson]\n"
                + "      ,[content_Lesson]\n"
                + "      ,[video_Link]\n"
                + "      ,[status]\n"
                + "      ,[id_Subject]\n"
                + "      ,[id_Type]\n"
                + "  FROM [dbo].[Lesson]\n"
                + "  where id_Subject = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Lesson l = new Lesson();
                l.setId_Lesson(rs.getInt("id_Lesson"));
                l.setName_Lesson(rs.getString("name_Lesson"));
                l.setContent_Lesson(rs.getString("content_Lesson"));
                l.setVideo_Link(rs.getString("video_Link"));
                l.setStatus(rs.getBoolean("status"));
                l.setId_Subject(id);
                l.setId_Type(rs.getInt("id_Type"));
                list.add(l);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public Lesson getLessonByIdLesson(int id) {
        Lesson l = new Lesson();
        String sql = "SELECT [id_Lesson]\n"
                + "      ,[name_Lesson]\n"
                + "      ,[content_Lesson]\n"
                + "      ,[video_Link]\n"
                + "      ,[status]\n"
                + "      ,[id_Subject]\n"
                + "      ,[id_Type]\n"
                + "      ,[create_Date]\n"
                + "      ,[update_Date]\n"
                + "  FROM [dbo].[Lesson]\n"
                + "  where id_Lesson = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                l.setId_Lesson(rs.getInt("id_Lesson"));
                l.setName_Lesson(rs.getString("name_Lesson"));
                l.setContent_Lesson(rs.getString("content_Lesson"));
                l.setVideo_Link(rs.getString("video_Link"));
                l.setStatus(rs.getBoolean("status"));
                l.setId_Subject(rs.getInt("id_Subject"));
                l.setId_Type(rs.getInt("id_Type"));
                l.setCreate_Date(rs.getDate("create_Date"));
                l.setUpdate_Date(rs.getDate("update_Date"));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return l;
    }

    public Lessons getLessonByIdLesson1(String id) {
        Lessons l = new Lessons();
        String sql = "SELECT l.id_Lesson\n"
                + "      ,l.name_Lesson\n"
                + "      ,l.content_Lesson\n"
                + "      ,l.video_Link\n"
                + "      ,l.status\n"
                + "      ,l.id_Subject,\n"
                + "	  a.subject_Name\n"
                + "      ,l.id_Type,\n"
                + "	  c.name_TypeLesson\n"
                + "      ,l.create_Date\n"
                + "      ,l.update_Date\n"
                + "  FROM [Lesson] l \n"
                + "  inner join [Subject] a on l.id_Subject = a.id_Subject\n"
                + "  inner join TypeLesson c on l.id_Type = c.id_Type and l.id_Lesson =?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                l.setId_Lesson(rs.getInt("id_Lesson"));
                l.setName_Lesson(rs.getString("name_Lesson"));
                l.setContent_Lesson(rs.getString("content_Lesson"));
                l.setVideo_Link(rs.getString("video_Link"));
                l.setStatus(rs.getBoolean("status"));
                l.setId_Subject(rs.getInt("id_Subject"));
                l.setName_Subject(rs.getString("subject_Name"));
                l.setId_Type(rs.getInt("id_Type"));
                l.setName_TypeLesson(rs.getString("name_TypeLesson"));
                l.setCreate_Date(rs.getDate("create_Date"));
                l.setUpdate_Date(rs.getDate("update_Date"));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return l;
    }


    public List<Lessons> getAllLesson() {
        List<Lessons> list = new ArrayList<>();
        String sql = "SELECT l.id_Lesson\n"
                + "      ,l.name_Lesson\n"
                + "      ,l.content_Lesson\n"
                + "      ,l.video_Link\n"
                + "      ,l.status\n"
                + "      ,l.id_Subject,\n"
                + "	  a.subject_Name\n"
                + "      ,l.id_Type,\n"
                + "	  c.name_TypeLesson\n"
                + "      ,l.create_Date\n"
                + "      ,l.update_Date\n"
                + "  FROM [Lesson] l \n"
                + "  inner join [Subject] a on l.id_Subject = a.id_Subject\n"
                + "  inner join TypeLesson c on l.id_Type = c.id_Type";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Lessons l = new Lessons();
                l.setId_Lesson(rs.getInt("id_Lesson"));
                l.setName_Lesson(rs.getString("name_Lesson"));
                l.setContent_Lesson(rs.getString("content_Lesson"));
                l.setVideo_Link(rs.getString("video_Link"));
                l.setStatus(rs.getBoolean("status"));
                l.setId_Subject(rs.getInt("id_Subject"));
                l.setName_Subject(rs.getString("subject_Name"));
                l.setId_Type(rs.getInt("id_Type"));
                l.setName_TypeLesson(rs.getString("name_TypeLesson"));
                l.setCreate_Date(rs.getDate("create_Date"));
                l.setUpdate_Date(rs.getDate("update_Date"));
                list.add(l);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }



    public List<Lessons> getAllLessonByIdSubject(String id) {
        List<Lessons> list = new ArrayList<>();
        String sql = "SELECT l.id_Lesson\n"
                + "           ,l.name_Lesson\n"
                + "              ,l.content_Lesson\n"
                + "                ,l.video_Link\n"
                + "                 ,l.status\n"
                + "                 ,l.id_Subject,\n"
                + "              a.subject_Name\n"
                + "                ,l.id_Type,\n"
                + "               c.name_TypeLesson\n"
                + "               ,l.create_Date\n"
                + "                ,l.update_Date\n"
                + "            FROM [Lesson] l \n"
                + "             inner join [Subject] a on l.id_Subject = a.id_Subject\n"
                + "            inner join TypeLesson c on l.id_Type = c.id_Type and l.id_Subject = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Lessons l = new Lessons();
                l.setId_Lesson(rs.getInt("id_Lesson"));
                l.setName_Lesson(rs.getString("name_Lesson"));
                l.setContent_Lesson(rs.getString("content_Lesson"));
                l.setVideo_Link(rs.getString("video_Link"));
                l.setStatus(rs.getBoolean("status"));
                l.setId_Subject(rs.getInt("id_Subject"));
                l.setName_Subject(rs.getString("subject_Name"));
                l.setId_Type(rs.getInt("id_Type"));
                l.setName_TypeLesson(rs.getString("name_TypeLesson"));
                l.setCreate_Date(rs.getDate("create_Date"));
                l.setUpdate_Date(rs.getDate("update_Date"));
                list.add(l);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }
public List<Lessons> getListByPage1(List<Lessons> list,
            int start, int end) {
        List<Lessons> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }
  public List<Lessons> getSearch(String search, String idtype, String status, String id) {
    List<Lessons> list = new ArrayList<>();
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT l.id_Lesson, l.name_Lesson, l.content_Lesson, l.video_Link, l.status, l.id_Subject, ");
    sql.append("a.subject_Name, l.id_Type, c.name_TypeLesson, l.create_Date, l.update_Date ");
    sql.append("FROM [Lesson] l ");
    sql.append("INNER JOIN [Subject] a ON l.id_Subject = a.id_Subject ");
    sql.append("INNER JOIN TypeLesson c ON l.id_Type = c.id_Type ");
    sql.append("WHERE 1 = 1 ");

    if (search != null && !search.trim().isEmpty()) {
        sql.append("AND l.name_lesson LIKE ? ");
    }

    if (id != null && !id.trim().isEmpty()) {
        sql.append("AND l.id_Subject = ? ");
    }

    if (idtype != null && !idtype.trim().isEmpty()) {
        sql.append("AND l.id_Type = ? ");
    }

    if (status != null && !status.trim().isEmpty()) {
        sql.append("AND l.status = ? ");
    }

    try {
        PreparedStatement st = connection.prepareStatement(sql.toString());

        int parameterIndex = 1;

        if (search != null && !search.trim().isEmpty()) {
            st.setString(parameterIndex, "%" + search + "%");
            parameterIndex++;
        }

        if (id != null && !id.trim().isEmpty()) {
            st.setString(parameterIndex, id);
            parameterIndex++;
        }

        if (idtype != null && !idtype.trim().isEmpty()) {
            st.setString(parameterIndex, idtype);
            parameterIndex++;
        }

        if (status != null && !status.trim().isEmpty()) {
            st.setString(parameterIndex, status);
            parameterIndex++;
        }

        ResultSet rs = st.executeQuery();

        while (rs.next()) {
            Lessons l = new Lessons();
            l.setId_Lesson(rs.getInt("id_Lesson"));
            l.setName_Lesson(rs.getString("name_Lesson"));
            l.setContent_Lesson(rs.getString("content_Lesson"));
            l.setVideo_Link(rs.getString("video_Link"));
            l.setStatus(rs.getBoolean("status"));
            l.setId_Subject(rs.getInt("id_Subject"));
            l.setName_Subject(rs.getString("subject_Name"));
            l.setId_Type(rs.getInt("id_Type"));
            l.setName_TypeLesson(rs.getString("name_TypeLesson"));
            l.setCreate_Date(rs.getDate("create_Date"));
            l.setUpdate_Date(rs.getDate("update_Date"));
            list.add(l);
        }

    } catch (SQLException e) {
        System.out.println(e);
    }

    return list;
}
   public static void main(String[] args) {
    LessonDAO dao = new LessonDAO();
    String search = "Java";
    String name_TypeLesson = null;
    String status = null;
    String id = "1";
    List<Lessons> result = dao.getSearch(search, name_TypeLesson, status, id);
    for (Lessons lesson : result) {
        System.out.println(lesson.getName_Lesson());
    }
}

   public void updateLesson(String id_Lesson, String name_Lesson, String content_Lesson, String video_Link, boolean status, String id_Subject, String id_Type, String create_Date, String update_Date) {
    String sql = "UPDATE [dbo].[Lesson]\n"
            + "   SET [name_Lesson] = ?\n"
            + "      ,[content_Lesson] = ?\n"
            + "      ,[video_Link] = ?\n"
            + "      ,[status] = ?\n" // Sửa lại câu lệnh SQL để cập nhật giá trị status
            + "      ,[id_Subject] = ?\n"
            + "      ,[id_Type] = ?\n"
            + "      ,[create_Date] = ?\n"
            + "      ,[update_Date] = ?\n"
            + " WHERE id_Lesson = ?";
    
    int statusInt = status ? 1 : 0; // Chuyển đổi giá trị boolean sang số nguyên tương ứng
    
    try {
        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setString(1, name_Lesson);
        ps.setString(2, content_Lesson);
        ps.setString(3, video_Link);
        ps.setInt(4, statusInt); // Sử dụng phương thức setInt() để đặt giá trị của tham số status
        ps.setString(5, id_Subject);
        ps.setString(6, id_Type);
        ps.setString(7, create_Date);
        ps.setString(8, update_Date);
        ps.setString(9, id_Lesson);
        ps.executeUpdate();
    } catch (SQLException e) {
        e.printStackTrace();
    }
}

   public void updateStatusLesson(String id_Lesson , Boolean status) {
        String sql = "UPDATE [dbo].[Lesson]\n"
                + "   SET [status] = ?\n"
                + " WHERE id_Lesson = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setBoolean(1, status);
            ps.setString(2, id_Lesson);

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public String getLessonByIdquestion(int id) {
        String lesson = null;
        String sql = "select l.name_lesson \n"
                + "from Lesson l, Quiz qi, Question q\n"
                + "where l.id_Lesson=qi.id_Lesson and qi.id_Quiz=q.id_Quiz\n"
                + "and q.id_Question=?";
        try{
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1,id);
            ResultSet rs = st.executeQuery();
            while(rs.next()) {
            lesson=rs.getString("name_lesson");}
            return lesson;
        } catch (Exception e) {
        } finally{
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    public String getLessonNameByQuizID(int qid) {
        String lesson = null;
        String sql = "select l.name_Lesson from Lesson l, Quiz q where q.id_Lesson=l.id_Lesson and q.id_Quiz=?";
        try{
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1,qid);
            ResultSet rs = st.executeQuery();
            while(rs.next()) {
            lesson=rs.getString("name_lesson");}
            return lesson;
        } catch (Exception e) {
        } finally{
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
    public ArrayList<Lesson> getLessonByRangeIdSubject(ArrayList<Subject> list) {
        String id_Subject = "";
        ArrayList<Lesson> listless = new ArrayList<>();
        String sql = "SELECT  [id_Lesson],[name_Lesson],[content_Lesson]\n"
                + "      ,[video_Link],[status],[id_Subject],[id_Type]\n"
                + "      ,[create_Date],[update_Date]\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Lesson]\n"
                + "  where id_Subject in";
        for (int i = 0; i < list.size(); i++) {
            if (i == 0) {
                id_Subject = id_Subject + "(" + list.get(i).getId_Subject() + ",";
            }
            if (i != 0 && i != list.size() - 1) {
                id_Subject = id_Subject + list.get(i).getId_Subject() + ",";
            }
            if (i == (list.size() - 1)) {
                id_Subject = id_Subject + list.get(i).getId_Subject() + ")";
            }
        }
        sql += id_Subject;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Lesson l = new Lesson();
                l.setId_Lesson(rs.getInt("id_Lesson"));
                l.setName_Lesson("name_Lesson");
                l.setContent_Lesson("content_Lesson");
                l.setVideo_Link("video_Link");
                l.setStatus(rs.getBoolean("status"));
                l.setId_Subject(rs.getInt("id_Subject"));
                l.setId_Type(rs.getInt("id_Type"));
                l.setCreate_Date(rs.getDate("create_Date"));
                l.setUpdate_Date(rs.getDate("update_Date"));
                listless.add(l);
            }
            return listless;
        } catch (SQLException e) {
            System.out.println(e);

        }
        return null;
    }

    public void DeleteLessonRangeH(ArrayList<Subject> list) {
        String id_Subject = "";

        String sql = "DELETE FROM [dbo].[Lesson]\n"
                + "      WHERE id_Subject in";
        for (int i = 0; i < list.size(); i++) {
            if (i == 0) {
                id_Subject = id_Subject + "(" + list.get(i).getId_Subject()+ ",";
            }
            if (i != 0 && i != list.size() - 1) {
                id_Subject = id_Subject + list.get(i).getId_Subject()+ ",";
            }
            if (i == (list.size() - 1)) {
                id_Subject = id_Subject + list.get(i).getId_Subject()+ ")";
            }
        }
        sql += id_Subject;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }

    }
     public void insertLesson(String nameLesson, String content, String video, 
            Boolean status, String idSubject, String idtype, String createDate, String updateDate) throws SQLException {
        String sql = "INSERT INTO [dbo].[Lesson] "
                + "([name_Lesson], [content_Lesson], [video_Link], [status], [id_Subject], [id_Type], [create_Date], [update_Date]) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
 try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, nameLesson);
            ps.setString(2, content);
            ps.setString(3, video);
            ps.setBoolean(4, status);
            ps.setString(5, idSubject);
            ps.setString(6, idtype);
            ps.setString(7, createDate);
            ps.setString(8, updateDate);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
