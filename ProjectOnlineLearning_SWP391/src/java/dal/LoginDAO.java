/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.User;

/**
 *
 * @author vuduy
 */
public class LoginDAO extends DBContext {

    public User checkLogin(String email, String password) {
        String query = "SELECT [id_User]\n"
                + "      ,[email]\n"
                + "      ,[password]\n"
                + "      ,[full_Name]\n"
                + "      ,[phone_Number]\n"
                + "      ,[gender]\n"
                + "      ,[avartar]\n"
                + "      ,[address]\n"
                + "      ,[status]\n"
                + "      ,[id_Role]\n"
                + "  FROM [dbo].[User]\n"
                + "  where email = ? and password = ?";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setString(1, email);
            st.setString(2, password);
            ResultSet rs = st.executeQuery();   // thuc thi cau lenh
            while (rs.next()) {
                User u = new User();
                u.setId_User(rs.getInt("id_User"));
                u.setEmail(rs.getString("email"));
                u.setPassword(rs.getString("password"));
                u.setFull_Name(rs.getString("full_Name"));
                u.setPhone_Number(rs.getString("phone_Number"));
                u.setGender(rs.getBoolean("gender"));
                u.setAvatar(rs.getString("avartar"));
                u.setAddress(rs.getString("address"));
                u.setStatus(rs.getBoolean("status"));
                u.setId_role(rs.getInt("id_role"));
                return u;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public static void main(String[] args) {
        LoginDAO l = new LoginDAO();
        User u = l.checkLogin("customer@gmail.com", "123a");
        System.out.println(u.getFull_Name());
    }

}
