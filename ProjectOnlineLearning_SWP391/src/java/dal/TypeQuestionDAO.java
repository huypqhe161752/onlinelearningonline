/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Type_Question;

/**
 *
 * @author Admin
 */
public class TypeQuestionDAO extends DBContext{
    public ArrayList<Type_Question> getALlType() {
        ArrayList<Type_Question> list = new ArrayList<>();
        String sql = "select * from type_question";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while(rs.next()){
                Type_Question tq = new Type_Question(rs.getInt(1),rs.getString(2),rs.getString(3));
                list.add(tq);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(TypeQuestionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
}
