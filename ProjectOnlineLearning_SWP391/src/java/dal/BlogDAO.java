package dal;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import model.Blog;

/**
 *
 * @author PhanQuangHuy59
 */
public class BlogDAO extends DBContext {

    /**
     * @author Naviank
     * @return List of all Blogs in database, the latest updated post will
     * appear first
     */
    /**
     * @author Naviank
     * @return List of all Blogs in database, the latest updated post will
     * appear first
     */
    public List<Blog> getAllBlogs() {
        List<Blog> list = new ArrayList<>();
        String sql = "select * from Blog";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Blog b = new Blog(rs.getInt(1),
                        rs.getString("thumbnail_Blog"),
                        rs.getString("title"),
                        rs.getString("content_Blog"),
                        rs.getString("brief_Infor_Blog"),
                        rs.getDate("update_Date"),
                        rs.getInt("id_User"),
                        rs.getInt("view"),
                        rs.getInt("id_CatergoryBlog"),
                        rs.getDate("create_Date"));
                list.add(b);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Blog> searchByStringInput(String search) {
        List<Blog> list = new ArrayList<>();
        String sql = "SELECT *\n"
                + "  FROM [dbo].[Blog]\n"
                + "  where [title] like ? or [content_Blog] like ?\n"
                + "  order by [view] ";
        if (search == null || "".equals(search)) {
            return getAllBlogs();
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + search + "%");
            st.setString(2, "%" + search + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Blog b = new Blog();
                b.setId_Blog(rs.getInt("id_Blog"));
                b.setThumbnail_Blog(rs.getString("thumbnail_Blog"));
                b.setTitle(rs.getString("title"));
                b.setContent_Blog(rs.getString("content_Blog"));
                b.setBrief_Infor_Blog(rs.getString("brief_Infor_Blog"));
                b.setUpdate_Date(rs.getDate("update_Date"));
                b.setId_User(rs.getInt("id_User"));
                b.setView(rs.getInt("view"));
                b.setId_CatergoryBlog(rs.getInt("id_CatergoryBlog"));
                b.setCreate_Date(rs.getDate("create_Date"));
                list.add(b);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public String getNameOfDayOfBlog(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        switch (dayOfWeek) {
            case 1:
                return "Sunday";
            case 2:
                return "Monday";
            case 3:
                return "Tuesday";
            case 4:
                return "Wednesday";
            case 5:
                return "Thursday";
            case 6:
                return "Friday";
            case 7:
                return "Saturday";
        }
        return null;
    }

    public List<Blog> searchByBlogTitle(String search) {
        List<Blog> list = new ArrayList<>();
        String sql = "SELECT *\n"
                + "  FROM [dbo].[Blog]\n"
                + "  where [title] like ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + search + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Blog b = new Blog();
                b.setId_Blog(rs.getInt("id_Blog"));
                b.setThumbnail_Blog(rs.getString("thumbnail_Blog"));
                b.setTitle(rs.getString("title"));
                b.setContent_Blog(rs.getString("content_Blog"));
                b.setBrief_Infor_Blog(rs.getString("brief_Infor_Blog"));
                b.setUpdate_Date(rs.getDate("update_Date"));
                b.setId_User(rs.getInt("id_User"));
                b.setView(rs.getInt("view"));
                b.setId_CatergoryBlog(rs.getInt("id_CatergoryBlog"));
                b.setCreate_Date(rs.getDate("create_Date"));
                list.add(b);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    /**
     * @author Naviank
     * @param desc
     * @param search
     * @return List of all Blogs in database, order by view in descending or
     * ascending order
     */
    public List<Blog> getAllBlogsByView(boolean desc, String search) {
        List<Blog> list = new ArrayList<>();
        String sql = "select * from Blog\n"
                + "  where [title] like ? or [content_Blog] like ?\n"
                + "  order by Blog.[view] " + (desc ? "desc" : "asc");
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + search + "%");
            st.setString(2, "%" + search + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Blog b = new Blog(rs.getInt(1),
                        rs.getString("thumbnail_Blog"),
                        rs.getString("title"),
                        rs.getString("content_Blog"),
                        rs.getString("brief_Infor_Blog"),
                        rs.getDate("update_Date"),
                        rs.getInt("id_User"),
                        rs.getInt("view"),
                        rs.getInt("id_CatergoryBlog"),
                        rs.getDate("create_Date"));
                list.add(b);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    /**
     * @author Naviank
     * @param id
     * @param desc
     * @return List of all Blogs belong to a specific category
     */
    public List<Blog> getBlogByCategory(int id, boolean desc) {
        List<Blog> list = new ArrayList<>();
        String sql;
        if (id != 0) {
            sql = "select b.*, c.name_CatergoryBlog from Blog b, CatergoryBlog c \n"
                    + "where b.id_CatergoryBlog = c.id_CatergoryBlog \n"
                    + "and c.id_CatergoryBlog = ? \n"
                    + "order by b.update_Date " + (desc ? "desc" : "asc");
//            st.setInt(1, id);
        } else {
            sql = "select b.*, c.name_CatergoryBlog from Blog b, CatergoryBlog c \n"
                    + "where b.id_CatergoryBlog = c.id_CatergoryBlog \n"
                    + "order by b.update_Date " + (desc ? "desc" : "asc");
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            if (id != 0) {
                st.setInt(1, id);
            }
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Blog b = new Blog(rs.getInt(1),
                        rs.getString("thumbnail_Blog"),
                        rs.getString("title"),
                        rs.getString("content_Blog"),
                        rs.getString("brief_Infor_Blog"),
                        rs.getDate("update_Date"),
                        rs.getInt("id_User"),
                        rs.getInt("view"),
                        rs.getInt("id_CatergoryBlog"),
                        rs.getDate("create_Date"));
                list.add(b);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    /**
     * @author Naviank
     * @param desc
     * @param search
     * @return List of all Blogs in database, order by update date in
     * descending/ascending order
     */
    public List<Blog> getAllBlogsByUpdateDate(boolean desc, String search) {
        List<Blog> list = new ArrayList<>();
        String sql = "select * from blog\n"
                + "  where [title] like ? or [content_Blog] like ?\n"
                + "order by update_Date " + (desc ? "desc" : "asc");
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + search + "%");
            st.setString(2, "%" + search + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Blog b = new Blog(rs.getInt(1),
                        rs.getString("thumbnail_Blog"),
                        rs.getString("title"),
                        rs.getString("content_Blog"),
                        rs.getString("brief_Infor_Blog"),
                        rs.getDate("update_Date"),
                        rs.getInt("id_User"),
                        rs.getInt("view"),
                        rs.getInt("id_CatergoryBlog"),
                        rs.getDate("create_Date"));
                list.add(b);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    /**
     * @author Naviank
     * @param desc
     * @return List of all Blogs in database, order by update date in
     * descending/ascending order
     */
    public List<Blog> get8BlogsByUpdateDate(boolean desc) {
        List<Blog> list = new ArrayList<>();
        String sql = "select * from blog\n"
                + "order by update_Date " + (desc ? "desc" : "asc");
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Blog b = new Blog(rs.getInt(1),
                        rs.getString("thumbnail_Blog"),
                        rs.getString("title"),
                        rs.getString("content_Blog"),
                        rs.getString("brief_Infor_Blog"),
                        rs.getDate("update_Date"),
                        rs.getInt("id_User"),
                        rs.getInt("view"),
                        rs.getInt("id_CatergoryBlog"),
                        rs.getDate("create_Date"));
                list.add(b);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    /**
     * @author Naviank
     * @param type - specify for the type you want to filter the result by(0-no
     * filter/1-by update_Date/2-by category/3-by views)
     * @param desc - indicate order of the list you want to sort
     * @param cat - indicate the category you want to filter (can be empty)
     * @param searchvalue
     * @return List of all blogs, filter by the parameter type
     */
    public List<Blog> filterBlogsList(int type, boolean desc, int cat, String searchvalue) {
        List<Blog> list = new ArrayList<>();
        switch (type) {
            case 0:
                list = getAllBlogs();
                break;
            case 1:
                list = getAllBlogsByUpdateDate(desc, searchvalue);
                break;
            case 2:
                list = getBlogByCategory(cat, desc);
                break;
            case 3:
                list = getAllBlogsByView(desc, searchvalue);
                break;
            case 4:
                list = searchByStringInput(searchvalue);
                break;
            default:
                break;
        }
        return list;
    }

    /**
     * @author Naviank
     * @param list
     * @param start
     * @param end
     * @return the sub-list of the received list from position start (inclusive)
     * to position end (exclusive)
     */
    public List<Blog> getListByPage(List<Blog> list, int start, int end) {
        List<Blog> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }

    public Blog getBlogById(int id_Blog) {
        String sql = "SELECT [id_Blog]\n"
                + "      ,[thumbnail_Blog]\n"
                + "      ,[title]\n"
                + "      ,[content_Blog]\n"
                + "      ,[brief_Infor_Blog]\n"
                + "      ,[update_date]\n"
                + "      ,[id_User]\n"
                + "      ,[view]\n"
                + "      ,[id_CatergoryBlog]\n"
                + "  FROM [dbo].[Blog]\n"
                + "  where id_Blog = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id_Blog);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Blog b = new Blog();
                b.setId_Blog(rs.getInt("id_Blog"));
                b.setThumbnail_Blog(rs.getString("thumbnail_Blog"));
                b.setTitle(rs.getString("title"));
                b.setContent_Blog(rs.getString("content_Blog"));
                b.setBrief_Infor_Blog(rs.getString("brief_Infor_Blog"));
                b.setUpdate_Date(rs.getDate("update_date"));
                b.setId_User(rs.getInt("id_User"));
                b.setView(rs.getInt("view"));
                b.setId_CatergoryBlog(rs.getInt("id_CatergoryBlog"));
                return b;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public List<Blog> getBlogWithAuthor() {
        List<Blog> list = new ArrayList<>();
        String sql = "SELECT b.[thumbnail_Blog], b.[title], b.[content_Blog], b.[id_Blog], b.[update_date], b.[view], b.[id_CatergoryBlog], u.full_Name\n"
                + "FROM Blog AS b\n"
                + "JOIN [User] AS u ON b.id_User = u.id_User order by update_date desc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Blog b = new Blog();
                b.setThumbnail_Blog(rs.getString("thumbnail_Blog"));
                b.setTitle(rs.getString("title"));
                b.setContent_Blog("content_Blog");
                b.setId_Blog(rs.getInt("id_Blog"));
                b.setUpdate_Date(rs.getDate("update_date"));
                b.setView(rs.getInt("view"));
                b.setId_CatergoryBlog(rs.getInt("id_CatergoryBlog"));
                b.setBrief_Infor_Blog(rs.getString("full_Name"));
                list.add(b);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    //huy
    public ArrayList<Blog> getAllBlogH() {
        ArrayList<Blog> list = new ArrayList<>();
        String sql = "SELECT [id_Blog],[thumbnail_Blog],[title],[content_Blog]\n"
                + "      ,[brief_Infor_Blog],[id_User],[view],[id_CatergoryBlog],[create_Date],[update_Date],[status]\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Blog]";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Blog b = new Blog();
                b.setId_Blog(rs.getInt("id_Blog"));
                b.setThumbnail_Blog(rs.getString("thumbnail_Blog"));
                b.setTitle(rs.getString("title"));
                b.setContent_Blog(rs.getString("content_Blog"));
                b.setBrief_Infor_Blog(rs.getString("brief_Infor_Blog"));
                b.setUpdate_Date(rs.getDate("update_Date"));
                b.setId_User(rs.getInt("id_User"));
                b.setView(rs.getInt("view"));
                b.setId_CatergoryBlog(rs.getInt("id_CatergoryBlog"));
                b.setCreate_Date(rs.getDate("create_Date"));
                b.setStatus(rs.getBoolean("status"));
                list.add(b);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }
//

    public List<Blog> getBlogFolowCategory(int idcate) {
        List<Blog> list = new ArrayList<>();
        String sql = "SELECT [id_Blog],[thumbnail_Blog],[title],[content_Blog]\n"
                + "      ,[brief_Infor_Blog],[id_User],[view],[id_CatergoryBlog],[create_Date],[update_Date],[status]\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Blog]\n"
                + "  where id_CatergoryBlog = ?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, idcate);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Blog b = new Blog();
                b.setId_Blog(rs.getInt("id_Blog"));
                b.setThumbnail_Blog(rs.getString("thumbnail_Blog"));
                b.setTitle(rs.getString("title"));
                b.setContent_Blog(rs.getString("content_Blog"));
                b.setBrief_Infor_Blog(rs.getString("brief_Infor_Blog"));
                b.setUpdate_Date(rs.getDate("update_Date"));
                b.setId_User(rs.getInt("id_User"));
                b.setView(rs.getInt("view"));
                b.setId_CatergoryBlog(rs.getInt("id_CatergoryBlog"));
                b.setCreate_Date(rs.getDate("create_Date"));
                b.setStatus(rs.getBoolean("status"));
                list.add(b);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    //huy
    public List<Blog> getBlogFolowUser(int iduser) {
        List<Blog> list = new ArrayList<>();
        String sql = "SELECT [id_Blog],[thumbnail_Blog],[title],[content_Blog]\n"
                + "      ,[brief_Infor_Blog],[id_User],[view],[id_CatergoryBlog],[create_Date],[update_Date],[status]\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Blog]\n"
                + "  where id_User = ?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, iduser);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Blog b = new Blog();
                b.setId_Blog(rs.getInt("id_Blog"));
                b.setThumbnail_Blog(rs.getString("thumbnail_Blog"));
                b.setTitle(rs.getString("title"));
                b.setContent_Blog(rs.getString("content_Blog"));
                b.setBrief_Infor_Blog(rs.getString("brief_Infor_Blog"));
                b.setUpdate_Date(rs.getDate("update_Date"));
                b.setId_User(rs.getInt("id_User"));
                b.setView(rs.getInt("view"));
                b.setId_CatergoryBlog(rs.getInt("id_CatergoryBlog"));
                b.setCreate_Date(rs.getDate("create_Date"));
                b.setStatus(rs.getBoolean("status"));
                list.add(b);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Blog> SortBlog(int sort) {
        List<Blog> list = new ArrayList<>();
        String sql = "SELECT [id_Blog],[thumbnail_Blog],[title],[content_Blog]\n"
                + "      ,[brief_Infor_Blog],[id_User],[view],[id_CatergoryBlog],[create_Date],[update_Date],[status]\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Blog]";
        if (sort == 1) {
            sql = sql + " order by CAST(title as nvarchar(max)) asc";
        } else if (sort == 2) {
            sql = sql + " order by CAST(title as nvarchar(max)) desc";
        } else if (sort == 3) {
            sql = sql + " order by [view] asc";
        } else if (sort == 4) {
            sql = sql + " order by create_Date desc";
        } else if (sort == 5) {
            sql = sql + " order by create_Date asc";
        } else if (sort == 6) {
            sql = sql + " order by [view] desc";
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Blog b = new Blog();
                b.setId_Blog(rs.getInt("id_Blog"));
                b.setThumbnail_Blog(rs.getString("thumbnail_Blog"));
                b.setTitle(rs.getString("title"));
                b.setContent_Blog(rs.getString("content_Blog"));
                b.setBrief_Infor_Blog(rs.getString("brief_Infor_Blog"));
                b.setUpdate_Date(rs.getDate("update_Date"));
                b.setId_User(rs.getInt("id_User"));
                b.setView(rs.getInt("view"));
                b.setId_CatergoryBlog(rs.getInt("id_CatergoryBlog"));
                b.setCreate_Date(rs.getDate("create_Date"));
                b.setStatus(rs.getBoolean("status"));

                list.add(b);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    //huy
    public Blog getBlogByIdH(int idblog) {

        String sql = "SELECT [id_Blog],[thumbnail_Blog],[title],[content_Blog]\n"
                + "      ,[brief_Infor_Blog],[id_User],[view],[id_CatergoryBlog],[create_Date],[update_Date],[status]\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Blog]\n"
                + "  where id_Blog = ?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, idblog);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Blog b = new Blog();
                b.setId_Blog(rs.getInt("id_Blog"));
                b.setThumbnail_Blog(rs.getString("thumbnail_Blog"));
                b.setTitle(rs.getString("title"));
                b.setContent_Blog(rs.getString("content_Blog"));
                b.setBrief_Infor_Blog(rs.getString("brief_Infor_Blog"));
                b.setUpdate_Date(rs.getDate("update_Date"));
                b.setId_User(rs.getInt("id_User"));
                b.setView(rs.getInt("view"));
                b.setId_CatergoryBlog(rs.getInt("id_CatergoryBlog"));
                b.setCreate_Date(rs.getDate("create_Date"));
                b.setStatus(rs.getBoolean("status"));
                return b;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public Blog checkExistBlog(String title, int idblog) {

        String sql = "SELECT [id_Blog],[thumbnail_Blog],[title],[content_Blog]\n"
                + "      ,[brief_Infor_Blog],[id_User]\n"
                + "      ,[view],[id_CatergoryBlog],[create_Date]\n"
                + "      ,[update_Date],[status]\n"
                + " FROM [dbo].[Blog]\n"
                + " where CAST (title as nvarchar(max)) = ? and id_Blog != " + idblog;

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, title);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Blog b = new Blog();
                b.setId_Blog(rs.getInt("id_Blog"));
                b.setThumbnail_Blog(rs.getString("thumbnail_Blog"));
                b.setTitle(rs.getString("title"));
                b.setContent_Blog(rs.getString("content_Blog"));
                b.setBrief_Infor_Blog(rs.getString("brief_Infor_Blog"));
                b.setUpdate_Date(rs.getDate("update_Date"));
                b.setId_User(rs.getInt("id_User"));
                b.setView(rs.getInt("view"));
                b.setId_CatergoryBlog(rs.getInt("id_CatergoryBlog"));
                b.setCreate_Date(rs.getDate("create_Date"));
                b.setStatus(rs.getBoolean("status"));
                return b;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public void insertBlog(Blog b) {
        String sql = " INSERT INTO [dbo].[Blog]([thumbnail_Blog],[title],[content_Blog]\n"
                + ",[brief_Infor_Blog],[id_User],[view],[id_CatergoryBlog]\n"
                + ",[create_Date],[update_Date],[status])\n"
                + "VALUES(?,?,?,?,?,0,?,GETDATE(),null,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, b.getThumbnail_Blog());
            st.setString(2, b.getTitle());
            st.setString(3, b.getContent_Blog());
            st.setString(4, b.getBrief_Infor_Blog());
            st.setInt(5, b.getId_User());
            st.setInt(6, b.getId_CatergoryBlog());
            st.setBoolean(7, b.isStatus());
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void updateBlog(Blog b) {
        String sql = " UPDATE [dbo].[Blog]\n"
                + "SET [thumbnail_Blog] = ?,[title] = ?,[content_Blog] = ?\n"
                + ",[brief_Infor_Blog] = ?,[id_User] = ?\n"
                + ",[id_CatergoryBlog] = ?,[update_Date] = GETDATE(),[status] = ?\n"
                + "WHERE id_Blog = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, b.getThumbnail_Blog());
            st.setString(2, b.getTitle());
            st.setString(3, b.getContent_Blog());
            st.setString(4, b.getBrief_Infor_Blog());
            st.setInt(5, b.getId_User());
            st.setInt(6, b.getId_CatergoryBlog());
            st.setBoolean(7, b.isStatus());
            st.setInt(8, b.getId_Blog());
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void deleteBlog(int id) {
        String sql = " DELETE FROM [dbo].[Blog]\n"
                + "      WHERE  id_Blog = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public static void main(String[] args) {
        BlogDAO bd = new BlogDAO();
        System.out.println(bd.getBlogById(1).getBrief_Infor_Blog());

    }

    public void updateView(int view, int id) {
        String sql = "update [Blog] SET [view] = ?\n"
                + "where id_Blog = ?";
                try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, view);
            st.setInt(2, id);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }
}
