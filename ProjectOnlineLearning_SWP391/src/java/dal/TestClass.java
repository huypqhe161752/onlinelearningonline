/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.util.List;
import model.Answer;
import model.Question;

/**
 *
 * @author Naviank
 */
public class TestClass {

    public static void main(String[] args) {
        AnswerDAO ad = new AnswerDAO();
        QuestionDAO qed = new QuestionDAO();
        Question cur_question = qed.getQuestionOfQuizByOrder(1, 3);
        System.out.println(cur_question.getContent_Question());
    }
}
