/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Question;
import model.Questions;
import model.Quiz;

/**
 *
 * @author PhanQuangHuy59
 */
public class QuestionDAO extends DBContext {

    /**
     * @author Naviank
     * @param id_Quiz
     * @return list of all question of a quiz specify by its id_Quiz
     */
    public List<Question> getAllQuestionsOfQuiz(int id_Quiz) {
        List<Question> list = new ArrayList<>();
        String sql = "SELECT [id_Question]\n"
                + "      ,[content_Question]\n"
                + "      ,[description]\n"
                + "      ,[id_Quiz]\n"
                + "      ,[createDate]\n"
                + "      ,[update_Date]\n"
                + "      ,[id_Type]\n"
                + "  FROM [dbo].[Question]"
                + "  WHERE id_Quiz = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id_Quiz);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Question(rs.getInt("id_Question"),
                        rs.getString("content_Question"),
                        rs.getInt("id_Quiz"),
                        rs.getString("description"),
                        rs.getInt("id_Type"),
                        rs.getDate("createDate"),
                        rs.getDate("update_Date")));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    /**
     * @author Naviank
     * @param id_Question
     * @return a question specify by its id_Question
     */
    public Question getQuestionByID(int id_Question) {
        String sql = "select * from Question where id_Question = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id_Question);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Question q = new Question(rs.getInt("id_Question"),
                        rs.getString("content_Question"),
                        rs.getInt("id_Quiz"),
                        rs.getString("description"),
                        rs.getInt("id_Type"),
                        rs.getDate("createDate"),
                        rs.getDate("update_Date"));
                return q;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    /**
     * @author Naviank
     * @param cur_number
     * @param id_Quiz
     * @return the question at a specific position (because the question
     * position in a quiz fixed)
     */
    public Question getQuestionOfQuizByOrder(int cur_number, int id_Quiz) {
        List<Question> list = getAllQuestionsOfQuiz(id_Quiz);
        if (cur_number > list.size()) {
            return null;
        } else {
            return list.get(cur_number - 1);
        }
    }

    public ArrayList<Questions> getAllQuest() {
        ArrayList<Questions> listQuest = new ArrayList<>();
        String sql = "select qe.id_Question,qe.content_Question,s.subject_Name,ls.name_Lesson,lv.name_Level,ls.status,cs.name_Catergory_Subject\n"
                + "from question qe, Quiz qi, Lesson ls, [Subject] s, [Level] lv,Catergory_Subject cs\n"
                + "where \n"
                + "qe.id_Quiz=qi.id_Quiz and\n"
                + "qi.id_Level = ls.id_Lesson and\n"
                + "ls.id_Subject=s.id_Subject and\n"
                + "lv.id_Level=qi.id_Level and \n"
                + "cs.id_Catergory_Subject=s.id_Catergory_Subject";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Questions q = new Questions();
                q.setId_Question(rs.getInt("id_question"));
                q.setContent_Question(rs.getString("content_question"));
                q.setLesson_Name(rs.getString("name_lesson"));
                q.setSubject_Name(rs.getString("subject_name"));
                q.setSubject_Category_Name(rs.getString("name_catergory_subject"));
                q.setLevel_Name(rs.getString("name_level"));
                q.setStatus(rs.getBoolean("status"));
                listQuest.add(q);
            }
        } catch (SQLException e) {
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return listQuest;
    }
    public ArrayList<Questions> getAllQuest2(String email) {
        ArrayList<Questions> listQuest = new ArrayList<>();
        String sql = "select qe.id_Question,qe.content_Question,s.subject_Name,ls.name_Lesson,lv.name_Level,ls.status,cs.name_Catergory_Subject\n"
                + "from [User] u, question qe, Quiz qi, Lesson ls, [Subject] s, [Level] lv,Catergory_Subject cs\n"
                + "where \n"
                + "qe.id_Quiz=qi.id_Quiz and\n"
                + "qi.id_Level = ls.id_Lesson and\n"
                + "ls.id_Subject=s.id_Subject and\n"
                + "lv.id_Level=qi.id_Level and \n"
                + "cs.id_Catergory_Subject=s.id_Catergory_Subject and u.id_User=s.id_User and u.id_User = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, email);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Questions q = new Questions();
                q.setId_Question(rs.getInt("id_question"));
                q.setContent_Question(rs.getString("content_question"));
                q.setLesson_Name(rs.getString("name_lesson"));
                q.setSubject_Name(rs.getString("subject_name"));
                q.setSubject_Category_Name(rs.getString("name_catergory_subject"));
                q.setLevel_Name(rs.getString("name_level"));
                q.setStatus(rs.getBoolean("status"));
                listQuest.add(q);
            }
        } catch (SQLException e) {
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return listQuest;
    }

    public ArrayList<Questions> getListByPage(ArrayList<Questions> list, int start, int end) {
        ArrayList<Questions> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }

    public ArrayList<Questions> getQuestionByContent(String content) {
        ArrayList<Questions> questions = new ArrayList<>();
        String sql = "select qe.id_Question,qe.content_Question,s.subject_Name,ls.name_Lesson,lv.name_Level,ls.status,cs.name_Catergory_Subject\n"
                + "from question qe, Quiz qi, Lesson ls, [Subject] s, [Level] lv,Catergory_Subject cs\n"
                + "where \n"
                + "qe.id_Quiz=qi.id_Quiz and\n"
                + "qi.id_Level = ls.id_Lesson and\n"
                + "ls.id_Subject=s.id_Subject and\n"
                + "lv.id_Level=qi.id_Level and \n"
                + "cs.id_Catergory_Subject=s.id_Catergory_Subject\n"
                + "and qe.content_question LIKE ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + content + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Questions q = new Questions();
                q.setId_Question(rs.getInt("id_question"));
                q.setContent_Question(rs.getString("content_question"));
                q.setLesson_Name(rs.getString("name_lesson"));
                q.setSubject_Name(rs.getString("subject_name"));
                q.setSubject_Category_Name(rs.getString("name_catergory_subject"));
                q.setLevel_Name(rs.getString("name_level"));
                q.setStatus(rs.getBoolean("status"));
                questions.add(q);
            }
        } catch (SQLException e) {
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return questions;
    }

    public void deleteQuestionById(int id) {
        String sql = "Delete from Answer where id_Question =?\n"
                + "DELETE FROM [dbo].[Question] \n"
                + "      WHERE id_Question=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            st.setInt(2, id);
            st.executeUpdate();
        } catch (SQLException e) {
        }
    }

    public void importQuestion(String[] fields, String[] corAns, String[] incorAns, String email, Date createdate) {
        String sql = "INSERT INTO [dbo].[Question] ([content_Question],[description],[id_Quiz],[createDate],[update_Date],[id_Type])\n"
                + "     VALUES(? ,? ,? ,?,null,?)\n "
                + "DECLARE @r int;\n"
                + "select @r = (select top 1 q.id_Question \n"
                + "from Question q, Quiz qi, Lesson l, [Subject] s,[User] u\n"
                + "where q.id_Quiz=qi.id_Quiz and qi.id_Lesson=l.id_Lesson and l.id_Subject=s.id_Subject and s.id_User = u.id_User and u.email=?\n"
                + "order by q.id_Question desc)\n";
        for (int i = 0; i < corAns.length; i++) {
            sql = sql.concat("INSERT INTO [dbo].[Answer] ([answer_Content],[is_Correct],[id_Question])\n"
                    + "     VALUES (?,'t',@r)\n");

        }
        for (int j = 0; j < incorAns.length; j++) {
            sql = sql.concat("INSERT INTO [dbo].[Answer] ([answer_Content],[is_Correct],[id_Question])\n"
                    + "     VALUES (?,'f',@r)\n");
        }
        System.out.println(sql);
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, fields[0]);
            st.setString(2, fields[1]);
            st.setInt(3, Integer.parseInt(fields[2]));
            st.setDate(4, createdate);
            st.setInt(5, Integer.parseInt(fields[3]));
            st.setString(6, email);
            int i = 7;
            for (i = 7; i < corAns.length + 7; i = i + 1) {
                st.setString(i, corAns[i - 7]);
            }

            for (int j = i; j < incorAns.length + i; j = j + 1) {
                st.setString(j, incorAns[j - i]);
            }
            st.executeUpdate();
        } catch (SQLException e) {
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public String getTypeByIdQuestion(int id) {
        String type = null;
        String sql = "select t.name_TypeQuestion\n"
                + "                from Question q, Type_Question t\n"
                + "                where q.id_type = t.id_Type\n"
                + "                and q.id_Question=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                type = rs.getString("name_TypeQuestion");
            }
            return type;
        } catch (Exception e) {
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    public Question getQuestionById2(int id_Question) {
        String sql = "select * from Question where id_Question = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id_Question);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Question q = new Question();
                q.setId_Question(id_Question);
                q.setContent_Question(rs.getString("content_question"));
                q.setCreateDate(rs.getDate("createDate"));
                q.setDescription(rs.getString("description"));
                q.setId_Quiz(rs.getInt("id_quiz"));
                q.setId_Type(rs.getInt("id_type"));
                q.setUpdate_Date(rs.getDate("Update_date"));
                return q;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public void updateQuestion(int id, String content, String description, int type, Date udate) {
        String sql = "UPDATE [dbo].[Question]\n"
                + "   SET [content_Question] = ?\n"
                + "      ,[description] = ?\n"
                + "      ,[id_Type] =?\n"
                + "      ,[update_Date] =?\n"
                + " WHERE id_Question=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, content);
            st.setString(2, description);
            st.setInt(3, type);
            st.setDate(4, udate);
            st.setInt(5, id);
            st.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(QuestionDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(QuestionDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public ArrayList<Questions> getQuestionsByDate(Date searchdate) {
        ArrayList<Questions> listQuest = new ArrayList<>();
        String sql = "select qe.id_Question,qe.content_Question,s.subject_Name,ls.name_Lesson,lv.name_Level,ls.status,cs.name_Catergory_Subject\n"
                + "from question qe, Quiz qi, Lesson ls, [Subject] s, [Level] lv,Catergory_Subject cs\n"
                + "where \n"
                + "qe.id_Quiz=qi.id_Quiz and\n"
                + "qi.id_Level = ls.id_Lesson and\n"
                + "ls.id_Subject=s.id_Subject and\n"
                + "lv.id_Level=qi.id_Level and \n"
                + "cs.id_Catergory_Subject=s.id_Catergory_Subject and "
                + "(qe.update_Date>? or qe.createDate>?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setDate(1, searchdate);
            st.setDate(2, searchdate);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Questions q = new Questions();
                q.setId_Question(rs.getInt("id_question"));
                q.setContent_Question(rs.getString("content_question"));
                q.setLesson_Name(rs.getString("name_lesson"));
                q.setSubject_Name(rs.getString("subject_name"));
                q.setSubject_Category_Name(rs.getString("name_catergory_subject"));
                q.setLevel_Name(rs.getString("name_level"));
                q.setStatus(rs.getBoolean("status"));
                listQuest.add(q);
            }
        } catch (SQLException e) {
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return listQuest;
    }

    public ArrayList<Question> getQuestionByIdQuiz(int qid) {
        ArrayList<Question> ar = new ArrayList<>();
        String sql = "select q.id_Question,q.content_Question from Question q, Quiz qe where q.id_Quiz=qe.id_Quiz and qe.id_Quiz=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, qid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Question q = new Question();
                q.setId_Question(rs.getInt("id_question"));
                q.setContent_Question(rs.getString("content_question"));
                q.setCreateDate(null);
                q.setDescription("");
                q.setId_Quiz(qid);
                q.setId_Type(1);
                q.setUpdate_Date(null);
                ar.add(q);
            }
        } catch (SQLException e) {
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return ar;
    }

    public void addNewQuestion(String content, String explain, String[] correct, String[] incorrect, int type, Date cDate, int qid) {
        String sql = "INSERT INTO [dbo].[Question]\n"
                + "           ([content_Question]\n"
                + "           ,[description]\n"
                + "           ,[id_Quiz]\n"
                + "           ,[createDate]\n"
                + "           ,[id_Type])\n"
                + "     VALUES\n"
                + "           (?,?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, content);
            st.setString(2, explain);
            st.setInt(3, qid);
            st.setDate(4, cDate);
            st.setInt(5, type);
            st.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(QuestionDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(QuestionDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        for (int i = 0; i < correct.length; i++) {
            String sql_2 = "INSERT INTO [dbo].[Answer]\n"
                    + "           ([answer_Content]\n"
                    + "           ,[is_Correct]\n"
                    + "           ,[id_Question])\n"
                    + "     VALUES (? ,'t' ,?)";
            try {
                PreparedStatement st = connection.prepareStatement(sql_2);
                st.setString(1, correct[i]);
                st.setInt(2, qid);
                
                st.executeUpdate();
            } catch (SQLException ex) {
                Logger.getLogger(QuestionDAO.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(QuestionDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        for (int i = 0; i < incorrect.length; i++) {
            String sql_2 = "INSERT INTO [dbo].[Answer]\n"
                    + "           ([answer_Content]\n"
                    + "           ,[is_Correct]\n"
                    + "           ,[id_Question])\n"
                    + "     VALUES (? ,'f' ,?)";
            try {
                PreparedStatement st = connection.prepareStatement(sql_2);
                st.setString(1, incorrect[i]);
                st.setInt(2, qid);
                
                st.executeUpdate();
            } catch (SQLException ex) {
                Logger.getLogger(QuestionDAO.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(QuestionDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }

    /**
     * @author Naviank
     * @param id_Quiz
     * @return the number of records deleted from table Question in database
     *
     */
    public int deleteQuestionOfQuiz(int id_Quiz) {
        int count = 0;
        List<Question> list = getAllQuestionsOfQuiz(id_Quiz);
        for (Question q : list) {
            deleteQuestionById(q.getId_Question());
            count++;
        }
        return count;
    }

    public ArrayList<Question> getQuesstionByRangeIdQuiz(ArrayList<Quiz> list) {
        String id_Subject = "";
        ArrayList<Question> listQuestion = new ArrayList<>();
        String sql = "SELECT [id_Question],[content_Question],[description],[id_Quiz]\n"
                + ",[createDate],[update_Date],[id_Type]\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Question]\n"
                + "  where id_Quiz in";
        for (int i = 0; i < list.size(); i++) {
            if (i == 0) {
                id_Subject = id_Subject + "(" + list.get(i).getId_Quiz() + ",";
            }
            if (i != 0 && i != list.size() - 1) {
                id_Subject = id_Subject + list.get(i).getId_Quiz() + ",";
            }
            if (i == (list.size() - 1)) {
                id_Subject = id_Subject + list.get(i).getId_Quiz() + ")";
            }
        }
        sql += id_Subject;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {

                Question q = new Question();
                q.setId_Question(rs.getInt("id_Question"));
                q.setContent_Question(rs.getString("content_Question"));
                q.setDescription(rs.getString("description"));
                q.setId_Quiz(rs.getInt("id_Quiz"));
                q.setCreateDate(rs.getDate("createDate"));
                q.setUpdate_Date(rs.getDate("update_Date"));
                q.setId_Type(rs.getInt("id_Type"));
                listQuestion.add(q);
            }
            return listQuestion;
        } catch (SQLException e) {
            System.out.println(e);

        }
        return null;
    }

    public void DeleteQuesstionByRangeIdQuiz(ArrayList<Quiz> list) {
        String id_Subject = "";
        String sql = "DELETE FROM [dbo].[Question]\n"
                + "      WHERE id_Quiz in";
        for (int i = 0; i < list.size(); i++) {
            if (i == 0) {
                id_Subject = id_Subject + "(" + list.get(i).getId_Quiz() + ",";
            }
            if (i != 0 && i != list.size() - 1) {
                id_Subject = id_Subject + list.get(i).getId_Quiz() + ",";
            }
            if (i == (list.size() - 1)) {
                id_Subject = id_Subject + list.get(i).getId_Quiz() + ")";
            }
        }
        sql += id_Subject;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e);

        }
    }
}
