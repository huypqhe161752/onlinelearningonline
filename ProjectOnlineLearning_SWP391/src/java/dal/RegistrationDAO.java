/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Course;
import model.MyRegistration;
import model.Registration;
import model1.RegistrationJoin;

/**
 *
 * @author PhanQuangHuy59
 */
public class RegistrationDAO extends DBContext {

    public void insertToRegistration(Registration r) {
        String sql = "INSERT INTO [dbo].[Registration]\n"
                + "           ([id_User]\n"
                + "           ,[id_Course]\n"
                + "           ,[registration_Date]\n"
                + "           ,[id_PackagePrice]\n"
                + "           ,[status]\n"
                + "           ,[valid_From]\n"
                + "           ,[valid_To])\n"
                + "     VALUES\n"
                + "           (?,?,?,?,NULL,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, r.getId_User());
            st.setInt(2, r.getId_Course());
            st.setDate(3, r.getRegistration_Date());
            st.setInt(4, r.getId_packagePrice());
            st.setDate(5, r.getValid_From());
            st.setDate(6, r.getValid_To());
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public Date getCurrentDate() {
        LocalDate currentDate = LocalDate.now();
        return Date.valueOf(currentDate);
    }

    public Date calculateExpirationDate(String startDate, int duration) {
        LocalDate startDateObj = LocalDate.parse(startDate);
        LocalDate expirationDate = startDateObj.plusMonths(duration);
        return Date.valueOf(expirationDate);
    }

    public List<Registration> getAll() {
        List<Registration> list = new ArrayList<>();
        Registration r = new Registration();
        String sql = "SELECT [id_Register]\n"
                + "      ,[id_User]\n"
                + "      ,[id_Course]\n"
                + "      ,[registration_Date]\n"
                + "      ,[id_PackagePrice]\n"
                + "      ,[status]\n"
                + "      ,[valid_From]\n"
                + "      ,[valid_To]\n"
                + "  FROM [dbo].[Registration]";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                r.setId_Register(rs.getInt(""));
                r.setId_packagePrice(rs.getInt(""));
                r.setId_Course(rs.getInt(""));
                r.setRegistration_Date(rs.getDate(""));
                r.setId_packagePrice(rs.getInt(""));
                r.setStatus(rs.getBoolean(""));
                r.setValid_From(rs.getDate(""));
                r.setValid_To(rs.getDate(""));
                list.add(r);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<RegistrationJoin> getAll1() {
        List<RegistrationJoin> list = new ArrayList<>();
        String sql = "SELECT r.id_Register,r.id_User, u.email,r.id_Course, c.course_Name, r.registration_Date,\n"
                + "r.id_PackagePrice,p.list_Price,p.sale_Price, r.status, r.valid_From, r.valid_To from Registration r\n"
                + "inner join [dbo].[User] u  on r.id_User = u.id_User\n"
                + "inner join Course c on r.id_Course = c.id_Course\n"
                + "inner join [dbo].[PackagePrice] p on p.id_Package = r.id_PackagePrice";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                RegistrationJoin r = new RegistrationJoin(); // Tạo đối tượng Registration mới cho mỗi vòng lặp
                r.setId_Register(rs.getInt("id_Register"));
                r.setId_User(rs.getInt("id_User"));
                r.setEmail(rs.getString("email"));
                r.setId_Course(rs.getInt("id_Course"));
                r.setCourse_Name(rs.getString("course_Name"));
                r.setRegistration_Date(rs.getDate("registration_Date"));
                r.setId_packagePrice(rs.getInt("id_PackagePrice"));
                r.setList_Price(rs.getInt("list_Price"));
                r.setSale_Price(rs.getInt("sale_Price"));
                if (rs.getString("status") == null) {
                    r.setStatus(null);
                } else if (rs.getString("status").equals("1")) {
                    r.setStatus(Boolean.TRUE);
                } else if (rs.getString("status").equals("0")) {
                    r.setStatus(Boolean.FALSE);
                }

                r.setValid_From(rs.getDate("valid_From"));
                r.setValid_To(rs.getDate("valid_To"));
                list.add(r);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public int checkid_registration() {

        int count = 1;

        RegistrationDAO r = new RegistrationDAO();
        List<Registration> list = r.getAll();
        for (int i = 0; i < list.size(); i++) {
            count++;
        }
        return count;
    }

    public ArrayList<Registration> getAllRegistrationOfUser(int idUser) {
        ArrayList<Registration> list = new ArrayList<>();
        String sql = "SELECT  [id_Register]\n"
                + "      ,[id_User]\n"
                + "      ,[id_Course]\n"
                + "      ,[registration_Date]\n"
                + "      ,[id_PackagePrice]\n"
                + "      ,[status]\n"
                + "      ,[valid_From]\n"
                + "      ,[valid_To]\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Registration]\n"
                + "  where status = 1 and id_User = ? order by valid_To asc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, idUser);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Registration r = new Registration();

                r.setId_Register(rs.getInt("id_Register"));
                r.setId_User(rs.getInt("id_User"));
                r.setId_Course(rs.getInt("id_Course"));
                r.setRegistration_Date(rs.getDate("registration_Date"));
                r.setId_packagePrice(rs.getInt("id_PackagePrice"));
                r.setStatus(rs.getBoolean("status"));
                r.setValid_From(rs.getDate("valid_From"));
                r.setValid_To(rs.getDate("valid_To"));
                list.add(r);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public ArrayList<Registration> getAllRegistrationOfUserUnfinish(int idUser, Date d) {
        ArrayList<Registration> list = new ArrayList<>();
        String sql = "SELECT  [id_Register]\n"
                + "      ,[id_User]\n"
                + "      ,[id_Course]\n"
                + "      ,[registration_Date]\n"
                + "      ,[id_PackagePrice]\n"
                + "      ,[status]\n"
                + "      ,[valid_From]\n"
                + "      ,[valid_To]\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Registration]\n"
                + "  where status = 1 and id_User = ? and valid_To > ? order by valid_To asc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, idUser);
            st.setDate(2, d);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Registration r = new Registration();

                r.setId_Register(rs.getInt("id_Register"));
                r.setId_User(rs.getInt("id_User"));
                r.setId_Course(rs.getInt("id_Course"));
                r.setRegistration_Date(rs.getDate("registration_Date"));
                r.setId_packagePrice(rs.getInt("id_PackagePrice"));
                r.setStatus(rs.getBoolean("status"));
                r.setValid_From(rs.getDate("valid_From"));
                r.setValid_To(rs.getDate("valid_To"));
                list.add(r);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public ArrayList<Registration> checkExpire(int idUser, Date from, Date to) {
        ArrayList<Registration> list = new ArrayList<>();
        String sql = "SELECT  [id_Register]\n"
                + "      ,[id_User]\n"
                + "      ,[id_Course]\n"
                + "      ,[registration_Date]\n"
                + "      ,[id_PackagePrice]\n"
                + "      ,[status]\n"
                + "      ,[valid_From]\n"
                + "      ,[valid_To]\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Registration]\n"
                + "  where status = 1 and id_User = ? and valid_To BETWEEN ? AND ? order by valid_To asc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, idUser);
            st.setDate(2, from);
            st.setDate(3, to);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Registration r = new Registration();

                r.setId_Register(rs.getInt("id_Register"));
                r.setId_User(rs.getInt("id_User"));
                r.setId_Course(rs.getInt("id_Course"));
                r.setRegistration_Date(rs.getDate("registration_Date"));
                r.setId_packagePrice(rs.getInt("id_PackagePrice"));
                r.setStatus(rs.getBoolean("status"));
                r.setValid_From(rs.getDate("valid_From"));
                r.setValid_To(rs.getDate("valid_To"));
                list.add(r);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public ArrayList<Registration> getAllRegistrationOfUserFinish(int idUser, Date d) {
        ArrayList<Registration> list = new ArrayList<>();
        String sql = "SELECT  [id_Register]\n"
                + "      ,[id_User]\n"
                + "      ,[id_Course]\n"
                + "      ,[registration_Date]\n"
                + "      ,[id_PackagePrice]\n"
                + "      ,[status]\n"
                + "      ,[valid_From]\n"
                + "      ,[valid_To]\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Registration]\n"
                + "  where status = 1 and id_User = ? and valid_To < ? order by valid_To asc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, idUser);
            st.setDate(2, d);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Registration r = new Registration();

                r.setId_Register(rs.getInt("id_Register"));
                r.setId_User(rs.getInt("id_User"));
                r.setId_Course(rs.getInt("id_Course"));
                r.setRegistration_Date(rs.getDate("registration_Date"));
                r.setId_packagePrice(rs.getInt("id_PackagePrice"));
                r.setStatus(rs.getBoolean("status"));
                r.setValid_From(rs.getDate("valid_From"));
                r.setValid_To(rs.getDate("valid_To"));
                list.add(r);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public ArrayList<Registration> getAllRegistrationFollowCatergoryCourse(int idUser, int Category) {
        ArrayList<Registration> list = new ArrayList<>();
        String sql = "SELECT  [id_Register]\n"
                + "      ,[id_User]\n"
                + "      ,[id_Course]\n"
                + "      ,[registration_Date]\n"
                + "      ,[id_PackagePrice]\n"
                + "      ,[status]\n"
                + "      ,[valid_From]\n"
                + "      ,[valid_To]\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Registration]\n"
                + "  where status = 1 and id_User = ? and id_Course in\n"
                + "  (select id_Course from Course where id_Catergory = ?) order by valid_To asc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, idUser);
            st.setInt(2, Category);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Registration r = new Registration();

                r.setId_Register(rs.getInt("id_Register"));
                r.setId_User(rs.getInt("id_User"));
                r.setId_Course(rs.getInt("id_Course"));
                r.setRegistration_Date(rs.getDate("registration_Date"));
                r.setId_packagePrice(rs.getInt("id_PackagePrice"));
                r.setStatus(rs.getBoolean("status"));
                r.setValid_From(rs.getDate("valid_From"));
                r.setValid_To(rs.getDate("valid_To"));
                list.add(r);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public ArrayList<MyRegistration> getMyRegistration(String email) {
        ArrayList<MyRegistration> listRe = new ArrayList<>();
        String sql = "select *\n"
                + "from Registration re, Course co, PackagePrice pa, [User] us, Course_Catergory cc\n"
                + "where us.id_User = re.id_User and re.id_Course = co.id_Course \n"
                + "and re.id_PackagePrice = pa.id_Package and co.id_Catergory = cc.id_Catergory \n"
                + "and us.email=? and re.status is null";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, email);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                MyRegistration re = new MyRegistration();
                re.setId_Course(rs.getInt("id_course"));
                re.setId_Register(rs.getInt("id_register"));
                re.setId_User(rs.getInt("id_user"));
                re.setId_packagePrice(rs.getInt("id_packageprice"));
                re.setRegistration_Date(rs.getDate("registration_date"));
                re.setStatus(rs.getBoolean("status"));
                re.setValid_From(rs.getDate("valid_from"));
                re.setValid_To(rs.getDate("valid_to"));
                re.setName_Course(rs.getString("course_name"));
                re.setName_Category(rs.getString("name_catergory"));
                re.setTotal_cost(rs.getInt("sale_price"));
                re.setImage(rs.getString("image"));
                listRe.add(re);
            }
        } catch (SQLException e) {
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return listRe;
    }

    public ArrayList<MyRegistration> getMyRegistrationByNameOfCourse(ArrayList<MyRegistration> arr, String email, String name) {
        ArrayList<MyRegistration> listRe = new ArrayList<>();

        String sql = "select *\n"
                + "from Registration re, Course co, PackagePrice pa, [User] us, Course_Catergory cc\n"
                + "where us.id_User = re.id_User and re.id_Course = co.id_Course \n"
                + "and re.id_PackagePrice = pa.id_Package and co.id_Catergory = cc.id_Catergory \n"
                + "and us.email=? and re.status is null and co.course_Name like ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, email);
            st.setString(2, "%" + name + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                MyRegistration re = new MyRegistration();
                re.setId_Course(rs.getInt("id_course"));
                re.setId_Register(rs.getInt("id_register"));
                re.setId_User(rs.getInt("id_user"));
                re.setId_packagePrice(rs.getInt("id_packageprice"));
                re.setRegistration_Date(rs.getDate("registration_date"));
                re.setStatus(rs.getBoolean("status"));
                re.setValid_From(rs.getDate("valid_from"));
                re.setValid_To(rs.getDate("valid_to"));
                re.setName_Course(rs.getString("course_name"));
                re.setName_Category(rs.getString("name_catergory"));
                re.setTotal_cost(rs.getInt("sale_price"));
                re.setImage(rs.getString("image"));
                listRe.add(re);
            }
        } catch (SQLException e) {
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return listRe;
    }

    public ArrayList<MyRegistration> getMyRegistrationByNameOfCourseCategory(String email, String name) {
        ArrayList<MyRegistration> listRe = new ArrayList<>();
        String sql = "select *\n"
                + "from Registration re, Course co, PackagePrice pa, [User] us, Course_Catergory cc\n"
                + "where us.id_User = re.id_User and re.id_Course = co.id_Course \n"
                + "and re.id_PackagePrice = pa.id_Package and co.id_Catergory = cc.id_Catergory \n"
                + "and us.email=? and re.status is null and cc.name_Catergory = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, email);
            st.setString(2, name);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                MyRegistration re = new MyRegistration();
                re.setId_Course(rs.getInt("id_course"));
                re.setId_Register(rs.getInt("id_register"));
                re.setId_User(rs.getInt("id_user"));
                re.setId_packagePrice(rs.getInt("id_packageprice"));
                re.setRegistration_Date(rs.getDate("registration_date"));
                re.setStatus(rs.getBoolean("status"));
                re.setValid_From(rs.getDate("valid_from"));
                re.setValid_To(rs.getDate("valid_to"));
                re.setName_Course(rs.getString("course_name"));
                re.setName_Category(rs.getString("name_catergory"));
                re.setTotal_cost(rs.getInt("sale_price"));
                re.setImage(rs.getString("image"));
                listRe.add(re);
            }
        } catch (SQLException e) {
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return listRe;
    }

    public ArrayList<MyRegistration> getListByPage(ArrayList<MyRegistration> list, int start, int end) {
        ArrayList<MyRegistration> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }

    public void deleteRegistrationById(int id) {
        String sql = "delete from [registration] where id_register=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            st.executeUpdate();
        } catch (SQLException e) {
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void UpdateMyRegistration(int pid, int rid) {
        String sql = "UPDATE [dbo].[Registration]\n"
                + "   SET [id_PackagePrice] = ?\n"
                + " WHERE id_Register = ?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, pid);
            st.setInt(2, rid);
            st.executeUpdate();
        } catch (SQLException e) {
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public int countRegisStrationAll() {
        int count = 0;
        String sql = "select count(id_Register) as numberRegister from Registration";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                count = rs.getInt("numberRegister");
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return count;
    }

    public int countRegisStrationSuccess() {
        int count = 0;
        String sql = "select count(id_Register) as numberRegister from Registration\n"
                + "where status = 1";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                count = rs.getInt("numberRegister");
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return count;
    }

    public long getRevenueAll() {
        long count = 0;
        String sql = "select sum(p.sale_Price) as revenue from Registration r , PackagePrice	p\n"
                + "where r.id_PackagePrice = p.id_Package and r.status = 1";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                count = rs.getInt("revenue");
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return count;
    }

    public long getRevenueByMonth(int month) {
        long count = 0;
        String sql = "select isnull(sum(p.sale_Price),0) as dt from Registration r, PackagePrice p\n"
                + "where YEAR(registration_Date) = YEAR(GETDATE()) and MONTH(registration_Date)= ?\n"
                + "and p.id_Package = r.id_PackagePrice";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, month + 1);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                count = rs.getInt("dt");
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return count;
    }

    public long getRevenueByCategory(int id) {
        long count = 0;
        String sql = "select isnull(sum(p.sale_Price),0) as dt from Registration r, PackagePrice p\n"
                + "where  p.id_Package = r.id_PackagePrice and YEAR(registration_Date) = YEAR(GETDATE()) and r.id_Course in(\n"
                + "select id_Course from Course\n"
                + "where id_Catergory = ?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                count = rs.getInt("dt");
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return count;
    }

    public long getRevenueByCourse(int id) {
        long count = 0;
        String sql = "select ISNULL(sum(p.sale_Price),0) as revenue from Registration r, PackagePrice p\n"
                + "where r.id_Course = ? and r.id_PackagePrice = p.id_Package and r.status = 1";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                count = rs.getInt("revenue");
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return count;
    }

    public long getTotalRevenueCourse(int check) {
        int count = 0;
        LocalDate now = LocalDate.now();
        System.out.println(now.toString());
        LocalDate monthBefore1 = now.plusMonths(-1);
        int month = monthBefore1.getMonthValue();
        int year = monthBefore1.getYear();
        YearMonth yearMonth = YearMonth.of(year, month);
        LocalDate monthBefore = yearMonth.atDay(1);
        System.out.println(monthBefore.toString());
        String sql = "";
        if (check == 1) {
            sql = "select ISNULL(SUM(p.sale_Price),0) as totalrevenue from Registration r , PackagePrice p\n"
                    + "where r.id_PackagePrice = p.id_Package and r.status = 1 and r.id_Course in(\n"
                    + "SELECT [id_Course]\n"
                    + " FROM [dbo].[Course] where create_Date	BETWEEN ? AND ?)";
        } else if (check == 2) {
            sql = "select ISNULL(SUM(p.sale_Price),0) as totalrevenue from Registration r , PackagePrice p\n"
                    + "where r.id_PackagePrice = p.id_Package and r.status = 1 and r.id_Course in(\n"
                    + "SELECT [id_Course]\n"
                    + " FROM [dbo].[Course])";
        }

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            if (check == 1) {
                st.setDate(1, Date.valueOf(monthBefore));
                st.setDate(2, Date.valueOf(now));
            }

            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                long revenue = rs.getLong("totalrevenue");
                return revenue;
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return 0;
    }

    public int getTotalRegistraionCourse(int check, int status) {
        int count = 0;
        LocalDate now = LocalDate.now();
        System.out.println(now.toString());
        LocalDate monthBefore1 = now.plusMonths(-1);
        int month = monthBefore1.getMonthValue();
        int year = monthBefore1.getYear();
        YearMonth yearMonth = YearMonth.of(year, month);
        LocalDate monthBefore = yearMonth.atDay(1);
        System.out.println(monthBefore.toString());
        String sql = "";
        if (check == 1 && status == -1) {
            sql = "select count(*) as totalregistration from Registration \n"
                    + " where id_Course  in(SELECT [id_Course]\n"
                    + " FROM [dbo].[Course] where create_Date BETWEEN ? AND ?)";
        } else if (check == 1 && status == 1) {
            sql = "select count(*) as totalregistration from Registration \n"
                    + " where status = 1 and id_Course  in(SELECT [id_Course]\n"
                    + " FROM [dbo].[Course] where create_Date BETWEEN ? AND ?)";
        } else if (check == 1 && status == 0) {
            sql = "select count(*) as totalregistration from Registration \n"
                    + " where status = 0 and id_Course  in(SELECT [id_Course]\n"
                    + " FROM [dbo].[Course] where create_Date BETWEEN ? AND ?)";
        }

        if (check == 2 && status == -1) {
            sql = "select count(*) as totalregistration from Registration \n"
                    + " where id_Course in(SELECT [id_Course]\n"
                    + " FROM [dbo].[Course])";
        } else if (check == 2 && status == 1) {
            sql = "select count(*) as totalregistration from Registration \n"
                    + " where status = 1 and id_Course  in(SELECT [id_Course]\n"
                    + " FROM [dbo].[Course])";
        } else if (check == 2 && status == 0) {
            sql = "select count(*) as totalregistration from Registration \n"
                    + " where status = 0 and id_Course  in(SELECT [id_Course]\n"
                    + " FROM [dbo].[Course])";
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            if (check == 1) {
                st.setDate(1, Date.valueOf(monthBefore));
                st.setDate(2, Date.valueOf(now));
            }

            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                int numberRegis = rs.getInt("totalregistration");
                return numberRegis;
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return 0;
    }

    public ArrayList<Registration> getRegistration(int check, Date from, Date to) {
        ArrayList<Registration> list = new ArrayList<>();
        LocalDate now = LocalDate.now();
        LocalDate monthbefore = now.plusMonths(-1);
        String sql = "";
        if (check == 3) {
            sql = " SELECT  [id_Register],[id_User],[id_Course],[registration_Date]\n"
                    + ",[id_PackagePrice],[status],[valid_From],[valid_To]\n"
                    + " FROM [OnlineLearningSystem].[dbo].[Registration]\n"
                    + " WHERE status  = 1";
        } else if (check == 4) {
            sql = " SELECT  [id_Register],[id_User],[id_Course],[registration_Date]\n"
                    + ",[id_PackagePrice],[status],[valid_From],[valid_To]\n"
                    + " FROM [OnlineLearningSystem].[dbo].[Registration]\n"
                    + " WHERE status  = 0";
        } else if (check == 5) {
            sql = " SELECT  [id_Register],[id_User],[id_Course],[registration_Date]\n"
                    + ",[id_PackagePrice],[status],[valid_From],[valid_To]\n"
                    + " FROM [OnlineLearningSystem].[dbo].[Registration]\n"
                    + " WHERE status is null";
        } else if (check == 6) {
            sql = " SELECT  [id_Register],[id_User],[id_Course],[registration_Date]\n"
                    + ",[id_PackagePrice],[status],[valid_From],[valid_To]\n"
                    + " FROM [OnlineLearningSystem].[dbo].[Registration]\n"
                    + "";
        } else if (check == 7) {
            sql = " SELECT  [id_Register],[id_User],[id_Course],[registration_Date]\n"
                    + ",[id_PackagePrice],[status],[valid_From],[valid_To]\n"
                    + " FROM [OnlineLearningSystem].[dbo].[Registration] where registration_Date BETWEEN ? and GETDATE()\n"
                    + " order by registration_Date desc";
        }
        if (check == 3 || check == 4 || check == 5 || check == 6) {
            if (from != null && to != null) {
                if (check == 3 || check == 4 || check == 5) {
                    sql = sql + " and registration_Date BETWEEN ? and ? order by registration_Date desc";
                } else if (check == 6) {
                    sql = sql + " where registration_Date BETWEEN ? and ? order by registration_Date desc";
                }
            } else {
                sql = sql + " order by registration_Date desc";
            }
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            if (check == 7) {
                st.setDate(1, Date.valueOf(monthbefore));
            }
            if (from != null && to != null && check != 7) {
                st.setDate(1, from);
                st.setDate(2, to);
            }
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Registration re = new Registration();
                re.setId_Register(rs.getInt("id_Register"));
                re.setId_User(rs.getInt("id_User"));
                re.setId_Course(rs.getInt("id_Course"));
                re.setRegistration_Date(rs.getDate("registration_Date"));
                re.setId_packagePrice(rs.getInt("id_PackagePrice"));
                
                if (rs.getString("status") == null) {
                    re.setStatus(null);
                } else if (rs.getString("status").equals("1")) {
                    re.setStatus(Boolean.TRUE);
                } else if (rs.getString("status").equals("0")) {
                    re.setStatus(Boolean.FALSE);
                }
                re.setValid_From(rs.getDate("valid_From"));
                re.setValid_To(rs.getDate("valid_To"));
                list.add(re);
            }
            return list;
        } catch (SQLException e) {
        }
        return null;
    }

    public ArrayList<Registration> getRegistrationByCategory(int id) {
        ArrayList<Registration> list = new ArrayList<>();

        String sql = "SELECT  [id_Register],[id_User],[id_Course],[registration_Date]\n"
                + "      ,[id_PackagePrice],[status],[valid_From],[valid_To]\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Registration]\n"
                + "  where status in (0,1) and id_Course in(select id_Course from Course where id_Catergory = ?)";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Registration re = new Registration();
                re.setId_Register(rs.getInt("id_Register"));
                re.setId_User(rs.getInt("id_User"));
                re.setId_Course(rs.getInt("id_Course"));
                re.setRegistration_Date(rs.getDate("registration_Date"));
                re.setId_packagePrice(rs.getInt("id_PackagePrice"));
                if (rs.getString("status") == null) {
                    re.setStatus(null);
                } else if (rs.getString("status").equals("1")) {
                    re.setStatus(Boolean.TRUE);
                } else if (rs.getString("status").equals("0")) {
                    re.setStatus(Boolean.FALSE);
                }
                re.setValid_From(rs.getDate("valid_From"));
                re.setValid_To(rs.getDate("valid_To"));
                list.add(re);
            }
            return list;
        } catch (SQLException e) {
        }
        return null;
    }

    public ArrayList<Registration> getRegistrationFormTo(Date from, Date to) {
        ArrayList<Registration> list = new ArrayList<>();

        String sql = "SELECT  [id_Register],[id_User],[id_Course],[registration_Date]\n"
                + "      ,[id_PackagePrice],[status],[valid_From],[valid_To]\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Registration]\n"
                + "  where status in(1,0) and registration_Date between ? and ?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setDate(1, from);
            st.setDate(2, to);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Registration re = new Registration();
                re.setId_Register(rs.getInt("id_Register"));
                re.setId_User(rs.getInt("id_User"));
                re.setId_Course(rs.getInt("id_Course"));
                re.setRegistration_Date(rs.getDate("registration_Date"));
                re.setId_packagePrice(rs.getInt("id_PackagePrice"));
                if (rs.getString("status") == null) {
                    re.setStatus(null);
                } else if (rs.getString("status").equals("1")) {
                    re.setStatus(Boolean.TRUE);
                } else if (rs.getString("status").equals("0")) {
                    re.setStatus(Boolean.FALSE);
                }
                re.setValid_From(rs.getDate("valid_From"));
                re.setValid_To(rs.getDate("valid_To"));
                list.add(re);
            }
            return list;
        } catch (SQLException e) {
        }
        return null;
    }

    public int numberRegistrationOfCouorse(int idcourse) {
        String sql = "select count(id_Register) as numberregis from Registration\n"
                + "where id_Course = ?";
        int count = 0;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, idcourse);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                count = rs.getInt("numberregis");
            }
        } catch (SQLException e) {
        }
        return count;
    }

    public List<RegistrationJoin> getSearchDate(String From, String To) {
        List<RegistrationJoin> list = new ArrayList<>();
        String sql = "SELECT r.id_Register,r.id_User, u.email,r.id_Course, c.course_Name, r.registration_Date,\n"
                + "r.id_PackagePrice,p.list_Price,p.sale_Price, r.status, r.valid_From, r.valid_To from Registration r\n"
                + "inner join [dbo].[User] u  on r.id_User = u.id_User\n"
                + "        inner join Course c on r.id_Course = c.id_Course\n"
                + "             inner join [dbo].[PackagePrice] p on p.id_Package = r.id_PackagePrice \n"
                + "			 WHERE r.registration_Date BETWEEN ? AND ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, From);
            st.setString(2, To);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                RegistrationJoin r = new RegistrationJoin(); // Tạo đối tượng Registration mới cho mỗi vòng lặp
                r.setId_Register(rs.getInt("id_Register"));
                r.setId_User(rs.getInt("id_User"));
                r.setEmail(rs.getString("email"));
                r.setId_Course(rs.getInt("id_Course"));
                r.setCourse_Name(rs.getString("course_Name"));
                r.setRegistration_Date(rs.getDate("registration_Date"));
                r.setId_packagePrice(rs.getInt("id_PackagePrice"));
                r.setList_Price(rs.getInt("list_Price"));
                r.setSale_Price(rs.getInt("sale_Price"));
                r.setStatus(rs.getBoolean("status"));
                r.setValid_From(rs.getDate("valid_From"));
                r.setValid_To(rs.getDate("valid_To"));
                list.add(r);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

     public List<RegistrationJoin> getSearch(String email, String status, String sortID) {
        List<RegistrationJoin> list = new ArrayList<>();
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT r.id_Register, r.id_User, u.email, r.id_Course, c.course_Name, r.registration_Date,\n"
                + "r.id_PackagePrice, p.list_Price, p.sale_Price, r.status, r.valid_From, r.valid_To\n"
                + "FROM Registration r\n"
                + "INNER JOIN [dbo].[User] u ON r.id_User = u.id_User\n"
                + "INNER JOIN Course c ON r.id_Course = c.id_Course\n"
                + "INNER JOIN [dbo].[PackagePrice] p ON p.id_Package = r.id_PackagePrice\n");

        if (email != null && !email.trim().isEmpty()) {
            sql.append("WHERE u.email LIKE ?\n");
        } else {
            sql.append("WHERE 1 = 1\n"); // Điều kiện mặc định để kết nối với các điều kiện lọc tiếp theo
        }

        if (status != null && !status.trim().isEmpty()) {
            sql.append("AND ");
            switch (status) {
                case "1":
                    sql.append("r.status = 1");
                    break;
                case "2":
                    sql.append("r.status IS NULL ");
                    break;
                default:
                    sql.append("r.status = 0");
                    break;
            }

        }

        if (sortID != null && !sortID.trim().isEmpty()) {
            sql.append("ORDER BY ");
            switch (sortID) {
                case "1":
                    sql.append("u.email ASC\n");
                    break;
                case "2":
                    sql.append("c.course_Name ASC\n");
                    break;
                case "3":
                    sql.append("r.registration_Date ASC\n");
                    break;
                case "4":
                    sql.append("r.registration_Date DESC\n");
                    break;
                case "5":
                    sql.append("p.sale_Price ASC\n");
                    break;
                case "6":
                    sql.append("p.sale_Price DESC\n");
                    break;
                default:
                    break;
            }
        }

        try {
            PreparedStatement st = connection.prepareStatement(sql.toString());

            int parameterIndex = 1;

            if (email != null && !email.trim().isEmpty()) {
                st.setString(parameterIndex, "%" + email + "%");
                parameterIndex++;
            }

            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                RegistrationJoin r = new RegistrationJoin();
                r.setId_Register(rs.getInt("id_Register"));
                r.setId_User(rs.getInt("id_User"));
                r.setEmail(rs.getString("email"));
                r.setId_Course(rs.getInt("id_Course"));
                r.setCourse_Name(rs.getString("course_Name"));
                r.setRegistration_Date(rs.getDate("registration_Date"));
                r.setId_packagePrice(rs.getInt("id_PackagePrice"));
                r.setList_Price(rs.getInt("list_Price"));
                r.setSale_Price(rs.getInt("sale_Price"));
                if (rs.getString("status") == null) {
                    r.setStatus(null);
                } else if (rs.getString("status").equals("1")) {
                    r.setStatus(Boolean.TRUE);
                } else if (rs.getString("status").equals("0")) {
                    r.setStatus(Boolean.FALSE);
                }
                r.setValid_From(rs.getDate("valid_From"));
                r.setValid_To(rs.getDate("valid_To"));
                list.add(r);
            }

        } catch (SQLException e) {
            System.out.println(e);
        }

        return list;
    }

    public void updateStatus(String id_Register, Boolean status) {
        String sql = "UPDATE [dbo].[Registration]\n"
                + "                  SET [status] = ?\n"
                + "                 WHERE [id_Register] = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setBoolean(1, status);
            ps.setString(2, id_Register);

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<RegistrationJoin> getListByPage1(List<RegistrationJoin> list,
            int start, int end) {
        List<RegistrationJoin> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }

    public static void main(String[] args) {

    }

    public void deleteRegistrationByIdCourse(int idCourse) {
        String sql = "DELETE FROM [dbo].[Registration]\n"
                + "      WHERE id_Course =?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, idCourse);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }
    //cao hung

    public Registration getRegistrationByID(int id) {
        String sql = "SELECT [id_Register]\n"
                + "      ,[id_User]\n"
                + "      ,[id_Course]\n"
                + "      ,[registration_Date]\n"
                + "      ,[id_PackagePrice]\n"
                + "      ,[status]\n"
                + "      ,[valid_From]\n"
                + "      ,[valid_To]\n"
                + "  FROM [dbo].[Registration]\n"
                + "  where id_Register = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            Registration r = new Registration();
            while (rs.next()) {
                r.setId_Register(rs.getInt("id_Register"));
                r.setId_User(rs.getInt("id_User"));
                r.setId_Course(rs.getInt("id_Course"));
                r.setRegistration_Date(rs.getDate("registration_Date"));
                r.setId_packagePrice(rs.getInt("id_PackagePrice"));
                Boolean status = rs.getBoolean("status");  // Lấy giá trị từ ResultSet
                if (rs.wasNull()) {
                    r.setStatus(null);  // Gán giá trị null cho status nếu giá trị trong cơ sở dữ liệu là NULL
                } else {
                    r.setStatus(status);
                }
                r.setValid_From(rs.getDate("valid_From"));
                r.setValid_To(rs.getDate("valid_To"));
                return r;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public void updateIntoRegistrationWithId(Boolean status, int id) {
        String sql = "UPDATE [dbo].[Registration]\n"
                + "   SET [status] = ?\n"
                + " WHERE id_Register = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setBoolean(1, status);
            st.setInt(2, id);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void updateStatusToNull(int id) {
        String sql = "UPDATE [dbo].[Registration] SET [status] = NULL WHERE id_Register = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }
    //caohung
}
