/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.User;

/**
 *
 * @author vuduy
 */
public class RegisterDAO extends DBContext {
    public User checkRegister(String email){
        
        String query = "  SELECT [id_User]\n"
                + "      ,[email]\n"
                + "      ,[password]\n"
                + "      ,[full_Name]\n"
                + "      ,[phone_Number]\n"
                + "      ,[gender]\n"
                + "      ,[avartar]\n"
                + "      ,[address]\n"
                + "      ,[status]\n"
                + "      ,[id_Role]\n"
                + "  FROM [dbo].[User] where [email] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setString(1, email);
            ResultSet rs = st.executeQuery();   // thuc thi cau lenh
            while (rs.next()) {
                User u = new User();
                u.setId_User(rs.getInt("id_User"));
                u.setEmail(rs.getString("email"));
                u.setPassword(rs.getString("password"));
                u.setFull_Name(rs.getString("full_Name"));
                u.setPhone_Number(rs.getString("phone_Number"));
                u.setGender(rs.getBoolean("gender"));
                u.setAvatar(rs.getString("avartar"));
                u.setAddress(rs.getString("address"));
                u.setStatus(rs.getBoolean("status"));
                u.setId_role(rs.getInt("id_role"));
                return u;
                
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }
    
    public void Register(String email, String pass, String name,String phone, String gender){
        String query =  "INSERT INTO [dbo].[User] ([email] ,[password] ,[full_Name],[status],[id_Role],[create_Date],[phone_Number],[gender])\n" +
                        "VALUES (? ,? ,?,1,3,GETDATE(),?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            st.setString(1, email);
            st.setString(2, pass);
            st.setString(3, name);
            st.setString(4, phone);
            st.setBoolean(5, gender.equals("1"));
            st.executeUpdate();
            
        } catch (Exception e) {
        }
    }
}
