<%-- 
    Document   : newbloglist
    Created on : May 22, 2023, 5:57:32 PM
    Author     : Naviank
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">

    <head>

        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="Online Learning System - Group 2 SE1714" />

        <!-- OG -->
        <meta property="og:title" content="Online Learning System - Group 2 SE1714" />
        <meta property="og:description" content="Online Learning System - Group 2 SE1714" />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no">

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />

        <!-- PAGE TITLE HERE ============================================= -->
        <title>Online Learning System - Group 2 SE1714</title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/assets.css">

        <!-- TYPOGRAPHY ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/typography.css">

        <!-- SHORTCODES ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/shortcodes/shortcodes.css">

        <!-- STYLESHEETS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <link rel="stylesheet" type="text/css" href="assets/css/bloglist.css">
        <link class="skin" rel="stylesheet" type="text/css" href="assets/css/color/color-1.css">

        <!-- REVOLUTION SLIDER CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/vendors/revolution/css/layers.css">
        <link rel="stylesheet" type="text/css" href="assets/vendors/revolution/css/settings.css">
        <link rel="stylesheet" type="text/css" href="assets/vendors/revolution/css/navigation.css">
        <!-- REVOLUTION SLIDER END -->	
        
        <style>
            .active button{
                background-color: pink;
            }
        </style>
    </head>
    <body id="bg">
        <div class="page-wraper">
            <div id="loading-icon-bx"></div>
            <!-- Header Top ==== -->
            <jsp:include page="header.jsp"/>
            <!-- header END ==== -->
            <!-- Content -->
            <div class="page-content bg-white">
                <!-- inner page banner -->
                <div class="page-banner ovbl-dark" style="background-image:url(assets/images/banner/banner1.jpg);height: 150px">
                    <!--                    <div class="container">
                                            <div class="page-banner-entry">
                                                <h1 class="text-white" style="margin-top: 50px">Blog List</h1>
                                            </div>
                                        </div>-->
                </div>
                <!-- Breadcrumb row -->
                <div class="breadcrumb-row">
                    <div class="container">
                        <ul class="list-inline">
                            <div class="row">
                                <li style="width: 64px;"><a href="home">Home</a></li>
                                <li style=""><a href="bloglist">Blog List</a></li>
                                <!--<li style=""><a href="bloglist">Page: ${currentpage}</a></li>-->
                            </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- Breadcrumb row END -->
                <div class="content-block">
                    <div class="section-area section-sp1">
                        <div class="container">

                            <c:set var="currentorder" value="${requestScope.currentorder}"/>
                            <c:set var="currentpage" value="${requestScope.currentpage}"/>
                            <c:set var="currentcate" value="${requestScope.currentcate}"/>                            
                            <c:set var="currentfilt" value="${requestScope.currentfilt}"/> 



                            <!-- // -->
                            <ul class="content-menu list-inline ">
                                <div class="row" style="background-color: #f1f1f1;">
                                    <li style="padding: 4px 8px; margin-top: 8px;">
                                        <span>Sort posts by: </span>
                                    </li>
                                    <li style="padding: 4px 8px; ">
                                        <form>
                                            <input name="searchvalue" 
                                                   type="hidden"
                                                   value="${sessionScope.searchvalue}">
                                            <input name="filter" value="${1}" type="hidden">
                                            <!--<input name="order" value="${currentorder}" type="hidden">-->
                                            <input name="cate" value="${currentcate}" type="hidden">
                                            <input type="hidden" value="${1}" name="page">

                                            <!--
                                                                                        <button style="background-color: rgb(93, 4, 176); border-radius: 2px;
                                                                                                border: 1px solid black; padding: 8px; color: white" 
                                                                                                onclick="this.form.submit()">
                                                                                            location.href = 'bloglist?filter=1&cate=0&page=1&order=${currentorder}'
                                                                                            Newest posts first
                                                                                        </button>
                                            -->

                                            <!--<label>Update date</label>-->
                                            <select name="order" onchange="this.form.submit()">
                                                <option value="">Update date</option>
                                                <option value="1" ${(currentfilt == 1 && corder)? "selected" : ""}>Newest first</option>
                                                <option value="2" ${(currentfilt == 1 && !corder)? "selected" : ""}>Oldest first</option>
                                            </select>
                                        </form>
                                    </li>
                                    <li style="padding: 4px 8px; ">
                                        <form>
                                            <input name="searchvalue" 
                                                   type="hidden"
                                                   value="${sessionScope.searchvalue}">
                                            <input name="filter" value="${3}" type="hidden">
                                            <!--<input name="order" value="${currentorder}" type="hidden">-->
                                            <input name="cate" value="${currentcate}" type="hidden">
                                            <input type="hidden" value="${1}" name="page">

                                            <!--                                            <button style="background-color: rgb(93, 4, 176); border-radius: 5px;
                                                                                                border: 1px solid black; padding: 8px; color: white" 
                                                                                                onclick="this.form.submit()">
                                                                                            location.href = 'bloglist?filter=3&cate=0&page=1&order=${currentorder}'
                                                                                            Most views posts first
                                                                                        </button>-->

                                            <!--<label>View</label>-->
                                            <select name="order" onchange="this.form.submit()">
                                                <option value="">View</option>
                                                <option value="1" ${(currentfilt == 3 && !order)? "selected" : ""}>Most first</option>
                                                <option value="2" ${(currentfilt == 3 && order)? "selected" : ""}>Least first</option>
                                            </select>
                                        </form>
                                    </li>
                                </div>
                            </ul>
                            <p><b>${requestScope.mess}</b></p>
                            <!-- // -->
                            <div class="row">
                                <!-- left part start -->
                                <div class="col-lg-8 col-xl-8 col-md-7">
                                    <!-- blog grid -->
                                    <div id="masonry" class="ttr-blog-grid-3 row">
                                        <c:forEach items="${requestScope.listBlog}" var="b">
                                            <div class="post action-card col-xl-6 col-lg-6 col-md-6 col-xs-12 m-b40">
                                                <div class="recent-news">
                                                    <div class="action-box">
                                                        <a href="blogdetail?id_Blog=${b.id_Blog}"><img src="${b.thumbnail_Blog}" alt="post-image"></a>
                                                    </div>
                                                    <div class="info-bx">
                                                        <ul class="media-post">

                                                            <c:forEach items="${requestScope.listCate}" var="lc">


                                                                <c:if test="${lc.id_Catergory_Blog == b.id_CatergoryBlog}">

                                                                    <li><a href="#">${lc.name_Catergory_Blog}</a> </li>

                                                                </c:if>
                                                            </c:forEach>
                                                            <li></li>

                                                            <li><a href="#"><i class="fa fa-calendar"></i>${b.update_Date}</a></li>

                                                            <c:forEach items="${requestScope.listMarketer}" var="mu">

                                                                <c:if test="${b.id_User eq mu.id_User}">

                                                                    <li><a href="#"><i class="fa fa-user"></i>By ${mu.full_Name}</a></li>

                                                                </c:if>

                                                            </c:forEach>
                                                        </ul>
                                                        <div  style="height: 200px;">
                                                            <h5 class="post-title"><a href="blogdetail?id_Blog=${b.id_Blog}">${b.title}.</a></h5>
                                                            <p>${b.brief_Infor_Blog}.</p>
                                                        </div>
                                                        <div class="post-extra">
                                                            <a href="blogdetail?id_Blog=${b.id_Blog}" class="btn-link">READ MORE</a>
                                                            <a href="#" class="comments-bx"><i class="fa fa-eye"></i>${b.view} Views</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </c:forEach>
                                    </div>
                                    <!-- blog grid END -->
                                    <!-- Pagination -->
                                    <div class="pagination-bx rounded-sm gray clearfix">

                                        <form action="bloglist" id="pg-form">

                                            <div class="input-group">
                                                <input name="searchvalue" 
                                                       class="form-control" 
                                                       placeholder="Enter the post titles..." 
                                                       type="hidden"
                                                       value="${sessionScope.searchvalue}">
                                                <input name="filter" value="${currentfilt}" type="hidden">
                                                <input name="order" value="${currentorder}" type="hidden">
                                                <input name="cate" value="${currentcate}" type="hidden">
                                                <input type="hidden" value="${i}" name="page" id="pginput">

                                                <ul class="pagination">
                                                    <c:if  test="${currentpage > 1}">
                                                        <li class="previous">
                                                            <button onclick="submitForm(${currentpage - 1})"><i class="ti-arrow-left"></i> Prev</button>
                                                        </li>
                                                    </c:if>
                                                    <c:forEach begin="1" end="${requestScope.numofpage}" var="i">
                                                        <li class="${i == currentpage?"active":""}">
                                                            <button onclick="submitForm(${i})">${i}</button>
                                                        </li>
                                                    </c:forEach>
                                                    <c:if test="${currentpage < numofpage}">
                                                        <li class="next">
                                                            <button onclick="submitForm(${currentpage + 1})">Next <i class="ti-arrow-right"></i></button>
                                                        </li>
                                                    </c:if>
                                                </ul>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- Pagination END -->
                                </div>
                                <!-- left part END -->
                                <!-- Side bar start -->
                                <div class="col-lg-4 col-xl-4 col-md-5 sticky-top">
                                    <aside class="side-bar sticky-top">
                                        <div class="widget">
                                            <h6 class="widget-title">Search</h6>
                                            <div class="search-bx style-1">
                                                <form action="bloglist">
                                                    <div class="input-group">
                                                        <input name="searchvalue" 
                                                               class="form-control" 
                                                               placeholder="Enter the post titles..." 
                                                               type="text"
                                                               value="${sessionScope.searchvalue}">
                                                        <input name="filter" value="${4}" type="hidden">
                                                        <input name="order" value="${currentorder}" type="hidden">
                                                        <span class="input-group-btn">
                                                            <button type="submit" class="fa fa-search text-primary"></button>
                                                        </span> 
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="widget widget_tag_cloud">
                                            <h6 class="widget-title">Interesting categories</h6>
                                            <div class="tagcloud" id="listcateblog"> 
                                                <c:forEach items="${requestScope.listCate}" var="cl">

                                                    <a class="blogcate" href="bloglist?filter=2&cate=${cl.id_Catergory_Blog}&page=">${cl.name_Catergory_Blog}</a> 

                                                </c:forEach>
                                            </div>
                                            <button onclick="loadMore()">Load more</button>
                                        </div>

                                        <div class="widget recent-posts-entry">
                                            <h6 class="widget-title">Recent Posts</h6>
                                            <div class="widget-post-bx">
                                                <c:forEach items="${requestScope.recentBlog}" var="rb">
                                                    <div class="widget-post clearfix">
                                                        <div class="ttr-post-media"> <img src="${rb.thumbnail_Blog}" width="200" height="143" alt=""> </div>
                                                        <div class="ttr-post-info">
                                                            <div class="ttr-post-header">
                                                                <h6 class="post-title"><a href="blogdetail?id_Blog=${rb.id_Blog}">${rb.title}.</a></h6>
                                                            </div>
                                                            <ul class="media-post">
                                                                <li><a href="#"><i class="fa fa-calendar"></i>${rb.update_Date}</a></li>
                                                                <li><a href="#"><i class="fa fa-eye"></i>${rb.view} Views</a></li>

                                                                <c:forEach items="${requestScope.listMarketer}" var="muu">

                                                                    <c:if test="${rb.id_User eq muu.id_User}">

                                                                        <li><a href="#"><i class="fa fa-user"></i>By ${muu.full_Name}</a></li>

                                                                    </c:if>

                                                                </c:forEach>                                                                 
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </c:forEach>
                                            </div>
                                        </div>

                                    </aside>
                                </div>
                                <!-- Side bar END -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Left & right section END -->
            <!-- Content END-->
            <jsp:include page="footer.jsp"/>
            <!-- Footer END ==== -->
            <!-- scroll top button -->
            <button class="back-to-top fa fa-chevron-up" ></button>
        </div>
        <!-- External JavaScripts -->
        <script>
            function submitForm(value) {
                document.getElementById("pginput").setAttribute("value", value);

                document.getElementById("pg-form").submit();

            }
        </script>

        <script>
            function loadMore() {
                var count = document.getElementsByClassName("blogcate").length;
                $.ajax({
                    url: "/projectswp391/loadmore",
                    type: "get", //send it through get method
                    data: {
                        offset: count
                    },
                    success: function (response) {
//                        alert(response);
                        if (response === "") {
                            alert("There are no more categories!!");
                        } else {
                            document.getElementById("listcateblog").innerHTML += response;
                        }
                        //Do Something
                    },
                    error: function (xhr) {
                        //Do Something to handle error
                    }
                });
            }
        </script>
        <!--<script src='assets/vendors/switcher/switcher.js'></script>-->

    </body>

</html>
