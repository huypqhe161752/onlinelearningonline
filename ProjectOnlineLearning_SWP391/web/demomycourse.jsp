<%-- 
    Document   : demomycourse
    Created on : Jun 3, 2023, 5:29:21 PM
    Author     : PhanQuangHuy59
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

    <head>

        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="EduChamp : Education HTML Template" />

        <!-- OG -->
        <meta property="og:title" content="EduChamp : Education HTML Template" />
        <meta property="og:description" content="EduChamp : Education HTML Template" />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no">

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />

        <!-- PAGE TITLE HERE ============================================= -->
        <title>EduChamp : Education HTML Template </title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/assets.css">

        <!-- TYPOGRAPHY ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/typography.css">

        <!-- SHORTCODES ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/shortcodes/shortcodes.css">

        <!-- STYLESHEETS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <link class="skin" rel="stylesheet" type="text/css" href="assets/css/color/color-1.css">
        <link rel="stylesheet" href="assets/css/mycourse.css">
        <style>
            #message{
                background-color: rgba(197, 197, 191, 0.2);
                color: black;
                width: 100%;
                box-sizing: border-box;
                padding:  200px 100px 100px 200px;
                text-align: center;
                align-items: center;
                margin-top: 45px;
                font-size: 50px;
                font-weight: 1000;
                height: 90%;

            }
            .pagination {
                display: inline-block;
                margin-left: 50px auto;
                display: flex;
                justify-content: center;
                margin-left: 48% ;
            }
            .pagination a {
                color: black;
                font-size: 22px;
                float: left;
                padding: 8px 16px;
                border-radius: 4px;
                text-decoration: none;
            }
            .pagination a.active {
                background-color: rgb(93, 4, 176);
                border: 1px solid black;
                color: white;
            }
            .pagination a:hover:not(.active) {
                background-color: rgb(93, 4, 176);
            }
            .active{
                color: orange;
            }
            
        </style>

    </head>
    <body id="bg">
        <div class="page-wraper">
            <div id="loading-icon-bx"></div>

            <!-- Header Top ==== -->
            <jsp:include page="header.jsp"/> 
            <!-- header END ==== -->
            <!-- Content -->
            <div class="page-content bg-white" >
                <!-- inner page banner -->
                <div class="page-banner ovbl-dark" style="background-image:url(assets/images/banner/banner2.jpg); height: 150px" >
                    <div class="container">
                        <div class="page-banner-entry">
                            <h1 class="text-white" style="margin-top:50px;">My Courses</h1>
                        </div>
                    </div>
                </div>

                <!-- Breadcrumb row -->
                <div class="breadcrumb-row">
                    <div class="container">
                        <ul class="list-inline">
                            <li><a href="home">Home</a></li>
                            <li><a href="mycourse">Mycourse</a></li>
                        </ul>
                    </div>
                </div>

                <!-- Breadcrumb row END -->
                <!-- inner page banner END -->
                <div class="content-block" >
                    <!-- About Us -->
                    <div class="section-area section-sp2">
                        <div class="container">
                            <div class="dropdown">
                                <button onclick="myFunction()" class="dropbtn">Status Course</button>
                                <div id="myDropdown" class="dropdown-content">
                                    <a ${requestScope.check == 1 ?"active":""} href="mycourse?check=1">All</a>
                                    <a ${requestScope.check == 2 ?"active":""} href="mycourse?check=2">Courses have not expired</a>
                                    <a ${requestScope.check == 3 ?"active":""} href="mycourse?check=3">Courses Expired</a>
                                </div>
                            </div>
                            <div class="feature-filters clearfix center m-b40" style="margin-left: 310px;">
                                <a style="color: orange" href="mycourse?check=1"><span>My Courses</span></a> 
                                <a href="myregistrationservlet"><span>My Registration</span></a> 
                                <!--                                <ul class="filters" data-toggle="buttons">
                                                                    
                                                                    <li data-filter="" class="btn active">
                                                                        <input type="radio">
                                                                        
                                                                    </li>
                                                                    <li data-filter="" class="btn">
                                                                        <input type="radio">
                                                                        <a href="myregistrationservlet"><span>My Registration</span></a> 
                                                                    </li>
                                
                                                                </ul>-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pricingtable-row">

                    <div class="function" >

                        <div  id="search" >
                            <form action="mycourse" method="get">
                                <input type="hidden" value="4" name="check"/>
                                <label  class="lableSearch"  for="namesubject" >Search Name Subject In Course: </label>
                                <input class="searchkey" type="text" placeholder="What course are you looking for?" name="namecourse" value="${requestScope.key}" >
                                <input class="submitSearch" type="submit" value="Search">
                            </form>
                        </div>

                        <div class="checklimitcourse">
                            <!--                            <H4 class="TitleCategory">Check course expiration time</H4>
                                                        <form action="mycourse" method="get">
                                                            <input type="hidden" name="check" value="5"/>
                                                            
                                                        </form>-->
                            <form action="mycourse" method="get">
                                <input type="hidden" name="check" value="5"/>
                                <label  class="lableSearch"  for="namesubject" >Check course expiration time: </label>
                                <input class="searchkey" type="date"  name="month" value="${requestScope.month}"  name="month" style="width: 90%;margin-left: 10px">
                                <input class="submitSearch" type="submit" value="Check">
                            </form>


                        </div>

                        <div class="categoryCourse" >
                            <H4 class="TitleCategory">Search Courses Following Category Course</H4>
                            <form action="mycourse" method="get">
                                <input type="hidden" name="check" value="6"/>
                                <select name="category" onchange="this.form.submit()">
                                    <option value="" disabled selected hidden>Choose 1 option to search</option>
                                    <c:forEach var="listcategory" items="${requestScope.listcategory}">
                                        <option${requestScope.category == listcategory.getId_Catergory_Course() ?"selected":""} value="${listcategory.getId_Catergory_Course()}">
                                            ${listcategory.getName_Catergory_Course()}
                                        </option>
                                    </c:forEach>
                                </select>
                            </form>


                        </div>

                    </div>

                    <div class="listproduct">
                        <c:if test="${requestScope.listregistration.size() == 0}">
                            <div id="message">
                                You don't have any courses in My Course!
                            </div>
                        </c:if>

                        <c:if test="${requestScope.listregistration.size() != 0}">
                            <c:set var="listregistration" value="${requestScope.listregistration}"/>
                            <c:set var="listcourse" value="${requestScope.listcourse}"/>
                            <c:set var="checkexpire" value="${requestScope.checkexpire}"/>
                            <c:set var="numbersubject" value="${requestScope.numbersubject}"/>
                            <c:forEach begin="0" end="${listregistration.size() - 1}" var="i">
                                <div class= ${checkexpire.get(i) == true?"item1":"item"}>
                                    <div class="pricingtable-wrapper">
                                        <div class="pricingtable-inner">
                                            <div class="pricingtable-main row"> 
                                                <div class="pricingtable-price"> 
                                                    <img src="${listcourse.get(i).image}" alt="EduSourse">
                                                </div>
                                                <div class="pricingtable-title col-12">
                                                    <h2> ${listcourse.get(i).course_Name}</h2>
                                                </div>
                                                <div class="Dateto col-12">
                                                    <div class="datefrom">  
                                                        <h5>Registration Date </h5>
                                                        <h5>${listregistration.get(i).formatDateValidFrom()} </h5>
                                                    </div>
                                                    <div class="dateto">
                                                        <h5>Course end date</h5>
                                                        <h5>${listregistration.get(i).formatDateValidTo()}</h5>
                                                    </div>
                                                </div>
                                                <div class="container1">
                                                    <div class="createDate">
                                                        <h5>Create Date : ${listcourse.get(i).formatDateValidTo()}</h5>
                                                    </div>
                                                    <div class="createDate">
                                                        <h5>Number Subject : ${numbersubject.get(i)} Subject</h5>
                                                    </div>
                                                </div>   
                                            </div>
                                            <div class="pricingtable-footer"> 
                                                <a href="mycoursedetail?idcourse=${listcourse.get(i).id_Course}" class="btn radius-xl">Go To Course Details</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                            </c:forEach>
                        </c:if>



                    </div>                    
                </div>

            </div>
            <c:set var="page" value="${requestScope.page}"/>
            <div class="pagination col-3">
                <c:forEach begin="${1}" end="${requestScope.num1}" var="i">
                    <a class="${(page == i)?"active":""}" href="mycourse?page=${i}&check=${requestScope.check}&namecourse=${requestScope.key}&month=${requestScope.month}&category=${requestScope.category}">${i}</a>   
                </c:forEach>
            </div>
            <p class="phone-icon">
                <a class="support" href="http://127.0.0.1:5500/EduChamp-Education-HTML-Template-Admin-Dashboard/mycourse.html"><i class="fa fa-user"> </i></a>
            </p>



            <!-- Content END-->

            <!-- Footer ==== -->
            <!-- <jsp:include page="footer.jsp"/> -->
            <!-- Footer END ==== -->
            <button class="back-to-top fa fa-chevron-up" ></button>
        </div>
        <script src="assets/js/mycourse.js"></script>
    </body>

</html>


