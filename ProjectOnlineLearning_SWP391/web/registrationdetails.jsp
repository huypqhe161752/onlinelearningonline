<%-- 
    Document   : PageCourse
    Created on : May 17, 2023, 3:51:55 PM
    Author     : PCT
--%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from educhamp.themetrades.com/demo/admin/courses.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:10:19 GMT -->
    <head>
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/subjectlist.css">
        <script src="assestsAdmin/js/registrationdetails.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link href="assetsAdmin/select2/select2.min.css" rel="stylesheet" />
        <script src="assetsAdmin/select2/select2.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
        <script src="assets/js/PopUp.js"></script>
        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="EduChamp : Education HTML Template" />

        <!-- OG -->
        <meta property="og:title" content="EduChamp : Education HTML Template" />
        <meta property="og:description" content="EduChamp : Education HTML Template" />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no">

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="../error-404.html" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />

        <!-- PAGE TITLE HERE ============================================= -->
        <title>EduChamp : Education HTML Template </title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/assets.css">
        <link rel="stylesheet" type="text/css" href="assestsAdmin/vendors/calendar/fullcalendar.css">

        <!-- TYPOGRAPHY ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/typography.css">

        <!-- SHORTCODES ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/shortcodes/shortcodes.css">

        <!-- STYLESHEETS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/style.css">
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/dashboard.css">
        <link class="skin" rel="stylesheet" type="text/css" href="assets/css/color/color-1.css">
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/SubjectAdmin.css">
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/page.css">
        <style>
            .active{
                color: orange;
            }
        </style>
    </head>
</head>
<body class="ttr-opened-sidebar ttr-pinned-sidebar">
    <!-- header start -->
    <header class="ttr-header">
        <div class="ttr-header-wrapper">
            <!--sidebar menu toggler start -->
            <div class="ttr-toggle-sidebar ttr-material-button">
                <i class="ti-close ttr-open-icon"></i>
                <i class="ti-menu ttr-close-icon"></i>
            </div>
            <!--sidebar menu toggler end -->
            <!--logo start -->
            <div class="ttr-logo-box">
                <div>
                    <a href="index.html" class="ttr-logo">
                        <img alt="" class="ttr-logo-mobile" src="assets/images/logo-mobile.png" width="30" height="30">
                        <img alt="" class="ttr-logo-desktop" src="assets/images/logo-white.png" width="160" height="27">
                    </a>
                </div>
            </div>
            <!--logo end -->
            <div class="ttr-header-menu">
                <!-- header left menu start -->
                <ul class="ttr-header-navigation">
                    <li>
                        <a href="home" class="ttr-material-button ttr-submenu-toggle">HOME</a>
                    </li>
                </ul>
                <!-- header left menu end -->
            </div>
            <div class="ttr-header-right ttr-with-seperator">
                <!-- header right menu start -->
                <ul class="ttr-header-navigation">

                    <c:if test="${sessionScope.user!=null}">
 <li>
                            <a href="#" class="ttr-material-button">${sessionScope.user.full_Name}</a>
                        </li>

                        <li>
                            <a href="#" class="ttr-material-button ttr-submenu-toggle"
                               ><span class="ttr-user-avatar"
                                   ><img
                                        alt=""
                                        src="${sessionScope.user.avatar}"
                                        width="32"
                                        height="32" /></span
                                ></a>
                            <div class="ttr-header-submenu">
                                <ul>
                                    <li><a onclick="openPopup1('profilesala')">My profile</a></li>
                                    <li><a onclick="openPopup1('profilesalechagepass')">Change Password</a></li>
                                    <li><a href="logout">Logout</a></li>
                                </ul>
                            </div>
                        </li>
                        <li style="background-color: orange">
                            <a href="logout" class="ttr-material-button">Logout</a>
                        </li>
                        </c:if> 
                </ul>
                <!-- header right menu end -->
            </div>
            <!--header search panel start -->
            <div class="ttr-search-bar">
                <form class="ttr-search-form">
                    <div class="ttr-search-input-wrapper">
                        <input type="text" name="qq" placeholder="search something..." class="ttr-search-input">
                        <button type="submit" name="search" class="ttr-search-submit"><i class="ti-arrow-right"></i></button>
                    </div>
                    <span class="ttr-search-close ttr-search-toggle">
                        <i class="ti-close"></i>
                    </span>
                </form>
            </div>
            <!--header search panel end -->
        </div>
    </header>
    <!-- header end -->
    <!-- Left sidebar menu start -->
    <c:set value="${sessionScope.user}" var="su"/>
    <div class="ttr-sidebar">
        <div class="ttr-sidebar-wrapper content-scroll">
            <!-- side menu logo start -->
            <div class="ttr-sidebar-logo">
                <a href="#"><img alt="" src="assets/images/logo.png" width="122" height="27"></a>
                <!-- <div class="ttr-sidebar-pin-button" title="Pin/Unpin Menu">
                        <i class="material-icons ttr-fixed-icon">gps_fixed</i>
                        <i class="material-icons ttr-not-fixed-icon">gps_not_fixed</i>
                </div> -->
                <div class="ttr-sidebar-toggle-button">
                    <i class="ti-arrow-left"></i>
                </div>
            </div>
            <!-- side menu logo end -->
            <!-- sidebar menu start -->
            <nav class="ttr-sidebar-navi">
                <c:set var="currentpage" value="${requestScope.xxx}"/>
                <c:set var="r" value="${requestScope.registration}"/>
                <c:set var="u" value="${requestScope.user}"/>
                <c:set var="c" value="${requestScope.course}"/>
                <c:set var="pp" value="${requestScope.packageprice}"/>
                <ul>
                  
                    <li>
                        <a href="registrationlist" class="ttr-material-button">
                            <span class="ttr-icon"
                                  ><i class="ti-layout-list-post"></i
                                ></span>
                            <span class="ttr-label ${currentpage eq "RegistrationDetails"? 'active':""}">Registration List</span>
                        </a>
                    </li>
                    
                    <li>
                            <a href="" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-user"></i></span>
                                <span class="ttr-label">My Profile</span>
                                <span class="ttr-arrow-icon"
                                      ><i class="fa fa-angle-down"></i
                                    ></span>
                            </a>
                            <ul>
                                <li>
                                    <a onclick="openPopup1('profilesale')" class="ttr-material-button"
                                       ><span class="ttr-label">User Profile</span></a
                                    >
                                </li>
                                 <li>
                                    <a onclick="openPopup1('profilesalechangepass')" class="ttr-material-button"
                                       ><span class="ttr-label">Change Password</span></a
                                    >
                                </li>
                                <li>
                                    <a href="logout" class="ttr-material-button"
                                       ><span class="ttr-label">Logout</span></a
                                    >
                                </li>
                            </ul>
                        </li>
                    <li class="ttr-seperate"></li>
                </ul>
            </nav>
            <!-- sidebar menu end -->
        </div>
    </div>
    <!-- Left sidebar menu end -->

    <!--Main container start -->
    <main class="ttr-wrapper">
        <div class="container-fluid">
            <div class="db-breadcrumb">
                <!-- Nav tabs -->


                <!-- Tab panes -->

                <h4 class="breadcrumb-title">Registration Detail</h4>
                <c:set value="${requestScope.ppofcourse}" var="ppcourse"/>
                <ul class="db-breadcrumb-list">
                    <li><a href="registrationlist"><i class="fa fa-home"></i>Home</a></li>
                    <li><a href = "registrationlist">Registration List</a></li>
                    <li><a href = "#">Registration Details</a></li>
                </ul>
            </div>
            <div class="row">
                <!-- Your Profile Views Chart -->
                <div class="col-lg-12 m-b30">
                    <div class="widget-box">
                        <div class="widget-inner">
                            <div class="tab-content">
                                <form class="edit-profile m-b30" id="editFormRegistration" style="display: block" action="registrationdetails" method="post">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="ml-auto">
                                                <h2>Registration Details</h2>
                                            </div>
                                        </div>
                                        <div class="col-12 m-t20">
                                            <div class="ml-auto m-b5">
                                                <h3>Registration Information</h3>
                                            </div>
                                        </div>
                                        <c:set value="${sessionScope.user}" var="su"/>
                                        
                                            <div class="col-12 m-t20">
                                                <label class="col-form-label">Subject Name</label>
                                                <div>
                                                    <input class="form-control" name="editSubjectName"  type="text" value="${c.course_Name}">
                                                </div>
                                            </div>
                                            <div class="form-group col-6">
                                                <label class="col-form-label">List Price</label>
                                                <div>
                                                    <input class="form-control" name="editListPrice" type="text" value="${pp.list_Price}">
                                                </div>
                                            </div>
                                            <div class="form-group col-6">
                                                <label class="col-form-label">Sale Price</label>
                                                <div>
                                                    <input class="form-control" name="editSalePrice" type="text" value="${pp.sale_Price}">
                                                </div>
                                            </div>
                                            <div class="form-group col-6">
                                                <label class="col-form-label">Full Name</label>
                                                <div>
                                                    <input onclick="showAlert()" readonly="" class="form-control"  type="text" value="${u.full_Name}">
                                                </div>
                                            </div>
                                            <div class="form-group col-6">
                                                <label class="col-form-label">Gender</label>
                                                <div>
                                                    <c:if test="${u.gender == true}">
                                                        <input onclick="showAlert()" disabled="" type="radio" name="gender" value="male" checked=""> Male
                                                        <input onclick="showAlert()" disabled="" type="radio" name="gender" value="female"> Female
                                                    </c:if>
                                                    <c:if test="${u.gender == false}">
                                                        <input onclick="showAlert()" disabled="" type="radio" name="gender" value="male"> Male
                                                        <input onclick="showAlert()" disabled="" type="radio" name="gender" value="female" checked=""> Female
                                                    </c:if>
                                                </div>
                                            </div>
                                            <div class="form-group col-6">
                                                <label class="col-form-label">Email</label>
                                                <div>
                                                    <input onclick="showAlert()" readonly="" class="form-control"  type="email" value="${u.email}">
                                                </div>
                                            </div>
                                            <div class="form-group col-6">
                                                <label class="col-form-label">Mobile</label>
                                                <div>
                                                    <input onclick="showAlert()" readonly="" class="form-control"  type="text" value="${u.phone_Number}">
                                                </div>
                                            </div>
                                            <div class="form-group col-6">
                                                <label class="col-form-label">Registration Time</label>
                                                <div>
                                                    <input onclick="showAlert()" readonly="" class="form-control"  type="date" value="${r.registration_Date}">
                                                </div>
                                            </div>
                                            <div class="form-group col-6">
                                                <label class="col-form-label">Sale</label>
                                                <div>
                                                    <input onclick="showAlert()" readonly="" class="form-control"  type="text" value="${pp.sale_Price}">
                                                </div>
                                            </div>
                                            <div class="form-group col-6">
                                                <label class="col-form-label">Status</label>
                                                <div class="dropdown">
                                                    <button class="btn-success element-to-take-1" type="button" id="subjectButtonstyl" data-toggle="dropdown">
                                                        ${requestScope.statusRegistration == null ? "Submit" : requestScope.statusRegistration == true ? "Success" :  "Cancel"}<i style="position: relative; left: 180px" class="ti-arrow-down"></i>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li><button type="button" class="btn-success" onclick="updateStatusPackage(true)">Paid</button></li>
                                                        <li><button type="button" class="btn-success" onclick="updateStatusPackage(null)">Waiting</button></li>
                                                        <li><button type="button" class="btn-success" onclick="updateStatusPackage(false)">Denied</button></li>
                                                    </ul>
                                                    <input type="hidden" id="statusInput" name="registrationStatus" value="${requestScope.statusRegistration}">
                                                </div>
                                            </div>
                                            <div class="form-group col-6">
                                                <label class="col-form-label">Valid From</label>
                                                <div>
                                                    <input onclick="showAlert()" readonly="" class="form-control"  type="date" value="${r.valid_From}">
                                                </div>
                                            </div>
                                            <div class="form-group col-6">
                                                <label class="col-form-label">Valid To</label>
                                                <div>
                                                    <input onclick="showAlert()" readonly="" class="form-control"  type="date" value="${r.valid_To}">
                                                </div>
                                            </div>
                                        
                                        <input hidden="" name="idregistration" value="${r.id_Register}">
                                    </div>
                                    <button class="btn gradient green" type="submit">Save Changes</button>
                                    <button class="btn gradient gray">Cancel</button>
                                    <button type="button" style="margin-left: 100px" onclick="addFormOpen()" class="btn gradient red">Add new registration</button>
                                    <input hidden="" name="checkFormValue" value="1">
                                </form>
                                <form class="edit-profile m-b30" id="addFormRegistration" style="display: none" action="registrationdetails" method="post">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="ml-auto">
                                                <h2>Registration Details</h2>
                                            </div>
                                        </div>
                                        <div class="col-12 m-t20">
                                            <div class="ml-auto m-b5">
                                                <h3>Add new registration</h3>
                                            </div>
                                        </div>
                                        <div class="col-12 m-t20">
                                            <label class="col-form-label">Subject Name</label>
                                            <div class="dropdown">
                                                <button class="btn-success element-to-take-2" type="button" style="width: 1045px; height: 40px; border: 1px solid #c5c5c5;" data-toggle="dropdown">
                                                    Select<i class="ti-arrow-down"></i></button>
                                                <ul class="dropdown-menu scrollable">
                                                    <li class="input-container-todata">
                                                        <input oninput="searchByName(this)" type="text" style="width: 1045px; height: 40px; border: 1px solid #c5c5c5;" value="">
                                                        <i class="fa fa-search" aria-hidden="true"></i>
                                                    </li>
                                                    <li id="content">
                                                        <c:forEach items="${requestScope.listCourse}" var="lc">
                                                            <button type="button" onclick="changeText('${lc.course_Name}', '${lc.id_Course}')">${lc.course_Name}</button>
                                                        </c:forEach>
                                                    </li>
                                                </ul>
                                                <input id="selectedValue2" name="courseID" value="">
                                            </div>
                                        </div>
                                        <div class="form-group col-6">
                                            <label class="col-form-label">Package Price Duration</label>
                                            <div class="dropdown">
                                                <button class="btn-success element-to-take-8" type="button" style="width: 500px; height: 40px; border: 1px solid #c5c5c5;" data-toggle="dropdown">
                                                    Select<i class="ti-arrow-down"></i></button>
                                                <ul class="dropdown-menu scrollable">
                                                    <li id="pacakgePriceIdDuration">
                                                        <c:forEach items="${requestScope.listpackageprice}" var="lc1">
                                                            <button type="button" onclick="changeDuration('${lc1.duration}', '${lc1.id_Package}')">${lc1.duration}</button>
                                                        </c:forEach>
                                                    </li>
                                                </ul>
                                                <input id="selectedValue5" name="" value="">
                                            </div>
                                            </div>
                                            <div class="form-group col-6">
                                                <label class="col-form-label">Full Name</label>
                                                <div>
                                                    <input required="" class="form-control"  type="text" value="">
                                                </div>
                                            </div>
                                            <div class="form-group col-6">
                                                <label class="col-form-label">Gender</label>
                                                <div>
                                                    <input  type="radio" name="genderAdd" value="male" > Male
                                                    <input  type="radio" name="genderAdd" value="female"> Female
                                                </div>
                                            </div>
                                            <div class="form-group col-6">
                                                <label class="col-form-label">Email</label>
                                                <div>
                                                    <input required="" class="form-control"  type="email" value="">
                                                </div>
                                            </div>
                                            <div class="form-group col-6">
                                                <label class="col-form-label">Mobile</label>
                                                <div>
                                                    <input required="" class="form-control"  type="text" value="${su.phone_Number}">
                                                </div>
                                            </div>
                                            <div class="form-group col-6">
                                                <label class="col-form-label">Status</label>
                                                <div class="dropdown">
                                                    <button class="btn-success element-to-take-1" type="button" id="subjectButtonstyl" data-toggle="dropdown">
                                                        ${requestScope.statusRegistration == null ? "Submitted" : requestScope.statusRegistration == true ? "Success" :  "Cancelled"}<i style="position: relative; left: 180px" class="ti-arrow-down"></i>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li><button type="button" class="btn-success" onclick="updateStatusPackage(true)">Success</button></li>
                                                        <li><button type="button" class="btn-success" onclick="updateStatusPackage(null)">Submitted</button></li>
                                                        <li><button type="button" class="btn-success" onclick="updateStatusPackage(false)">Cancelled</button></li>
                                                    </ul>
                                                    <input type="hidden" id="statusInput" name="registrationStatus" value="${requestScope.statusRegistration}">
                                                </div>
                                            </div> 
                                            <input hidden="" name="idregistration" value="${r.id_Register}">
                                        </div>
                                        <button class="btn gradient green" type="submit">Add</button>
                                        <button class="btn gradient gray" type="button" onclick="hideAddForm()">Cancel</button>
                                        <input hidden="" name="checkFormValue" value="2">
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- Your Profile Views Chart END-->
                    </div>

                </div>
        </main>

        <div class="ttr-overlay"></div>

        <!-- External JavaScripts -->
        <script src="assestsAdmin/js/jquery.min.js"></script>
        <script src="assestsAdmin/vendors/bootstrap/js/popper.min.js"></script>
        <script src="assestsAdmin/vendors/bootstrap/js/bootstrap.min.js"></script>
        <script src="assestsAdmin/vendors/bootstrap-select/bootstrap-select.min.js"></script>
        <script src="assestsAdmin/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
        <script src="assestsAdmin/vendors/magnific-popup/magnific-popup.js"></script>
        <script src="assestsAdmin/vendors/counter/waypoints-min.js"></script>
        <script src="assestsAdmin/vendors/counter/counterup.min.js"></script>
        <script src="assestsAdmin/vendors/imagesloaded/imagesloaded.js"></script>
        <script src="assestsAdmin/vendors/masonry/masonry.js"></script>
        <script src="assestsAdmin/vendors/masonry/filter.js"></script>
        <script src="assestsAdmin/vendors/owl-carousel/owl.carousel.js"></script>
        <script src='assestsAdmin/vendors/scroll/scrollbar.min.js'></script>
        <script src="assestsAdmin/js/functions.js"></script>
        <script src="assestsAdmin/vendors/chart/chart.min.js"></script>
        <script src="assestsAdmin/js/admin.js"></script>

    </body>

    <!-- Mirrored from educhamp.themetrades.com/demo/admin/courses.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:11:35 GMT -->
</html>