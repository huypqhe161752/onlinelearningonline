<%-- 
    Document   : attemptquiz
    Created on : Jun 3, 2023, 8:13:36 PM
    Author     : Naviank
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>

        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="Online Learning System - Group 2 SE1714" />

        <!-- OG -->
        <meta property="og:title" content="Online Learning System - Group 2 SE1714" />
        <meta property="og:description" content="Online Learning System - Group 2 SE1714" />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no">

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />

        <!-- PAGE TITLE HERE ============================================= -->
        <title>Online Learning System - Group 2 SE1714</title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/assets.css">

        <!-- TYPOGRAPHY ============================================= -->
        <!--<link rel="stylesheet" type="text/css" href="assets/css/typography.css">-->

        <!-- SHORTCODES ============================================= -->
        <!--<link rel="stylesheet" type="text/css" href="assets/css/shortcodes/shortcodes.css">-->

        <!-- STYLESHEETS ============================================= -->
        <!--        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
                <link class="skin" rel="stylesheet" type="text/css" href="assets/css/color/color-1.css">-->

        <!-- REVOLUTION SLIDER CSS ============================================= -->
        <!--        <link rel="stylesheet" type="text/css" href="assets/vendors/revolution/css/layers.css">
                <link rel="stylesheet" type="text/css" href="assets/vendors/revolution/css/settings.css">
                <link rel="stylesheet" type="text/css" href="assets/vendors/revolution/css/navigation.css">-->
        <!-- REVOLUTION SLIDER END -->	

        <link rel="stylesheet" type="text/css" href="assets/css/vanpk_style.css">

    </head>
    <body>
        <div class="page-wraper">
            <div class="quiz-screen">
                <c:set var="cur_quiz" value="${requestScope.cur_quiz}"/>
                <c:set var="cur_number" value="${requestScope.cur_number}"/>
                <div class="quiz-pos">
                    <span ><button id="start-btn" onclick="countTimer(${cur_quiz.time_Limit})">Start Quiz</button>  </span>
                    <span id="question-position"><i class="fa fa-eye"></i> 1/${cur_quiz.number_Question}</span>
                    <span id="remaining-time"><i class="fa fa-eye"></i></span>
                </div>
                <c:set var="questionMap" value="${requestScope.questionMap}"/>
                <c:forEach items="${questionMap}" var="item">
                    <c:set var="cur_number" value="${cur_number + 1}"/>
                    <c:set var="cur_question" value="${item.getKey()}"/>
                    <c:set var="cur_answerlist" value="${item.getValue()}"/>
                    <div class="quiz-content">
                        <div class="quiz-id">
                            <div class="number">
                                <span>${cur_number}</span>
                            </div>
                            <div class="qid">
                                <span>Question ID: ${cur_question.id_Question}</span>
                            </div>
                        </div>
                        <div class="quiz-qa">

                            <div class="quiz-question">
                                <p>Question content: ${cur_question.content_Question}</p>
                            </div>
                            <div class="quiz-answer">
                                <c:forEach items="${cur_answerlist}" var="cur_answer">
                                    <div class="answer">
                                        <input type="radio" name="" value=""> <span>&emsp;${cur_answer.answer_Content}</span>
                                    </div>   
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                </c:forEach>
                <div class="quiz-nav">
                    <button onclick="openPopUp('reviewprogress', 800, 600)">Review Progress</button>
                    <button onclick="openPopUp('peekatanswer', 400, 300)">Peek At Answer</button>
                    <button onclick="openPopUp('peekatanswer', 400, 300)">Next Question</button>
                </div>

                <div>
                    <p id="demo">Window size:</p>
                    <p id="w-width">Width:</p>
                    <p id="w-height">Height:</p>
                </div>

            </div>
        </div>

<!--        <script>
            function openPopUp(url, width, height) {
                let params = 'scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,width=' + width + ',height=' + height + ',left=500,top=500';
                alert(height+ ", " + width);
                open(url, 'test', params);
            }
        </script>-->
        <script type="text/javascript"　src="assets/js/attemptquiz.js"></script>
        <script src="assets/js/PopUp.js"></script>
        <script src="assets/js/jquery.min.js"></script>
    </body>
</html>
