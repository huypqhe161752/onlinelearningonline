var successMessage = document.getElementById("success-message");

if (successMessage) {
    successMessage.style.display = "none"; // Ẩn ban đầu

    setTimeout(function () {
        successMessage.style.display = "none"; // Ẩn sau 3 giây
    }, 3000);

    successMessage.style.display = "block"; // Hiển thị khi điều kiện xảy ra
}