<%-- 
    Document   : test
    Created on : Jun 7, 2023, 11:23:35 AM
    Author     : Naviank
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <c:set var="quiz" value="${requestScope.quiz}"/>
        <c:set var="remain" value="${requestScope.remain}"/>
        <c:set var="start_time" value="${sessionScope.start_time}"/>
        <c:set var="cur_answer_string" value="${sessionScope.cur_answer_string}"/>
        <p>Quiz_ID: ${quiz.id_Quiz}</p>
        <p>Time: ${remain}</p>
        <p>Start time: ${start_time}</p>
        <!--<p>Answer string: ${cur_answer_string}</p>-->
        <p>${requestScope.count} / ${quiz.number_Question}</p>
        
        <a href="home">Home</a>
    </body>
</html>
