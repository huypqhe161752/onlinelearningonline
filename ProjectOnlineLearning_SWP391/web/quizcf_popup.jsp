<%-- 
    Document   : newjsp
    Created on : Jun 18, 2023, 12:55:44 AM
    Author     : Naviank
--%>
<%--<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="assets/css/vanpk_style.css">

        <title>Quiz confirm popup</title>
    </head>
    <body>
        <c:set var="cur_quiz" value="${sessionScope.cur_quiz}"/>
        <c:set var="cur_less" value="${sessionScope.cur_less}"/>

        <div id="cfsubmit">
            <h1>Score exam!?</h1>
            <p class="description">
                ${requestScope.message}
            </p>
            <div class="score-button">
                <!--<p>${sessionScope.cur_answer_string}</p>-->
                <button onclick="closeWindowAndPreventSubmit(event)">Back</button>
                <button onclick="changeLayout()">Score exam</button>
            </div>
        </div>
        <div id="congrat" style="display: none;">
            <p>Congratulations! You have finish the quiz! </p>
            <div class="">
                <button onclick="closePopupAndRedirect('quizlesson?id_lesson=${cur_quiz.id_Lession}')">Try other quiz</button>
                <button onclick="closePopupAndRedirect('lessonview?idsubject=${sessionScope.cur_less.id_Subject}')">Back to lesson</button>
                <button id="viewrs-btn">View result</button>
            </div>
        </div>
        <script 
            src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js">

        </script>

        <script>
            function changeLayout() {

                $.ajax({
                    url: "/projectswp391/quizsubmit",
                    type: "get", //send it through get method
                    data: {
                    },
                    success: function (response) {
//                        location.href = "quiz_aftercf_popup.jsp";
//                        openPopUp("quiz_aftercf_popup.jsp", 600, 400);
                        document.getElementById("cfsubmit").style.display = "none";
                        document.getElementById("congrat").style.display = "block";
                        document.getElementById("viewrs-btn").addEventListener("click", closePopupAndRedirect("reviewdetails?id_Result=" + response));
//                        alert();
                        //Do Something
                    },
                    error: function (xhr) {
                        //Do Something to handle error
                    }
                });
            }
        </script>

        <script 
            src="assets/js/PopUp.js">
        </script>
    </body>
</html>
