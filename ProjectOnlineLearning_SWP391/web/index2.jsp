<%-- 
    Document   : index2
    Created on : Jun 29, 2023, 3:01:07 PM
    Author     : Naviank
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

    <head>

        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="EduChamp : Education HTML Template" />

        <!-- OG -->
        <meta property="og:title" content="EduChamp : Education HTML Template" />
        <meta property="og:description" content="EduChamp : Education HTML Template" />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no">

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />

        <!-- PAGE TITLE HERE ============================================= -->
        <title>EduChamp : Education HTML Template </title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/assets.css">

        <!-- TYPOGRAPHY ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/typography.css">

        <!-- SHORTCODES ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/shortcodes/shortcodes.css">

        <!-- STYLESHEETS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <link class="skin" rel="stylesheet" type="text/css" href="assets/css/color/color-1.css">

        <!-- REVOLUTION SLIDER CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/vendors/revolution/css/layers.css">
        <link rel="stylesheet" type="text/css" href="assets/vendors/revolution/css/settings.css">
        <link rel="stylesheet" type="text/css" href="assets/vendors/revolution/css/navigation.css">
        <!-- REVOLUTION SLIDER END -->	
    </head>
    <body id="bg">
        <div class="page-wraper">
            <div id="loading-icon-bx"></div>
            <!-- Header Top ==== -->            
            <jsp:include page="header.jsp"/>
            <div class="page-banner ovbl-dark" style="background-image:url(assets/images/banner/banner1.jpg);height: 80px">
                <div class="container">
                </div>
            </div>
            <!-- Header Top END ==== -->
            <!-- Content -->
            <div class="page-content bg-white">
                <!-- Main Slider -->
                <div class="rev-slider">
                    <div id="rev_slider_486_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="news-gallery36" data-source="gallery" style="margin:0px auto;background-color:#ffffff;padding:0px;margin-top:0px;margin-bottom:0px;">
                        <!-- START REVOLUTION SLIDER 5.3.0.2 fullwidth mode -->
                        <div id="rev_slider_486_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.3.0.2">
                            <ul>	<!-- SLIDE  -->
                                <c:set var="number" value="100"/>
                                <c:forEach items="${requestScope.listSlider}" var="sl">
                                    <!--data-index="rs-100"--> 
                                    <li 
                                        data-transition="parallaxvertical" 
                                        data-slotamount="default" 
                                        data-hideafterloop="0" 
                                        data-hideslideonmobile="off" 
                                        data-easein="default" 
                                        data-easeout="default" 
                                        data-masterspeed="default" 
                                        data-thumb="error-404.html" 
                                        data-rotate="0" 
                                        data-fstransition="fade" 
                                        data-fsmasterspeed="1500" 
                                        data-fsslotamount="7" 
                                        data-saveperformance="off" 
                                        data-title="Naviank X Lyly" 
                                        data-param1="" data-param2="" 
                                        data-param3="" data-param4="" 
                                        data-param5="" data-param6="" 
                                        data-param7="" data-param8="" 
                                        data-param9="" data-param10="" 
                                        data-description="Naviank X Lyly">
                                        <!-- MAIN IMAGE -->
                                        <img src="${sl.thumbnail_Image}" alt="Slider thumbnail image" 
                                             data-bgposition="center center" 
                                             data-bgfit="cover" 
                                             data-bgrepeat="no-repeat" 
                                             data-bgparallax="10" 
                                             class="rev-slidebg" 
                                             data-no-retina />
                                        <!-- LAYER NR. 1 -->
                                        <div class="tp-caption tp-shape tp-shapewrapper " 
                                             id="slide-100-layer-1" 
                                             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                             data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
                                             data-width="full"
                                             data-height="full"
                                             data-whitespace="nowrap"
                                             data-type="shape" 
                                             data-basealign="slide" 
                                             data-responsive_offset="off" 
                                             data-responsive="off"
                                             data-frames='[{"from":"opacity:0;","speed":1,"to":"o:1;","delay":0,"ease":"Power4.easeOut"},{"delay":"wait","speed":1,"to":"opacity:0;","ease":"Power4.easeOut"}]'
                                             data-textAlign="['left','left','left','left']"
                                             data-paddingtop="[0,0,0,0]"
                                             data-paddingright="[0,0,0,0]"
                                             data-paddingbottom="[0,0,0,0]"
                                             data-paddingleft="[0,0,0,0]"
                                             style="z-index: 5;background-color:rgba(2, 0, 11, 0.80);border-color:rgba(0, 0, 0, 0);border-width:0px;">
                                        </div>	
                                        <!-- LAYER NR. 2 -->
                                        <div class="tp-caption Newspaper-Title   tp-resizeme" 
                                             id="slide-100-layer-2" 
                                             data-x="['center','center','center','center']" 
                                             data-hoffset="['0','0','0','0']" 
                                             data-y="['top','top','top','top']" 
                                             data-voffset="['250','250','250','240']" 
                                             data-fontsize="['50','50','50','30']"
                                             data-lineheight="['55','55','55','35']"
                                             data-width="full"
                                             data-height="none"
                                             data-whitespace="normal"
                                             data-type="text" 
                                             data-responsive_offset="on" 
                                             data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                                             data-textAlign="['center','center','center','center']"
                                             data-paddingtop="[0,0,0,0]"
                                             data-paddingright="[0,0,0,0]"
                                             data-paddingbottom="[10,10,10,10]"
                                             data-paddingleft="[0,0,0,0]"
                                             style="z-index: 6; font-family:rubik; font-weight:700; text-align:center; white-space: normal;">
                                            ${sl.title_Slider}
                                        </div>

                                        <!-- LAYER NR. 3 -->
                                        <div class="tp-caption Newspaper-Subtitle   tp-resizeme" 
                                             id="slide-100-layer-3" 
                                             data-x="['center','center','center','center']" 
                                             data-hoffset="['0','0','0','0']" 
                                             data-y="['top','top','top','top']" 
                                             data-voffset="['210','210','210','210']" 
                                             data-width="none"
                                             data-height="none"
                                             data-whitespace="nowrap"
                                             data-type="text" 
                                             data-responsive_offset="on"
                                             data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                                             data-textAlign="['left','left','left','left']"
                                             data-paddingtop="[0,0,0,0]"
                                             data-paddingright="[0,0,0,0]"
                                             data-paddingbottom="[0,0,0,0]"
                                             data-paddingleft="[0,0,0,0]"
                                             style="z-index: 7; white-space: nowrap; color:#fff; font-family:rubik; font-size:18px; font-weight:400;">
                                            ${sl.content_Slider}
                                        </div>

                                        <!-- LAYER NR. 3 -->
                                        <div class="tp-caption Newspaper-Subtitle   tp-resizeme" 
                                             id="slide-100-layer-4" 
                                             data-x="['center','center','center','center']" 
                                             data-hoffset="['0','0','0','0']" 
                                             data-y="['top','top','top','top']" 
                                             data-voffset="['320','320','320','290']" 
                                             data-width="['800','800','700','420']"
                                             data-height="['100','100','100','120']"
                                             data-whitespace="unset"
                                             data-type="text" 
                                             data-responsive_offset="on"
                                             data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                                             data-textAlign="['center','center','center','center']"
                                             data-paddingtop="[0,0,0,0]"
                                             data-paddingright="[0,0,0,0]"
                                             data-paddingbottom="[0,0,0,0]"
                                             data-paddingleft="[0,0,0,0]"
                                             style="z-index: 7; text-transform:capitalize; white-space: unset; color:#fff; font-family:rubik; font-size:18px; line-height:28px; font-weight:400;">
                                            ${sl.notes}
                                        </div>
                                        <!-- LAYER NR. 4 -->
                                        <div class="tp-caption Newspaper-Button rev-btn " 
                                             id="slide-100-layer-5" 
                                             data-x="['center','center','center','center']" 
                                             data-hoffset="['90','80','75','90']" 
                                             data-y="['top','top','top','top']" 
                                             data-voffset="['400','400','400','420']" 
                                             data-width="none"
                                             data-height="none"
                                             data-whitespace="nowrap"
                                             data-type="button" 
                                             data-responsive_offset="on" 
                                             data-responsive="off"
                                             data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);bw:1px 1px 1px 1px;"}]'
                                             data-textAlign="['center','center','center','center']"
                                             data-paddingtop="[12,12,12,12]"
                                             data-paddingright="[30,35,35,15]"
                                             data-paddingbottom="[12,12,12,12]"
                                             data-paddingleft="[30,35,35,15]"
                                             style="z-index: 8; white-space: nowrap; outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer; background-color:var(--primary) !important; border:0; border-radius:30px; margin-right:5px;">
                                            <a style="color: pink; font-size: 16px" href="${pageContext.request.contextPath}${sl.backlink}">VISIT LINK</a> 
                                        </div>
                                        <div class="tp-caption Newspaper-Button rev-btn" 
                                             id="slide-100-layer-6" 
                                             data-x="['center','center','center','center']" 
                                             data-hoffset="['-90','-80','-75','-90']" 
                                             data-y="['top','top','top','top']" 
                                             data-voffset="['400','400','400','420']" 
                                             data-width="none"
                                             data-height="none"
                                             data-whitespace="nowrap"
                                             data-type="button" 
                                             data-responsive_offset="on" 
                                             data-responsive="off"
                                             data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);bw:1px 1px 1px 1px;"}]'
                                             data-textAlign="['center','center','center','center']"
                                             data-paddingtop="[12,12,12,12]"
                                             data-paddingright="[30,35,35,15]"
                                             data-paddingbottom="[12,12,12,12]"
                                             data-paddingleft="[30,35,35,15]"
                                             style="z-index: 8; white-space: nowrap; outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer; border-radius:30px;">
                                            <!--<a style="color: pink; font-size: 16px" href="${pageContext.request.contextPath}${sl.backlink}">VISIT LINK</a>--> 
                                        </div>
                                    </li>
                                </c:forEach>
                                <!-- SLIDE  -->
                            </ul>
                        </div><!-- END REVOLUTION SLIDER -->  
                    </div>  
                </div>  
                <!-- Main Slider -->
                <div class="content-block">

                    <!-- Our Services -->
                    <!--                    
                                        <div class="section-area content-inner service-info-bx">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-lg-4 col-md-4 col-sm-6">
                                                        <div class="service-bx">
                                                            <div class="action-box">
                                                                <img src="assets/images/our-services/pic1.jpg" alt="">
                                                            </div>
                                                            <div class="info-bx text-center">
                                                                <div class="feature-box-sm radius bg-white">
                                                                    <i class="fa fa-bank text-primary"></i>
                                                                </div>
                                                                <h4><a href="#">Best Industry Leaders</a></h4>
                                                                <a href="#" class="btn radius-xl">View More</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-6">
                                                        <div class="service-bx">
                                                            <div class="action-box">
                                                                <img src="assets/images/our-services/pic2.jpg" alt="">
                                                            </div>
                                                            <div class="info-bx text-center">
                                                                <div class="feature-box-sm radius bg-white">
                                                                    <i class="fa fa-book text-primary"></i>
                                                                </div>
                                                                <h4><a href="#">Learn Courses Online</a></h4>
                                                                <a href="#" class="btn radius-xl">View More</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                                        <div class="service-bx m-b0">
                                                            <div class="action-box">
                                                                <img src="assets/images/our-services/pic3.jpg" alt="">
                                                            </div>
                                                            <div class="info-bx text-center">
                                                                <div class="feature-box-sm radius bg-white">
                                                                    <i class="fa fa-file-text-o text-primary"></i>
                                                                </div>
                                                                <h4><a href="#">Book Library & Store</a></h4>
                                                                <a href="#" class="btn radius-xl">View More</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                    -->
                    <!-- Our Services END -->

                    <!-- Popular Courses -->
                    <div class="section-area section-sp2 popular-courses-bx">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 heading-bx left">
                                    <h2 class="title-head">Popular <span>Courses</span></h2>
                                    <p>Our most valuable courses</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="courses-carousel owl-carousel owl-btn-1 col-12 p-lr0">
                                    <jsp:useBean id="coursecate" scope="request" class="model.CatergoryCourse"/>
                                    <c:forEach  items="${requestScope.listPopularCourse}" var="pc">

                                        <div class="item">
                                            <div class="cours-bx">
                                                <div class="action-box">
                                                    <img src="${pc.image}" alt="">
                                                    <a href="coursedetail?idcourse=${pc.id_Course}" class="btn">Read More</a>
                                                </div>
                                                <div class="info-bx text-center" style="height: 160px;">
                                                    <h5><a href="coursedetail?idcourse=${pc.id_Course}">${pc.course_Name}</a></h5>
                                                        <jsp:setProperty name="coursecate" property="id_Catergory_Course" value="${pc.id_Category}"/>
                                                    <span>
                                                        <jsp:getProperty name="coursecate" property="name_Catergory_Course"/>
                                                    </span>
                                                </div>
                                                <div class="cours-more-info">
                                                    <div class="review">
                                                        <span>Created from: ${pc.create_Date}</span>
                                                    </div>   
                                                    <div class="price">
                                                        <c:forEach items="${requestScope.lowestPPrice}" var="pp">
                                                            <c:if test="${pp.id_Course eq pc.id_Course}">
                                                                <del>
                                                                    ${pp.list_Price}
                                                                </del>
                                                                <h5>
                                                                    ${pp.sale_Price}
                                                                </h5>
                                                            </c:if>
                                                        </c:forEach>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </c:forEach>
                                </div>
                            </div>
                            <center style="margin: 12px;">
                                <a href="courseservlet" style="background-color: rgb(93, 4, 176); border-radius: 2px; border: 1px solid black; padding: 8px; color: white";>
                                    See more
                                </a>
                            </center>
                        </div>
                    </div>
                    <!-- Popular Courses END -->

                    <!-- Form -->
                    <div class="section-area section-sp1 ovpr-dark bg-fix online-cours" style="background-image:url(assets/images/background/bg1.jpg);">
                        <div class="container">
                            <div class="mw800 m-auto">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="cours-search-bx m-b30">
                                            <div class="icon-box">
                                                <h3><i class="ti-user"></i><span class="counter">${requestScope.numofcus}</span></h3>
                                            </div>
                                            <span class="cours-search-text">Over ${requestScope.numofcus} students</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="cours-search-bx m-b30">
                                            <div class="icon-box">
                                                <h3><i class="ti-book"></i><span class="counter">${requestScope.numofcourse}</span></h3>
                                            </div>
                                            <span class="cours-search-text">${requestScope.numofcourse} Courses.</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Form END -->
                    <!-- Testimonials -->
                    <!-- Testimonials END -->

                    <!-- Recent News -->
                    <div class="section-area section-sp2">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 heading-bx left">
                                    <h2 class="title-head">Recent <span>News</span></h2>
                                    <p>See our most recently posts</p>
                                </div>
                            </div>
                            <div class="recent-news-carousel owl-carousel owl-btn-1 col-12 p-lr0">

                                <c:forEach items="${requestScope.listBlog}" var="b">
                                    <div class="item">
                                        <div class="recent-news">
                                            <div class="action-box">
                                                <img src="${b.thumbnail_Blog}" alt="">
                                            </div>
                                            <div class="info-bx">
                                                <ul class="media-post">
                                                    <li><a href="#"><i class="fa fa-calendar"></i>${b.update_Date}</a></li>

                                                    <c:forEach items="${requestScope.listMarketer}" var="mu">
                                                        <c:if test="${b.id_User eq mu.id_User}">
                                                            <li><a href="#"><i class="fa fa-user"></i>By ${mu.full_Name}</a></li>
                                                            </c:if>
                                                        </c:forEach>

                                                </ul>
                                                <div style="height: 180px;  ">
                                                    <h5 class="post-title"><a href="blogdetail?id_Blog=${b.id_Blog}">${b.title}.</a></h5>
                                                    <p>${b.brief_Infor_Blog}.</p>
                                                </div>
                                                <div class="post-extra">
                                                    <a href="#" class="btn-link">READ MORE</a>
                                                    <a href="#" class="comments-bx"><i class="fa fa-eye"></i>${b.view} Views</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </c:forEach>
                            </div>
                            <center style="margin: 12px;">
                                <a href="bloglist" style="background-color: rgb(93, 4, 176); border-radius: 2px; border: 1px solid black; padding: 8px; color: white";>
                                    See all
                                </a>
                            </center>
                        </div>
                    </div>
                    <!-- Recent News End -->

                </div>
                <!-- contact area END -->
            </div>
            <!-- Content END-->
            <!-- Footer ==== -->
            <footer>
            <div class="footer-top">
                <div class="pt-exebar">
                    <div class="container">
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 col-md-12 col-sm-12 footer-col-4">
                            <div class="widget">
                                <a href="home"><img src="assets/images/logo-white.png" alt=""/></a>
                            </div>
                        </div>
                        <div class="col-12 col-lg-5 col-md-7 col-sm-12">
                            <div class="row">
                                <div class="col-4 col-lg-4 col-md-4 col-sm-4">
                                    <div class="widget footer_widget">
                                        <h5 class="footer-title">SOL system</h5>
                                        <ul>
                                            <li><a href="home">Home</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-4 col-lg-4 col-md-4 col-sm-4">
                                    <div class="widget footer_widget">
                                        <h5 class="footer-title">Information</h5>
                                        <ul>
                                            <li><a href="bloglist">Blogs</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-4 col-lg-4 col-md-4 col-sm-4">
                                    <div class="widget footer_widget">
                                        <h5 class="footer-title">Learn</h5>
                                        <ul>
                                            <li><a href="courseservlet">Courses</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-3 col-md-5 col-sm-12 footer-col-4">
                            <div class="widget widget_gallery gallery-grid-4">
                                <h5 class="footer-title">Location</h5>
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1809.2232232466235!2d105.52507393553647!3d21.013675153824213!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31345b465a4e65fb%3A0xaae6040cfabe8fe!2zVHLGsOG7nW5nIMSQ4bqhaSBI4buNYyBGUFQ!5e0!3m2!1svi!2s!4v1689500393820!5m2!1svi!2s" width="400" height="300" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 text-center"> <a target="_blank" href="https://facebook.com/naviank.pham.2407">Modified by Naviank</a></div>
                    </div>
                </div>
            </div>
        </footer>
            <!-- Footer END ==== -->
            <button class="back-to-top fa fa-chevron-up" ></button>
        </div>

        <!-- External JavaScripts -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/vendors/bootstrap/js/popper.min.js"></script>
        <script src="assets/vendors/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/vendors/bootstrap-select/bootstrap-select.min.js"></script>
        <script src="assets/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
        <script src="assets/vendors/magnific-popup/magnific-popup.js"></script>
        <script src="assets/vendors/counter/waypoints-min.js"></script>
        <script src="assets/vendors/counter/counterup.min.js"></script>
        <script src="assets/vendors/imagesloaded/imagesloaded.js"></script>
        <script src="assets/vendors/masonry/masonry.js"></script>
        <script src="assets/vendors/masonry/filter.js"></script>
        <script src="assets/vendors/owl-carousel/owl.carousel.js"></script>
        <script src="assets/js/functions.js"></script>
        <script src="assets/js/contact.js"></script>
        <!-- <script src='assets/vendors/switcher/switcher.js'></script> -->
        <!-- Revolution JavaScripts Files -->
        <script src="assets/vendors/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script src="assets/vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <!-- Slider revolution 5.0 Extensions  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
        <script src="assets/vendors/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script src="assets/vendors/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
        <script src="assets/vendors/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
        <script src="assets/vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script src="assets/vendors/revolution/js/extensions/revolution.extension.migration.min.js"></script>
        <script src="assets/vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script src="assets/vendors/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
        <script src="assets/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script src="assets/vendors/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <script>
            jQuery(document).ready(function () {
                var ttrevapi;
                var tpj = jQuery;
                if (tpj("#rev_slider_486_1").revolution === undefined) {
                    revslider_showDoubleJqueryError("#rev_slider_486_1");
                } else {
                    ttrevapi = tpj("#rev_slider_486_1").show().revolution({
                        sliderType: "standard",
                        jsFileLocation: "assets/vendors/revolution/js/",
                        sliderLayout: "fullwidth",
                        dottedOverlay: "none",
                        delay: 9000,
                        navigation: {
                            keyboardNavigation: "on",
                            keyboard_direction: "horizontal",
                            mouseScrollNavigation: "off",
                            mouseScrollReverse: "default",
                            onHoverStop: "on",
                            touch: {
                                touchenabled: "on",
                                swipe_threshold: 75,
                                swipe_min_touches: 1,
                                swipe_direction: "horizontal",
                                drag_block_vertical: false
                            }
                            ,
                            arrows: {
                                style: "uranus",
                                enable: true,
                                hide_onmobile: false,
                                hide_onleave: false,
                                tmp: '',
                                left: {
                                    h_align: "left",
                                    v_align: "center",
                                    h_offset: 10,
                                    v_offset: 0
                                },
                                right: {
                                    h_align: "right",
                                    v_align: "center",
                                    h_offset: 10,
                                    v_offset: 0
                                }
                            }

                        },
                        viewPort: {
                            enable: true,
                            outof: "pause",
                            visible_area: "80%",
                            presize: false
                        },
                        responsiveLevels: [1240, 1024, 778, 480],
                        visibilityLevels: [1240, 1024, 778, 480],
                        gridwidth: [1240, 1024, 778, 480],
                        gridheight: [768, 600, 600, 600],
                        lazyType: "none",
                        parallax: {
                            type: "scroll",
                            origo: "enterpoint",
                            speed: 400,
                            levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 46, 47, 48, 49, 50, 55]
                        },
                        shadow: 0,
                        spinner: "off",
                        stopLoop: "off",
                        stopAfterLoops: -1,
                        stopAtSlide: -1,
                        shuffle: "off",
                        autoHeight: "off",
                        hideThumbsOnMobile: "off",
                        hideSliderAtLimit: 0,
                        hideCaptionAtLimit: 0,
                        hideAllCaptionAtLilmit: 0,
                        debugMode: false,
                        fallbacks: {
                            simplifyAll: "off",
                            nextSlideOnWindowFocus: "off",
                            disableFocusListener: false
                        }
                    });
                }
            });
        </script>
    </body>

</html>

