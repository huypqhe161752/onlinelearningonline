<%-- 
    Document   : PageCourse
    Created on : May 17, 2023, 3:51:55 PM
    Author     : PCT
--%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from educhamp.themetrades.com/demo/admin/courses.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:10:19 GMT -->
    <head>

        <script src="assets/js/PopUp.js"></script>
        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="EduChamp : Education HTML Template" />

        <!-- OG -->
        <meta property="og:title" content="EduChamp : Education HTML Template" />
        <meta property="og:description" content="EduChamp : Education HTML Template" />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no">

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="../error-404.html" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />

        <!-- PAGE TITLE HERE ============================================= -->
        <title>EduChamp : Education HTML Template </title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/assets.css">
        <link rel="stylesheet" type="text/css" href="assestsAdmin/vendors/calendar/fullcalendar.css">

        <!-- TYPOGRAPHY ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/typography.css">

        <!-- SHORTCODES ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/shortcodes/shortcodes.css">

        <!-- STYLESHEETS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/style.css">
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/dashboard.css">
        <link class="skin" rel="stylesheet" type="text/css" href="assets/css/color/color-1.css">
        <link rel="stylesheet" type="text/css" href="assets/css/SubjectAdmin.css">
        <link rel="stylesheet" type="text/css" href="assets/css/page.css">
        <style>
            .active{
                color: orange;
            }
            table {
                border-collapse: collapse;
                width: 100%;
            }

            th, td {
                text-align: left;
                padding: 8px;
            }

            th:last-child, td:last-child {
                border-right: none;
            }

            tr:nth-child(even) {
                background-color: #f2f2f2;
            }

            tr:hover {
                background-color: #ddd;
            }

            td:nth-child(3), td:nth-child(7) {
                text-transform: capitalize;
            }

            td:nth-child(5) {
                white-space: nowrap;
            }

            .action-wrapper {
                display: flex;
                justify-content: space-between;
                align-items: center;
            }

            .action-wrapper a {
                color: #fff;
                text-decoration: none;
                padding: 5px 10px;
                border-radius: 5px;
            }

            .action-wrapper a:first-child {
                background-color: #f1c40f;
            }

            .action-wrapper a:last-child {
                background-color: #3498db;
            }

            .action-wrapper a:first-child:hover {
                background-color: #f39c12;
            }

            .action-wrapper a:last-child:hover {
                background-color: #2980b9;
            }
            .pagination {
                display: inline-block;
                margin-left: 50px auto;
                display: flex;
                margin-left: 37% ;

            }
            .pagination a {
                color: black;
                font-size: 22px;
                float: left;
                padding: 8px 16px;
                border-radius: 4px;
                text-decoration: none;
            }
            .pagination a.active {
                background-color: rgb(93, 4, 176);
                border: 1px solid black;
                color: white;
            }
            .pagination a:hover:not(.active) {
                background-color: rgb(93, 4, 176);
            }
            .message1 {
                position: fixed;
                color: white;
                background-color: #f5f5f5;
                height: 100vh;
                width: 100vw;
                z-index: 999;
                padding: 10px;
                background: rgba(0, 0, 0, 0.6);
                border: 1px solid #ddd;
                border-radius: 4px;
                box-shadow: 0px 2px 8px rgba(0, 0, 0, 0.15);
                font-size: 16px;
                text-align: center;
            }
            .message1.show {
                display: block;
            }
            a.active-btn {
                background-color: #ffc107;
                color: #fff;
                padding: 6px 12px;
                border: none;
                border-radius: 4px;
            }
            a.deactive-btn {
                background-color:#3498db;
                color: #fff;
                padding: 6px 12px;
                border: none;
                border-radius: 4px;
            }
        </style>
    </head>
</head>
<body class="ttr-opened-sidebar ttr-pinned-sidebar">

    <!-- header start -->
    <header class="ttr-header">
        <c:if test="${not empty sessionScope.message}">

            <div id="message1" class="message1" style="">${sessionScope.message}</div>
            <% session.removeAttribute("message"); %>
        </c:if>
        <div class="ttr-header-wrapper">
            <!--sidebar menu toggler start -->
            <div class="ttr-toggle-sidebar ttr-material-button">
                <i class="ti-close ttr-open-icon"></i>
                <i class="ti-menu ttr-close-icon"></i>
            </div>
            <!--sidebar menu toggler end -->
            <!--logo start -->
            <div class="ttr-logo-box">
                <div>
                    <a href="index.html" class="ttr-logo">
                        <img alt="" class="ttr-logo-mobile" src="assets/images/logo-mobile.png" width="30" height="30">
                        <img alt="" class="ttr-logo-desktop" src="assets/images/logo-white.png" width="160" height="27">
                    </a>
                </div>
            </div>
            <!--logo end -->
            <div class="ttr-header-menu">
                <!-- header left menu start -->
                <!-- header left menu end -->
            </div>
            <div class="ttr-header-right ttr-with-seperator">
                <!-- header right menu start -->
                <ul class="ttr-header-navigation">
                    <li>
                            <a href="#" class="ttr-material-button">${sessionScope.user.full_Name}</a>
                        </li>

                        <li>
                            <a href="#" class="ttr-material-button ttr-submenu-toggle"
                               ><span class="ttr-user-avatar"
                                   ><img
                                        alt=""
                                        src="${sessionScope.user.avatar}"
                                        width="32"
                                        height="32" /></span
                                ></a>
                            <div class="ttr-header-submenu">
                                <ul>
                                    <li><a onclick="openPopup1('profileadmin')">My profile</a></li>
                                    <li><a onclick="openPopup1('profileadminchangepass')">Change Password</a></li>
                                    <li><a href="logout">Logout</a></li>
                                </ul>
                            </div>
                        </li>
                        <li style="background-color: orange">
                            <a href="logout" class="ttr-material-button">Logout</a>
                        </li>
                </ul>
                <!-- header right menu end -->
            </div>
            <!--header search panel start -->
            <div class="ttr-search-bar">
                <form class="ttr-search-form">
                    <div class="ttr-search-input-wrapper">
                        <input type="text" name="qq" placeholder="search something..." class="ttr-search-input">
                        <button type="submit" name="search" class="ttr-search-submit"><i class="ti-arrow-right"></i></button>
                    </div>
                    <span class="ttr-search-close ttr-search-toggle">
                        <i class="ti-close"></i>
                    </span>
                </form>
            </div>
            <!--header search panel end -->
        </div>
    </header>
    <!-- header end -->
    <!-- Left sidebar menu start -->
    <div class="ttr-sidebar">
        <div class="ttr-sidebar-wrapper content-scroll">
            <!-- side menu logo start -->
            <div class="ttr-sidebar-logo">
                <a href="#"><img alt="" src="assets/images/logo.png" width="122" height="27"></a>
                <!-- <div class="ttr-sidebar-pin-button" title="Pin/Unpin Menu">
                        <i class="material-icons ttr-fixed-icon">gps_fixed</i>
                        <i class="material-icons ttr-not-fixed-icon">gps_not_fixed</i>
                </div> -->
                <div class="ttr-sidebar-toggle-button">
                    <i class="ti-arrow-left"></i>
                </div>
            </div>
            <!-- side menu logo end -->
            <!-- sidebar menu start -->
            <nav class="ttr-sidebar-navi">
                <c:set var="currentpage" value="${requestScope.xxx}"/>
                <ul>

                    <li>
                        <a href="admincourselist" class="ttr-material-button">
                            <span class="ttr-icon"><i class="ti-book"></i></span>
                            <span class="ttr-label">Course Management</span>
                        </a>
                    </li>
                    <li>
                        <a href="adminsubject" class="ttr-material-button">
                            <span  class="ttr-icon"><i class="ti-book"></i></span>
                            <span   class="ttr-label">Subject Management</span>
                        </a>
                    </li>
                    <li>
                        <a href="useradmin" class="ttr-material-button">
                            <span class="ttr-icon"
                                  ><i class="ti-layout-list-post"></i
                                ></span>
                            <span class="ttr-label ${currentpage eq "user"? 'active':""}">User Management</span>
                        </a>
                    </li>
                    <li>
                        <a href="quizzeslist" class="ttr-material-button ">
                            <span class="ttr-icon"><i class="ti-book"></i></span>
                            <span class="ttr-label">Quizzes Management</span>
                        </a>
                    </li>
                    <li>
                        <a href="listquestions" class="ttr-material-button ">
                            <span class="ttr-icon"><i class="ti-book"></i></span>
                            <span class="ttr-label">Questions Management</span>
                        </a>
                    </li>
                   <li>
                            <a href="profileadmin" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-user"></i></span>
                                <span  class="ttr-label">My Profile</span>
                                <span class="ttr-arrow-icon"
                                      ><i class="fa fa-angle-down"></i
                                    ></span>
                            </a>
                            <ul>
                                <li>
                                    <a onclick="openPopup1('profileadmin')" class="ttr-material-button"
                                       ><span class="ttr-label">User Profile</span></a
                                    >
                                </li>
                                <li>
                                    <a onclick="openPopup1('profileadminchangepass')" class="ttr-material-button"
                                       ><span class="ttr-label">Change Password</span></a
                                    >
                                </li>
                                <li>
                                    <a href="logout" class="ttr-material-button"
                                       ><span class="ttr-label">Logout</span></a
                                    >
                                </li>
                            </ul>
                        </li>

                    <li class="ttr-seperate"></li>
                </ul>
            </nav>
            <!-- sidebar menu end -->
        </div>
    </div>
    <!-- Left sidebar menu end -->

    <!--Main container start -->
    <main class="ttr-wrapper">
        <div class="container-fluid">
            <div class="db-breadcrumb">
                <h4 class="breadcrumb-title">Admin</h4>
                <ul class="db-breadcrumb-list">
                    <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                    <a href = "useradmin"><li>User Management</li></a>
                </ul>
            </div>	
            <div class="row">
                <!-- Your Profile Views Chart -->
                <div class="col-lg-12 m-b30">
                    <div class="widget-box">

                        <div class="search-bar">
                            <h4>User Management</h4>                          
                        </div>
                        <div class="search-bar">
                            <form action="searchuser" method="get">
                                <input type="text" name="searchName" value="${requestScope.searchName}" placeholder="User Name...">
                                <select class="col-6" name="sortID" onchange="this.form.submit()">
                                    <option value="" disabled selected hidden>Choose 1 opinion Sort</option>
                                    <option ${param.sortID == '1' ? 'selected' : ''} value="1">Sort User By Full Name</option>
                                    <option ${param.sortID == '2' ? 'selected' : ''} value="2">Sort User By email</option>
                                    <option ${param.sortID == '3' ? 'selected' : ''} value="3">Sort User By mobile</option>
                                    <option ${param.sortID == '4' ? 'selected' : ''} value="4">Sort User By Role</option>
                                    <option value="">Clear</option>
                                </select>
                                <button type="submit"><i>Search</i></button>
                            </form>
                            <button class="add-subject"><a href = "adduseradmin">Add new User</a></button>
                        </div>
                        <div class="search-bar d-flex justify-content-start">
                            <form action="filteridrole" method="get" class="mr-auto">
                                <select name="id_Role" onchange="this.form.submit()">
                                    <option value="" disabled selected hidden>Choose Role</option>
                                    <c:forEach var="i" items="${requestScope.RoleName}">
                                        <option value="${i.id_Role}" ${param.id_Role == i.id_Role ? 'selected' : ''}>${i.nameRole}</option>
                                    </c:forEach>
                                    <option value="">Clear</option>
                                </select>
                            </form>
                            <form action="filterstatus" method="get" class="mx-auto">
                                <select name="status" onchange="this.form.submit()">
                                    <option value="" disabled selected hidden>Choose Status</option>
                                    <option value="1" ${param.status == '1' ? 'selected' : ''}>Active</option>
                                    <option value="0" ${param.status == '0' ? 'selected' : ''}>Deactive</option>
                                    <option value="">Clear</option>
                                </select>
                            </form>
                            <form action="filtergender" method="get" class="ml-auto">
                                <select name="gender" onchange="this.form.submit()">
                                    <option value="" disabled selected hidden>Choose Gender</option>
                                    <option value="0" ${param.gender == '0' ? 'selected' : ''}>Female</option>
                                    <option value="1" ${param.gender == '1' ? 'selected' : ''}>Male</option>
                                    <option value="">Clear</option>
                                </select>
                            </form>
                        </div>
                        <div class="widget-inner">
                            <table>
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Full Name</th>
                                        <th>Gender</th>
                                        <th>Email</th>
                                        <th>Mobile</th>

                                        <th>Role</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${requestScope.list}" var="i">
                                        <tr>
                                            <td>${i.id_User}</td>
                                            <td>${i.full_Name}</td>
                                            <td>${i.gender eq true? 'male':'female'}</td>
                                            <td>${i.email}</td>
                                            <td>${i.phone_Number}</td>

                                            <td>${i.nameRole}</td>

                                            <td>
                                                <c:if test="${i.status}">
                                                    <a href="useradmin?id_User=${i.id_User}&status=0" class="deactive-btn" onclick="return confirm('Are you sure you want to deactive the User?')">Active</a>
                                                </c:if>
                                                <c:if test="${not i.status}">
                                                    <a href="useradmin?id_User=${i.id_User}&status=1 " class="active-btn" onclick="return confirm('Are you sure you want to active the User?')">Deactive</a>
                                                </c:if>
                                            </td>

                                            <td>
                                                <div class="action-wrapper">
                                                    <a href="userdetailadmin?iduser=${i.id_User}">View</a>
                                                    <a href="updateuser?iduser=${i.id_User}">Edit</a>
                                                </div>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    <!-- Add more rows as needed -->
                                </tbody>                               
                            </table>
                            <c:set var="page" value="${requestScope.page}"/>
                            <div style="justify-content: center;" class="pagination col-3">
                                <c:choose>
                                    <c:when test="${not empty searchName}">
                                        <c:forEach begin="${1}" end="${requestScope.num1}" var="i">
                                            <a class="${(page == i) ? 'active' : ''}" href="searchuser?page=${i}&searchName=${searchName}&sortID=${sortID}">${i}</a>
                                        </c:forEach>
                                    </c:when>
                                    <c:when test="${not empty sortID}">
                                        <c:forEach begin="${1}" end="${requestScope.num1}" var="i">
                                            <a class="${(page == i) ? 'active' : ''}" href="searchuser?page=${i}&sortID=${sortID}&searchName=${searchName}">${i}</a>
                                        </c:forEach>
                                    </c:when>
                                    <c:when test="${not empty gender}">
                                        <c:forEach begin="${1}" end="${requestScope.num1}" var="i">
                                            <a class="${(page == i) ? 'active' : ''}" href="filtergender?page=${i}&gender=${gender}">${i}</a>
                                        </c:forEach>
                                    </c:when>
                                    <c:when test="${not empty id_Role}">
                                        <c:forEach begin="${1}" end="${requestScope.num1}" var="i">
                                            <a class="${(page == i) ? 'active' : ''}" href="filteridrole?page=${i}&id_Role=${id_Role}">${i}</a>
                                        </c:forEach>
                                    </c:when>
                                    <c:when test="${not empty status}">
                                        <c:forEach begin="${1}" end="${requestScope.num1}" var="i">
                                            <a class="${(page == i) ? 'active' : ''}" href="filterstatus?page=${i}&status=${status}">${i}</a>
                                        </c:forEach>
                                    </c:when>
                                    <c:otherwise>
                                        <c:forEach begin="${1}" end="${requestScope.num1}" var="i">
                                            <a class="${(page == i)?"active":""}" href="useradmin?page=${i}">${i}</a>   
                                        </c:forEach>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </main>
    <div style="height: 100px;
         width: 100%;
         background-color: rgb(93, 4, 176);"></div>
    <div class="ttr-overlay"></div>

    <!-- External JavaScripts -->
    <script src="assestsAdmin/js/jquery.min.js"></script>
    <script src="assestsAdmin/vendors/bootstrap/js/popper.min.js"></script>
    <script src="assestsAdmin/vendors/bootstrap/js/bootstrap.min.js"></script>
    <script src="assestsAdmin/vendors/bootstrap-select/bootstrap-select.min.js"></script>
    <script src="assestsAdmin/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
    <script src="assestsAdmin/vendors/magnific-popup/magnific-popup.js"></script>
    <script src="assestsAdmin/vendors/counter/waypoints-min.js"></script>
    <script src="assestsAdmin/vendors/counter/counterup.min.js"></script>
    <script src="assestsAdmin/vendors/imagesloaded/imagesloaded.js"></script>
    <script src="assestsAdmin/vendors/masonry/masonry.js"></script>
    <script src="assestsAdmin/vendors/masonry/filter.js"></script>
    <script src="assestsAdmin/vendors/owl-carousel/owl.carousel.js"></script>
    <script src='assestsAdmin/vendors/scroll/scrollbar.min.js'></script>
    <script src="assestsAdmin/js/functions.js"></script>
    <script src="assestsAdmin/vendors/chart/chart.min.js"></script>
    <script 
        src="assestsAdmin/js/admin.js">

    </script>
    <script>

        var messageBox = document.getElementById("message1");
        messageBox.style.display = "none"; // Ẩn thông báo ban đầu

        function showMessage() {
            // Hiển thị thông báo
            messageBox.style.display = "block";
            messageBox.style.top = (window.innerHeight - messageBox.offsetHeight) / 2 + "px"; // Căn giữa theo chiều dọc
            messageBox.style.left = (window.innerWidth - messageBox.offsetWidth) / 2 + "px"; // Căn giữa theo chiều ngang

            // Tự động tắt thông báo sau 3 giây
            setTimeout(function () {
                messageBox.style.display = "none";
            }, 1000);
        }

        // Gọi hàm hiển thị thông báo
        showMessage();
    </script>
</body>

<!-- Mirrored from educhamp.themetrades.com/demo/admin/courses.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:11:35 GMT -->
</html>