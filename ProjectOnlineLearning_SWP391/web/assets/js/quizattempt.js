/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/ClientSide/javascript.js to edit this template
 */

function startQuiz(quiz_id) {
    $.ajax({
        url: "/projectswp391/setSession",
        type: "get", //send it through get method
        data: {
            quiz_id: quiz_id
        },
        success: function (response) {
            location.href = "attemptquiz?quiz_id=" + quiz_id;
        },
        error: function (xhr) {
        }
    });
}

function getlink(value, cur_number) {
    let hreflink = document.getElementById("hreflink").innerHTML;
    if (value > 0) {
//        hreflink += "&next=" + value + "&cur_number=" + (cur_number - 1) + "&modified=" + modified + "&new_answer=" + new_answer;
        hreflink += "&cur_number=" + (cur_number - 1);
    } else {
//        hreflink += "&next=" + value + "&cur_number=" + (cur_number + 1) + "&modified=" + modified + "&new_answer=" + new_answer;
        hreflink += "&cur_number=" + (cur_number + 1);
    }
    location.href = hreflink;
}

function alertTime(to) {
    var now = new Date().getTime();
    var distance = to.getTime() - now;
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    alert("Your quiz will be expired in: " + hours + "h " + minutes + "m " + seconds + "s ");
}

function timer() {
    const from = document.getElementById("quiz-starttime").innerHTML;
    const duration = document.getElementById("quiz-timelimit").innerHTML;
    const currentTime = new Date(from).toString();
    const to = new Date(from);
    to.setMilliseconds(to.getMilliseconds() + ((duration) * 1000));
//    alertTime(to);
    document.getElementById("to").innerHTML = to.toString();

    var now = new Date().getTime();
    var distance = to.getTime() - now;
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    document.getElementById("remaining-time").innerHTML = hours + "h " + minutes + "m " + seconds + "s ";

    var x = setInterval(function () {
        //alert(duartion);
        var now = new Date().getTime();
        var distance = to.getTime() - now;
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        document.getElementById("remaining-time").innerHTML = hours + "h " + minutes + "m " + seconds + "s ";



        if (distance < 0) {
            clearInterval(x);
            alert("Time up!!! You will be redirect to review this quiz now!");
//            location.href = ("home");
            $.ajax({
                url: "/projectswp391/quizsubmit",
                type: "get", //send it through get method
                data: {
                },
                success: function (response) {
                    location.href = "reviewdetails?id_Result=" + response;
                    //Do Something
                },
                error: function (xhr) {
                    //Do Something to handle error
                }
            });
        }
    }, 1000);
}


function callTimerServlet(limit) {
    var now = new Date().getTime();
    var distance = limit - now;
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    document.getElementById("remaining-time").innerHTML = hours + "h " + minutes + "m " + seconds + "s ";
    var x = setInterval(function () {
        $.ajax({
            url: "/projectswp391/quiztimer",
            type: "get", //send it through get method
            data: {
            },
            success: function (response) {
                if (response.endsWith("outoftime")) {
                    clearInterval(x);
                    $.ajax({
                        url: "/projectswp391/quizsubmit",
                        type: "get", //send it through get method
                        data: {
                        },
                        success: function (response) {
//                            alert(response);
                            location.href = "reviewdetails?id_Result=" + response;
                            //Do Something
                        },
                        error: function (xhr) {
                            //Do Something to handle error
                        }
                    });
                } else {
                    var now = new Date().getTime();
                    var distance = limit - now;
                    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                    document.getElementById("remaining-time").innerHTML = hours + "h " + minutes + "m " + seconds + "s ";
                }
                //Do Something
            },
            error: function (xhr) {
                //Do Something to handle error
            }
        });
    }, 1000);
}
