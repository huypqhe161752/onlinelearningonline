/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/ClientSide/javascript.js to edit this template
 */


//function changeBackgroudColor() {
//    $("review-board list-items").css("background-color", "yellow");
//}

function getMonthName(month) {
    const monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];
    return monthNames[month - 1];
}

function createDaySelect() {
    const monthSelect = document.getElementById('month');
    const daySelect = document.getElementById('day');

    monthSelect.addEventListener('change', updateDays);
    monthSelect.addEventListener('change', listByDate(monthSelect, daySelect));

    function updateDays() {
        const month = monthSelect.value;
        let daysInMonth;

        if (month === '02') {
            // Check for leap year
            const year = new Date().getFullYear();
            if (year % 4 === 0 && (year % 100 !== 0 || year % 400 === 0)) {
                daysInMonth = 29;
            } else {
                daysInMonth = 28;
            }
        } else if (month === '04' || month === '06' || month === '09' || month === '11') {
            daysInMonth = 30;
        } else {
            daysInMonth = 31;
        }

        // Remove existing day options
        while (daySelect.firstChild) {
            daySelect.removeChild(daySelect.firstChild);
        }

        // Add new day options
        const defaultOption = document.createElement('option');
        defaultOption.value = '';
        defaultOption.text = '--Select a day--';
        daySelect.appendChild(defaultOption);

        for (let i = 1; i <= daysInMonth; i++) {
            const option = document.createElement('option');
            option.value = i;
            option.text = i;
            daySelect.appendChild(option);
        }
    }

    // Initialize day options based on current month
    updateDays();
}

function reverseOrder(order, url) {
    order = !order;
    url = url + "&order=" + order;
    alert(url);
}

function listByDate(month, day) {
    $.ajax({
        url: "/projectswp391",
        type: "get", //send it through get method
        data: {
            month: month,
            day: day
        },
        success: function (response) {
            //Do Something
        },
        error: function (xhr) {
            //Do Something to handle error
        }
    });
}

//$('table tbody').on('click', 'tr', function () {
//    alert('clicked');
//});
$('#reversebtn').on('click', function () {
    var tbody = $('table tbody');
    tbody.html($('.revertable', tbody).get().reverse());
});