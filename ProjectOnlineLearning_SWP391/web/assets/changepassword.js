/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */
function checkFormPassword() {
    var opass = document.getElementById("oldpass").value;
    var currentpassword = document.getElementById("currentpassword").value;
    var newpassword = document.getElementById('newpassword').value;
    var repassword = document.getElementById("confirmpassword").value;
    var isValid = true;
    if (currentpassword !== opass) {
        document.getElementById("checkpassword").innerHTML = "Current password is incorrect. Please re-input!";
        isValid = false;
    } else if (newpassword !== repassword) {
        document.getElementById("checkpassword").innerHTML = "Confirm password does not match. Please re-input!";
        isValid = false;
    } else if (currentpassword === newpassword) {
        document.getElementById("checkpassword").innerHTML = 'Current password and New password must be different. Please re-input!';
        isValid = false;
    } else {
        var result = confirm("Do you want to continue?");
        if (result) {
// Người dùng chọn "Có"
            alert("Save successful!");
            return true;
        } else {
// Người dùng chọn "Không"
            return false;
        }
    }
}
function myFunctionView() {
    var x = document.getElementById("currentpassword");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
    var y = document.getElementById("newpassword");
    if (y.type === "password") {
        y.type = "text";
    } else {
        y.type = "password";
    }
    var z = document.getElementById("confirmpassword");
    if (z.type === "password") {
        z.type = "text";
    } else {
        z.type = "password";
    }

}


