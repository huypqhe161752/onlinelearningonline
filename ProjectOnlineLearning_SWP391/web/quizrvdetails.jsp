<%-- 
    Document   : reviewprogress_popup
    Created on : Jun 4, 2023, 2:43:40 PM
    Author     : Naviank
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>

        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="Online Learning System - Group 2 SE1714" />

        <!-- PAGE TITLE HERE ============================================= -->
        <!--<title>Online Learning System - Group 2 SE1714</title>-->
        <title>Review details quiz</title>

        <!-- OG -->
        <meta property="og:title" content="Online Learning System - Group 2 SE1714" />
        <meta property="og:description" content="Online Learning System - Group 2 SE1714" />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no">

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" type="text/css" href="assets/css/vanpk_style.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.0/css/bootstrap.min.css">

    </head>
    <body>
        <div class="page-wraper">
            <jsp:include page="header_quiz.jsp"/>
            <div class="page-banner ovbl-dark" style="background-image:url(assets/images/banner/banner1.jpg);height: 80px">
                <div class="container">
                    <!--                    <div class="page-banner-entry">
                                            <h1 class="text-white" style="margin-top: 50px"></h1>
                                        </div>-->
                </div>
            </div>
            <c:set var="quiz" value="${requestScope.quiz}" />
            <c:set var="result" value="${requestScope.result}" />
            <c:set var="listQues" value="${requestScope.listQues}" />
            <c:set var="ncorrect" value="${requestScope.ncorrect}" />
            <c:set var="quesgrade" value="${requestScope.quesgrade}" />
            <c:set var="mapQues" value="${requestScope.mapQues}" />
            <div class="breadcrumb-row">
                <div class="container">
                    <ul class="list-inline">
                        <div class="">
                            <!--<li style="width: 64px;"><a href="home">Home</a></li>-->
                            <li style=""><a href="lessonview?idsubject=${requestScope.subjname.id_Subject}">Lesson</a></li>
                            <li style=""><a href="quizlesson?id_lesson=${quiz.id_Lession}">View quiz</a></li>
                            <li style=""><a href="reviewquiz?quiz_id=${quiz.id_Quiz}">Quiz review board</a></li>
                            <li style=""><a href="#">Quiz review details</a></li>
                        </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="review-title">
                <span>Subject: ${requestScope.subjname.subject_Name}</span>
                <br/>
                <span>Review for: ${quiz.name_Quiz}</span>
            </div>
            <div class="container-fluid">
                <div class="review-body ">
                    <c:set var="length" value="${listQues.size()}"/>
                    <c:set var="string_answer" value="${result.doResult}"/>
                    <c:set var="correctanwser" value="${result.correctAnwser}"/>
                    <div class="review-content row">
                        <div class="review-info">
                            <table class="border-bottom">
                                <tr class="row">
                                    <th class="border-left border-top">Start time:</th>
                                    <td class="border-left border-top">${result.start_Time.toLocaleString()}</td>
                                </tr>
                                <tr class="row">
                                    <th class="border-left border-top">State:</th>
                                    <td class="border-left border-top">Finished</td>
                                </tr>
                                <tr class="row">
                                    <th class="border-left border-top">Time taken:</th>
                                    <td class="border-left border-top">${result.duration} s</td>
                                </tr>
                                <tr class="row">
                                    <th class="border-left border-top">Mark:</th>
                                    <td class="border-left border-top">${requestScope.grade} / ${quiz.number_Question}</td>
                                </tr>
                                <tr class="row">
                                    <th class="border-left border-top">Grade</th>
                                    <td class="border-left border-top">
                                        <fmt:formatNumber value="${requestScope.grade * 10 / quiz.number_Question}" maxFractionDigits="1" minFractionDigits="1" />
                                        / 10 
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="review-board col-lg-10 container">
                            <c:forEach begin="1" end="${length}" var="i">
                                <c:set var="cur_ques" value="${listQues.get(i-1)}"/>
                                <c:set var="nc" value="${ncorrect[i - 1]}"/>
                                <c:set var="cur_ques_ans" value="${mapQues.get(cur_ques)}"/>
                                <div class="row review-item" id="item${i}">
                                    <div class="col-lg-2 border-bottom">
                                        <span>
                                            Question 
                                            <b style="font-size: 40px">${i}</b>
                                        </span>
                                        <br/>
                                        <span>
                                            Mark ${quesgrade[i-1]} out of 1
                                        </span>
                                    </div>
                                    <div class="col-lg-10 border-left border-bottom border-right">
                                        <div class="review-question">
                                            ${i}. ${cur_ques.content_Question}
                                        </div>
                                        <div class="review-answer">
                                            <ul>
                                                <c:set var="cur_apos" value="${(i-1) * 5}"/>
                                                <c:forEach items="${cur_ques_ans}" var="a">
                                                    <c:if test="${nc == 1}">
                                                        <li>
                                                            <div class="answer">
                                                                <c:if test="${(a.answer_Content.charAt(0)) == string_answer.charAt(cur_apos)}">
                                                                    <input type="radio" 
                                                                           name="ans" 
                                                                           checked=""
                                                                           disabled=""
                                                                           > 
                                                                </c:if>
                                                                <c:if test="${(a.answer_Content.charAt(0)) != string_answer.charAt(cur_apos)}">
                                                                    <input type="radio" 
                                                                           name="ans"
                                                                           disabled=""
                                                                           > 
                                                                </c:if>
                                                                <span class="">
                                                                    &emsp;${a.answer_Content}
                                                                </span>
                                                                <c:if test="${correctanwser.charAt(cur_apos) == (a.answer_Content.charAt(0)) && (a.answer_Content.charAt(0)) == string_answer.charAt(cur_apos)}">
                                                                    <i class="fa fa-check" style="color: green"></i>
                                                                </c:if>
                                                                <c:if test="${correctanwser.charAt(cur_apos) == (a.answer_Content.charAt(0)) && (a.answer_Content.charAt(0)) != string_answer.charAt(cur_apos)}">
                                                                    <i class="fa fa-close" style="color: red"></i>
                                                                </c:if>
                                                                <c:set var="cur_apos" value="${cur_apos + 1}"/>
                                                            </div> 
                                                        </li>
                                                    </c:if>
                                                    <c:if test="${nc > 1}">
                                                        <li>
                                                            <div class="answer">
                                                                <c:if test="${(a.answer_Content.charAt(0)) == string_answer.charAt(cur_apos)}">
                                                                    <input type="checkbox" 
                                                                           name="ans" 
                                                                           checked
                                                                           disabled=""
                                                                           > 
                                                                </c:if>
                                                                <c:if test="${(a.answer_Content.charAt(0)) != string_answer.charAt(cur_apos)}">
                                                                    <input type="checkbox" 
                                                                           name="ans"
                                                                           disabled=""
                                                                           > 
                                                                </c:if>
                                                                <span class="">
                                                                    &emsp;${a.answer_Content}
                                                                </span>
                                                                <c:if test="${correctanwser.charAt(cur_apos) == (a.answer_Content.charAt(0)) && (a.answer_Content.charAt(0)) == string_answer.charAt(cur_apos)}">
                                                                    <i class="fa fa-check" style="color: green"></i>
                                                                </c:if>
                                                                <c:if test="${correctanwser.charAt(cur_apos) == (a.answer_Content.charAt(0)) && (a.answer_Content.charAt(0)) != string_answer.charAt(cur_apos)}">
                                                                    <i class="fa fa-close" style="color: red"></i>
                                                                </c:if>
                                                                <c:set var="cur_apos" value="${cur_apos + 1}"/>
                                                            </div> 
                                                        </li>
                                                    </c:if>
                                                </c:forEach>
                                            </ul>
                                        </div>
                                        <div class="review-desc">
                                            ${cur_ques.description}
                                        </div>    
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                        <div class="review-navboard col-lg-2">
                            <div style="margin: 20px; ">Quiz navigation</div>
                            <div class="real-nav">
                                <c:forEach begin="1" end="${length}" var="i">
                                    <c:if test="${quesgrade[i-1] < 1}">
                                        <button onclick="location.href = '#item${i}'" class="navi-number rv-incorr">${i}</button>
                                    </c:if>
                                    <c:if test="${quesgrade[i-1] == 1}">
                                        <button onclick="location.href = '#item${i}'" class="navi-number rv-corr">${i}</button>
                                    </c:if>
                                </c:forEach>
                            </div>
                            <div class="attemptquiz">
                                <!--<button onclick="startQuiz(${quiz.id_Quiz})">Re-attempt quiz</button>-->
                                <button onclick="startQuiz(${quiz.id_Quiz})">Re-attempt quiz</button>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <script 
            src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js">
        </script>
        <script src="assets/js/PopUp.js"></script>
        <script src="assets/js/quizattempt.js"></script>

    </body>
</html>
