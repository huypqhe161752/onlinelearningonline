<%-- 
    Document   : updatesubject
    Created on : Jun 7, 2023, 4:46:33 PM
    Author     : PCT
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from educhamp.themetrades.com/demo/admin/add-listing.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:09:05 GMT -->
    <head>

        <script src="assets/js/PopUp.js"></script>
        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="EduChamp : Education HTML Template" />

        <!-- OG -->
        <meta property="og:title" content="EduChamp : Education HTML Template" />
        <meta property="og:description" content="EduChamp : Education HTML Template" />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no">

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="../error-404.html" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />

        <!-- PAGE TITLE HERE ============================================= -->
        <title>EduChamp : Education HTML Template </title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/assets.css">
        <link rel="stylesheet" type="text/css" href="assestsAdmin/vendors/calendar/fullcalendar.css">

        <!-- TYPOGRAPHY ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/typography.css">

        <!-- SHORTCODES ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/shortcodes/shortcodes.css">

        <!-- STYLESHEETS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/style.css">
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/dashboard.css">
        <link class="skin" rel="stylesheet" type="text/css" href="assets/css/color/color-1.css">
        <link rel="stylesheet" type="text/css" href="assets/css/SubjectAdmin.css">
        <link rel="stylesheet" type="text/css" href="assets/css/page.css">
        <link rel="stylesheet" type="text/css" href="assets/css/slider.css">
        <style>
            #active{
                color: orange;
            }
            .message1 {
                position: fixed;
                color: white;
                background-color: #f5f5f5;
                height: 100vh;
                width: 100vw;
                z-index: 999;
                padding: 10px;
                background: rgba(0, 0, 0, 0.6);
                border: 1px solid #ddd;
                border-radius: 4px;
                box-shadow: 0px 2px 8px rgba(0, 0, 0, 0.15);
                font-size: 16px;
                text-align: center;
            }
            .message1.show {
                display: block;
            }
            
        </style>
    </head>
</head>
<body class="ttr-opened-sidebar ttr-pinned-sidebar">

    <!-- header start -->
    <header class="ttr-header">
        <c:if test="${not empty sessionScope.message}">

            <div id="message1" class="message1" style="">${sessionScope.message}</div>
            <% session.removeAttribute("message"); %>
        </c:if>
        <div class="ttr-header-wrapper">
            <!--sidebar menu toggler start -->
            <div class="ttr-toggle-sidebar ttr-material-button">
                <i class="ti-close ttr-open-icon"></i>
                <i class="ti-menu ttr-close-icon"></i>
            </div>
            <!--sidebar menu toggler end -->
            <!--logo start -->
            <div class="ttr-logo-box">
                <div>
                    <a href="index.html" class="ttr-logo">
                        <img alt="" class="ttr-logo-mobile" src="assets/images/logo-mobile.png" width="30" height="30">
                        <img alt="" class="ttr-logo-desktop" src="assets/images/logo-white.png" width="160" height="27">
                    </a>
                </div>
            </div>
            <!--logo end -->
            <div class="ttr-header-menu">
                <!-- header left menu start -->
                <ul class="ttr-header-navigation">
                    <li>
                        <a href="../index.html" class="ttr-material-button ttr-submenu-toggle">HOME</a>
                    </li>
                </ul>
                <!-- header left menu end -->
            </div>
            <div class="ttr-header-right ttr-with-seperator">
                <!-- header right menu start -->
                <ul class="ttr-header-navigation">
                    <li>
                        <a href="#" class="ttr-material-button">${sessionScope.user.full_Name}</a>
                    </li>

                    <li>
                        <a href="#" class="ttr-material-button ttr-submenu-toggle"
                           ><span class="ttr-user-avatar"
                               ><img
                                    alt=""
                                    src="${sessionScope.user.avatar}"
                                    width="32"
                                    height="32" /></span
                            ></a>
                        <div class="ttr-header-submenu">
                            <ul>
                                <li><a href="#">My profile</a></li>
                                <li><a href="#">Logout</a></li>
                            </ul>
                        </div>
                    </li>
                    <li style="background-color: orange">
                        <a href="logout" class="ttr-material-button">Logout</a>
                    </li>
                </ul>
                <!-- header right menu end -->
            </div>            <!--header search panel start -->
            <div class="ttr-search-bar">
                <form class="ttr-search-form">
                    <div class="ttr-search-input-wrapper">
                        <input type="text" name="qq" placeholder="search something..." class="ttr-search-input">
                        <button type="submit" name="search" class="ttr-search-submit"><i class="ti-arrow-right"></i></button>
                    </div>
                    <span class="ttr-search-close ttr-search-toggle">
                        <i class="ti-close"></i>
                    </span>
                </form>
            </div>
            <!--header search panel end -->
        </div>
    </header>
    <!-- header end -->
    <!-- Left sidebar menu start -->
    <div class="ttr-sidebar">
        <div class="ttr-sidebar-wrapper content-scroll">
            <!-- side menu logo start -->
            <div class="ttr-sidebar-logo">
                <a href="#"><img alt="" src="assets/images/logo.png" width="122" height="27"></a>
                <!-- <div class="ttr-sidebar-pin-button" title="Pin/Unpin Menu">
                        <i class="material-icons ttr-fixed-icon">gps_fixed</i>
                        <i class="material-icons ttr-not-fixed-icon">gps_not_fixed</i>
                </div> -->
                <div class="ttr-sidebar-toggle-button">
                    <i class="ti-arrow-left"></i>
                </div>
            </div>
            <!-- side menu logo end -->
            <!-- sidebar menu start -->
            <nav class="ttr-sidebar-navi">
                <c:set var="currentpage" value="${requestScope.xxx}"/>
                <ul>
                    <li>
                        <a href="dashboardmarketing" class="ttr-material-button">
                            <span class="ttr-icon"><i class="ti-home"></i></span>
                            <span class="ttr-label">Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a href="admincourselist" class="ttr-material-button">
                            <span class="ttr-icon"><i class="ti-book"></i></span>
                            <span class="ttr-label">Management Course</span>
                        </a>
                    </li>
                    <li>
                        <a href="expertsubjectlistservlet" class="ttr-material-button">
                            <span  class="ttr-icon"><i class="ti-book"></i></span>
                            <span   class="ttr-label">Management Subject</span>
                        </a>
                    </li>
                    <li>
                        <a id="active" href="useradmin" class="ttr-material-button">
                            <span class="ttr-icon"
                                  ><i class="ti-layout-list-post"></i
                                ></span>
                            <span id="active" class="ttr-label ${currentpage eq "user"? 'active':""}">Management User</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="ttr-material-button">
                            <span class="ttr-icon"
                                  ><i class="ti-layout-list-post"></i
                                ></span>
                            <span class="ttr-label">Management Setting</span>
                        </a>
                    </li>

                    <li>
                        <a href="#" class="ttr-material-button">
                            <span class="ttr-icon"><i class="ti-user"></i></span>
                            <span class="ttr-label">My Profile</span>
                            <span class="ttr-arrow-icon"
                                  ><i class="fa fa-angle-down"></i
                                ></span>
                        </a>
                        <ul>
                            <li>
                                <a href="#" class="ttr-material-button"
                                   ><span class="ttr-label">User Profile</span></a
                                >
                            </li>
                            <li>
                                <a href="#" class="ttr-material-button"
                                   ><span class="ttr-label">Logout</span></a
                                >
                            </li>
                        </ul>
                    </li>

                    <li class="ttr-seperate"></li>
                </ul>
                <!-- sidebar menu end -->
            </nav>
            <!-- sidebar menu end -->
        </div>
    </div>
    <!-- Left sidebar menu end -->

    <!--Main container start -->
    <main class="ttr-wrapper">
        <div class="container-fluid">
            <div class="db-breadcrumb">
                <h4 class="breadcrumb-title">Add User</h4>
                <ul class="db-breadcrumb-list">
                    <li><a href="useradmin"><i class="fa fa-home"></i>Management User</a></li>
                    <li>Add User</li>
                </ul>
            </div>	
            <div class="row">
                <!-- Your Profile Views Chart -->
                <div class="col-lg-12 m-b30">
                    <div class="widget-box">
                        <div class="wc-title">
                            <h4>Add User</h4>
                        </div>
                        <div class="widget-inner">
                            <form class="edit-profile m-b30" action="adduseradmin" method="post" enctype="multipart/form-data" >
                                <div class="row">
                                    <div class="col-12">
                                        <div class="ml-auto">
                                            <h3>1. Basic info</h3>
                                        </div>
                                    </div>
                                    <div class="form-group col-6">

                                        <label class="col-form-label">Full Name</label>
                                        <div>
                                            <input class="form-control" name="full_Name" type="text" value="">
                                        </div>
                                    </div>
                                    <div class="form-group col-6">
                                        <label class="col-form-label">Mobile Phone</label>
                                        <div>
                                            <input class="form-control" name="phone" type="text" value="">
                                        </div>
                                    </div>
                                    <div class="form-group col-6">
                                        <label class="col-form-label">Address</label>
                                        <div>
                                            <input class="form-control" type="text" name="address" value="">
                                        </div>
                                    </div>
                                    <div class="form-group col-6">
                                        <label class="col-form-label">Gender</label>
                                        <div>
                                            <select class="form-control" name="gender">
                                                <option value="" disabled selected hidden>choose gender</option>
                                                <option value="1" >male</option>
                                                <option value="2">female</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-6">
                                        <label class="col-form-label">Status</label>
                                        <div>
                                            <select class="form-control" name="status">
                                                <option value="" disabled selected hidden>choose status</option>
                                                <option value="1" >active</option>
                                                <option value="0" >deactive</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-6">
                                        <label class="col-form-label">Name Role</label>
                                        <div>
                                            <select class="form-control" name="id_role">
                                                <option value="" disabled selected hidden>choose Role</option>
                                                <c:forEach var="i" items="${requestScope.RoleName}">
                                                    <option value="${i.id_Role}">${i.nameRole}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-6">
                                        <label class="col-form-label">Email</label>
                                        <div>

                                            <input class="form-control" type="email" name="email" value="${email}">


                                        </div>
                                    </div>
                                    <div class="form-group col-6">
                                        <label class="col-form-label">Password</label>
                                        <div>

                                            <input class="form-control" type="text" name="password" value="${pass}">

                                        </div>
                                    </div>
                                    <div class="form-group col-6">
                                        <label class="col-form-label">Create Date</label>
                                        <div>
                                            <input class="form-control" type="hidden" name="create_Date" value="" id="create-date-field" readonly>
                                        </div>
                                    </div>
                                    <div class="seperator"></div>

                                    <div class="col-12 m-t20">
                                        <div class="ml-auto m-b5">
                                            <h3>2. Avatar</h3>
                                        </div>
                                    </div>
                                    <div class="form-group col-12">
                                        <label class="col-form-label">Avatar </label>
                                        <div>

                                            <img id="previewImage1" style ="width: 100px " src="${avatar}" alt="Preview Image">
                                            <input class="form-control" name="avatar" type="file" id="imageInput1" onchange="previewImage('imageInput1', 'previewImage1')">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <button type="submit" class="btn-secondry m-r5" onclick="return confirm('Are you sure you want to Add?')"><i class="fa fa-fw fa-plus-circle"></i>Add User</button>
                                        <a href ="useradmin" class="btn-secondry m-r5" onclick="return confirm('Are you sure you want to cancel?')"><i class="fa fa-fw fa-plus-circle"></i>Cancel</a>
                                    </div>      
                                </div>      
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Your Profile Views Chart END-->
    </main>
    <div class="ttr-overlay"></div>

    <!-- External JavaScripts -->
    <script src="assestsAdmin/js/jquery.min.js"></script>
    <script src="assestsAdmin/vendors/bootstrap/js/popper.min.js"></script>
    <script src="assestsAdmin/vendors/bootstrap/js/bootstrap.min.js"></script>
    <script src="assestsAdmin/vendors/bootstrap-select/bootstrap-select.min.js"></script>
    <script src="assestsAdmin/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
    <script src="assestsAdmin/vendors/magnific-popup/magnific-popup.js"></script>
    <script src="assestsAdmin/vendors/counter/waypoints-min.js"></script>
    <script src="assestsAdmin/vendors/counter/counterup.min.js"></script>
    <script src="assestsAdmin/vendors/imagesloaded/imagesloaded.js"></script>
    <script src="assestsAdmin/vendors/masonry/masonry.js"></script>
    <script src="assestsAdmin/vendors/masonry/filter.js"></script>
    <script src="assestsAdmin/vendors/owl-carousel/owl.carousel.js"></script>
    <script src='assestsAdmin/vendors/scroll/scrollbar.min.js'></script>
    <script src="assestsAdmin/js/functions.js"></script>
    <script src="assestsAdmin/vendors/chart/chart.min.js"></script>
    <script 
        src="assestsAdmin/js/admin.js">
    </script>
    <script>
        function previewImage(inputId, imageId) {
            var input = document.getElementById(inputId);
            var previewImage = document.getElementById(imageId);

            if (input.files && input.files[0]) {
                var file = input.files[0];
                var fileExtension = file.name.substring(file.name.lastIndexOf('.')).toLowerCase();
                var allowedExtensions = ['.jpg', '.png'];

                if (allowedExtensions.includes(fileExtension)) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        previewImage.src = e.target.result;
                    };

                    reader.readAsDataURL(file);
                } else {
                    // Đuôi tệp tin không hợp lệ, xử lý theo nhu cầu của bạn
                    console.log('Chỉ cho phép chọn tệp tin có đuôi .jpg hoặc .png.');
                }
            }
        }
        var messageBox = document.getElementById("message1");
        messageBox.style.display = "none"; // Ẩn thông báo ban đầu

        function showMessage() {
            // Hiển thị thông báo
            messageBox.style.display = "block";
            messageBox.style.top = (window.innerHeight - messageBox.offsetHeight) / 2 + "px"; // Căn giữa theo chiều dọc
            messageBox.style.left = (window.innerWidth - messageBox.offsetWidth) / 2 + "px"; // Căn giữa theo chiều ngang

            // Tự động tắt thông báo sau 3 giây
            setTimeout(function () {
                messageBox.style.display = "none";
            }, 1000);
        }

        // Gọi hàm hiển thị thông báo
        showMessage();

    </script>
    <script>
        var today = new Date().toISOString().substr(0, 10);
        document.getElementById("create-date-field").value = today;
    </script>

</body>

<!-- Mirrored from educhamp.themetrades.com/demo/admin/add-listing.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:09:05 GMT -->
</html>
