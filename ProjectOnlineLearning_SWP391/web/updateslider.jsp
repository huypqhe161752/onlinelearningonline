<%-- 
    Document   : updateslider
    Created on : Jun 6, 2023, 5:05:31 PM
    Author     : PCT
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <link rel="stylesheet" type="text/css" href="assets/css/slider.css">
  <head>
      <script src="assets/js/PopUp.js"></script>
    <meta charset="UTF-8">
    <title>Update Subject Form</title>
    
  </head>
  <body>
    <h1>Update Slider Form</h1>
    <form action="updateslider" method="POST">
      <label for="id_Slider">id_Slider:</label>
      <input type="text" id="id_Slider" name="id_Slider" value = "${sliderdetail.getId_Slider()}" readonly>

      <label for="title_Slider">title_Slider:</label>
      <input type="text" id="title_Slider" name="title_Slider" value = "${sliderdetail.getTitle_Slider() }" required>
      
      <label for="thumbnail_Image">thumbnail_Image:</label>
      <input type="text" id="thumbnail_Image" name="thumbnail_Image" value = "${sliderdetail.getThumbnail_Image() }" required>
      
           <label for="create_Date">create_Date:</label>
      <input type="date" id="update_Date" name="update_Date" value = "${sliderdetaillider.getUpdate_Date() }" required>
      
            <label for="update_Date">update_Date:</label>
      <input type="date" id="create_Date" name="create_Date" value = "${sliderdetaillider.getCreate_Date() }" required>
      
      <label for="status">Status :</label>
                    <input type="radio" name="status" value="True" ${sliderdetaillider.isStatus() == True ? "Checked" :""}>Active</input>
                        <input type="radio" name="status" value="False" ${sliderdetaillider.isStatus() == False ? "Checked" :""}>DeActive</input>
                

      <label for="notes">Notes:</label>
      <input type="text" id="notes" name="notes" value = "${sliderdetail.getNotes() }" required>

      <label for="backlink">Backlink:</label>
      <input type="text" id="backlink" name="backlink" value = "${sliderdetail.getBacklink() }" required>
      
      <label for="content_Slider">content_Slider:</label>
      <input type="text" id="content_Slider" name="content_Slider" value = "${sliderdetail.getContent_Slider() }" required>

      <label for="id_user">User ID:</label>
      <input type="text" id="id_user" name="id_user" value = "${sliderdetail.getId_User() }" required>

      <div class="button-container">
        <input type="submit" value="Update Subject" >
        <input type="button" value="Cancel" onclick="closeWindowAndPreventSubmit(event)">
      </div>
    </form>
  </body>
</html>
