<%-- 
    Document   : PageCourse
    Created on : May 17, 2023, 3:51:55 PM
    Author     : PCT
--%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from educhamp.themetrades.com/demo/admin/courses.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:10:19 GMT -->
    <head>

        <script src="assets/js/PopUp.js"></script>
        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="EduChamp : Education HTML Template" />

        <!-- OG -->
        <meta property="og:title" content="EduChamp : Education HTML Template" />
        <meta property="og:description" content="EduChamp : Education HTML Template" />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no">

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="../error-404.html" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />

        <!-- PAGE TITLE HERE ============================================= -->
        <title>EduChamp : Education HTML Template </title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/assets.css">

        <!-- TYPOGRAPHY ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/typography.css">

        <!-- SHORTCODES ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/shortcodes/shortcodes.css">

        <!-- STYLESHEETS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <link rel="stylesheet" type="text/css" href="assets/css/dashboard.css">
        <link class="skin" rel="stylesheet" type="text/css" href="assets/css/color/color-1.css">

        <link rel="stylesheet" type="text/css" href="assets/css/page.css">
        <style>
            .active{
                color: orange;
            }
        </style>
    </head>
</head>
<body class="ttr-opened-sidebar ttr-pinned-sidebar">

    <!-- header start -->
    <header class="ttr-header">
        <div class="ttr-header-wrapper">
            <!--sidebar menu toggler start -->
            <div class="ttr-toggle-sidebar ttr-material-button">
                <i class="ti-close ttr-close-icon"></i>
                <i class="ti-menu ttr-open-icon"></i>
            </div>
            <!--sidebar menu toggler end -->
            <!--logo start -->
            <div class="ttr-logo-box">
                <div>
                    <a href="index.html" class="ttr-logo">
                        <img alt="" class="ttr-logo-mobile" src="assets/images/logo-mobile.png" width="30" height="30">
                        <img alt="" class="ttr-logo-desktop" src="assets/images/logo-white.png" width="160" height="27">
                    </a>
                </div>
            </div>
            <!--logo end -->
            <div class="ttr-header-menu">
                <!-- header left menu start -->
                <!-- header left menu end -->
            </div>
            <div class="ttr-header-right ttr-with-seperator">
                <!-- header right menu start -->
                <ul class="ttr-header-navigation">

                    <c:if test="${sessionScope.user!=null}">

                        <li> 
                            <a href="userprofile">
                                <img 
                                    src="${sessionScope.user.avatar}" 
                                    style="height: 48px; width: 48px; overflow: hidden; border-radius: 50%;"
                                    alt="${sessionScope.user.full_Name}"/>
                            </a>
                        </li>
                        <li><a href="userprofile">${user.full_Name}</a></li>
                        <li><a href="logout">Logout</a></li>

                    </c:if> 
                </ul>
                <!-- header right menu end -->
            </div>
            <!--header search panel start -->
            <div class="ttr-search-bar">
                <form class="ttr-search-form">
                    <div class="ttr-search-input-wrapper">
                        <input type="text" name="qq" placeholder="search something..." class="ttr-search-input">
                        <button type="submit" name="search" class="ttr-search-submit"><i class="ti-arrow-right"></i></button>
                    </div>
                    <span class="ttr-search-close ttr-search-toggle">
                        <i class="ti-close"></i>
                    </span>
                </form>
            </div>
            <!--header search panel end -->
        </div>
    </header>
    <!-- header end -->
    <!-- Left sidebar menu start -->
    <div class="ttr-sidebar">
        <div class="ttr-sidebar-wrapper content-scroll">
            <!-- side menu logo start -->
            <div class="ttr-sidebar-logo">
                <a href="#"><img alt="" src="assets/images/logo.png" width="122" height="27"></a>
                <!-- <div class="ttr-sidebar-pin-button" title="Pin/Unpin Menu">
                        <i class="material-icons ttr-fixed-icon">gps_fixed</i>
                        <i class="material-icons ttr-not-fixed-icon">gps_not_fixed</i>
                </div> -->
                <div class="ttr-sidebar-toggle-button">
                    <i class="ti-arrow-left"></i>
                </div>
            </div>
            <!-- side menu logo end -->
            <!-- sidebar menu start -->
            <nav class="ttr-sidebar-navi">
                <ul>
                    <c:if test = "${sessionScope.user.id_role==1}">
                        <li>
                            <a href="admincourselist" class="ttr-material-button">
                                <span id="active"  class="ttr-icon"><i class="ti-book"></i></span>
                                <span  id="active" class="ttr-label">Course Management</span>
                            </a>
                        </li>
                    </c:if>
                    <c:if test = "${sessionScope.user.id_role==1}">
                        <li>
                        <a href="adminsubject" class="ttr-material-button">
                            <span class="ttr-icon"><i class="ti-book"></i></span>
                            <span class="ttr-label ${currentpage eq "SubjectManagment"? 'active':""}">Subject Management</span>
                        </a>
                    </li>
                    </c:if>
                    <c:if test = "${sessionScope.user.id_role==2}">
                        <li>
                        <a href="expertsubjectlistservlet" class="ttr-material-button">
                            <span class="ttr-icon"><i class="ti-book"></i></span>
                            <span class="ttr-label ${currentpage eq "SubjectManagment"? 'active':""}">Subject Management</span>
                        </a>
                    </li>
                    </c:if>
                    <c:if test = "${sessionScope.user.id_role==1}">
                        <li>
                            <a href="useradmin" class="ttr-material-button">
                                <span class="ttr-icon"
                                      ><i class="ti-layout-list-post"></i
                                    ></span>
                                <span class="ttr-label">User Management</span>
                            </a>
                        </li>
                    </c:if>
                    <li>
                        <a href="quizzeslist" class="ttr-material-button ">
                            <span class="ttr-icon"><i class="ti-book"></i></span>
                            <span class="ttr-label">Quizzes Management</span>
                        </a>
                    </li>
                    <li>
                        <a href="listquestions" class="ttr-material-button ">
                            <span class="ttr-icon"><i class="ti-book"></i></span>
                            <span class="ttr-label active">Questions Management</span>
                        </a>
                    </li>
                    <li style="margin-left: 60px">
                        <a href="#" class="ttr-material-button">
                            <span class="ttr-icon"><i class="ti-pencil-alt"></i></span>
                            <span class="ttr-label active">Questions Detail</span>
                        </a>
                    </li>

                    <li class="ttr-seperate"></li>
                </ul>
            </nav>
            <!-- sidebar menu end -->
        </div>
    </div>
    <!-- Left sidebar menu end -->

    <!--Main container start -->
    <main class="ttr-wrapper">
        <div class="container-fluid">
            <div class="db-breadcrumb">
                <h4 class="breadcrumb-title">Question Management</h4>
                <ul class="db-breadcrumb-list">
                    <a href = "listquestions"><li>Questions List</li></a> > 
                    <a href = "questiondetail?qid=${requestScope.q.id_Question}"><li>Questions Detail</li></a>
                </ul>
            </div>	
            <div class="row">
                <!-- Your Profile Views Chart -->
                <div class="col-lg-12 m-b30">
                    <div class="widget-box">
                        <h4>Questions Detail</h4>
                        <div class="search-bar">
                            <div class="container">
                                <div class="feature-filters clearfix center m-b40" style="display: flex; margin-left: 7%">
                                    <ul class="nav nav-tabs">
                                        <li class="nav-item">
                                            <a type="button" style="margin-top: 10px; color: black; width: 200px; border-radius: 5px" class="nav-link btn" data-toggle="tab" href="#question">Question</a>
                                        </li>
                                        <li class="nav-item">
                                            <a type="button" style="margin-top: 10px; color: black; width: 200px; border-radius: 5px" class="nav-link btn" data-toggle="tab" href="#answer">Answer</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-9 col-md-8 col-sm-12 m-b30" style="margin-top: 10px">
                            <div class="profile-content-bx" style="width: 135%">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="question" style="color: black">
                                        <div class="profile-head">
                                            <h3>Question detail</h3>
                                        </div>
                                        <div class="courses-filter">
                                            <div class="row">

                                                <div class="col-md-6 col-lg-6">
                                                    <ul class="course-features">
                                                        <li><span class="label">Id question</span> <span class="value">${requestScope.q.id_Question}</span></li>
                                                        <li><span class="label">Question's content </span> <span class="value">${requestScope.q.content_Question}</span></li>
                                                        <li><span class="label">Subject</span> <span class="value">${requestScope.s}</span></li>
                                                        <li><span class="label">Dimension</span> <span class="value">${requestScope.cs}</span></li>
                                                        <li><span class="label">Lesson</span> <span class="value">${requestScope.l}</span></li>

                                                    </ul>
                                                </div>
                                                <div class="col-md-6 col-lg-6">
                                                    <ul class="course-features">
                                                        <li><span class="label">Quiz</span> <span class="value">${requestScope.qi}</span></li>
                                                        <li><span class="label">Type</span> <span class="value">${requestScope.type}</span></li>
                                                        <li><span class="label">Date created</span> <span class="value">${requestScope.q.createDate}</span></li>
                                                        <li><span class="label">Last Updated</span> <span class="value">${requestScope.q.update_Date}</span></li>

                                                    </ul>
                                                </div>
                                                <div style="display: flex">
                                                    <div class="nav-item">
                                                        <a type="button" style="margin-bottom: 10px" class="btn" data-toggle="modal" href="#edit-question">Edit question</a>
                                                    </div>
                                                    <div class="nav-item" style="margin-left: 40%">
                                                        <a type="button" style="margin-bottom: 10px" class="btn" data-toggle="modal" href="#preview">Preview</a>
                                                    </div>
                                                    <a href="quizquestions?qid=${requestScope.q.id_Quiz}" style="margin-left: 40%;height: 40px" class="btn" type="button">Go to Quiz Content Question</a>
                                                    <!--<button class="btn" style="margin-bottom: 20px">Edit Question</button>-->
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="tab-pane" id="answer" style="color: black">
                                        <div class="profile-head">
                                            <h3>Answer detail</h3>
                                        </div>
                                        <div class="courses-filter">
                                            <div class="row">
                                                <div class="col-md-8 col-lg-8">
                                                    <ul class="course-features">
                                                        <input type="hidden" id="countcorrect" value="${requestScope.corAns.size()}"/>
                                                        <li> <span class="label">Correct answers</span> </li>

                                                        <c:forEach items="${requestScope.corAns}" var="corAns">
                                                            <li> <span class="label"></span><span class="value">${corAns.answer_Content}</span> <button style="width: 100px" class="btn" data-toggle="modal" href="#edit-answer${corAns.id_Answer}">Edit</button><button class="btn" style="margin-left: 5px;width: 100px"><a href="deleteanswer?aid=${corAns.id_Answer}&qid=${requestScope.q.id_Question}" onclick="return alertDelete()">Delete</a></button></li>
                                                                </c:forEach>
                                                        <li><span class="label">Incorrect answers</span> </li>
                                                            <c:forEach items="${requestScope.incorAns}" var="incorAns">
                                                            <li> <span class="label"></span><span class="value">${incorAns.answer_Content}</span> <button style="width: 100px" class="btn" data-toggle="modal" href="#edit-answer${incorAns.id_Answer}">Edit</button><button class="btn" style="margin-left: 5px;width: 100px"><a href="deleteanswer?aid=${incorAns.id_Answer}&qid=${requestScope.q.id_Question}" onclick="return alertDelete()">Delete</a></button></li>
                                                                </c:forEach>

                                                        <li><span class="label">Explanation</span><span style="margin-left: -13%">${requestScope.q.description}</span> </li>

                                                        <a type="button" style="margin-bottom: 10px" class="btn" data-toggle="modal" href="#add-answer">Add answer</a>
                                                    </ul>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
<div class="modal fade" id="edit-question" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top: 10%; margin-left: -15%">
    <div class="modal-dialog" role="document" >
        <div class="modal-content clearfix" style="width: 800px">
            <right><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><div class="form-control" style="background-color: red; color: black">X</div></span></button></right>
            <form action="editquestion" method="get">
                <div class="modal-body" >
                    <div class="tab-pane" id="edit-profile" >
                        <input type="hidden" id="typeQuest" value ="${requestScope.q.id_Type}"/>
                        <div class="container">
                            <h3>Edit Question</h3>
                        </div>

                        <div class="form-group row">
                            <label class="col-12 col-sm-3 col-md-3 col-lg-2 col-form-label">Question Content</label>
                            <div class="col-12 col-sm-9 col-md-9 col-lg-7">
                                <input class="form-control" type="text" name="content_q" required 
                                       value="${requestScope.q.content_Question}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-12 col-sm-3 col-md-3 col-lg-2 col-form-label">Question description</label>
                            <div class="col-12 col-sm-9 col-md-9 col-lg-7">
                                <input class="form-control" type="text" name="description_q" required 
                                       value="${requestScope.q.description}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-12 col-sm-3 col-md-3 col-lg-2 col-form-label">Chose type of question:</label>
                            <div class="col-12 col-sm-9 col-md-9 col-lg-7">
                                <select id ="idTypeQuest" name="idtypequest">
                                    <c:forEach items="${requestScope.types}" var="tl">
                                        <option id="idTypeQuest"  ${requestScope.type==tl.name_TypeQuestion?"selected":""} value="${tl.id_Type}">${tl.name_TypeQuestion}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>

                        <div style="display: flex">
                            <input type="hidden" name="idquest" value="${requestScope.q.id_Question}"/>
                            <button class="btn" onclick="return alertchangeQuest()" style="width: 100px">Save</button>
                            <button class="btn" data-dismiss="modal" aria-label="Close" style="width: 100px; margin-left: 73%">Cancel</button>
                        </div>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>                                                       
<div class="modal fade" id="add-answer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top: 10%; margin-left: -15%">
    <div class="modal-dialog" role="document" >
        <div class="modal-content clearfix" style="width: 800px">
            <right><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><div class="form-control" style="background-color: red; color: black">X</div></span></button></right>
            <form action="addanswer" method="get">
                <div class="modal-body" >
                    <div class="tab-pane" id="edit-profile" >

                        <div class="container">
                            <h3>Add answer</h3>
                        </div>

                        <div class="form-group row">
                            <label class="col-12 col-sm-3 col-md-3 col-lg-2 col-form-label">Answer</label>
                            <div class="col-12 col-sm-9 col-md-9 col-lg-7">
                                <input class="form-control" type="text" name="content_a" required 
                                       value="">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-12 col-sm-3 col-md-3 col-lg-2 col-form-label">Is corrected?</label>
                            <div class="col-12 col-sm-9 col-md-9 col-lg-7">
                                <select name="iscorrect">
                                    <option value="t">Correct</option>
                                    <option value="f">Incorrect</option>
                                </select>
                            </div>
                        </div>

                        <div style="display: flex">
                            <input type="hidden" name="idquest" value="${requestScope.q.id_Question}"/>
                            <input type="hidden" name="idtypequestion" value="${requestScope.q.id_Type}"/>
                            <button class="btn" onclick="return alertChangeAnswer()" style="width: 100px">Save</button>
                            <button class="btn" data-dismiss="modal" aria-label="Close" style="width: 100px; margin-left: 73%">Cancel</button>
                        </div>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>   
<c:forEach items="${requestScope.Ans}" var="Ans">
    <div class="modal fade" id="edit-answer${Ans.id_Answer}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top: 10%; margin-left: -15%">
        <div class="modal-dialog" role="document" >
            <div class="modal-content clearfix" style="width: 800px">
                <right><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><div class="form-control" style="background-color: red; color: black">X</div></span></button></right>
                <form action="editanswer" method="get">
                    <div class="modal-body" >
                        <div class="tab-pane" id="edit-profile" >

                            <div class="container">
                                <h3>Edit answer</h3>
                            </div>

                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-md-3 col-lg-2 col-form-label">Answer</label>
                                <div class="col-12 col-sm-9 col-md-9 col-lg-7">
                                    <input class="form-control" type="text" name="content_ans" required 
                                           value="${Ans.answer_Content}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-12 col-sm-3 col-md-3 col-lg-2 col-form-label">Is corrected?</label>
                                <div class="col-12 col-sm-9 col-md-9 col-lg-7">
                                    <select name="is_correct">
                                        <option value="t" ${Ans.is_Corret=="t"?"selected":""}>Correct</option>
                                        <option value="f" ${Ans.is_Corret=="f"?"selected":""}>Incorrect</option>
                                    </select>
                                </div>
                            </div>

                            <div style="display: flex">
                                <input type="hidden" name="idquestion" value="${requestScope.q.id_Question}"/>
                                <input type="hidden" name="aid" value="${Ans.id_Answer}"/>
                                <button class="btn" onclick="return alertChangeAnswer()" style="width: 100px">Save</button>
                                <button class="btn" data-dismiss="modal" aria-label="Close" style="width: 100px; margin-left: 73%">Cancel</button>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>         
</c:forEach>
<div class="modal fade" id="preview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top: 10%; margin-left: -15%">
    <div class="modal-dialog" role="document" >
        <div class="modal-content clearfix" style="width: 800px">
            <right><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><div class="form-control" style="background-color: red; color: black">X</div></span></button></right>

            <div class="modal-body" >
                <div class="tab-pane" id="edit-profile" >

                    <div class="container">
                        <h3>Preview</h3>
                    </div>
                    <div>
                        ${requestScope.q.content_Question}
                    </div>
                    <br>
                    <br>
                    <div>
                        <c:forEach items="${requestScope.Ans}" var="Ans">
                            <c:if test="${requestScope.q.id_Type==1}">
                                <input type="radio" name="check">${Ans.answer_Content}
                            </c:if>
                            <c:if test="${requestScope.q.id_Type==2}">
                                <input type="checkbox" name="check">${Ans.answer_Content}
                            </c:if>
                            <br>
                        </c:forEach>
                        <br>
                    </div>
                    <div style="display: flex">
                        <button class="btn" style="width: 100px">Pre</button>
                        <button class="btn" style="width: 100px; margin-left: 73%">Next</button>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>
<div class="ttr-overlay"></div>
<!-- External JavaScripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/vendors/bootstrap/js/popper.min.js"></script>
<script src="assets/vendors/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/vendors/bootstrap-select/bootstrap-select.min.js"></script>
<script src="assets/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
<script src="assets/vendors/magnific-popup/magnific-popup.js"></script>
<script src="assets/vendors/counter/waypoints-min.js"></script>
<script src="assets/vendors/counter/counterup.min.js"></script>
<script src="assets/vendors/imagesloaded/imagesloaded.js"></script>
<script src="assets/vendors/masonry/masonry.js"></script>
<script src="assets/vendors/masonry/filter.js"></script>
<script src="assets/vendors/owl-carousel/owl.carousel.js"></script>
<script src='assets/vendors/scroll/scrollbar.min.js'></script>
<script src="assets/js/functions.js"></script>
<script src="assets/vendors/chart/chart.min.js"></script>
<script src="assets/js/admin.js"></script>
<script src="assets/js/filterquestion.js" type="text/javascript"></script>
<script src="assets/alertDelete.js" type="text/javascript"></script>
<script src="assets/js/contact.js"></script>
<script src="assets/alertEditQuestion.js" type="text/javascript"></script>
</body>

<!-- Mirrored from educhamp.themetrades.com/demo/admin/courses.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:11:35 GMT -->
</html>