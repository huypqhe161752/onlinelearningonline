<%-- 
    Document   : PageCourse
    Created on : May 17, 2023, 3:51:55 PM
    Author     : PCT
--%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from educhamp.themetrades.com/demo/admin/courses.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:10:19 GMT -->
    <head>

        <script src="assets/js/PopUp.js"></script>
        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="EduChamp : Education HTML Template" />

        <!-- OG -->
        <meta property="og:title" content="EduChamp : Education HTML Template" />
        <meta property="og:description" content="EduChamp : Education HTML Template" />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no">

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="../error-404.html" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />

        <!-- PAGE TITLE HERE ============================================= -->
        <title>EduChamp : Education HTML Template </title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/assets.css">
        <link rel="stylesheet" type="text/css" href="assets/vendors/calendar/fullcalendar.css">

        <!-- TYPOGRAPHY ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/typography.css">

        <!-- SHORTCODES ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/shortcodes/shortcodes.css">

        <!-- STYLESHEETS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <link rel="stylesheet" type="text/css" href="assets/css/dashboard.css">
        <link class="skin" rel="stylesheet" type="text/css" href="assets/css/color/color-1.css">
        <link href="assets/css/SubjectAdmin.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="assets/css/page.css">
        <style>
            .active{
                color: orange;
            }
        </style>
    </head>
</head>
<body class="ttr-opened-sidebar ttr-pinned-sidebar">

    <!-- header start -->
    <header class="ttr-header">
        <div class="ttr-header-wrapper">
            <!--sidebar menu toggler start -->
            <div class="ttr-toggle-sidebar ttr-material-button">
                <i class="ti-close ttr-close-icon"></i>
                <i class="ti-menu ttr-open-icon"></i>
            </div>
            <!--sidebar menu toggler end -->
            <!--logo start -->
            <div class="ttr-logo-box">
                <div>
                    <a href="index.html" class="ttr-logo">
                        <img alt="" class="ttr-logo-mobile" src="assets/images/logo-mobile.png" width="30" height="30">
                        <img alt="" class="ttr-logo-desktop" src="assets/images/logo-white.png" width="160" height="27">
                    </a>
                </div>
            </div>
            <!--logo end -->
            <div class="ttr-header-menu">
                <!-- header left menu start -->
                <!-- header left menu end -->
            </div>
            <div class="ttr-header-right ttr-with-seperator">
                <!-- header right menu start -->
                <ul class="ttr-header-navigation">

                    <c:if test="${sessionScope.user!=null}">

                        <li> 
                            <a href="#" class="ttr-material-button ttr-submenu-toggle"
                               ><span class="ttr-user-avatar"
                                   ><img
                                        alt=""
                                        src="${sessionScope.user.avatar}"
                                        width="32"
                                        height="32" /></span
                                ></a>
                            <div class="ttr-header-submenu">
                                <ul>
                                    <li><a onclick="openPopup1('profileexpert')">My profile</a></li>
                                    <li><a onclick="openPopup1('profileexpertchangepass')">Change Password</a></li>
                                    <li><a href="logout">Logout</a></li>
                                </ul>
                            </div>
                        </li>
                        <li><a href="#">${user.full_Name}</a></li>
                        <li><a href="logout">Logout</a></li>

                    </c:if> 
                </ul>
                <!-- header right menu end -->
            </div>
            <!--header search panel start -->
            <div class="ttr-search-bar">
                <form class="ttr-search-form">
                    <div class="ttr-search-input-wrapper">
                        <input type="text" name="qq" placeholder="search something..." class="ttr-search-input">
                        <button type="submit" name="search" class="ttr-search-submit"><i class="ti-arrow-right"></i></button>
                    </div>
                    <span class="ttr-search-close ttr-search-toggle">
                        <i class="ti-close"></i>
                    </span>
                </form>
            </div>
            <!--header search panel end -->
        </div>
    </header>
    <!-- header end -->
    <!-- Left sidebar menu start -->
    <div class="ttr-sidebar">
        <div class="ttr-sidebar-wrapper content-scroll">
            <!-- side menu logo start -->
            <div class="ttr-sidebar-logo">
                <a href="#"><img alt="" src="assets/images/logo.png" width="122" height="27"></a>
                <!-- <div class="ttr-sidebar-pin-button" title="Pin/Unpin Menu">
                        <i class="material-icons ttr-fixed-icon">gps_fixed</i>
                        <i class="material-icons ttr-not-fixed-icon">gps_not_fixed</i>
                </div> -->
                <div class="ttr-sidebar-toggle-button">
                    <i class="ti-arrow-left"></i>
                </div>
            </div>
            <!-- side menu logo end -->
            <!-- sidebar menu start -->
            <nav class="ttr-sidebar-navi">
                <c:set var="currentpage" value="${requestScope.xxx}"/>
                <ul>
                    <c:if test = "${sessionScope.user.id_role==1}">
                        <li>
                            <a href="admincourselist" class="ttr-material-button">
                                <span id="active"  class="ttr-icon"><i class="ti-book"></i></span>
                                <span  id="active" class="ttr-label">Course Management</span>
                            </a>
                        </li>
                    </c:if>
                    <c:if test = "${sessionScope.user.id_role==1}">
                        <li>
                            <a href="adminsubject" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-book"></i></span>
                                <span class="ttr-label ${currentpage eq "SubjectManagment"? 'active':""}">Subject Management</span>
                            </a>
                        </li>
                    </c:if>
                    <c:if test = "${sessionScope.user.id_role==2}">
                        <li>
                            <a href="expertsubjectlistservlet" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-book"></i></span>
                                <span class="ttr-label ${currentpage eq "SubjectManagment"? 'active':""}">Subject Management</span>
                            </a>
                        </li>
                    </c:if>
                    <c:if test = "${sessionScope.user.id_role==1}">
                        <li>
                            <a href="useradmin" class="ttr-material-button">
                                <span class="ttr-icon"
                                      ><i class="ti-layout-list-post"></i
                                    ></span>
                                <span class="ttr-label">User Management</span>
                            </a>
                        </li>
                    </c:if>

                    <li>
                        <a href="quizzeslist" class="ttr-material-button ">
                            <span class="ttr-icon"><i class="ti-book"></i></span>
                            <span class="ttr-label">Quizzes Management</span>
                        </a>
                    </li>
                    <li>
                        <a href="listquestions" class="ttr-material-button ">
                            <span class="ttr-icon"><i class="ti-book"></i></span>
                            <span class="ttr-label active">Questions Management</span>
                        </a>
                    </li>
                    <li>
                        <a href="profileexpert" class="ttr-material-button">
                            <span class="ttr-icon"><i class="ti-user"></i></span>
                            <span class="ttr-label">My Profile</span>
                            <span class="ttr-arrow-icon"
                                  ><i class="fa fa-angle-down"></i
                                ></span>
                        </a>
                        <ul>

                            <li>
                                <a onclick="openPopup1('profileexpert')" class="ttr-material-button"
                                   ><span class="ttr-label">User Profile</span></a
                                >
                            </li>
                            <li>
                                <a onclick="openPopup1('profileexpertchangepass')" class="ttr-material-button"
                                   ><span class="ttr-label">Change Password</span></a
                                >
                            </li>
                            <li>
                                <a href="logout" class="ttr-material-button"
                                   ><span class="ttr-label">Logout</span></a
                                >
                            </li>
                        </ul>
                    </li>
                    <li class="ttr-seperate"></li>
                </ul>
            </nav>
            <!-- sidebar menu end -->
        </div>
    </div>
    <!-- Left sidebar menu end -->

    <!--Main container start -->
    <main class="ttr-wrapper">
        <div class="container-fluid">
            <div class="db-breadcrumb">
                <h4 class="breadcrumb-title">Question Management</h4>
                <ul class="db-breadcrumb-list">
                    <a href = "#"><li>Questions List</li></a>
                </ul>
            </div>	
            <div class="row">
                <!-- Your Profile Views Chart -->
                <div class="col-lg-12 m-b30">
                    <div class="widget-box">

                        <div class="search-bar">
                            <h4>Questions List</h4>
                            <!--<button class="add-subject">Add new Question</button>-->
                            <div class="nav-item">
                                <a type="button" style="margin-top: 10px; color: black; width: 200px; border-radius: 5px" class="btn" data-toggle="modal" href="#import-question"></i>Add new question</a>
                            </div>
                        </div>


                        <div class="search-bar">
                            <form action = "listquestions" method="post">
                                <input style="width: 300px" type="text" name ="searchkey" value="${requestScope.key}" placeholder="Search question by content...">
                                <button type="submit" name="btn" value="1"><i>Search</i></button>
                            </form>
                        </div>
                        <div class="search-bar">
                            <form action = "listquestions" method="post">
                                <input type="date" name ="datesearchkey" value="${requestScope.datekey}">
                                <button type="submit" name="btn" value="2"><i>Search by date</i></button>
                            </form>
                        </div>

                        <div class="widget-inner">
                            <form action="listquestions" method="post">
                                <!-- Table -->
                                <table style="margin-top: 10px">
                                    <thead style="background-color: #4cd5ff">
                                    <td>Id</td>
                                    <td>Content</td>
                                    <td>
                                        Filter by Subject:<select id="subject" onchange="filterDataBySubject()">
                                            <option value="all">All Subject</option>
                                            <c:forEach items="${requestScope.sList}" var="s">
                                                <option value="${s}">${s}</option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                    <td>
                                        Filter by Dimention:<select id="category" onchange="filterDataByCategory()">
                                            <option value="all">All Dimension</option>
                                            <c:forEach items="${requestScope.dimensions}" var="d">
                                                <option value="${d}">${d}</option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                    <td>
                                        Filter by Lesson :<select id="lesson" onchange="filterDataByLesson()">
                                            <option value="all">All Lesson</option>
                                            <c:forEach items="${requestScope.lList}" var="l">
                                                <option value="${l}">${l}</option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                    <td>
                                        Filter by Level:<select id="level" onchange="filterDataByLevel()">
                                            <option value="all">All Level</option>
                                            <option value="easy">easy</option>
                                            <option value="medium">medium</option>
                                            <option value="hard">hard</option>
                                        </select>
                                    </td>
                                    <td>
                                        Filter by Status:<select id="status" onchange="filterDataByStatus()">
                                            <option value="all">All Status</option>
                                            <option value="true">active</option>
                                            <option value="false">inactive</option>
                                        </select>
                                    </td>
                                    </thead>
                                    <c:forEach items="${requestScope.list}" var="list" varStatus="loop">
                                        <c:if test="${loop.index%2!=0}">
                                            <tr style="background-color: gray; color: white" class="objq" data-json='{"subject_Name":"all,${list.subject_Name}","subject_Category_Name":"all,${list.subject_Category_Name}",
                                                "lesson_Name":"all,${list.lesson_Name}","level_Name":"all,${list.level_Name}",
                                                "status":"all,${list.status}"}'>
                                                <td>${list.id_Question}</td>
                                                <td>${list.content_Question}</td>
                                                <td>${list.subject_Name}</td>
                                                <td>${list.subject_Category_Name}</td>
                                                <td>${list.lesson_Name}</td>
                                                <td>${list.level_Name}</td>
                                                <td>
                                                    <c:if test="${list.status==true}">active</c:if>
                                                    <c:if test="${list.status==false}">in active</c:if>
                                                    </td>
                                                    <td><a href="questiondetail?qid=${list.id_Question}" type="submit" class="btn">Detail</a>
                                                <td><a href="deletequestion?qid=${list.id_Question}" onclick="return alertDelete()" type="submit" class="btn">Delete</a></td></td>
                                            </tr>
                                        </c:if>
                                        <c:if test="${loop.index%2==0}">
                                            <tr class="objq" data-json='{"subject_Name":"all,${list.subject_Name}","subject_Category_Name":"all,${list.subject_Category_Name}",
                                                "lesson_Name":"all,${list.lesson_Name}","level_Name":"all,${list.level_Name}",
                                                "status":"all,${list.status}"}'>
                                                <td>${list.id_Question}</td>
                                                <td>${list.content_Question}</td>
                                                <td>${list.subject_Name}</td>
                                                <td>${list.subject_Category_Name}</td>
                                                <td>${list.lesson_Name}</td>
                                                <td>${list.level_Name}</td>
                                                <td>
                                                    <c:if test="${list.status==true}">active</c:if>
                                                    <c:if test="${list.status==false}">in active</c:if>
                                                    </td>
                                                    <td><a href="questiondetail?qid=${list.id_Question}" type="submit" class="btn">Detail</a></td>
                                                <td><a href="deletequestion?qid=${list.id_Question}" onclick="return alertDelete()" type="submit" class="btn">Delete</a></td>
                                            </tr>
                                        </c:if>
                                    </c:forEach>

                                </table>
                            </form>
                            <div style="margin-left: 50%;margin-right: 50%" class="pagination-bx rounded-sm gray clearfix">
                                <ul class="pagination" style="display: flex" >

                                    <c:if  test="${requestScope.currentpage > 1}">


                                        <li class="previous">
                                            <a href="listquestions?&page=${requestScope.currentpage - 1}"><i class="ti-arrow-left"></i> Prev</a>
                                        </li>

                                    </c:if>
                                    <c:forEach begin="1" end="${requestScope.numofpage}" var="i">

                                        <li class="${i == requestScope.currentpage?"active":""}">
                                            <a href="listquestions?&page=${i}">${i}</a>
                                        </li>

                                    </c:forEach>
                                    <c:if test="${requestScope.currentpage < requestScope.numofpage}">

                                        <li class="next">
                                            <a href="listquestions?&page=${requestScope.currentpage + 1}">Next <i class="ti-arrow-right"></i></a>
                                        </li>

                                    </c:if>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
<div class="ttr-overlay"></div>

<div class="modal fade" id="import-question" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top: 200px">
    <div class="modal-dialog" role="document">
        <div class="modal-content clearfix">
            <right><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><div class="form-control" style="background-color: red; color: black">X</div></span></button></right>
            <div class="modal-body">
                <div class="profile-head">
                    <h3>Import question</h3>
                </div>

                <form action="addquestions" method="post" enctype="multipart/form-data">
                    <div class="">
                        <div class="form-group row" style="margin-top: 30px">
                            <label class="col-12 col-sm-4 col-md-4 col-lg-3 col-form-label">Sample File</label>
                            <div class="col-12 col-sm-8 col-md-8 col-lg-7">
                                <a href="expert/Sample/SampleQuestion.txt" class="btn" download>Download sample file</a>
                            </div>
                        </div>
                        <div class="form-group row" style="margin-top: 30px">
                            <label class="col-12 col-sm-4 col-md-4 col-lg-3 col-form-label">Chose file to import</label>
                            <div class="col-12 col-sm-8 col-md-8 col-lg-7">
                                <input class="form-control" name="file" id="fileInput" type="file" value="" required accept=".txt">
                            </div>
                        </div>
                        <div style="display: flex;margin-top: 30px">
                            <div class="col-12 col-sm-8 col-md-8 col-lg-7">
                                <button type="submit"  id="submit" onclick="return checkfile()" class="btn">Import</button>
                            </div>
                            <div class="col-12 col-sm-8 col-md-8 col-lg-7">
                                <button type="submit" class="btn-close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- External JavaScripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/vendors/bootstrap/js/popper.min.js"></script>
<script src="assets/vendors/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/vendors/bootstrap-select/bootstrap-select.min.js"></script>
<script src="assets/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
<script src="assets/vendors/magnific-popup/magnific-popup.js"></script>
<script src="assets/vendors/counter/waypoints-min.js"></script>
<script src="assets/vendors/counter/counterup.min.js"></script>
<script src="assets/vendors/imagesloaded/imagesloaded.js"></script>
<script src="assets/vendors/masonry/masonry.js"></script>
<script src="assets/vendors/masonry/filter.js"></script>
<script src="assets/vendors/owl-carousel/owl.carousel.js"></script>
<script src='assets/vendors/scroll/scrollbar.min.js'></script>
<script src="assets/js/functions.js"></script>
<script src="assets/vendors/chart/chart.min.js"></script>
<script src="assets/js/admin.js"></script>
<script src="assets/js/filterquestion.js" type="text/javascript"></script>
<script src="assets/alertDelete.js" type="text/javascript"></script>
<script src="assets/readfile.js" type="text/javascript"></script>
</body>

<!-- Mirrored from educhamp.themetrades.com/demo/admin/courses.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:11:35 GMT -->
</html>