<%-- 
    Document   : quizlesson.jsp
    Created on : Jun 8, 2023, 8:35:25 AM
    Author     : Naviank
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Quiz lesson Page</title>
        <link rel="stylesheet" type="text/css" href="assets/css/vanpk_style.css"/>
    </head>
    <body>
        <div class="pg-wraper">
            <!--<div id="loading-icon-bx"></div>-->
            <jsp:include page="header.jsp"/>

            <div class="page-banner ovbl-dark" style="background-image:url(assets/images/banner/banner1.jpg);height: 120px">

                <div class="container">
                    <!--                    <div class="page-banner-entry">
                                            <h1 class="text-white" style="margin-top: 50px"></h1>
                                        </div>-->
                </div>
            </div>
            <div class="breadcrumb-row">
                <div class="container">
                    <ul class="list-inline">
                        <div class="row">
                            <i class="ti-arrow-left"></i> &nbsp;
                            <li style=""><a href="lessonview?idsubject=${requestScope.subject.id_Subject}">Back to lesson</a></li>
                        </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="q-lessoninfo">
                <span>
                    Subject: ${requestScope.subject.subject_Name}
                </span>
                <span>
                    ${requestScope.lesson.name_Lesson}
                </span>
            </div>
            <div class="q-container">
                <p id="q-guide">Choose quiz to attempt:</p>

                <c:set var="quizlevel" value="${requestScope.quizlevel}"/>
                <c:set var="quizlist" value="${requestScope.quizlist}"/>
                <c:set var="timeatt" value="${requestScope.timeatt}"/>
                <c:set var="arrgrade" value="${requestScope.arrgrade}"/>


                <div class="q-box">
                    <c:set value="0" var="i"/>
                    <c:forEach items="${quizlist}" var="cq">
                        <div class="q-items">
                            <label>Quiz level: ${quizlevel.get(i)}</label>
                            <div class="q-info">
                                <p>Number question: ${cq.getNumber_Question()}</p>
                                <p>Time limit: ${cq.getTime_Limit() / 60} minutes</p>
                                <p>Point per question: 1</p>
                                <p>Attempt time: ${timeatt[i]}</p>
                                <p>Best result: ${arrgrade[i]}/${cq.number_Question}.0</p>
                            </div>
                            <div class="q-btn">
                                <button onclick="location.href = 'reviewquiz?quiz_id=${cq.id_Quiz}'">View result</button>
                                <button onclick="startQuiz(${cq.id_Quiz})">Attempt</button>
                            </div>
                        </div>
                        <c:set value="${i + 1}" var="i"/>
                    </c:forEach>
                </div>
            </div>
        </div>

        <jsp:include page="footer.jsp"/>

        <script 
            src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js">
        </script>

        <script src="assets/js/quizattempt.js"></script>


    </body>

</html>
