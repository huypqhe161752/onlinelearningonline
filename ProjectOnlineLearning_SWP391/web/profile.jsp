<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

    <head>

        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="EduChamp : Education HTML Template" />

        <!-- OG -->
        <meta property="og:title" content="EduChamp : Education HTML Template" />
        <meta property="og:description" content="EduChamp : Education HTML Template" />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no">

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />

        <!-- PAGE TITLE HERE ============================================= -->
        <title>EduChamp : Education HTML Template </title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/assets.css">

        <!-- TYPOGRAPHY ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/typography.css">

        <!-- SHORTCODES ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/shortcodes/shortcodes.css">

        <!-- STYLESHEETS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <link class="skin" rel="stylesheet" type="text/css" href="assets/css/color/color-1.css">
        
    </head>
    <body id="bg">
        <div class="page-wraper">
            <div id="loading-icon-bx"></div>
            <!-- Header Top ==== -->
            <jsp:include page="header.jsp"></jsp:include>
                <!-- Content -->
                <div class="page-content bg-white">
                    <!-- inner page banner -->
                    <div class="page-banner ovbl-dark" style="background-image:url(assets/images/banner/banner1.jpg);">
                        <div class="container">
                            <div class="page-banner-entry">
                                <h1 class="text-white">Profile</h1>
                            </div>
                        </div>
                    </div>
                    <!-- Breadcrumb row -->
                    <div class="breadcrumb-row">
                        <div class="container">
                            <ul class="list-inline">
                                <li><a href="home">Home</a></li>
                                <li>Profile</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Breadcrumb row END -->
                    <!-- inner page banner END -->
                    <div class="content-block">
                        <!-- About Us -->
                        <div class="section-area section-sp1">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-3 col-md-4 col-sm-12 m-b30">
                                        <div class="profile-bx text-center">
                                            <div class="user-profile-thumb" style="width: 150px;height: 150px">
                                                <img src="${sessionScope.user.avatar}" alt="" style="width: 150px;height: 150px"/>
                                        </div>
                                        <div class="profile-info">
                                            <h6>${sessionScope.user.email}</h6>
                                            </div>
                                            <div class="profile-tabnav">
                                                <ul class="nav nav-tabs">
                                                    <li class="nav-item">
                                                        <a type="button" style="margin-top: 10px; color: black; width: 200px; border-radius: 5px" class="btn active" data-toggle="tab" href="#user-profile">User profile</a>
                                                    </li>
                                                    
                                                    <li class="nav-item">
                                                        <a type="button" style="margin-top: 10px; color: black; width: 200px; border-radius: 5px" class="btn" data-toggle="modal" href="#edit-profile"><i class="ti-pencil-alt"></i>Change Profile</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a type="button" style="margin-top: 10px; color: black; width: 200px; border-radius: 5px" class="btn" data-toggle="modal" href="#change-password"><i class="ti-lock"></i>Change Password</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-9 col-md-8 col-sm-12 m-b30">
                                        <div class="profile-content-bx">
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="user-profile">
                                                    <div class="profile-head">
                                                        <h3>User Profile</h3>
                                                    </div>
                                                    <form class="edit-profile">
                                                        <div class="">
                                                            <div class="form-group row">
                                                                <div class="col-12 col-sm-9 col-md-9 col-lg-10 ml-auto">
                                                                    <h3>1. Personal Details</h3>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-12 col-sm-3 col-md-3 col-lg-2 col-form-label">Full Name</label>
                                                                <div class="col-12 col-sm-9 col-md-9 col-lg-7">
                                                                    <div class="form-control">${sessionScope.user.full_Name}</div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-12 col-sm-3 col-md-3 col-lg-2 col-form-label">Gender</label>
                                                            <div class="col-12 col-sm-9 col-md-9 col-lg-7">
                                                                <div class="form-control">
                                                                    <c:if test="${sessionScope.user.isGender()==true}">Male</c:if>
                                                                    <c:if test="${sessionScope.user.isGender()==false}">Female</c:if>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-12 col-sm-3 col-md-3 col-lg-2 col-form-label">Phone No.</label>
                                                            <div class="col-12 col-sm-9 col-md-9 col-lg-7">
                                                                <div class="form-control">${sessionScope.user.phone_Number}</div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-12 col-sm-3 col-md-3 col-lg-2 col-form-label">Email</label>
                                                            <div class="col-12 col-sm-9 col-md-9 col-lg-7">
                                                                <div class="form-control">${sessionScope.user.email}</div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-12 col-sm-3 col-md-3 col-lg-2 col-form-label">Role</label>
                                                            <div class="col-12 col-sm-9 col-md-9 col-lg-7">
                                                                <c:if test="${sessionScope.user.id_role==1}"><span>admin</span></c:if>
                                                                <c:if test="${sessionScope.user.id_role==2}"><span>Expert</span></c:if>
                                                                <c:if test="${sessionScope.user.id_role==4}"><span>Marketing</span></c:if>
                                                                <c:if test="${sessionScope.user.id_role==5}"><span>Sale</span></c:if>
                                                                <c:if test="${sessionScope.user.id_role==3}"><span>Customer</span></c:if>
                                                            </div>
                                                        </div>
                                                        <div class="seperator"></div>
                                                        <div class="form-group row">
                                                            <div class="col-12 col-sm-9 col-md-9 col-lg-10 ml-auto">
                                                                <h3>2. Address</h3>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-12 col-sm-3 col-md-3 col-lg-2 col-form-label">Address</label>
                                                            <div class="col-12 col-sm-9 col-md-9 col-lg-7">
                                                                <div class="form-control">${sessionScope.user.address}</div>
                                                            </div>
                                                        </div>
                                                        <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="edit-profile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top: 50px">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content clearfix">
                                            <right><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><div class="form-control" style="background-color: red; color: black">X</div></span></button></right>
                                            <div class="modal-body">
                                                <div class="tab-pane" id="edit-profile">
                                                    <div class="profile-head">
                                                        <h3>Edit Profile</h3>
                                                    </div>
                                                    <form action="userprofile" method="post" class="edit-profile" enctype="multipart/form-data">
                                                        <input type="hidden" id ="pass" value="${sessionScope.user.password}"/>
                                                        <div class="">
                                                            <div class="form-group row">
                                                                <div class="col-12 col-sm-9 col-md-9 col-lg-10 ml-auto">
                                                                    <h3>1. Personal Details</h3>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-12 col-sm-3 col-md-3 col-lg-2 col-form-label">Full Name</label>
                                                                <div class="col-12 col-sm-9 col-md-9 col-lg-7">
                                                                    <input class="form-control" type="text" name="full_name" required maxlength="50" 
                                                                           value="${sessionScope.user.full_Name}">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-12 col-sm-3 col-md-3 col-lg-2 col-form-label">Gender</label>
                                                                <div class="col-12 col-sm-9 col-md-9 col-lg-7">
                                                                    <div style="display: flex">
                                                                        <div style="margin-left: 5px;  height: 30px; width: 30px;">
                                                                            <div>
                                                                                Male<input value="1"  id ="1" name="gender" type="radio" class="form-control" ${sessionScope.user.isGender() == true ? "checked":""}>
                                                                            </div>
                                                                        </div >
                                                                        <div style="margin-left: 50px; height: 30px; width: 30px;">
                                                                            <div>
                                                                                Female<input value="0" id="0" name="gender" type="radio" class="form-control" ${sessionScope.user.isGender() == false ? "checked":""}>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row" style="margin-top: 50px">
                                                                <label class="col-12 col-sm-3 col-md-3 col-lg-2 col-form-label">Avatar</label>
                                                                <div class="col-12 col-sm-9 col-md-9 col-lg-7">
                                                                    <img id="previewImage1" src="${sessionScope.user.avatar}" alt="Preview Image">
                                                                    <input class="form-control" name="avatar" type="file" id="imageInput1" required onchange="previewImage('imageInput1', 'previewImage1')" value="${sessionScope.user.avatar}">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-12 col-sm-3 col-md-3 col-lg-2 col-form-label">Phone No.</label>
                                                                <div class="col-12 col-sm-9 col-md-9 col-lg-7">
                                                                    <input class="form-control" name="phone" type="text" value="${sessionScope.user.phone_Number}" 
                                                                           maxlength="10" pattern="(84|0[3|5|7|8|9])+([0-9]{8})" 
                                                                           title="This input have to match with template.Eg 0987654321.">
                                                                </div>
                                                            </div>

                                                            <div class="seperator"></div>

                                                            <div class="form-group row">
                                                                <div class="col-12 col-sm-9 col-md-9 col-lg-10 ml-auto">
                                                                    <h3>2. Address</h3>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-12 col-sm-3 col-md-3 col-lg-2 col-form-label">Address</label>
                                                                <div class="col-12 col-sm-9 col-md-9 col-lg-7">
                                                                    <input class="form-control" name="address" type="text" value="${sessionScope.user.address}" maxlength="200">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="">
                                                            <div class="">
                                                                <div class="row">
                                                                    <div class="col-12 col-sm-3 col-md-3 col-lg-2">
                                                                    </div>
                                                                    <div class="col-12 col-sm-9 col-md-9 col-lg-7">
                                                                        <button type="submit" class="btn" onclick="alertSave()">Save changes</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="change-password" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top: 100px">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content clearfix">
                                            <right><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><div class="form-control" style="background-color: red; color: black">X</div></span></button></right>
                                            <div class="modal-body">
                                                <div class="profile-head">
                                                    <h3>Change Password</h3>
                                                </div>
                                                <form action="changepassword" method="post" class="edit-profile" onsubmit="return checkFormPassword()">
                                                    <input type="hidden" id="oldpass" value="${sessionScope.user.password}" />
                                                    <div class="">
                                                        <div class="form-group row">
                                                            <div class="col-12 col-sm-8 col-md-8 col-lg-9 ml-auto">
                                                                <h3>Password</h3>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-12 col-sm-4 col-md-4 col-lg-3 col-form-label">Current Password</label>
                                                            <div class="col-12 col-sm-8 col-md-8 col-lg-7">
                                                                <input class="form-control" id="currentpassword" type="password" value="" required>
                                                                <input id="oldpass" type="hidden" value="${u.password}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-12 col-sm-4 col-md-4 col-lg-3 col-form-label">New Password</label>
                                                            <div class="col-12 col-sm-8 col-md-8 col-lg-7">
                                                                <input class="form-control" name="nPass" id="newpassword" type="password" value="" required>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-12 col-sm-4 col-md-4 col-lg-3 col-form-label">Confirm Password</label>
                                                            <div class="col-12 col-sm-8 col-md-8 col-lg-7">
                                                                <input class="form-control" id="confirmpassword" type="password" value="" required>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-12 col-sm-4 col-md-4 col-lg-3 col-form-label"></label>
                                                            <div class="col-12 col-sm-8 col-md-8 col-lg-7">
                                                                <input type="checkbox" onclick="myFunctionView()"> Show Password
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-12 col-sm-4 col-md-4 col-lg-3 col-form-label"></label>
                                                            <div class="col-12 col-sm-8 col-md-8 col-lg-7">
                                                                <div id="checkpassword" style="color: red"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12 col-sm-4 col-md-4 col-lg-3">
                                                        </div>
                                                        <div class="col-12 col-sm-8 col-md-8 col-lg-7">
                                                            <button type="submit" id="submit" class="btn">Save changes</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- contact area END -->
</div>
<!-- Content END-->
<!-- Footer ==== -->
<jsp:include page="footer.jsp"></jsp:include>
</div>
<!-- External JavaScripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/vendors/bootstrap/js/popper.min.js"></script>
<script src="assets/vendors/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/vendors/bootstrap-select/bootstrap-select.min.js"></script>
<script src="assets/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
<script src="assets/vendors/magnific-popup/magnific-popup.js"></script>
<script src="assets/vendors/counter/waypoints-min.js"></script>
<script src="assets/vendors/counter/counterup.min.js"></script>
<script src="assets/vendors/imagesloaded/imagesloaded.js"></script>
<script src="assets/vendors/masonry/masonry.js"></script>
<script src="assets/vendors/masonry/filter.js"></script>
<script src="assets/vendors/owl-carousel/owl.carousel.js"></script>
<script src="assets/js/functions.js"></script>
<script src="assets/js/contact.js"></script>
<script src="assets/changepassword.js" type="text/javascript"></script>
<script src="assets/alertDelete.js" type="text/javascript"></script>
<script src="assets/checkinputpassword.js" type="text/javascript"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.0.min.js"></script><script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script>
          
            function previewImage(inputId, imageId) {
                var input = document.getElementById(inputId);
                var previewImage = document.getElementById(imageId);

                if (input.files && input.files[0]) {
                    var file = input.files[0];
                    var fileExtension = file.name.substring(file.name.lastIndexOf('.')).toLowerCase();
                    var allowedExtensions = ['.jpg', '.png'];

                    if (allowedExtensions.includes(fileExtension)) {
                        var reader = new FileReader();

                        reader.onload = function (e) {
                            previewImage.src = e.target.result;
                        };

                        reader.readAsDataURL(file);
                    } else {
                        // Đuôi tệp tin không hợp lệ, xử lý theo nhu cầu của bạn
                        console.log('Chỉ cho phép chọn tệp tin có đuôi .jpg hoặc .png.');
                    }
                }
            }
              window.onload = function () {
                var id = 1;

                if (id == 1) {
                    var myDiv = document.getElementById("myDiv");

                    setTimeout(function () {
                        myDiv.style.display = "block";

                        setTimeout(function () {
                            myDiv.style.display = "none";
                        }, 3000);
                    }, 0);
                }
            };
        </script>
</body>

</html>
