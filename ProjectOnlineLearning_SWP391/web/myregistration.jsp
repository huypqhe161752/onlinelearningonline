<%-- 
    Document   : myregistration
    Created on : Jun 6, 2023, 8:01:20 AM
    Author     : Admin
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

    <head>

        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="EduChamp : Education HTML Template" />

        <!-- OG -->
        <meta property="og:title" content="EduChamp : Education HTML Template" />
        <meta property="og:description" content="EduChamp : Education HTML Template" />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no">

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />

        <!-- PAGE TITLE HERE ============================================= -->
        <title>EduChamp : Education HTML Template </title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/assets.css">

        <!-- TYPOGRAPHY ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/typography.css">

        <!-- SHORTCODES ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/shortcodes/shortcodes.css">

        <!-- STYLESHEETS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <link class="skin" rel="stylesheet" type="text/css" href="assets/css/color/color-1.css">
        <link rel="stylesheet" href="assets/css/mycourse.css">

    </head>
    <body id="bg">
        <div class="page-wraper">
            <div id="loading-icon-bx"></div>
            <c:set var="currentpage" value="${requestScope.currentpage}"/>
            <!-- Header Top ==== -->
            <jsp:include page="header.jsp"/>
            <!-- header END ==== -->
            <!-- Content -->
            <div class="page-content bg-white" >
                <!-- inner page banner -->
                <div class="page-banner ovbl-dark" style="background-image:url(assets/images/banner/banner2.jpg); height: 150px" >
                    <div class="container">
                        <div class="page-banner-entry">
                            <h1 class="text-white" style="margin-top:50px;">My Registrations</h1>
                        </div>
                    </div>
                </div>
                <!-- Breadcrumb row -->
                <div class="breadcrumb-row">
                    <div class="container">
                        <ul class="list-inline">
                            <li><a href="home">Home</a></li>
                            <li><a href="courseservlet">My registration</a></li>
                        </ul>
                    </div>
                </div>
                <!-- Breadcrumb row END -->
                <!-- inner page banner END -->
                <div class="content-block" >
                    <!-- About Us -->
                    <div class="section-area section-sp2">
                        <div class="container">
                            <div class="feature-filters clearfix center m-b40" style="margin-left:310px;">
                                <a href="mycourse?check=1"><span>My Courses</span></a> 
                                <a style="color: orange" href="myregistrationservlet"><span>My Registration</span></a>
                                <!--                                <ul class="filters" data-toggle="buttons">
                                                                    <li data-filter="" class="btn">
                                                                        <input type="radio">
                                                                        <a href="mycourse?check=1"><span>My Courses</span></a> 
                                                                    </li>
                                                                    <li data-filter="happening" class="btn active">
                                                                        <input type="radio">
                                                                        <a href="myregistrationservlet"><span>My Registration</span></a> 
                                                                    </li>
                                                                </ul>-->
                            </div>
                        </div>
                    </div>
                    <div class="pricingtable-row" style="margin-top: 50px">

                        <div class="function" >
                            <div class="checklimitcourse">
                            </div>
                            <div  id="search" style="height: 250px; margin-top: -220px">
                                <form action="myregistrationservlet" method="post">
                                    <input type="hidden"  name="idcourse"/>
                                    <label  class="lableSearch"  for="namesubject" ><h4 class="TitleCategory">Search by name of course:</h4>
                                    </label>
                                    <input style="margin-top: 1px" class="searchkey" type="text" placeholder="Search content" name="searchcontent" value="${requestScope.contentsearch}">
                                    <input type="hidden" name="typeofsearch" value="1">
                                    <input style="margin-top: 1px" name="btn"  class="submitSearch" type="submit" value="Search">
                                </form>
                            </div>
                            <div class="categoryCourse" style="margin-top: 50px">
                                <H4 class="TitleCategory">Search Courses Following Category Course</H4>
                                <form action="myregistrationservlet" method="post">
                                    <input type="hidden" name="typeofsearch" value="2"/>
                                    <select name="categorysearch" onchange="this.form.submit()">
                                        <c:if test="${requestScope.type != null}" ><option value="${requestScope.type}" disabled selected hidden>${requestScope.type}</option></c:if>
                                        <c:if test="${requestScope.type == null}" ><option value="all" disabled selected hidden>Choose 1 option to search</option></c:if>
                                            <option value="All">All</option>
                                        <c:forEach var="l" items="${sessionScope.listCate}">
                                            <option value="${l}">
                                                ${l}
                                            </option>
                                        </c:forEach>
                                    </select>
                                </form>


                            </div>
                        </div>
                        <div class="listproduct">
                            <c:forEach items="${sessionScope.listMyRe}" var="l">
                                <div class="item" data-json='{"status":"all,${l.status}"}'>
                                    <div class="pricingtable-wrapper">
                                        <div class="pricingtable-inner">
                                            <div class="pricingtable-main row"> 
                                                <div class="pricingtable-price"> 
                                                    <img src="${l.image}" alt="EduSourse">
                                                </div>
                                                <div class="pricingtable-title col-12" style="height: 150px">
                                                    <h2>${l.name_Course}</h2>
                                                </div>
                                                <div class="Dateto col-12">
                                                    <div class="datefrom">  
                                                        <h5>Valid from</h5>
                                                        <h5>${l.valid_From}</h5>
                                                    </div>
                                                    <div class="dateto">
                                                        <h5>Valid to</h5>
                                                        <h5>${l.valid_To}</h5>
                                                    </div>
                                                </div>
                                                <div class="container1">
                                                    <div class="createDate">
                                                        <h5 style="font-size: 15px">Register Date: ${l.registration_Date}</h5>
                                                    </div>
                                                </div>  

                                            </div>
                                            <c:if test="${l.status==true}"><h5>Successful</h5></c:if>
                                            <c:if test="${l.status==false}"><h5>Submitted</h5></c:if>   
                                                <div class="container1">
                                                    <div class="createDate">
                                                        <h6>Total cost: $${l.total_cost}</h6>
                                                </div>
                                            </div>  
                                            <div class="pricingtable-footer" style="height:80px">
                                                <c:if test="${l.status==false}">
                                                    <div style="display:flex;"
                                                         <h5><a href="deletemyregistration?rid=${l.id_Register}" class="btn radius-xl"  style="height: 45px;width: 110px;margin-left: 0%" onclick="return alertDelete()">Cancel</a></h5>
                                                        <h5>
                                                            <div class="nav-item">
                                                                <a type="button"  class="btn radius-xl"  style="height: 45px;width: 110px;margin-left:47%" data-toggle="modal" href="#change-myregister${l.id_Course}">Edit</a>
                                                            </div>
                                                        </h5>
                                                    </div>
                                                </c:if>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </div>

                    <div style="margin-left: 20%" class="pagination-bx rounded-sm gray clearfix">
                        <ul class="pagination">

                            <c:if  test="${currentpage > 1}">


                                <li class="previous">
                                    <a href="myregistrationservlet?&page=${currentpage - 1}"><i class="ti-arrow-left"></i> Prev</a>
                                </li>

                            </c:if>
                            <c:forEach begin="1" end="${requestScope.numofpage}" var="i">

                                <li class="${i == currentpage?"active":""}">
                                    <a href="myregistrationservlet?&page=${i}">${i}</a>
                                </li>

                            </c:forEach>
                            <c:if test="${currentpage < numofpage}">
                                <li class="next">
                                    <a href="myregistrationservlet?&page=${currentpage + 1}">Next <i class="ti-arrow-right"></i></a>
                                </li>
                            </c:if>
                        </ul>
                    </div>
                    <p class="phone-icon">
                        <i class="fa fa-phone"> Contact us</i>
                    </p>
                    <c:forEach items="${sessionScope.listMyRe}" var="l">
                        <div class="modal fade" id="change-myregister${l.id_Course}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top: 50px;height:700px">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content clearfix">
                                    <right><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><div class="form-control" style="background-color: red; color: black">X</div></span></button></right>
                                    <div class="account-form-inner col-lg-7  col-md-7 col-sm-12 ">
                                        <div class="account-container">
                                            <div class="heading-bx left">
                                                <h2 class="title-head">Course Register</h2>
                                                <h2 style="text-align: center;">${c.course_Name}</h2>
                                            </div>	
                                            <form class="contact-bx" action="editmyregistration" method="get" >
                                                <c:set value="${sessionScope.user}" var="u"/>
                                                <div class="row placeani">
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <p>Your Email Address</p>
                                                                <c:if test="${sessionScope.user == null}">
                                                                    <input id="n1" name="email" type="email" required="" class="form-control">
                                                                </c:if>

                                                                <c:if test="${sessionScope.user != null}">
                                                                    <input name="email" type="email" readonly="" class="form-control" value="${u.getEmail()}">
                                                                </c:if>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <div class="input-group"> 
                                                                <p>Your Full Name</p>
                                                                <c:if test="${sessionScope.user == null}">
                                                                    <input name="userFullName" type="text" required="" class="form-control">
                                                                </c:if>

                                                                <c:if test="${sessionScope.user != null}">
                                                                    <input name="userFullName" type="text" readonly="" required="" class="form-control" value="${u.getFull_Name()}">
                                                                </c:if>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-12" style="margin-left: -12px">
                                                        <div class="form-group">
                                                            <div class="input-group"> 
                                                                <p>Your Phone Number</p>
                                                                <c:if test="${sessionScope.user == null}">
                                                                    <input name="userPhoneNumber" type="text" class="form-control" pattern="[0-9]{10,11}" required="" >
                                                                </c:if>

                                                                <c:if test="${sessionScope.user != null}">
                                                                    <input name="userPhoneNumber" type="text" readonly="" class="form-control" pattern="[0-9]{10,11}" required="" value="${u.phone_Number}">
                                                                </c:if>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12" style="margin-left: -12px">
                                                        <div>
                                                            <div> 
                                                                <p>Select Price</p>

                                                                <select name="packagepriceoption">
                                                                    <c:forEach items="${requestScope.listprice}" var="pp2">
                                                                        <c:if test="${l.id_Course==pp2.id_Course}"><option name="packagepriceoption" value="${pp2.id_Package}">${pp2.duration} Months - ${pp2.sale_Price}$</option></c:if>
                                                                    </c:forEach>
                                                                </select>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 m-b30" style="margin-top: 120px">
                                                    <input type="hidden" name ="reid" value="${l.id_Register}"/>
                                                    <button  onclick="alertSave()" name="submit" type="submit" value="Submit" class="btn button-md" style="margin-left: -50px ">Save</button> 
                                                    <button class="btn button-md" style="margin-left: 10px " data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Cancel</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                    <!-- Content END-->
                    <jsp:include page="footer.jsp"/>
                    <!-- Footer END ==== -->
                    <button class="back-to-top fa fa-chevron-up" ></button>
                </div>
                <script src="assets/js/mycourse.js"></script>
                <script src="assets/alertDelete.js" type="text/javascript"></script>
                <script src="assets/js/PopUp.js" type="text/javascript"></script>
                <script src="assets/changepassword.js" type="text/javascript"></script>
                </body>

                </html>

