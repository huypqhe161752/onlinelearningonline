<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">

    <head>
        <script src="assets/js/PopUp.js"></script>
        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="EduChamp : Education HTML Template" />

        <!-- OG -->
        <meta property="og:title" content="EduChamp : Education HTML Template" />
        <meta property="og:description" content="EduChamp : Education HTML Template" />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no">

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />

        <!-- PAGE TITLE HERE ============================================= -->
        <title>EduChamp : Education HTML Template </title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/assets.css">

        <!-- TYPOGRAPHY ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/typography.css">

        <!-- SHORTCODES ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/shortcodes/shortcodes.css">

        <!-- STYLESHEETS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <link class="skin" rel="stylesheet" type="text/css" href="assets/css/color/color-1.css">

    </head>
    <body id="bg">
        <div class="page-wraper" style="margin-left:300px">
            <div id="loading-icon-bx"></div>
            <c:set value="${requestScope.course}" var="c"/>
            <c:set value="${requestScope.packageprice}" var="p"/>
            <div style="margin-left: -100px" class="account-form">

                <div class="account-form-inner col-lg-7  col-md-7 col-sm-12 ">
                    <div class="account-container">
                        <div class="heading-bx left">
                            <h2 class="title-head">Course Register</h2>
                            <h2 style="text-align: center;">${c.course_Name}</h2>
                        </div>	
                        <form class="contact-bx" action="courseregister" method="post" >
                            <c:set value="${sessionScope.user}" var="u"/>
                            <div class="row placeani">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <p>Your Email Address</p>
                                            <c:if test="${sessionScope.user == null}">
                                                <input id="n1" name="email" type="email" required="" class="form-control">
                                            </c:if>

                                            <c:if test="${sessionScope.user != null}">
                                                <input name="email" type="email" readonly="" class="form-control" value="${u.getEmail()}">
                                            </c:if>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="input-group"> 
                                            <p>Your Full Name</p>
                                            <c:if test="${sessionScope.user == null}">
                                                <input name="userFullName" type="text" required="" class="form-control">
                                            </c:if>

                                            <c:if test="${sessionScope.user != null}">
                                                <input name="userFullName" type="text" readonly="" required="" class="form-control" value="${u.getFull_Name()}">
                                            </c:if>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="input-group"> 
                                            <p>Your Gender</p>
                                            <c:if test="${sessionScope.user == null}">
                                                <div style="display: flex;">
                                                    <div style="margin-left: 100px; display: flex">
                                                        <div>
                                                            <input value="true" id ="1" name="userGender" required="" type="radio" class="form-control">
                                                        </div>
                                                        <div ><label for="1" style="position: relative; right: 200px">Male</label>

                                                        </div>

                                                    </div >
                                                    <div style="margin-left: 100px; display: flex">
                                                        <div>
                                                            <input value="false" id="0" name="userGender" required="" type="radio" class="form-control">
                                                        </div>
                                                        <div>
                                                            <label for="0" style="position: relative; right: 200px">Female</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </c:if>
                                            <c:if test="${sessionScope.user != null}">
                                                <div style="display: flex;">
                                                    <div style="margin-left: 100px; display: flex">
                                                        <div>
                                                            <input value="true" id ="1" name="userGender" disabled="" type="radio" class="form-control" ${u.isGender() == true ? "checked":""}>
                                                        </div>
                                                        <div ><label for="1" style="position: relative; right: 200px">Male</label>

                                                        </div>

                                                    </div >
                                                    <div style="margin-left: 100px; display: flex">
                                                        <div>
                                                            <input value="false" id="0" name="userGender" disabled="" type="radio" class="form-control" ${u.isGender() == false ? "checked":""}>
                                                        </div>
                                                        <div>
                                                            <label for="0" style="position: relative; right: 200px">Female</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </c:if>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12" style="margin-left: -12px">
                                    <div class="form-group">
                                        <div class="input-group"> 
                                            <p>Your Phone Number</p>
                                            <c:if test="${sessionScope.user == null}">
                                                <input name="userPhoneNumber" type="text" class="form-control" pattern="[0-9]{10,11}" required="" >
                                            </c:if>

                                            <c:if test="${sessionScope.user != null}">
                                                <input name="userPhoneNumber" type="text" readonly="" class="form-control" pattern="[0-9]{10,11}" required="" value="${u.phone_Number}">
                                            </c:if>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12" style="margin-left: -12px">
                                    <div>
                                        <div> 
                                            <p>Select Price</p>
                                            <c:if test="${p == null}">
                                                <select name="idpackageprice" id="1">
                                                    <c:forEach items="${requestScope.listprice}" var="pp2" varStatus="s">
                                                        <option value="pp2">Subject price ${s.count} - ${pp2.sale_Price}$ </option>
                                                    </c:forEach>
                                                </select>
                                            </c:if>
                                            <c:if test="${p != null}">
                                                <select name="idpackageprice" id="2">
                                                    <option value="${p.id_Package}">Subject price choose - ${p.sale_Price}$</option>
                                                    <c:forEach items="${requestScope.listprice}" var="pp" varStatus="status1">
                                                        <c:if test="${p.id_Package != pp.id_Package }">        
                                                            <option value="${pp.id_Package}">Subject price ${status1.count} - ${pp.sale_Price}$ </option>
                                                        </c:if>
                                                    </c:forEach>
                                                </select>
                                            </c:if>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 m-b30" style="margin-top: 120px">
                                <c:if test="${sessionScope.user != null}">
                                    <input type="hidden" name="id_user" value="${u.id_User}" /><p>
                                    </c:if>
                                    <input type="hidden" name="id_course" value="${c.id_Course}" />
                                    <input type="hidden" name="registration_date" value="${requestScope.dateNow}" />
                                    <input type="hidden" name="id_package" value="${p.id_Package}" />
                                    <input type="hidden" name="status" value="false" />
                                    <input type="hidden" name="valid_from" value="${requestScope.dateNow}" />
                                    <div style="display: flex; justify-content: space-between">
                                    <button name="submit" type="submit" value="Submit" class="btn button-md">Register Course</button> 
                                    <button onclick="closeWindowAndPreventSubmit(event)" class="btn button-md">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>

    </div>           
</div>

<!-- External JavaScripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/vendors/bootstrap/js/popper.min.js"></script>
<script src="assets/vendors/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/vendors/bootstrap-select/bootstrap-select.min.js"></script>
<script src="assets/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
<script src="assets/vendors/magnific-popup/magnific-popup.js"></script>
<script src="assets/vendors/counter/waypoints-min.js"></script>
<script src="assets/vendors/counter/counterup.min.js"></script>
<script src="assets/vendors/imagesloaded/imagesloaded.js"></script>
<script src="assets/vendors/masonry/masonry.js"></script>
<script src="assets/vendors/masonry/filter.js"></script>
<script src="assets/vendors/owl-carousel/owl.carousel.js"></script>
<script src="assets/js/functions.js"></script>
<script src="assets/js/contact.js"></script>
</body>

</html>
