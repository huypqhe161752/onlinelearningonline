USE [master]
GO
/****** Object:  Database [OnlineLearningSystem]    Script Date: 7/17/2023 10:50:36 AM ******/
CREATE DATABASE [OnlineLearningSystem]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'OnlineLearningSystem', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER01\MSSQL\DATA\OnlineLearningSystem.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'OnlineLearningSystem_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER01\MSSQL\DATA\OnlineLearningSystem_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [OnlineLearningSystem] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [OnlineLearningSystem].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [OnlineLearningSystem] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [OnlineLearningSystem] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [OnlineLearningSystem] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [OnlineLearningSystem] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [OnlineLearningSystem] SET ARITHABORT OFF 
GO
ALTER DATABASE [OnlineLearningSystem] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [OnlineLearningSystem] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [OnlineLearningSystem] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [OnlineLearningSystem] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [OnlineLearningSystem] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [OnlineLearningSystem] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [OnlineLearningSystem] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [OnlineLearningSystem] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [OnlineLearningSystem] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [OnlineLearningSystem] SET  ENABLE_BROKER 
GO
ALTER DATABASE [OnlineLearningSystem] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [OnlineLearningSystem] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [OnlineLearningSystem] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [OnlineLearningSystem] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [OnlineLearningSystem] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [OnlineLearningSystem] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [OnlineLearningSystem] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [OnlineLearningSystem] SET RECOVERY FULL 
GO
ALTER DATABASE [OnlineLearningSystem] SET  MULTI_USER 
GO
ALTER DATABASE [OnlineLearningSystem] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [OnlineLearningSystem] SET DB_CHAINING OFF 
GO
ALTER DATABASE [OnlineLearningSystem] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [OnlineLearningSystem] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [OnlineLearningSystem] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [OnlineLearningSystem] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'OnlineLearningSystem', N'ON'
GO
ALTER DATABASE [OnlineLearningSystem] SET QUERY_STORE = OFF
GO
USE [OnlineLearningSystem]
GO
/****** Object:  Table [dbo].[Answer]    Script Date: 7/17/2023 10:50:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Answer](
	[id_answer] [int] IDENTITY(1,1) NOT NULL,
	[answer_Content] [ntext] NULL,
	[is_Correct] [ntext] NULL,
	[id_Question] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_answer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Blog]    Script Date: 7/17/2023 10:50:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Blog](
	[id_Blog] [int] IDENTITY(1,1) NOT NULL,
	[thumbnail_Blog] [ntext] NOT NULL,
	[title] [text] NOT NULL,
	[content_Blog] [text] NOT NULL,
	[brief_Infor_Blog] [ntext] NULL,
	[id_User] [int] NULL,
	[view] [int] NULL,
	[id_CatergoryBlog] [int] NULL,
	[create_Date] [date] NULL,
	[update_Date] [date] NULL,
	[status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Blog] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Catergory_Subject]    Script Date: 7/17/2023 10:50:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Catergory_Subject](
	[id_Catergory_Subject] [int] IDENTITY(1,1) NOT NULL,
	[name_Catergory_Subject] [nvarchar](255) NULL,
	[description_Catergory_Subject] [ntext] NULL,
	[id_Type_Catergory_Subjec] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Catergory_Subject] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CatergoryBlog]    Script Date: 7/17/2023 10:50:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CatergoryBlog](
	[id_CatergoryBlog] [int] IDENTITY(1,1) NOT NULL,
	[name_CatergoryBlog] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id_CatergoryBlog] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Course]    Script Date: 7/17/2023 10:50:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Course](
	[id_Course] [int] IDENTITY(1,1) NOT NULL,
	[course_Name] [nvarchar](255) NOT NULL,
	[description_Course] [text] NULL,
	[create_Date] [date] NULL,
	[update_Date] [date] NULL,
	[status] [bit] NULL,
	[image] [text] NULL,
	[id_Catergory] [int] NULL,
	[id_User] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Course] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Course_Catergory]    Script Date: 7/17/2023 10:50:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Course_Catergory](
	[id_Catergory] [int] IDENTITY(1,1) NOT NULL,
	[name_Catergory] [nvarchar](300) NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Catergory] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Checkbox_Learn]    Script Date: 7/17/2023 10:50:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Checkbox_Learn](
	[id_Lesson] [int] NULL,
	[id_User] [int] NULL,
	[learn_Date] [date] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lesson]    Script Date: 7/17/2023 10:50:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lesson](
	[id_Lesson] [int] IDENTITY(1,1) NOT NULL,
	[name_Lesson] [nvarchar](500) NOT NULL,
	[content_Lesson] [ntext] NOT NULL,
	[video_Link] [ntext] NULL,
	[status] [bit] NULL,
	[id_Subject] [int] NULL,
	[id_Type] [int] NULL,
	[create_Date] [date] NULL,
	[update_Date] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Lesson] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Level]    Script Date: 7/17/2023 10:50:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Level](
	[id_Level] [int] IDENTITY(1,1) NOT NULL,
	[name_Level] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Level] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PackagePrice]    Script Date: 7/17/2023 10:50:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PackagePrice](
	[id_Package] [int] IDENTITY(1,1) NOT NULL,
	[duration] [int] NULL,
	[list_Price] [int] NULL,
	[sale_Price] [int] NULL,
	[status] [bit] NULL,
	[id_Course] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Package] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Question]    Script Date: 7/17/2023 10:50:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Question](
	[id_Question] [int] IDENTITY(1,1) NOT NULL,
	[content_Question] [ntext] NULL,
	[description] [ntext] NULL,
	[id_Quiz] [int] NULL,
	[createDate] [date] NULL,
	[update_Date] [date] NULL,
	[id_Type] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Question] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Quiz]    Script Date: 7/17/2023 10:50:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Quiz](
	[id_Quiz] [int] IDENTITY(1,1) NOT NULL,
	[name_Quiz] [ntext] NOT NULL,
	[number_Quesson] [int] NULL,
	[time_Limit] [int] NULL,
	[description_Quiz] [ntext] NULL,
	[id_Lesson] [int] NULL,
	[id_Level] [int] NULL,
	[create_Date] [date] NULL,
	[update_Date] [date] NULL,
	[id_User] [int] NULL,
	[passpercent] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Quiz] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Registration]    Script Date: 7/17/2023 10:50:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Registration](
	[id_Register] [int] IDENTITY(1,1) NOT NULL,
	[id_User] [int] NULL,
	[id_Course] [int] NULL,
	[registration_Date] [date] NULL,
	[id_PackagePrice] [int] NULL,
	[status] [bit] NULL,
	[valid_From] [date] NULL,
	[valid_To] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Register] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Result]    Script Date: 7/17/2023 10:50:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Result](
	[id_Result] [int] IDENTITY(1,1) NOT NULL,
	[start_Time] [datetime] NULL,
	[duration] [int] NULL,
	[doResult] [text] NULL,
	[correctAnwser] [text] NULL,
	[id_User] [int] NULL,
	[id_Quiz] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Result] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 7/17/2023 10:50:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[id_Role] [int] IDENTITY(1,1) NOT NULL,
	[nameRole] [nvarchar](255) NOT NULL,
	[description] [text] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Role] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Slider]    Script Date: 7/17/2023 10:50:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Slider](
	[id_Slider] [int] IDENTITY(1,1) NOT NULL,
	[title_Slider] [ntext] NOT NULL,
	[thumbnail_Image] [ntext] NOT NULL,
	[create_Date] [date] NULL,
	[update_Date] [date] NULL,
	[status] [bit] NULL,
	[notes] [ntext] NULL,
	[backlink] [ntext] NULL,
	[content_Slider] [ntext] NULL,
	[id_User] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Slider] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Subject]    Script Date: 7/17/2023 10:50:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Subject](
	[id_Subject] [int] IDENTITY(1,1) NOT NULL,
	[subject_Name] [nvarchar](255) NULL,
	[description] [ntext] NULL,
	[numberLession] [int] NULL,
	[create_Date] [date] NULL,
	[update_Date] [date] NULL,
	[status] [bit] NULL,
	[image_Subject] [ntext] NULL,
	[id_Course] [int] NULL,
	[id_Catergory_Subject] [int] NULL,
	[id_User] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Subject] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Type_Catergory_Subject]    Script Date: 7/17/2023 10:50:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Type_Catergory_Subject](
	[id_Type_Catergory_Subjec] [int] NOT NULL,
	[name_Catergory_Subject] [nvarchar](max) NULL,
	[description] [ntext] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Type_Catergory_Subjec] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Type_Question]    Script Date: 7/17/2023 10:50:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Type_Question](
	[id_Type] [int] NOT NULL,
	[name_TypeQuestion] [nvarchar](100) NULL,
	[description] [nvarchar](400) NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TypeLesson]    Script Date: 7/17/2023 10:50:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TypeLesson](
	[id_Type] [int] NOT NULL,
	[name_TypeLesson] [nvarchar](400) NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 7/17/2023 10:50:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[id_User] [int] IDENTITY(1,1) NOT NULL,
	[email] [nvarchar](255) NOT NULL,
	[password] [nvarchar](255) NOT NULL,
	[full_Name] [nvarchar](255) NOT NULL,
	[phone_Number] [nvarchar](255) NULL,
	[gender] [bit] NULL,
	[avartar] [text] NULL,
	[address] [nvarchar](400) NULL,
	[status] [bit] NULL,
	[id_Role] [int] NULL,
	[create_Date] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_User] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Answer] ON 
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (1, N'A. Pham Khai Van', N't', 1)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (2, N'B. Pham Van Khai', N'f', 1)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (3, N'C. Phan Khai Van', N'f', 1)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (4, N'D. Phan Van Khai', N'f', 1)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (5, N'A. 29/07/2003', N't', 2)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (6, N'B. 24/07/2002', N't', 2)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (7, N'C. 29/11/2004', N'f', 2)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (8, N'D. 19/10/2001', N'f', 2)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (9, N'A. Yes, Tran Hong Ngoc', N'f', 3)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (10, N'B. No, He doesn''t', N't', 3)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (11, N'C. Yes, Nguyen Khanh Linh', N't', 3)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (12, N'D. Yes, Ngo Ngoc Loan', N'f', 3)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (13, N'A. Pham Khai Van', N't', 4)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (14, N'B. Pham Van Khai', N'f', 4)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (15, N'C. Phan Khai Van', N'f', 4)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (16, N'D. Phan Van Khai', N'f', 4)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (17, N'A. 19/07/2002', N't', 5)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (18, N'B. 24/07/2002', N't', 5)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (19, N'C. 29/11/2004', N'f', 5)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (20, N'D. 19/10/2001', N'f', 5)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (21, N'A. Yes, Tran Hong Ngoc', N'f', 6)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (22, N'B. No, He doesn''t', N't', 6)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (23, N'C. Yes, Nguyen Khanh Linh', N't', 6)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (24, N'D. Yes, Ngo Ngoc Loan', N'f', 6)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (25, N'A. 3', N'f', 7)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (26, N'B. 5', N'f', 7)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (27, N'C. 4', N't', 7)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (28, N'D. 6', N'f', 7)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (29, N'A. NEU', N'f', 8)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (30, N'B. FTU', N'f', 8)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (31, N'C. HLU', N'f', 8)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (32, N'D. FPTU', N't', 8)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (33, N'A. 5', N't', 9)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (34, N'B. 4', N'f', 9)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (35, N'C. 9', N'f', 9)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (36, N'D. 7', N'f', 9)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (37, N'A. Pham Khai Van', N't', 10)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (38, N'B. Pham Van Khai', N'f', 10)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (39, N'C. Phan Khai Van', N'f', 10)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (40, N'D. Phan Van Khai', N'f', 10)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (41, N'A. 19/07/2002', N't', 11)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (42, N'B. 24/07/2002', N't', 11)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (43, N'C. 29/11/2004', N'f', 11)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (44, N'D. 19/10/2001', N'f', 11)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (45, N'A. Yes, Tran Hong Ngoc', N'f', 12)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (46, N'B. No, He doesn''t', N't', 12)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (47, N'C. Yes, Nguyen Khanh Linh', N't', 12)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (48, N'D. Yes, Ngo Ngoc Loan', N'f', 12)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (49, N'A. 3', N'f', 13)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (50, N'B. 5', N'f', 13)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (51, N'C. 4', N't', 13)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (52, N'D. 6', N'f', 13)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (53, N'A. NEU', N'f', 14)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (54, N'B. FTU', N'f', 14)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (55, N'C. HLU', N'f', 14)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (56, N'D. FPTU', N't', 14)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (57, N'A. 5', N't', 15)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (58, N'B. 4', N'f', 15)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (59, N'C. 9', N'f', 15)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (60, N'D. 7', N'f', 15)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (61, N'A. Mrs AnhKD', N'f', 16)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (62, N'B. Mr TuanVM', N't', 16)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (63, N'C. Mrs HoaNTT', N'f', 16)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (64, N'D. Mrs HoaiBM', N'f', 16)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (65, N'A. Mr TuanVM', N'f', 17)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (66, N'B. Mr KhuongPD', N't', 17)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (67, N'C. Mrs HoaiBM', N'f', 17)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (68, N'D. Mrs NangNTH', N'f', 17)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (69, N'A. Mr Tuan VM', N'f', 18)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (70, N'B. Mr KhuongPD', N'f', 18)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (71, N'C. Mrs HoaiBM', N'f', 18)
GO
INSERT [dbo].[Answer] ([id_answer], [answer_Content], [is_Correct], [id_Question]) VALUES (72, N'D. Mrs NangNTH', N't', 18)
GO
SET IDENTITY_INSERT [dbo].[Answer] OFF
GO
SET IDENTITY_INSERT [dbo].[Blog] ON 
GO
INSERT [dbo].[Blog] ([id_Blog], [thumbnail_Blog], [title], [content_Blog], [brief_Infor_Blog], [id_User], [view], [id_CatergoryBlog], [create_Date], [update_Date], [status]) VALUES (1, N'assets/images/post/post1.jpg', N'SOL system was releasedadfad', N'SOL system is the biggest online learning system in the world. It was developed by Naviank and his friends. They only need 3 months to complete such a wonderful system like this', N'Naviank team global released the SOL system web app', 1, 24000, 3, CAST(N'2023-05-01' AS Date), CAST(N'2023-07-16' AS Date), 0)
GO
INSERT [dbo].[Blog] ([id_Blog], [thumbnail_Blog], [title], [content_Blog], [brief_Infor_Blog], [id_User], [view], [id_CatergoryBlog], [create_Date], [update_Date], [status]) VALUES (2, N'assets/images/post/post2.jpg', N'SOL has released its related app', N'Naviank team has developed the app for SOL system. Now user can download it and start learning on their own mobile phone. This is more comfortable for users when they are not convenient for using compupter ', N'Naviank team global released SOL-mb1 mobile app', 7, 28003, 1, CAST(N'2023-06-10' AS Date), CAST(N'2023-05-05' AS Date), NULL)
GO
INSERT [dbo].[Blog] ([id_Blog], [thumbnail_Blog], [title], [content_Blog], [brief_Infor_Blog], [id_User], [view], [id_CatergoryBlog], [create_Date], [update_Date], [status]) VALUES (3, N'assets/images/post/post3.jpg', N'SOL become so hot after only 1 week released', N'After only 7 days 4 hours, SOL received the 2.000.000th user on their system (both web app and mobile app). This is the biggest number recorded ever in the world. Naviank team has proved their talents and success through this.', N'2.000.000 users after 1 week released, SOL is becoming so hot', 6, 20000, 1, CAST(N'2023-06-10' AS Date), CAST(N'2023-05-08' AS Date), NULL)
GO
INSERT [dbo].[Blog] ([id_Blog], [thumbnail_Blog], [title], [content_Blog], [brief_Infor_Blog], [id_User], [view], [id_CatergoryBlog], [create_Date], [update_Date], [status]) VALUES (4, N'assets/images/post/post4.jpg', N'BMW release new car version with new technology', N'New BMW car version: JAV-2023 applied the Nana technology -  which is also used in SOL system', N'SOL and JAV-2023 used the same technology', 4, 300000, 2, CAST(N'2023-06-10' AS Date), CAST(N'2023-05-10' AS Date), NULL)
GO
INSERT [dbo].[Blog] ([id_Blog], [thumbnail_Blog], [title], [content_Blog], [brief_Infor_Blog], [id_User], [view], [id_CatergoryBlog], [create_Date], [update_Date], [status]) VALUES (5, N'assets/images/post/post5.jpg', N'Lamborghini new car version', N'New car version of Lamborghini compete in the market with BMW', N'', 1, 18900, 2, CAST(N'2023-06-10' AS Date), CAST(N'2023-05-11' AS Date), NULL)
GO
INSERT [dbo].[Blog] ([id_Blog], [thumbnail_Blog], [title], [content_Blog], [brief_Infor_Blog], [id_User], [view], [id_CatergoryBlog], [create_Date], [update_Date], [status]) VALUES (6, N'assets/images/post/post6.jpg', N'Audi release new car version with new technology', N'New Audi car version: AVdi-2023 applied the Nana technology -  which is also used in SOL system', N'SOL and AVdi-2023 used the same technology', 2, 19800, 2, CAST(N'2023-06-10' AS Date), CAST(N'2023-05-12' AS Date), NULL)
GO
INSERT [dbo].[Blog] ([id_Blog], [thumbnail_Blog], [title], [content_Blog], [brief_Infor_Blog], [id_User], [view], [id_CatergoryBlog], [create_Date], [update_Date], [status]) VALUES (7, N'assets/images/post/post29.jpg', N'Covid-23 pandemic cause jobless, what should we do at home!?', N'Because of the pandemic, we have to stay at home. Do not waste precious time, spend it on our amazing courses and you will receive back many things', N'People have to stay at home because of Covid-23. Spend time learning online so as not to waste time', 6, 18500, 3, CAST(N'2023-06-10' AS Date), CAST(N'2023-05-15' AS Date), NULL)
GO
INSERT [dbo].[Blog] ([id_Blog], [thumbnail_Blog], [title], [content_Blog], [brief_Infor_Blog], [id_User], [view], [id_CatergoryBlog], [create_Date], [update_Date], [status]) VALUES (8, N'assets/images/post/post19.jpg', N'All countries are preparing for the Internatinal Children Day', N'All countries are preparing for the Internatinal Children Day. As a mother/father, you should let your chil learn as soon as possible, so that he will have passion with learning before his friend. At the same time, you may need to join our Teaching children courses on our system', N'Happy International Children Day', 7, 15000, 3, CAST(N'2023-06-10' AS Date), CAST(N'2023-05-14' AS Date), NULL)
GO
INSERT [dbo].[Blog] ([id_Blog], [thumbnail_Blog], [title], [content_Blog], [brief_Infor_Blog], [id_User], [view], [id_CatergoryBlog], [create_Date], [update_Date], [status]) VALUES (9, N'assets/images/post/post17.jpg', N'Professor Naviank, confidence of SOL system', N'Naviank - Pham Khai Van is only 21 years old. He does not have a girlfriend. He is handsome and talented. Van has a passion with beautiful girl, but he does not like a relationship. He feels that it will be constraints between him and his work', N'Pham Khai Van - leader of Naviank team is looking for a girlfriend', 6, 100000, 4, CAST(N'2023-06-10' AS Date), CAST(N'2023-05-15' AS Date), NULL)
GO
INSERT [dbo].[Blog] ([id_Blog], [thumbnail_Blog], [title], [content_Blog], [brief_Infor_Blog], [id_User], [view], [id_CatergoryBlog], [create_Date], [update_Date], [status]) VALUES (10, N'assets/images/post/post17.jpg', N'Naviank team will have a presentation', N'Naviank team will have a presentation at class SE1714, in slot 3 lesson SWP291. Mr TuanVM will help them in this class', N'2023/25/05, Naviank teaem will present their project at class SWP291 FPT University', 5, 10000, 4, CAST(N'2023-06-11' AS Date), CAST(N'2023-05-16' AS Date), NULL)
GO
INSERT [dbo].[Blog] ([id_Blog], [thumbnail_Blog], [title], [content_Blog], [brief_Infor_Blog], [id_User], [view], [id_CatergoryBlog], [create_Date], [update_Date], [status]) VALUES (11, N'assets/images/post/post20.jpg', N'Trapping - neccessary skills for both men and women', N'Once upon a time, people started loving. We meet others, feel in love, and get married... However, we often get cheated. In this course, we will have subjects to teach you how to recognize you were cheated, how to deal with this situation, and how to re-treat people who cheated on you! Enjoy it!!!', N'A course teach you recognize signal of cheating, how to deal with cheating, and even how to re-treating', 4, 50001, 5, CAST(N'2023-06-12' AS Date), CAST(N'2023-05-20' AS Date), NULL)
GO
INSERT [dbo].[Blog] ([id_Blog], [thumbnail_Blog], [title], [content_Blog], [brief_Infor_Blog], [id_User], [view], [id_CatergoryBlog], [create_Date], [update_Date], [status]) VALUES (12, N'assets/images/post/post30.jpg', N'SOL received N3T - the biggest prize of web application', N'In the morning 2023/05/21, Naviank team has reach The 9 Ha Noi to received their first prize. They are totally deserve its. Let us send congratulations to them', N'Congratulations Naviank team received the first big prize of them', 3, 32003, 6, CAST(N'2023-06-12' AS Date), CAST(N'2023-05-21' AS Date), NULL)
GO
INSERT [dbo].[Blog] ([id_Blog], [thumbnail_Blog], [title], [content_Blog], [brief_Infor_Blog], [id_User], [view], [id_CatergoryBlog], [create_Date], [update_Date], [status]) VALUES (13, N'assets/images/post/post13.jpg', N'Offline-meeting event of SOL system', N'On 01/06/2023, SOL and Naviank team will have a meeting in Cong vien nuoc ho Tay. This is the first big event of SOL from it first released. Join us at Ha Noi, we are looking forward to your participation', N'Join our offline meeting at Tay Lake - park', 2, 30006, 7, CAST(N'2023-06-11' AS Date), CAST(N'2023-05-24' AS Date), NULL)
GO
SET IDENTITY_INSERT [dbo].[Blog] OFF
GO
SET IDENTITY_INSERT [dbo].[Catergory_Subject] ON 
GO
INSERT [dbo].[Catergory_Subject] ([id_Catergory_Subject], [name_Catergory_Subject], [description_Catergory_Subject], [id_Type_Catergory_Subjec]) VALUES (1, N'Web Programming', N'HTML, CSS, JavaScript, PHP, Ruby, Python, Java, C#, ASP.NET, Node.js, For beginner newbie 123', 1)
GO
INSERT [dbo].[Catergory_Subject] ([id_Catergory_Subject], [name_Catergory_Subject], [description_Catergory_Subject], [id_Type_Catergory_Subjec]) VALUES (2, N'Mobile Technologies', N'Android, iOS, Swift, Kotlin, React Native, Flutter, Xamarin, Ionic, PhoneGap, Unity', 2)
GO
INSERT [dbo].[Catergory_Subject] ([id_Catergory_Subject], [name_Catergory_Subject], [description_Catergory_Subject], [id_Type_Catergory_Subjec]) VALUES (3, N'Database', N'SQL, MySQL, PostgreSQL, Oracle, MongoDB, Redis, SQLite, Cassandra, Firebase, Elasticsearch', 1)
GO
INSERT [dbo].[Catergory_Subject] ([id_Catergory_Subject], [name_Catergory_Subject], [description_Catergory_Subject], [id_Type_Catergory_Subjec]) VALUES (4, N'Artificial Intelligence', N'Python, TensorFlow, PyTorch, scikit-learn, Keras, Caffe, Theano, Natural Language Processing (NLP), Computer Vision', 2)
GO
INSERT [dbo].[Catergory_Subject] ([id_Catergory_Subject], [name_Catergory_Subject], [description_Catergory_Subject], [id_Type_Catergory_Subjec]) VALUES (5, N'Internet of Things (IoT)', N'C, C++, Python, Arduino, Raspberry Pi, Node-RED, MQTT, Zigbee, Bluetooth, Home Assistant', 1)
GO
INSERT [dbo].[Catergory_Subject] ([id_Catergory_Subject], [name_Catergory_Subject], [description_Catergory_Subject], [id_Type_Catergory_Subjec]) VALUES (6, N'Version Control Systems', N'Git, GitHub, GitLab, Bitbucket, Subversion (SVN), Mercurial, CVS, Team Foundation Server (TFS), Perforce', 2)
GO
INSERT [dbo].[Catergory_Subject] ([id_Catergory_Subject], [name_Catergory_Subject], [description_Catergory_Subject], [id_Type_Catergory_Subjec]) VALUES (7, N'Information Security', N'Encryption engineering, security testing, malware analysis, firewall, IDS/IPS, VPN, penetration testing, access control list (ACL) management, intrusion detection system (IDS) ', 1)
GO
INSERT [dbo].[Catergory_Subject] ([id_Catergory_Subject], [name_Catergory_Subject], [description_Catergory_Subject], [id_Type_Catergory_Subjec]) VALUES (8, N'Cybersecurity and Data Privacy', N'This category deals with protecting computer systems, networks, and data from unauthorized access, attacks, and breaches. It encompasses technologies, practices, and measures to ensure the confidentiality, integrity, and availability of data, as well as safeguarding user privacy.', 1)
GO
INSERT [dbo].[Catergory_Subject] ([id_Catergory_Subject], [name_Catergory_Subject], [description_Catergory_Subject], [id_Type_Catergory_Subjec]) VALUES (9, N'Blockchain Technology', N'Blockchain is a decentralized and distributed digital ledger that securely records transactions across multiple computers. It provides transparency, immutability, and tamper resistance, making it useful for applications such as cryptocurrencies, supply chain management, voting systems, and more.', 2)
GO
INSERT [dbo].[Catergory_Subject] ([id_Catergory_Subject], [name_Catergory_Subject], [description_Catergory_Subject], [id_Type_Catergory_Subjec]) VALUES (10, N'Virtual Reality (VR) and Augmented Reality (AR)', N'VR involves creating simulated environments that users can interact with using headsets or other devices.', 1)
GO
INSERT [dbo].[Catergory_Subject] ([id_Catergory_Subject], [name_Catergory_Subject], [description_Catergory_Subject], [id_Type_Catergory_Subjec]) VALUES (11, N'Cloud Computing', N'Cloud computing involves the delivery of computing resources, including storage, processing power, and software, over the internet.', 2)
GO
INSERT [dbo].[Catergory_Subject] ([id_Catergory_Subject], [name_Catergory_Subject], [description_Catergory_Subject], [id_Type_Catergory_Subjec]) VALUES (12, N'Robotics and Automation', N'This category focuses on the development and application of robotic systems and automation technologies', 1)
GO
INSERT [dbo].[Catergory_Subject] ([id_Catergory_Subject], [name_Catergory_Subject], [description_Catergory_Subject], [id_Type_Catergory_Subjec]) VALUES (13, N'Biotechnology and Genetic Engineering', N'This category encompasses technologies that involve modifying living organisms or their components to develop new products or improve existing ones. I', 2)
GO
INSERT [dbo].[Catergory_Subject] ([id_Catergory_Subject], [name_Catergory_Subject], [description_Catergory_Subject], [id_Type_Catergory_Subjec]) VALUES (14, N'Sustainable Technology and Green Solutions', N'This category focuses on technologies that aim to minimize environmental impact and promote sustainable practices', 2)
GO
SET IDENTITY_INSERT [dbo].[Catergory_Subject] OFF
GO
SET IDENTITY_INSERT [dbo].[CatergoryBlog] ON 
GO
INSERT [dbo].[CatergoryBlog] ([id_CatergoryBlog], [name_CatergoryBlog]) VALUES (1, N'Page updation posts')
GO
INSERT [dbo].[CatergoryBlog] ([id_CatergoryBlog], [name_CatergoryBlog]) VALUES (2, N'Techonology posts')
GO
INSERT [dbo].[CatergoryBlog] ([id_CatergoryBlog], [name_CatergoryBlog]) VALUES (3, N'Social posts')
GO
INSERT [dbo].[CatergoryBlog] ([id_CatergoryBlog], [name_CatergoryBlog]) VALUES (4, N'Expert marketing posts')
GO
INSERT [dbo].[CatergoryBlog] ([id_CatergoryBlog], [name_CatergoryBlog]) VALUES (5, N'Course marketing posts')
GO
INSERT [dbo].[CatergoryBlog] ([id_CatergoryBlog], [name_CatergoryBlog]) VALUES (6, N'Page marketing posts')
GO
INSERT [dbo].[CatergoryBlog] ([id_CatergoryBlog], [name_CatergoryBlog]) VALUES (7, N'Event marketing post')
GO
SET IDENTITY_INSERT [dbo].[CatergoryBlog] OFF
GO
SET IDENTITY_INSERT [dbo].[Course] ON 
GO
INSERT [dbo].[Course] ([id_Course], [course_Name], [description_Course], [create_Date], [update_Date], [status], [image], [id_Catergory], [id_User]) VALUES (1, N' HTML, CSS, and Javascript for Web Developers pro1', N'Learn how to build websites from scratch using HTML, CSS – these are the 2 simplest and best languages for beginners. In this course, you will program a restaurant web page for mobile devices and a fully functional web app.', CAST(N'2023-05-11' AS Date), CAST(N'2023-07-17' AS Date), 1, N'assets/images/courses/1689552954330.jpg', 1, 9)
GO
INSERT [dbo].[Course] ([id_Course], [course_Name], [description_Course], [create_Date], [update_Date], [status], [image], [id_Catergory], [id_User]) VALUES (2, N' Java Programming and Software Engineering Fundamentals (Specialization)', N'This specilization consists
of 5 sub-courses that mainly teach the fundamentals of Java programming. This is the ideal place for students who
are just starting to learn the basics of software development and build a project of their own.', CAST(N'2023-05-14' AS Date), CAST(N'2023-05-16' AS Date), 1, N'./assets/images/courses/pic2.jpg', 1, 2)
GO
INSERT [dbo].[Course] ([id_Course], [course_Name], [description_Course], [create_Date], [update_Date], [status], [image], [id_Catergory], [id_User]) VALUES (3, N' Swift 5 iOS Application Developer (Specialization)', N'In this 4-course mini-specialization, you will go from 
the basics of Swift and how to program iOS apps to publish on the app store. You will also learn how to create fully 
functional apps with user interactions, in-app purchase integration, and subscription features.', CAST(N'2023-05-13' AS Date), CAST(N'2023-05-17' AS Date), 1, N'./assets/images/courses/pic3.jpg', 1, 2)
GO
INSERT [dbo].[Course] ([id_Course], [course_Name], [description_Course], [create_Date], [update_Date], [status], [image], [id_Catergory], [id_User]) VALUES (4, N' Project Planning and Execution', N'This course focuses on the process of planning and executing projects. 
Participants will learn how to define project objectives, develop work plans, estimate time and resources, and 
identify methods for monitoring progress. The course provides skills in risk management, quality control, and 
financial management during project implementation. Participants will apply project management methods and tools 
through practical exercises and real-world projects', CAST(N'2023-05-14' AS Date), CAST(N'2023-05-20' AS Date), 1, N'./assets/images/courses/pic4.jpg', 2, 2)
GO
INSERT [dbo].[Course] ([id_Course], [course_Name], [description_Course], [create_Date], [update_Date], [status], [image], [id_Catergory], [id_User]) VALUES (5, N'Team Leadership and Communication in Project Management', N'This course emphasizes the importance of 
effective leadership and communication within project management. Participants will learn strategies for building 
and leading high-performing teams, motivating team members, and resolving conflicts. The course covers communication
techniques for project stakeholders, including clear and concise reporting, facilitating meetings, and managing 
expectations. Participants will engage in interactive activities and case studies to enhance their leadership and
communication skills in project management', CAST(N'2023-05-16' AS Date), CAST(N'2023-05-18' AS Date), 1, N'./assets/images/courses/pic5.jpg', 2, 2)
GO
INSERT [dbo].[Course] ([id_Course], [course_Name], [description_Course], [create_Date], [update_Date], [status], [image], [id_Catergory], [id_User]) VALUES (6, N' Digital Marketing Strategies', N'This course focuses on the principles and strategies of digital marketing. 
Participants will learn about various digital channels such as social media, search engine optimization (SEO), 
content marketing, email marketing, and online advertising. The course covers topics such as target audience analysis, 
creating effective digital marketing campaigns, measuring and analyzing performance metrics, and optimizing digital 
marketing strategies. ', CAST(N'2023-05-18' AS Date), CAST(N'2023-05-21' AS Date), 1, N'./assets/images/courses/pic6.jpg', 3, 3)
GO
INSERT [dbo].[Course] ([id_Course], [course_Name], [description_Course], [create_Date], [update_Date], [status], [image], [id_Catergory], [id_User]) VALUES (7, N' Branding and Advertising', N' This course delves into the concepts and practices of branding and advertising.
Participants will learn about brand positioning, brand identity development, and brand communication strategies.
The course covers advertising principles, creative messaging, media planning, and campaign execution. ', CAST(N'2023-05-22' AS Date), CAST(N'2023-05-24' AS Date), 1, N'./assets/images/courses/pic7.jpg', 3, 3)
GO
INSERT [dbo].[Course] ([id_Course], [course_Name], [description_Course], [create_Date], [update_Date], [status], [image], [id_Catergory], [id_User]) VALUES (8, N' Fundamentals of Graphic Design', N'This course focuses on the basics of graphic design. It introduces design principles, 
the creative process, the use of colors, images, and fonts. Students will learn how to create logical compositions, use 
effects, and create aesthetic designs. This course often provides exercises and projects to practice the skills
learned.', CAST(N'2023-05-21' AS Date), CAST(N'2023-05-22' AS Date), 1, N'./assets/images/courses/pic8.jpg', 4, 3)
GO
INSERT [dbo].[Course] ([id_Course], [course_Name], [description_Course], [create_Date], [update_Date], [status], [image], [id_Catergory], [id_User]) VALUES (9, N' Adobe Photoshop for Graphic Design', N'Adobe Photoshop for Graphic Design: This is a course that focuses on using Adobe
Photoshop software, a popular tool in the field of graphic design. This course helps students master basic and advanced 
techniques in retouching images, creating special effects, mixing colors, and creating professional-looking designs.
Students will be guided through practical exercises to apply the knowledge they have learned to design projects.', CAST(N'2023-05-24' AS Date), NULL, 1, N'./assets/images/courses/pic9.jpg', 4, 3)
GO
INSERT [dbo].[Course] ([id_Course], [course_Name], [description_Course], [create_Date], [update_Date], [status], [image], [id_Catergory], [id_User]) VALUES (10, N'adfasdfa', N'asdfasd', CAST(N'2023-06-22' AS Date), CAST(N'2023-06-22' AS Date), 0, N'assets\images\courses\image_2.png', 1, 2)
GO
INSERT [dbo].[Course] ([id_Course], [course_Name], [description_Course], [create_Date], [update_Date], [status], [image], [id_Catergory], [id_User]) VALUES (11, N'Java Couse 1', N'asdfasd', CAST(N'2023-06-22' AS Date), CAST(N'2023-06-22' AS Date), 0, N'assets\images\courses\pic3.jpg', 2, 2)
GO
INSERT [dbo].[Course] ([id_Course], [course_Name], [description_Course], [create_Date], [update_Date], [status], [image], [id_Catergory], [id_User]) VALUES (12, N'Java Couse 1345', N'lkasd', CAST(N'2023-06-22' AS Date), CAST(N'2023-06-22' AS Date), 0, N'assets\images\courses\pic5.jpg', 1, 2)
GO
INSERT [dbo].[Course] ([id_Course], [course_Name], [description_Course], [create_Date], [update_Date], [status], [image], [id_Catergory], [id_User]) VALUES (13, N'Java Couse 13455', N'lkasd', CAST(N'2023-06-22' AS Date), CAST(N'2023-06-22' AS Date), 0, N'assets\images\courses\pic5.jpg', 1, 2)
GO
INSERT [dbo].[Course] ([id_Course], [course_Name], [description_Course], [create_Date], [update_Date], [status], [image], [id_Catergory], [id_User]) VALUES (14, N'Programming Fundamentals', N'Learn how to build websites from scratch using HTML, CSS – 
these are the 2 simplest and best languages', CAST(N'2023-05-26' AS Date), CAST(N'2023-05-12' AS Date), 1, N'./assets/images/courses/pic1.jpg', 1, 2)
GO
INSERT [dbo].[Course] ([id_Course], [course_Name], [description_Course], [create_Date], [update_Date], [status], [image], [id_Catergory], [id_User]) VALUES (15, N'Data Structures and Algorithms', N'Learn how to build websites from scratch using HTML, CSS – 
these are the 2 simplest and best languages', CAST(N'2023-06-26' AS Date), CAST(N'2023-05-12' AS Date), 1, N'./assets/images/courses/pic1.jpg', 1, 2)
GO
INSERT [dbo].[Course] ([id_Course], [course_Name], [description_Course], [create_Date], [update_Date], [status], [image], [id_Catergory], [id_User]) VALUES (16, N'Object-Oriented Programming', N'Learn how to build websites from scratch using HTML, CSS – 
these are the 2 simplest and best languages', CAST(N'2023-06-16' AS Date), CAST(N'2023-05-12' AS Date), 1, N'./assets/images/courses/pic1.jpg', 1, 2)
GO
INSERT [dbo].[Course] ([id_Course], [course_Name], [description_Course], [create_Date], [update_Date], [status], [image], [id_Catergory], [id_User]) VALUES (17, N'Database Management Systems', N'Learn how to build websites from scratch using HTML, CSS – 
these are the 2 simplest and best languages', CAST(N'2023-06-22' AS Date), CAST(N'2023-05-12' AS Date), 1, N'./assets/images/courses/pic1.jpg', 1, 2)
GO
INSERT [dbo].[Course] ([id_Course], [course_Name], [description_Course], [create_Date], [update_Date], [status], [image], [id_Catergory], [id_User]) VALUES (18, N'Web Development', N'Learn how to build websites from scratch using HTML, CSS – 
these are the 2 simplest and best languages', CAST(N'2023-06-25' AS Date), CAST(N'2023-05-12' AS Date), 1, N'./assets/images/courses/pic1.jpg', 1, 2)
GO
INSERT [dbo].[Course] ([id_Course], [course_Name], [description_Course], [create_Date], [update_Date], [status], [image], [id_Catergory], [id_User]) VALUES (1014, N'Nana', N'Test ', CAST(N'2023-07-16' AS Date), CAST(N'2023-05-12' AS Date), 1, N'./assets/images/courses/pic1.jpg', 1, 2)
GO
INSERT [dbo].[Course] ([id_Course], [course_Name], [description_Course], [create_Date], [update_Date], [status], [image], [id_Catergory], [id_User]) VALUES (1015, N'Nana2', N'Test2', CAST(N'2023-07-16' AS Date), CAST(N'2023-05-12' AS Date), 1, N'./assets/images/courses/pic1.jpg', 1, 2)
GO
INSERT [dbo].[Course] ([id_Course], [course_Name], [description_Course], [create_Date], [update_Date], [status], [image], [id_Catergory], [id_User]) VALUES (1016, N'Java Sourse web', N'Course For wweb', CAST(N'2023-05-16' AS Date), CAST(N'2023-07-16' AS Date), 1, N'./assets/images/courses/pic9.jpg', 2, 9)
GO
INSERT [dbo].[Course] ([id_Course], [course_Name], [description_Course], [create_Date], [update_Date], [status], [image], [id_Catergory], [id_User]) VALUES (1017, N'Java Sourse web1', N'Course For wweb', CAST(N'2023-05-16' AS Date), CAST(N'2023-07-16' AS Date), 0, N'./assets/images/courses/pic5.jpg', 2, 9)
GO
INSERT [dbo].[Course] ([id_Course], [course_Name], [description_Course], [create_Date], [update_Date], [status], [image], [id_Catergory], [id_User]) VALUES (1018, N'Java Sourse web11', N'adfad', CAST(N'2023-07-16' AS Date), CAST(N'2023-07-16' AS Date), 0, N'./assets/images/courses/pic5.jpg', 1, 2)
GO
INSERT [dbo].[Course] ([id_Course], [course_Name], [description_Course], [create_Date], [update_Date], [status], [image], [id_Catergory], [id_User]) VALUES (1019, N'development with c#', N'demo add course', CAST(N'2023-07-17' AS Date), CAST(N'2023-07-17' AS Date), 1, N'./assets/images/courses/1689554919906.jpg', 1, 9)
GO
SET IDENTITY_INSERT [dbo].[Course] OFF
GO
SET IDENTITY_INSERT [dbo].[Course_Catergory] ON 
GO
INSERT [dbo].[Course_Catergory] ([id_Catergory], [name_Catergory]) VALUES (1, N'Programming Course')
GO
INSERT [dbo].[Course_Catergory] ([id_Catergory], [name_Catergory]) VALUES (2, N'Project Management  Course')
GO
INSERT [dbo].[Course_Catergory] ([id_Catergory], [name_Catergory]) VALUES (3, N'Marketing and Course')
GO
INSERT [dbo].[Course_Catergory] ([id_Catergory], [name_Catergory]) VALUES (4, N'Graphic Design Skills Course')
GO
SET IDENTITY_INSERT [dbo].[Course_Catergory] OFF
GO
INSERT [dbo].[Checkbox_Learn] ([id_Lesson], [id_User], [learn_Date]) VALUES (1, 1, CAST(N'2023-06-21' AS Date))
GO
INSERT [dbo].[Checkbox_Learn] ([id_Lesson], [id_User], [learn_Date]) VALUES (6, 1, CAST(N'2023-06-21' AS Date))
GO
INSERT [dbo].[Checkbox_Learn] ([id_Lesson], [id_User], [learn_Date]) VALUES (12, 8, CAST(N'2023-06-22' AS Date))
GO
INSERT [dbo].[Checkbox_Learn] ([id_Lesson], [id_User], [learn_Date]) VALUES (6, 8, CAST(N'2023-06-22' AS Date))
GO
INSERT [dbo].[Checkbox_Learn] ([id_Lesson], [id_User], [learn_Date]) VALUES (9, 16, CAST(N'2023-07-16' AS Date))
GO
SET IDENTITY_INSERT [dbo].[Lesson] ON 
GO
INSERT [dbo].[Lesson] ([id_Lesson], [name_Lesson], [content_Lesson], [video_Link], [status], [id_Subject], [id_Type], [create_Date], [update_Date]) VALUES (1, N'Lesson 1: Introduction to Java Syntax....', N'Understand the basic syntax of Java, such as data types, variables, operators, and control structures.
		   Learn how to write simple programs that perform basic operations and make decisions using if-else statements.', N'assets/video/Subject1_Lesson1.mp4', 1, 1, 3, CAST(N'2020-06-13' AS Date), CAST(N'2023-07-16' AS Date))
GO
INSERT [dbo].[Lesson] ([id_Lesson], [name_Lesson], [content_Lesson], [video_Link], [status], [id_Subject], [id_Type], [create_Date], [update_Date]) VALUES (2, N'Lesson 2: Object-Oriented Programming', N'Explore the fundamental principles of object-oriented programming (OOP), including classes, objects, inheritance, and polymorphism.
		   Understand how to create and use objects in Java, define classes, and apply inheritance to build complex software systems.', N'assets/video/Subject1_Lesson2.mp4', 1, 1, 3, CAST(N'2019-03-13' AS Date), CAST(N'2022-07-12' AS Date))
GO
INSERT [dbo].[Lesson] ([id_Lesson], [name_Lesson], [content_Lesson], [video_Link], [status], [id_Subject], [id_Type], [create_Date], [update_Date]) VALUES (3, N'Lesson 3: Exception Handling and File I/O', N'Learn how to handle exceptions and errors in Java programs effectively.
		   Understand the importance of exception handling to maintain program stability and robustness.
		   Explore file input/output (I/O) operations to read from and write to files, and handle various file-related exceptions.', N'assets/video/Subject1_Lesson3.mp4', 1, 1, 3, CAST(N'2015-12-30' AS Date), CAST(N'2023-01-12' AS Date))
GO
INSERT [dbo].[Lesson] ([id_Lesson], [name_Lesson], [content_Lesson], [video_Link], [status], [id_Subject], [id_Type], [create_Date], [update_Date]) VALUES (4, N'Lesson 4: Java Collections Framework', N'Dive into the Java Collections Framework and its core data structures such as ArrayLists, LinkedLists, HashMaps, and HashSet.
		   Learn how to effectively use collections to store, manipulate, and retrieve data.
		   Understand the concepts of iteration, sorting, searching, and filtering using collection classes.', N'assets/video/Subject1_Lesson4.mp4', 1, 1, 3, CAST(N'2021-11-11' AS Date), CAST(N'2020-05-15' AS Date))
GO
INSERT [dbo].[Lesson] ([id_Lesson], [name_Lesson], [content_Lesson], [video_Link], [status], [id_Subject], [id_Type], [create_Date], [update_Date]) VALUES (5, N'Lesson 5: Advanced Java Concepts', N'Explore advanced Java concepts such as multithreading, synchronization, and concurrency.
		   Learn about design patterns and best practices for writing efficient, maintainable, and scalable code.
		   Gain knowledge of other advanced topics like networking, database connectivity, and GUI programming in Java.', N'assets/video/Subject1_Lesson5.mp4', 1, 1, 3, CAST(N'2022-08-07' AS Date), CAST(N'2023-06-12' AS Date))
GO
INSERT [dbo].[Lesson] ([id_Lesson], [name_Lesson], [content_Lesson], [video_Link], [status], [id_Subject], [id_Type], [create_Date], [update_Date]) VALUES (6, N'Lesson 6: Exam Quiz', N'Check your knowdleged after finshed a row of lesson', NULL, 1, 1, 1, CAST(N'2003-08-08' AS Date), CAST(N'2023-01-12' AS Date))
GO
INSERT [dbo].[Lesson] ([id_Lesson], [name_Lesson], [content_Lesson], [video_Link], [status], [id_Subject], [id_Type], [create_Date], [update_Date]) VALUES (7, N'C++ Tutorial #1: IDE & Hello World', N'
Learn how to set up your C++ Integrated Development Environment (IDE) and write your first "Hello World" program in this beginner-friendly tutorial. Get started with C++ programming and explore the basics of code compilation and execution.', N'assets/video/Subject3_Lesson1.mp4', 1, 3, 3, CAST(N'2015-01-21' AS Date), CAST(N'2022-07-17' AS Date))
GO
INSERT [dbo].[Lesson] ([id_Lesson], [name_Lesson], [content_Lesson], [video_Link], [status], [id_Subject], [id_Type], [create_Date], [update_Date]) VALUES (8, N'C++ Tutorial #2: Variables & Input', N'Discover the fundamentals of variables in C++ and how to accept user input in this tutorial. Explore data types, variable declaration, initialization, and learn how to prompt and receive input from the user, expanding your programming knowledge.', N'assets/video/Subject3_Lesson2.mp4', 1, 3, 3, CAST(N'2017-08-19' AS Date), CAST(N'2022-02-12' AS Date))
GO
INSERT [dbo].[Lesson] ([id_Lesson], [name_Lesson], [content_Lesson], [video_Link], [status], [id_Subject], [id_Type], [create_Date], [update_Date]) VALUES (9, N'C++ Tutorial #3: Arrays', N'Understand how to declare, initialize, and access array elements, as well as explore important concepts like array size, multi-dimensional arrays, and common array operations, expanding your programming skills further.', N'assets/video/Subject3_Lesson3.mp4', 1, 3, 3, CAST(N'2021-08-19' AS Date), CAST(N'2023-02-22' AS Date))
GO
INSERT [dbo].[Lesson] ([id_Lesson], [name_Lesson], [content_Lesson], [video_Link], [status], [id_Subject], [id_Type], [create_Date], [update_Date]) VALUES (10, N'C++ Tutorial #4: Functions', N'Explore the power of functions in C++ in this tutorial. Learn how to define, call, and pass arguments to functions, enabling code reusability and modular programming.', N'assets/video/Subject3_Lesson4.mp4', 1, 3, 3, CAST(N'2018-08-16' AS Date), CAST(N'2023-03-12' AS Date))
GO
INSERT [dbo].[Lesson] ([id_Lesson], [name_Lesson], [content_Lesson], [video_Link], [status], [id_Subject], [id_Type], [create_Date], [update_Date]) VALUES (11, N'C++ Tutorial #5: If Else Conditional Statement', N'Explore the power of functions in C++ in this tutorial. Learn how to define, call, and pass arguments to functions, enabling code reusability and modular programming.', N'Master the art of conditional statements in C++ with this tutorial focusing on the "if else" construct. Learn how to make decisions based on different conditions, execute specific code blocks, and handle multiple branching scenarios. Dive into comparison operators, logical operators, and nested if-else statements to enhance your programming logic. Gain a strong foundation in writing conditional statements to control the flow of your C++ programs and make them more versatile and responsive.', 1, 3, 2, CAST(N'2013-06-12' AS Date), CAST(N'2023-04-17' AS Date))
GO
INSERT [dbo].[Lesson] ([id_Lesson], [name_Lesson], [content_Lesson], [video_Link], [status], [id_Subject], [id_Type], [create_Date], [update_Date]) VALUES (12, N'Quiz 1: Review Exam', N'Review your knowledge after a row of learning', NULL, 1, 3, 1, CAST(N'2020-07-13' AS Date), CAST(N'2023-05-21' AS Date))
GO
INSERT [dbo].[Lesson] ([id_Lesson], [name_Lesson], [content_Lesson], [video_Link], [status], [id_Subject], [id_Type], [create_Date], [update_Date]) VALUES (13, N'Lesson 1 Subject 2', N'Quiz subject 2', NULL, 1, 2, 1, CAST(N'2023-07-06' AS Date), CAST(N'2023-07-06' AS Date))
GO
INSERT [dbo].[Lesson] ([id_Lesson], [name_Lesson], [content_Lesson], [video_Link], [status], [id_Subject], [id_Type], [create_Date], [update_Date]) VALUES (14, N'Lesson 1 Subject 4', N'Quiz subject 4', NULL, 1, 4, 1, CAST(N'2023-07-06' AS Date), CAST(N'2023-07-06' AS Date))
GO
SET IDENTITY_INSERT [dbo].[Lesson] OFF
GO
SET IDENTITY_INSERT [dbo].[Level] ON 
GO
INSERT [dbo].[Level] ([id_Level], [name_Level]) VALUES (1, N'Easy')
GO
INSERT [dbo].[Level] ([id_Level], [name_Level]) VALUES (2, N'Medium')
GO
INSERT [dbo].[Level] ([id_Level], [name_Level]) VALUES (3, N'Hard')
GO
SET IDENTITY_INSERT [dbo].[Level] OFF
GO
SET IDENTITY_INSERT [dbo].[PackagePrice] ON 
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (1, 1, 18000, 18000, 1, 1)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (2, 3, 18000, 12300, 1, 1)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (3, 6, 20000, 18000, 1, 1)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (4, 12, 24000, 21000, 1, 1)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (5, 100, 39000, 24000, 1, 1)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (6, 1, 16000, 12500, 1, 2)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (7, 3, 18000, 15400, 1, 2)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (8, 6, 21000, 18400, 1, 2)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (9, 12, 22000, 20300, 1, 2)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (10, 100, 31000, 23000, 1, 2)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (11, 1, 16000, 12300, 1, 3)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (12, 3, 17400, 15400, 1, 3)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (13, 6, 20000, 18700, 1, 3)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (14, 12, 24300, 21000, 1, 3)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (15, 100, 32300, 24400, 1, 3)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (16, 1, 17000, 13300, 1, 4)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (17, 3, 23000, 12000, 1, 4)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (18, 6, 22500, 18000, 1, 4)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (19, 12, 24005, 20400, 1, 4)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (20, 100, 31000, 24300, 1, 4)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (21, 1, 16000, 15000, 1, 5)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (22, 3, 17600, 15400, 1, 5)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (23, 6, 23500, 15000, 1, 5)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (24, 12, 25000, 21000, 1, 5)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (25, 100, 30000, 24000, 1, 5)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (26, 1, 14000, 9000, 1, 6)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (27, 3, 18000, 15000, 1, 6)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (28, 6, 20000, 18000, 1, 6)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (29, 12, 24000, 20000, 1, 6)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (30, 100, 30000, 24000, 1, 6)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (31, 1, 15000, 10000, 1, 7)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (32, 3, 19000, 12000, 1, 7)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (33, 6, 21000, 18000, 1, 7)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (34, 12, 22000, 21200, 1, 7)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (35, 100, 30000, 22300, 1, 7)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (36, 1, 12000, 8000, 1, 8)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (37, 3, 18000, 5300, 1, 8)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (38, 6, 20000, 8000, 1, 8)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (39, 12, 24000, 23000, 1, 8)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (40, 100, 30000, 24000, 1, 8)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (41, 1, 15000, 10000, 1, 9)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (42, 3, 19000, 12000, 1, 9)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (43, 6, 21000, 18000, 1, 9)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (44, 12, 22000, 21200, 1, 9)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (45, 100, 30000, 22300, 1, 9)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (46, 12, 234, 123, 1, 1016)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (47, 1, 234, 123, 1, 1017)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (48, 1, 234000, 123000, 1, 1018)
GO
INSERT [dbo].[PackagePrice] ([id_Package], [duration], [list_Price], [sale_Price], [status], [id_Course]) VALUES (51, 3, 12000000, 1000000, 1, 1019)
GO
SET IDENTITY_INSERT [dbo].[PackagePrice] OFF
GO
SET IDENTITY_INSERT [dbo].[Question] ON 
GO
INSERT [dbo].[Question] ([id_Question], [content_Question], [description], [id_Quiz], [createDate], [update_Date], [id_Type]) VALUES (1, N'What is Naviank''s real name?', N'His name is Pham Khai Van, then the correct answer is A', 1, CAST(N'2023-06-09' AS Date), CAST(N'2023-06-09' AS Date), 1)
GO
INSERT [dbo].[Question] ([id_Question], [content_Question], [description], [id_Quiz], [createDate], [update_Date], [id_Type]) VALUES (2, N'What is Naviank''s birthday?', N'He has 2 birthday, 19/07 and 24/07', 1, CAST(N'2023-06-09' AS Date), CAST(N'2023-06-10' AS Date), 2)
GO
INSERT [dbo].[Question] ([id_Question], [content_Question], [description], [id_Quiz], [createDate], [update_Date], [id_Type]) VALUES (3, N'Does Naviank have a girlfriend?', N'Yes, of course', 1, CAST(N'2023-06-09' AS Date), CAST(N'2023-06-10' AS Date), 1)
GO
INSERT [dbo].[Question] ([id_Question], [content_Question], [description], [id_Quiz], [createDate], [update_Date], [id_Type]) VALUES (4, N'What is Naviank''s real name?', N'His name is Pham Khai Van, then the correct answer is A', 2, CAST(N'2023-06-09' AS Date), CAST(N'2023-06-10' AS Date), 1)
GO
INSERT [dbo].[Question] ([id_Question], [content_Question], [description], [id_Quiz], [createDate], [update_Date], [id_Type]) VALUES (5, N'What is Naviank''s birthday?', N'He has 2 birthday, 19/07 and 24/07', 2, CAST(N'2023-06-09' AS Date), CAST(N'2023-06-11' AS Date), 2)
GO
INSERT [dbo].[Question] ([id_Question], [content_Question], [description], [id_Quiz], [createDate], [update_Date], [id_Type]) VALUES (6, N'Does Naviank have a girlfriend?', N'Yes, of course', 2, CAST(N'2023-06-09' AS Date), CAST(N'2023-06-11' AS Date), 1)
GO
INSERT [dbo].[Question] ([id_Question], [content_Question], [description], [id_Quiz], [createDate], [update_Date], [id_Type]) VALUES (7, N'How many people are there in Naviank''s family?', N'There are 4 people: Van, his brother, and his parents', 2, CAST(N'2023-06-09' AS Date), CAST(N'2023-06-09' AS Date), 1)
GO
INSERT [dbo].[Question] ([id_Question], [content_Question], [description], [id_Quiz], [createDate], [update_Date], [id_Type]) VALUES (8, N'Which university does Van study at?', N'He studys at FPT University', 2, CAST(N'2023-06-09' AS Date), CAST(N'2023-06-10' AS Date), 1)
GO
INSERT [dbo].[Question] ([id_Question], [content_Question], [description], [id_Quiz], [createDate], [update_Date], [id_Type]) VALUES (9, N'How many subject is Van studying this semester?', N'Van is studying SWP, SWT, SWR, PRN, and ITE', 2, CAST(N'2023-06-09' AS Date), CAST(N'2023-06-12' AS Date), 2)
GO
INSERT [dbo].[Question] ([id_Question], [content_Question], [description], [id_Quiz], [createDate], [update_Date], [id_Type]) VALUES (10, N'What is Naviank''s real name?', N'His name is Pham Khai Van, then the correct answer is A', 3, CAST(N'2023-06-09' AS Date), CAST(N'2023-06-09' AS Date), 1)
GO
INSERT [dbo].[Question] ([id_Question], [content_Question], [description], [id_Quiz], [createDate], [update_Date], [id_Type]) VALUES (11, N'What is Naviank''s birthday?', N'He has 2 birthday, 19/07 and 24/07', 3, CAST(N'2023-06-09' AS Date), CAST(N'2023-06-09' AS Date), 2)
GO
INSERT [dbo].[Question] ([id_Question], [content_Question], [description], [id_Quiz], [createDate], [update_Date], [id_Type]) VALUES (12, N'Does Naviank have a girlfriend?', N'Yes, of course', 3, CAST(N'2023-06-09' AS Date), CAST(N'2023-06-12' AS Date), 1)
GO
INSERT [dbo].[Question] ([id_Question], [content_Question], [description], [id_Quiz], [createDate], [update_Date], [id_Type]) VALUES (13, N'How many people are there in Naviank''s family?', N'There are 4 people: Van, his brother, and his parents', 3, CAST(N'2023-06-09' AS Date), CAST(N'2023-06-12' AS Date), 1)
GO
INSERT [dbo].[Question] ([id_Question], [content_Question], [description], [id_Quiz], [createDate], [update_Date], [id_Type]) VALUES (14, N'Which university does Van study at?', N'He studys at FPT University', 3, CAST(N'2023-06-09' AS Date), CAST(N'2023-06-11' AS Date), 1)
GO
INSERT [dbo].[Question] ([id_Question], [content_Question], [description], [id_Quiz], [createDate], [update_Date], [id_Type]) VALUES (15, N'How many subject is Van studying this semester?', N'Van is studying SWP, SWT, SWR, PRN, and ITE', 3, CAST(N'2023-06-09' AS Date), CAST(N'2023-06-11' AS Date), 2)
GO
INSERT [dbo].[Question] ([id_Question], [content_Question], [description], [id_Quiz], [createDate], [update_Date], [id_Type]) VALUES (16, N'Who teaches Van SWP?', N'Mr TuanVM', 3, CAST(N'2023-06-09' AS Date), CAST(N'2023-06-11' AS Date), 1)
GO
INSERT [dbo].[Question] ([id_Question], [content_Question], [description], [id_Quiz], [createDate], [update_Date], [id_Type]) VALUES (17, N'Who teaches Van PRN', N'Mr KhuongPD', 3, CAST(N'2023-06-09' AS Date), CAST(N'2023-06-10' AS Date), 1)
GO
INSERT [dbo].[Question] ([id_Question], [content_Question], [description], [id_Quiz], [createDate], [update_Date], [id_Type]) VALUES (18, N'Who teaches Van SWT', N'Mrs NangNTH', 3, CAST(N'2023-06-09' AS Date), CAST(N'2023-06-10' AS Date), 1)
GO
INSERT [dbo].[Question] ([id_Question], [content_Question], [description], [id_Quiz], [createDate], [update_Date], [id_Type]) VALUES (19, N'What is Naviank''s real name?', N'His name is Pham Khai Van, then the correct answer is A', 4, CAST(N'2023-06-09' AS Date), CAST(N'2023-06-09' AS Date), 1)
GO
INSERT [dbo].[Question] ([id_Question], [content_Question], [description], [id_Quiz], [createDate], [update_Date], [id_Type]) VALUES (20, N'What is Naviank''s real name?', N'His name is Pham Khai Van, then the correct answer is A', 5, CAST(N'2023-06-09' AS Date), CAST(N'2023-06-09' AS Date), 1)
GO
SET IDENTITY_INSERT [dbo].[Question] OFF
GO
SET IDENTITY_INSERT [dbo].[Quiz] ON 
GO
INSERT [dbo].[Quiz] ([id_Quiz], [name_Quiz], [number_Quesson], [time_Limit], [description_Quiz], [id_Lesson], [id_Level], [create_Date], [update_Date], [id_User], [passpercent]) VALUES (1, N'Easy final quiz C++', 3, 360, N'This is the quiz in level easy of this lesson', 6, 1, CAST(N'2023-06-09' AS Date), CAST(N'2023-06-12' AS Date), 1, 80)
GO
INSERT [dbo].[Quiz] ([id_Quiz], [name_Quiz], [number_Quesson], [time_Limit], [description_Quiz], [id_Lesson], [id_Level], [create_Date], [update_Date], [id_User], [passpercent]) VALUES (2, N'Medium final quiz C++', 6, 720, N'This is the quiz in level medium of this lesson', 6, 2, CAST(N'2023-06-09' AS Date), CAST(N'2023-06-12' AS Date), 1, 80)
GO
INSERT [dbo].[Quiz] ([id_Quiz], [name_Quiz], [number_Quesson], [time_Limit], [description_Quiz], [id_Lesson], [id_Level], [create_Date], [update_Date], [id_User], [passpercent]) VALUES (3, N'Hard final quiz C++', 9, 1080, N'This is the quiz in level hard of this lesson', 6, 3, CAST(N'2023-06-09' AS Date), CAST(N'2023-06-12' AS Date), 1, 80)
GO
INSERT [dbo].[Quiz] ([id_Quiz], [name_Quiz], [number_Quesson], [time_Limit], [description_Quiz], [id_Lesson], [id_Level], [create_Date], [update_Date], [id_User], [passpercent]) VALUES (4, N'Easy final quiz Java', 1, 120, N'This is the quiz in level easy of this lesson', 12, 1, CAST(N'2023-06-10' AS Date), CAST(N'2023-06-13' AS Date), 1, 80)
GO
INSERT [dbo].[Quiz] ([id_Quiz], [name_Quiz], [number_Quesson], [time_Limit], [description_Quiz], [id_Lesson], [id_Level], [create_Date], [update_Date], [id_User], [passpercent]) VALUES (5, N'Medium final quiz Java', 1, 120, N'This is the quiz in level medium of this lesson', 12, 2, CAST(N'2023-06-10' AS Date), CAST(N'2023-06-13' AS Date), 1, 80)
GO
INSERT [dbo].[Quiz] ([id_Quiz], [name_Quiz], [number_Quesson], [time_Limit], [description_Quiz], [id_Lesson], [id_Level], [create_Date], [update_Date], [id_User], [passpercent]) VALUES (6, N'Hard final quiz Java', 0, 0, N'This is the quiz in level hard of this lesson', 12, 3, CAST(N'2023-06-10' AS Date), CAST(N'2023-06-13' AS Date), 1, 80)
GO
INSERT [dbo].[Quiz] ([id_Quiz], [name_Quiz], [number_Quesson], [time_Limit], [description_Quiz], [id_Lesson], [id_Level], [create_Date], [update_Date], [id_User], [passpercent]) VALUES (20, N'Easy final quiz Sample 0', 0, 0, N'This is the quiz in level easy of this lesson', 13, 1, CAST(N'2023-06-10' AS Date), CAST(N'2023-06-13' AS Date), 1, 80)
GO
INSERT [dbo].[Quiz] ([id_Quiz], [name_Quiz], [number_Quesson], [time_Limit], [description_Quiz], [id_Lesson], [id_Level], [create_Date], [update_Date], [id_User], [passpercent]) VALUES (21, N'Medium final quiz Sample 1', 0, 0, N'This is the quiz in level medium of this lesson', 14, 2, CAST(N'2023-06-10' AS Date), CAST(N'2023-06-13' AS Date), 1, 80)
GO
INSERT [dbo].[Quiz] ([id_Quiz], [name_Quiz], [number_Quesson], [time_Limit], [description_Quiz], [id_Lesson], [id_Level], [create_Date], [update_Date], [id_User], [passpercent]) VALUES (22, N'Hard final quiz Sample 2', 0, 0, N'This is the quiz in level hard of this lesson', 13, 3, CAST(N'2023-06-11' AS Date), CAST(N'2023-06-13' AS Date), 1, 80)
GO
INSERT [dbo].[Quiz] ([id_Quiz], [name_Quiz], [number_Quesson], [time_Limit], [description_Quiz], [id_Lesson], [id_Level], [create_Date], [update_Date], [id_User], [passpercent]) VALUES (23, N'Easy final quiz Sample 3', 0, 0, N'This is the quiz in level easy of this lesson', 14, 1, CAST(N'2023-06-11' AS Date), CAST(N'2023-06-13' AS Date), 1, 80)
GO
INSERT [dbo].[Quiz] ([id_Quiz], [name_Quiz], [number_Quesson], [time_Limit], [description_Quiz], [id_Lesson], [id_Level], [create_Date], [update_Date], [id_User], [passpercent]) VALUES (24, N'Testing quiz data 1', 0, 0, N'Testing quiz data 1', 14, 1, CAST(N'2023-07-07' AS Date), CAST(N'2023-07-07' AS Date), 9, 80)
GO
INSERT [dbo].[Quiz] ([id_Quiz], [name_Quiz], [number_Quesson], [time_Limit], [description_Quiz], [id_Lesson], [id_Level], [create_Date], [update_Date], [id_User], [passpercent]) VALUES (25, N'Testing quiz data 2', 0, 0, N'Testing quiz data 1', 14, 1, CAST(N'2023-07-07' AS Date), CAST(N'2023-07-07' AS Date), 9, 80)
GO
INSERT [dbo].[Quiz] ([id_Quiz], [name_Quiz], [number_Quesson], [time_Limit], [description_Quiz], [id_Lesson], [id_Level], [create_Date], [update_Date], [id_User], [passpercent]) VALUES (26, N'Testing quiz data 3', 0, 0, N'Testing quiz data 1', 14, 1, CAST(N'2023-07-07' AS Date), CAST(N'2023-07-07' AS Date), 9, 80)
GO
INSERT [dbo].[Quiz] ([id_Quiz], [name_Quiz], [number_Quesson], [time_Limit], [description_Quiz], [id_Lesson], [id_Level], [create_Date], [update_Date], [id_User], [passpercent]) VALUES (27, N'Testing quiz data 4', 0, 0, N'Testing quiz data 1', 14, 1, CAST(N'2023-07-07' AS Date), CAST(N'2023-07-07' AS Date), 9, 80)
GO
INSERT [dbo].[Quiz] ([id_Quiz], [name_Quiz], [number_Quesson], [time_Limit], [description_Quiz], [id_Lesson], [id_Level], [create_Date], [update_Date], [id_User], [passpercent]) VALUES (28, N'Testing quiz data 5', 0, 0, N'Testing quiz data 1', 14, 1, CAST(N'2023-07-07' AS Date), CAST(N'2023-07-07' AS Date), 9, 80)
GO
INSERT [dbo].[Quiz] ([id_Quiz], [name_Quiz], [number_Quesson], [time_Limit], [description_Quiz], [id_Lesson], [id_Level], [create_Date], [update_Date], [id_User], [passpercent]) VALUES (29, N'Testing quiz data 6', 0, 0, N'Testing quiz data 1', 14, 1, CAST(N'2023-07-07' AS Date), CAST(N'2023-07-07' AS Date), 9, 80)
GO
INSERT [dbo].[Quiz] ([id_Quiz], [name_Quiz], [number_Quesson], [time_Limit], [description_Quiz], [id_Lesson], [id_Level], [create_Date], [update_Date], [id_User], [passpercent]) VALUES (30, N'Testing quiz data 7', 0, 0, N'Testing quiz data 1', 14, 1, CAST(N'2023-07-07' AS Date), CAST(N'2023-07-07' AS Date), 1, 80)
GO
INSERT [dbo].[Quiz] ([id_Quiz], [name_Quiz], [number_Quesson], [time_Limit], [description_Quiz], [id_Lesson], [id_Level], [create_Date], [update_Date], [id_User], [passpercent]) VALUES (31, N'Testing quiz data 8', 0, 0, N'Testing quiz data 1', 14, 1, CAST(N'2023-07-07' AS Date), CAST(N'2023-07-07' AS Date), 1, 80)
GO
INSERT [dbo].[Quiz] ([id_Quiz], [name_Quiz], [number_Quesson], [time_Limit], [description_Quiz], [id_Lesson], [id_Level], [create_Date], [update_Date], [id_User], [passpercent]) VALUES (32, N'Testing quiz data 9', 0, 0, N'Testing quiz data 1', 14, 1, CAST(N'2023-07-07' AS Date), CAST(N'2023-07-07' AS Date), 1, 80)
GO
INSERT [dbo].[Quiz] ([id_Quiz], [name_Quiz], [number_Quesson], [time_Limit], [description_Quiz], [id_Lesson], [id_Level], [create_Date], [update_Date], [id_User], [passpercent]) VALUES (33, N'Testing quiz data 10', 0, 0, N'Testing quiz data 1', 14, 1, CAST(N'2023-07-07' AS Date), CAST(N'2023-07-07' AS Date), 1, 80)
GO
INSERT [dbo].[Quiz] ([id_Quiz], [name_Quiz], [number_Quesson], [time_Limit], [description_Quiz], [id_Lesson], [id_Level], [create_Date], [update_Date], [id_User], [passpercent]) VALUES (34, N'Testing quiz data 11a', 0, 0, N'Testing quiz data 1', 14, 1, CAST(N'2023-07-07' AS Date), CAST(N'2023-07-07' AS Date), 1, 80)
GO
INSERT [dbo].[Quiz] ([id_Quiz], [name_Quiz], [number_Quesson], [time_Limit], [description_Quiz], [id_Lesson], [id_Level], [create_Date], [update_Date], [id_User], [passpercent]) VALUES (35, N'Testing quiz data 11b', 0, 0, N'Testing quiz data 1', 14, 1, CAST(N'2023-07-07' AS Date), CAST(N'2023-07-07' AS Date), 1, 80)
GO
INSERT [dbo].[Quiz] ([id_Quiz], [name_Quiz], [number_Quesson], [time_Limit], [description_Quiz], [id_Lesson], [id_Level], [create_Date], [update_Date], [id_User], [passpercent]) VALUES (36, N'Testing quiz data 12', 0, 0, N'Testing quiz data 1', 14, 1, CAST(N'2023-07-07' AS Date), CAST(N'2023-07-07' AS Date), 1, 80)
GO
INSERT [dbo].[Quiz] ([id_Quiz], [name_Quiz], [number_Quesson], [time_Limit], [description_Quiz], [id_Lesson], [id_Level], [create_Date], [update_Date], [id_User], [passpercent]) VALUES (37, N'Testing quiz data 13', 0, 0, N'Testing quiz data 1', 14, 1, CAST(N'2023-07-07' AS Date), CAST(N'2023-07-07' AS Date), 1, 80)
GO
INSERT [dbo].[Quiz] ([id_Quiz], [name_Quiz], [number_Quesson], [time_Limit], [description_Quiz], [id_Lesson], [id_Level], [create_Date], [update_Date], [id_User], [passpercent]) VALUES (38, N'Testing quiz data 14', 0, 0, N'Testing quiz data 1', 14, 1, CAST(N'2023-07-07' AS Date), CAST(N'2023-07-07' AS Date), 1, 80)
GO
INSERT [dbo].[Quiz] ([id_Quiz], [name_Quiz], [number_Quesson], [time_Limit], [description_Quiz], [id_Lesson], [id_Level], [create_Date], [update_Date], [id_User], [passpercent]) VALUES (41, N'Easy final quiz C++', 0, 0, N'This is the quiz in level easy of this lesson', 1, 1, CAST(N'2023-07-07' AS Date), CAST(N'2023-07-07' AS Date), 1, 80)
GO
SET IDENTITY_INSERT [dbo].[Quiz] OFF
GO
SET IDENTITY_INSERT [dbo].[Registration] ON 
GO
INSERT [dbo].[Registration] ([id_Register], [id_User], [id_Course], [registration_Date], [id_PackagePrice], [status], [valid_From], [valid_To]) VALUES (1, 1, 1, CAST(N'2023-06-03' AS Date), 1, 1, CAST(N'2023-06-03' AS Date), CAST(N'2023-07-24' AS Date))
GO
INSERT [dbo].[Registration] ([id_Register], [id_User], [id_Course], [registration_Date], [id_PackagePrice], [status], [valid_From], [valid_To]) VALUES (2, 1, 2, CAST(N'2023-06-06' AS Date), 7, 1, CAST(N'2023-06-06' AS Date), CAST(N'2023-07-24' AS Date))
GO
INSERT [dbo].[Registration] ([id_Register], [id_User], [id_Course], [registration_Date], [id_PackagePrice], [status], [valid_From], [valid_To]) VALUES (3, 1, 3, CAST(N'2023-12-03' AS Date), 14, 1, CAST(N'2023-12-03' AS Date), CAST(N'2024-06-05' AS Date))
GO
INSERT [dbo].[Registration] ([id_Register], [id_User], [id_Course], [registration_Date], [id_PackagePrice], [status], [valid_From], [valid_To]) VALUES (4, 1, 4, CAST(N'2023-12-03' AS Date), 17, 1, CAST(N'2023-12-04' AS Date), CAST(N'2024-04-12' AS Date))
GO
INSERT [dbo].[Registration] ([id_Register], [id_User], [id_Course], [registration_Date], [id_PackagePrice], [status], [valid_From], [valid_To]) VALUES (5, 1, 5, CAST(N'2023-12-01' AS Date), 17, 1, CAST(N'2023-12-03' AS Date), CAST(N'2023-07-24' AS Date))
GO
INSERT [dbo].[Registration] ([id_Register], [id_User], [id_Course], [registration_Date], [id_PackagePrice], [status], [valid_From], [valid_To]) VALUES (6, 1, 6, CAST(N'2023-12-01' AS Date), 28, 0, CAST(N'2023-12-03' AS Date), CAST(N'2023-10-05' AS Date))
GO
INSERT [dbo].[Registration] ([id_Register], [id_User], [id_Course], [registration_Date], [id_PackagePrice], [status], [valid_From], [valid_To]) VALUES (7, 1, 7, CAST(N'2023-12-01' AS Date), 28, 1, CAST(N'2022-12-03' AS Date), CAST(N'2023-10-03' AS Date))
GO
INSERT [dbo].[Registration] ([id_Register], [id_User], [id_Course], [registration_Date], [id_PackagePrice], [status], [valid_From], [valid_To]) VALUES (8, 6, 1, CAST(N'2023-06-03' AS Date), 1, 0, CAST(N'2023-06-03' AS Date), CAST(N'2023-06-07' AS Date))
GO
INSERT [dbo].[Registration] ([id_Register], [id_User], [id_Course], [registration_Date], [id_PackagePrice], [status], [valid_From], [valid_To]) VALUES (9, 6, 2, CAST(N'2023-06-06' AS Date), 7, 1, CAST(N'2023-06-05' AS Date), CAST(N'2023-10-09' AS Date))
GO
INSERT [dbo].[Registration] ([id_Register], [id_User], [id_Course], [registration_Date], [id_PackagePrice], [status], [valid_From], [valid_To]) VALUES (10, 6, 3, CAST(N'2023-12-03' AS Date), 14, 0, CAST(N'2023-12-03' AS Date), CAST(N'2024-06-07' AS Date))
GO
INSERT [dbo].[Registration] ([id_Register], [id_User], [id_Course], [registration_Date], [id_PackagePrice], [status], [valid_From], [valid_To]) VALUES (11, 7, 9, CAST(N'2023-06-18' AS Date), 41, 1, CAST(N'2023-06-18' AS Date), CAST(N'2023-07-18' AS Date))
GO
INSERT [dbo].[Registration] ([id_Register], [id_User], [id_Course], [registration_Date], [id_PackagePrice], [status], [valid_From], [valid_To]) VALUES (12, 8, 1, CAST(N'2023-06-19' AS Date), 3, 1, CAST(N'2023-06-19' AS Date), CAST(N'2023-12-19' AS Date))
GO
INSERT [dbo].[Registration] ([id_Register], [id_User], [id_Course], [registration_Date], [id_PackagePrice], [status], [valid_From], [valid_To]) VALUES (13, 2, 2, CAST(N'2023-06-22' AS Date), 7, 1, CAST(N'2022-12-03' AS Date), CAST(N'2024-10-03' AS Date))
GO
INSERT [dbo].[Registration] ([id_Register], [id_User], [id_Course], [registration_Date], [id_PackagePrice], [status], [valid_From], [valid_To]) VALUES (14, 5, 1, CAST(N'2023-06-19' AS Date), 2, 0, CAST(N'2023-06-03' AS Date), CAST(N'2023-06-12' AS Date))
GO
INSERT [dbo].[Registration] ([id_Register], [id_User], [id_Course], [registration_Date], [id_PackagePrice], [status], [valid_From], [valid_To]) VALUES (15, 5, 1, CAST(N'2023-06-19' AS Date), 2, 0, CAST(N'2023-06-03' AS Date), CAST(N'2024-06-12' AS Date))
GO
INSERT [dbo].[Registration] ([id_Register], [id_User], [id_Course], [registration_Date], [id_PackagePrice], [status], [valid_From], [valid_To]) VALUES (16, 2, 3, CAST(N'2023-06-18' AS Date), 10, 1, CAST(N'2023-06-05' AS Date), CAST(N'2024-10-09' AS Date))
GO
INSERT [dbo].[Registration] ([id_Register], [id_User], [id_Course], [registration_Date], [id_PackagePrice], [status], [valid_From], [valid_To]) VALUES (17, 2, 4, CAST(N'2023-06-20' AS Date), 18, 1, CAST(N'2023-06-05' AS Date), CAST(N'2024-10-09' AS Date))
GO
INSERT [dbo].[Registration] ([id_Register], [id_User], [id_Course], [registration_Date], [id_PackagePrice], [status], [valid_From], [valid_To]) VALUES (18, 8, 1, CAST(N'2023-06-12' AS Date), 3, 1, CAST(N'2023-06-11' AS Date), CAST(N'2023-12-17' AS Date))
GO
INSERT [dbo].[Registration] ([id_Register], [id_User], [id_Course], [registration_Date], [id_PackagePrice], [status], [valid_From], [valid_To]) VALUES (19, 8, 1, CAST(N'2023-06-13' AS Date), 2, 1, CAST(N'2023-06-12' AS Date), CAST(N'2023-09-16' AS Date))
GO
INSERT [dbo].[Registration] ([id_Register], [id_User], [id_Course], [registration_Date], [id_PackagePrice], [status], [valid_From], [valid_To]) VALUES (20, 8, 2, CAST(N'2023-06-14' AS Date), 6, NULL, CAST(N'2023-06-14' AS Date), CAST(N'2023-07-15' AS Date))
GO
INSERT [dbo].[Registration] ([id_Register], [id_User], [id_Course], [registration_Date], [id_PackagePrice], [status], [valid_From], [valid_To]) VALUES (21, 8, 2, CAST(N'2023-06-15' AS Date), 8, NULL, CAST(N'2023-06-15' AS Date), CAST(N'2023-12-18' AS Date))
GO
INSERT [dbo].[Registration] ([id_Register], [id_User], [id_Course], [registration_Date], [id_PackagePrice], [status], [valid_From], [valid_To]) VALUES (22, 8, 3, CAST(N'2023-06-15' AS Date), 13, NULL, CAST(N'2023-06-20' AS Date), CAST(N'2023-12-17' AS Date))
GO
INSERT [dbo].[Registration] ([id_Register], [id_User], [id_Course], [registration_Date], [id_PackagePrice], [status], [valid_From], [valid_To]) VALUES (23, 8, 4, CAST(N'2023-06-13' AS Date), 18, NULL, CAST(N'2023-06-15' AS Date), CAST(N'2023-12-11' AS Date))
GO
INSERT [dbo].[Registration] ([id_Register], [id_User], [id_Course], [registration_Date], [id_PackagePrice], [status], [valid_From], [valid_To]) VALUES (24, 8, 5, CAST(N'2023-06-11' AS Date), 22, NULL, CAST(N'2023-06-15' AS Date), CAST(N'2023-12-17' AS Date))
GO
INSERT [dbo].[Registration] ([id_Register], [id_User], [id_Course], [registration_Date], [id_PackagePrice], [status], [valid_From], [valid_To]) VALUES (25, 8, 6, CAST(N'2023-06-10' AS Date), 26, NULL, CAST(N'2023-06-15' AS Date), CAST(N'2023-12-27' AS Date))
GO
INSERT [dbo].[Registration] ([id_Register], [id_User], [id_Course], [registration_Date], [id_PackagePrice], [status], [valid_From], [valid_To]) VALUES (26, 8, 7, CAST(N'2023-06-17' AS Date), 32, NULL, CAST(N'2023-06-15' AS Date), CAST(N'2023-12-12' AS Date))
GO
INSERT [dbo].[Registration] ([id_Register], [id_User], [id_Course], [registration_Date], [id_PackagePrice], [status], [valid_From], [valid_To]) VALUES (27, 1, 1, CAST(N'2023-06-26' AS Date), 1, 0, CAST(N'2023-06-03' AS Date), CAST(N'2024-06-06' AS Date))
GO
INSERT [dbo].[Registration] ([id_Register], [id_User], [id_Course], [registration_Date], [id_PackagePrice], [status], [valid_From], [valid_To]) VALUES (28, 2, 2, CAST(N'2023-06-23' AS Date), 3, 0, CAST(N'2023-06-03' AS Date), CAST(N'2024-06-06' AS Date))
GO
INSERT [dbo].[Registration] ([id_Register], [id_User], [id_Course], [registration_Date], [id_PackagePrice], [status], [valid_From], [valid_To]) VALUES (29, 3, 3, CAST(N'2023-06-16' AS Date), 5, 0, CAST(N'2023-06-03' AS Date), CAST(N'2024-06-06' AS Date))
GO
INSERT [dbo].[Registration] ([id_Register], [id_User], [id_Course], [registration_Date], [id_PackagePrice], [status], [valid_From], [valid_To]) VALUES (30, 4, 4, CAST(N'2023-06-25' AS Date), 12, 0, CAST(N'2023-06-03' AS Date), CAST(N'2024-06-06' AS Date))
GO
INSERT [dbo].[Registration] ([id_Register], [id_User], [id_Course], [registration_Date], [id_PackagePrice], [status], [valid_From], [valid_To]) VALUES (31, 5, 5, CAST(N'2023-06-23' AS Date), 13, 0, CAST(N'2023-06-03' AS Date), CAST(N'2024-06-06' AS Date))
GO
INSERT [dbo].[Registration] ([id_Register], [id_User], [id_Course], [registration_Date], [id_PackagePrice], [status], [valid_From], [valid_To]) VALUES (32, 16, 1, CAST(N'2023-07-16' AS Date), 3, 1, CAST(N'2023-07-16' AS Date), CAST(N'2024-01-16' AS Date))
GO
SET IDENTITY_INSERT [dbo].[Registration] OFF
GO
SET IDENTITY_INSERT [dbo].[Result] ON 
GO
INSERT [dbo].[Result] ([id_Result], [start_Time], [duration], [doResult], [correctAnwser], [id_User], [id_Quiz]) VALUES (1, CAST(N'2023-07-07T13:18:18.500' AS DateTime), 6, N'A***,****,****', N'A***,AB**,*BC*', 8, 1)
GO
INSERT [dbo].[Result] ([id_Result], [start_Time], [duration], [doResult], [correctAnwser], [id_User], [id_Quiz]) VALUES (2, CAST(N'2023-07-07T13:18:27.110' AS DateTime), 6, N'A***,*B**,****', N'A***,AB**,*BC*', 8, 1)
GO
INSERT [dbo].[Result] ([id_Result], [start_Time], [duration], [doResult], [correctAnwser], [id_User], [id_Quiz]) VALUES (3, CAST(N'2023-07-07T13:18:35.723' AS DateTime), 7, N'A***,AB**,****', N'A***,AB**,*BC*', 8, 1)
GO
INSERT [dbo].[Result] ([id_Result], [start_Time], [duration], [doResult], [correctAnwser], [id_User], [id_Quiz]) VALUES (4, CAST(N'2023-07-07T13:18:45.890' AS DateTime), 7, N'A***,AB**,*B**', N'A***,AB**,*BC*', 8, 1)
GO
INSERT [dbo].[Result] ([id_Result], [start_Time], [duration], [doResult], [correctAnwser], [id_User], [id_Quiz]) VALUES (5, CAST(N'2023-07-07T13:18:54.200' AS DateTime), 8, N'A***,AB**,*BC*', N'A***,AB**,*BC*', 8, 1)
GO
INSERT [dbo].[Result] ([id_Result], [start_Time], [duration], [doResult], [correctAnwser], [id_User], [id_Quiz]) VALUES (6, CAST(N'2023-07-11T17:20:14.033' AS DateTime), 18, N'A***,AB**,*BC*,**C*,***D,****', N'A***,AB**,*BC*,**C*,***D,A***', 8, 2)
GO
INSERT [dbo].[Result] ([id_Result], [start_Time], [duration], [doResult], [correctAnwser], [id_User], [id_Quiz]) VALUES (7, CAST(N'2023-07-11T17:52:47.800' AS DateTime), 11, N'A***,AB**,*BC*', N'A***,AB**,*BC*', 8, 1)
GO
INSERT [dbo].[Result] ([id_Result], [start_Time], [duration], [doResult], [correctAnwser], [id_User], [id_Quiz]) VALUES (8, CAST(N'2023-07-11T17:53:07.877' AS DateTime), 17, N'A***,AB**,*BC*,**C*,***D,A***', N'A***,AB**,*BC*,**C*,***D,A***', 8, 2)
GO
INSERT [dbo].[Result] ([id_Result], [start_Time], [duration], [doResult], [correctAnwser], [id_User], [id_Quiz]) VALUES (9, CAST(N'2023-07-11T17:53:31.263' AS DateTime), 10, N'A***,****,****,****,****,****,****,****,****', N'A***,AB**,*BC*,**C*,***D,A***,*B**,*B**,***D', 8, 3)
GO
INSERT [dbo].[Result] ([id_Result], [start_Time], [duration], [doResult], [correctAnwser], [id_User], [id_Quiz]) VALUES (10, CAST(N'2023-07-11T17:57:59.867' AS DateTime), 1082, N'****,****,****,****,****,****,****,****,****', N'A***,AB**,*BC*,**C*,***D,A***,*B**,*B**,***D', 8, 3)
GO
INSERT [dbo].[Result] ([id_Result], [start_Time], [duration], [doResult], [correctAnwser], [id_User], [id_Quiz]) VALUES (11, CAST(N'2023-07-12T01:57:07.817' AS DateTime), 25, N'A***,AB**,*BC*,**C*,***D,A***,*B**,*B**,***D', N'A***,AB**,*BC*,**C*,***D,A***,*B**,*B**,***D', 8, 3)
GO
INSERT [dbo].[Result] ([id_Result], [start_Time], [duration], [doResult], [correctAnwser], [id_User], [id_Quiz]) VALUES (12, CAST(N'2023-07-16T16:36:45.453' AS DateTime), 6, N'', N'', 16, 4)
GO
INSERT [dbo].[Result] ([id_Result], [start_Time], [duration], [doResult], [correctAnwser], [id_User], [id_Quiz]) VALUES (13, CAST(N'2023-07-17T02:37:27.957' AS DateTime), 5, N'', N'', 16, 5)
GO
INSERT [dbo].[Result] ([id_Result], [start_Time], [duration], [doResult], [correctAnwser], [id_User], [id_Quiz]) VALUES (14, CAST(N'2023-07-17T10:18:49.163' AS DateTime), 3, N'', N'', 1, 5)
GO
INSERT [dbo].[Result] ([id_Result], [start_Time], [duration], [doResult], [correctAnwser], [id_User], [id_Quiz]) VALUES (15, CAST(N'2023-07-17T10:47:47.810' AS DateTime), 16, N'*B**,ABCD,*BC*', N'A***,AB**,*BC*', 16, 1)
GO
SET IDENTITY_INSERT [dbo].[Result] OFF
GO
SET IDENTITY_INSERT [dbo].[Role] ON 
GO
INSERT [dbo].[Role] ([id_Role], [nameRole], [description]) VALUES (1, N'Admin', N'The organization leader/manager, acts as the system administrator')
GO
INSERT [dbo].[Role] ([id_Role], [nameRole], [description]) VALUES (2, N'Expert', N'Access & prepare the course/test contents as assigned by admin')
GO
INSERT [dbo].[Role] ([id_Role], [nameRole], [description]) VALUES (3, N'Customer', N'They are registered users who are actual customers or potential customers')
GO
INSERT [dbo].[Role] ([id_Role], [nameRole], [description]) VALUES (4, N'Marketing', N'The marketing members of the organization')
GO
INSERT [dbo].[Role] ([id_Role], [nameRole], [description]) VALUES (5, N'Sale', N'The sale members of the organization')
GO
SET IDENTITY_INSERT [dbo].[Role] OFF
GO
SET IDENTITY_INSERT [dbo].[Slider] ON 
GO
INSERT [dbo].[Slider] ([id_Slider], [title_Slider], [thumbnail_Image], [create_Date], [update_Date], [status], [notes], [backlink], [content_Slider], [id_User]) VALUES (1, N'Wonderful home page', N'./assets/images/slider/slider6.jpg', CAST(N'2023-06-09' AS Date), CAST(N'2023-06-10' AS Date), 1, N'Create on 2023/24/05', N'/home', N'Home slider content', 2)
GO
INSERT [dbo].[Slider] ([id_Slider], [title_Slider], [thumbnail_Image], [create_Date], [update_Date], [status], [notes], [backlink], [content_Slider], [id_User]) VALUES (2, N'Interesting posts on our page', N'./assets/images/slider/slider8.jpg', CAST(N'2023-06-10' AS Date), CAST(N'2023-06-11' AS Date), 0, N'Create on 2023/24/05', N'/bloglist', N'Blog list slider content', 2)
GO
INSERT [dbo].[Slider] ([id_Slider], [title_Slider], [thumbnail_Image], [create_Date], [update_Date], [status], [notes], [backlink], [content_Slider], [id_User]) VALUES (3, N'Our top courses', N'./assets/images/slider/slider9.jpg', CAST(N'2023-06-10' AS Date), CAST(N'2023-06-11' AS Date), 1, N'Create on 2023/24/05', N'/courseservlet', N'Course list slider content', 2)
GO
INSERT [dbo].[Slider] ([id_Slider], [title_Slider], [thumbnail_Image], [create_Date], [update_Date], [status], [notes], [backlink], [content_Slider], [id_User]) VALUES (4, N'This post is extremely amazing', N'./assets/images/slider/slider2.jpg', CAST(N'2023-06-10' AS Date), CAST(N'2023-06-12' AS Date), 0, N'Create on 2023/24/05', N'/#', N'Blog details slider content', 2)
GO
INSERT [dbo].[Slider] ([id_Slider], [title_Slider], [thumbnail_Image], [create_Date], [update_Date], [status], [notes], [backlink], [content_Slider], [id_User]) VALUES (5, N'Blogs List ', N'./assets/images/slider/slider5.jpg', CAST(N'2023-06-11' AS Date), CAST(N'2023-06-12' AS Date), 1, N'Create on 2023/24/05', N'/bloglist', N'Blog details slider content', 2)
GO
INSERT [dbo].[Slider] ([id_Slider], [title_Slider], [thumbnail_Image], [create_Date], [update_Date], [status], [notes], [backlink], [content_Slider], [id_User]) VALUES (6, N'EA Sport', N'assets/images/slider/EA.jpg', CAST(N'2023-07-16' AS Date), CAST(N'2023-07-16' AS Date), 0, NULL, N'/easport', NULL, 4)
GO
INSERT [dbo].[Slider] ([id_Slider], [title_Slider], [thumbnail_Image], [create_Date], [update_Date], [status], [notes], [backlink], [content_Slider], [id_User]) VALUES (7, N'Java Code', N'assets/images/slider/html_course.png', CAST(N'2023-07-16' AS Date), CAST(N'2023-07-16' AS Date), 0, NULL, N'/java', NULL, 4)
GO
INSERT [dbo].[Slider] ([id_Slider], [title_Slider], [thumbnail_Image], [create_Date], [update_Date], [status], [notes], [backlink], [content_Slider], [id_User]) VALUES (8, N'AI learning', N'assets/images/slider/AILearning.jpg', CAST(N'2023-07-16' AS Date), CAST(N'2023-07-16' AS Date), 0, NULL, N'/AI', NULL, 4)
GO
INSERT [dbo].[Slider] ([id_Slider], [title_Slider], [thumbnail_Image], [create_Date], [update_Date], [status], [notes], [backlink], [content_Slider], [id_User]) VALUES (9, N'Test Data', N'assets/images/slider/csharppurple.jpg', CAST(N'2023-07-16' AS Date), CAST(N'2023-07-16' AS Date), 0, NULL, N'/#', NULL, 4)
GO
SET IDENTITY_INSERT [dbo].[Slider] OFF
GO
SET IDENTITY_INSERT [dbo].[Subject] ON 
GO
INSERT [dbo].[Subject] ([id_Subject], [subject_Name], [description], [numberLession], [create_Date], [update_Date], [status], [image_Subject], [id_Course], [id_Catergory_Subject], [id_User]) VALUES (1, N'Java Programming', N'This course focuses on the Java programming language,a popular language widely used in web, mobile, and desktop application development.', 25, CAST(N'2023-06-12' AS Date), CAST(N'2023-05-31' AS Date), 1, N'assestsAdmin/images/Subject/Subject1689505040490.jpg', 1, 1, 9)
GO
INSERT [dbo].[Subject] ([id_Subject], [subject_Name], [description], [numberLession], [create_Date], [update_Date], [status], [image_Subject], [id_Course], [id_Catergory_Subject], [id_User]) VALUES (2, N'Python Programming', N'This course focuses on the Java programming language,
a popular language widely used in web, mobile, and desktop application development.', 29, CAST(N'2023-06-12' AS Date), CAST(N'2023-06-13' AS Date), 1, N'assets/images/courses/pic1.jpg', 1, 1, 9)
GO
INSERT [dbo].[Subject] ([id_Subject], [subject_Name], [description], [numberLession], [create_Date], [update_Date], [status], [image_Subject], [id_Course], [id_Catergory_Subject], [id_User]) VALUES (3, N'C++ Programming', N'This course focuses on the Java programming language,
a popular language widely used in web, mobile, and desktop application development.', 27, CAST(N'2023-06-12' AS Date), CAST(N'2023-08-17' AS Date), 1, N'assets/images/courses/pic1.jpg', 1, 1, 9)
GO
INSERT [dbo].[Subject] ([id_Subject], [subject_Name], [description], [numberLession], [create_Date], [update_Date], [status], [image_Subject], [id_Course], [id_Catergory_Subject], [id_User]) VALUES (4, N'JavaScript Programming', N'This course focuses on the Java programming language,
a popular language widely used in web, mobile, and desktop application development.', 27, CAST(N'2023-06-12' AS Date), CAST(N'2023-06-14' AS Date), 1, N'assets/images/courses/pic1.jpg', 1, 1, 9)
GO
INSERT [dbo].[Subject] ([id_Subject], [subject_Name], [description], [numberLession], [create_Date], [update_Date], [status], [image_Subject], [id_Course], [id_Catergory_Subject], [id_User]) VALUES (5, N'Swift Programming', N'This course focuses on the Java programming language,
a popular language widely used in web, mobile, and desktop application development.', 27, CAST(N'2023-06-12' AS Date), CAST(N'2023-12-12' AS Date), 1, N'assets/images/courses/pic1.jpg', 1, 1, 9)
GO
INSERT [dbo].[Subject] ([id_Subject], [subject_Name], [description], [numberLession], [create_Date], [update_Date], [status], [image_Subject], [id_Course], [id_Catergory_Subject], [id_User]) VALUES (6, N'Blockchain', N'This specialization introduces blockchain, a revolutionary technology that enables peer-to-peer transfer of digital assets without any intermediaries, and is predicted to be just as impactful as the Internet. More specifically, it prepares learners to program on the Ethereum blockchain.', 25, CAST(N'2023-06-14' AS Date), CAST(N'2023-09-15' AS Date), 1, N'assets/images/courses/pic1.jpg', 2, 2, 1)
GO
INSERT [dbo].[Subject] ([id_Subject], [subject_Name], [description], [numberLession], [create_Date], [update_Date], [status], [image_Subject], [id_Course], [id_Catergory_Subject], [id_User]) VALUES (7, N'Blockchain Basics', N'This first course of the Blockchain specialization provides a broad overview of the essential concepts of blockchain technology – by initially exploring the Bitcoin protocol followed by the Ethereum protocol – to lay the foundation necessary for developing applications and programming. ', 29, CAST(N'2023-06-12' AS Date), CAST(N'2023-10-13' AS Date), 1, N'assets/images/courses/pic1.jpg', 2, 2, 1)
GO
INSERT [dbo].[Subject] ([id_Subject], [subject_Name], [description], [numberLession], [create_Date], [update_Date], [status], [image_Subject], [id_Course], [id_Catergory_Subject], [id_User]) VALUES (8, N'The Blockchain', N'Begin your exploration of blockchain technologies with a look at fundamental blockchain concepts along with an application in which blockchain technology plays a critical role — cryptofinance. This course also introduces distributed digital systems in terms of software and network architecture, and shows how these systems underlie the functionality of the blockchain', 27, CAST(N'2023-06-11' AS Date), CAST(N'2023-11-11' AS Date), 1, N'assets/images/courses/pic1.jpg', 2, 2, 1)
GO
INSERT [dbo].[Subject] ([id_Subject], [subject_Name], [description], [numberLession], [create_Date], [update_Date], [status], [image_Subject], [id_Course], [id_Catergory_Subject], [id_User]) VALUES (9, N'Blockchain Revolution', N'Blockchain is poised to transform every industry and managerial function—redefining the ways we transact online, share ideas, and manage workflows. It’s a new technology that every business professional needs to understand.', 27, CAST(N'2023-06-22' AS Date), CAST(N'2023-06-30' AS Date), 1, N'assets/images/courses/pic1.jpg', 2, 2, 1)
GO
INSERT [dbo].[Subject] ([id_Subject], [subject_Name], [description], [numberLession], [create_Date], [update_Date], [status], [image_Subject], [id_Course], [id_Catergory_Subject], [id_User]) VALUES (10, N'Web3 and Blockchain in Global Commerce', N'Global commerce has grown in complexity and magnitude over millennia, but its processes remain relatively unchanged. Enter blockchain—the Internet of Value. For the first time in human history, individuals and organizations can manage and trade their assets digitally, peer to peer.', 27, CAST(N'2023-06-22' AS Date), CAST(N'2023-06-22' AS Date), 1, N'assets/images/courses/pic1.jpg', 2, 2, 1)
GO
INSERT [dbo].[Subject] ([id_Subject], [subject_Name], [description], [numberLession], [create_Date], [update_Date], [status], [image_Subject], [id_Course], [id_Catergory_Subject], [id_User]) VALUES (11, N'An Introduction to Programming the Internet of Things (IOT)', N'This Specialization covers embedded systems, the Raspberry Pi Platform, and the Arduino environment for building devices that can control the physical world.', 25, CAST(N'2023-06-23' AS Date), CAST(N'2023-06-25' AS Date), 1, N'assets/images/courses/pic1.jpg', 3, 3, 1)
GO
INSERT [dbo].[Subject] ([id_Subject], [subject_Name], [description], [numberLession], [create_Date], [update_Date], [status], [image_Subject], [id_Course], [id_Catergory_Subject], [id_User]) VALUES (12, N'Hands-on Internet of Things', N'This specialization is intended for technologically minded persons who are interested in getting to know the latest in ubiquitous computing, also known as ', 29, CAST(N'2023-06-19' AS Date), CAST(N'2023-06-29' AS Date), 1, N'assets/images/courses/pic1.jpg', 3, 3, 1)
GO
INSERT [dbo].[Subject] ([id_Subject], [subject_Name], [description], [numberLession], [create_Date], [update_Date], [status], [image_Subject], [id_Course], [id_Catergory_Subject], [id_User]) VALUES (13, N'Cybersecurity and the Internet of Things', N'Welcome! You may have heard about the Internet of Things (IoT). But you may also have wondered about what it is. Or for that matter, what does it mean to you or an organization.', 27, CAST(N'2023-06-19' AS Date), CAST(N'2023-06-28' AS Date), 1, N'assets/images/courses/pic1.jpg', 3, 3, 1)
GO
INSERT [dbo].[Subject] ([id_Subject], [subject_Name], [description], [numberLession], [create_Date], [update_Date], [status], [image_Subject], [id_Course], [id_Catergory_Subject], [id_User]) VALUES (14, N'Introduction to DevOps', N'DevOps skills are in demand! DevOps skills are expected to be one of the fastest-growing skills in the workforce. This course can be a first step in obtaining those skills. ', 25, CAST(N'2023-06-14' AS Date), CAST(N'2023-06-15' AS Date), 1, N'assets/images/courses/pic1.jpg', 4, 4, 1)
GO
INSERT [dbo].[Subject] ([id_Subject], [subject_Name], [description], [numberLession], [create_Date], [update_Date], [status], [image_Subject], [id_Course], [id_Catergory_Subject], [id_User]) VALUES (15, N'Introduction to Agile Development and Scrum', N'After successfully completing this course, you will be able to embrace the Agile concepts of adaptive planning, iterative development, and continuous improvement - resulting in early deliveries and value to customers. ', 29, CAST(N'2023-06-15' AS Date), CAST(N'2023-06-19' AS Date), 1, N'assets/images/courses/pic1.jpg', 4, 4, 1)
GO
INSERT [dbo].[Subject] ([id_Subject], [subject_Name], [description], [numberLession], [create_Date], [update_Date], [status], [image_Subject], [id_Course], [id_Catergory_Subject], [id_User]) VALUES (16, N'Continuous Integration and Continuous Delivery (CI/CD)', N'A principle of DevOps is to replace manual processes with automation to improve efficiency, reduce human error, and accelerate software delivery. ', 27, CAST(N'2023-06-17' AS Date), CAST(N'2023-06-19' AS Date), 1, N'assets/images/courses/pic1.jpg', 4, 4, 1)
GO
INSERT [dbo].[Subject] ([id_Subject], [subject_Name], [description], [numberLession], [create_Date], [update_Date], [status], [image_Subject], [id_Course], [id_Catergory_Subject], [id_User]) VALUES (17, N'
Introduction to Web Development with HTML, CSS, JavaScript', N'Want to take the first steps to become a Web Developer? This course will help you discover the languages, frameworks, and tools that you will need to create interactive and engaging websites right from the beginning', 25, CAST(N'2023-05-22' AS Date), CAST(N'2023-06-12' AS Date), 1, N'assets/images/courses/pic1.jpg', 5, 5, 1)
GO
INSERT [dbo].[Subject] ([id_Subject], [subject_Name], [description], [numberLession], [create_Date], [update_Date], [status], [image_Subject], [id_Course], [id_Catergory_Subject], [id_User]) VALUES (18, N'HTML and CSS: Building a Single-Page Website', N'In this 1-hour long project-based course, you will learn how to create a single page website for an imaginary travel agent using HTML and CSS. HTML and CSS are the core for building any website or web application and are indispensable knowledge for any web developer. HTML enables the creation of the web pages layout and structures while CSS enriches the HTML pages by adding the style and feel to them. Eventually, you will be able to use the knowledge acquired on far complex projects that employ these technologies in one way or another.

Note: This course works best for learners who are based in the North America region. We’re currently working on providing the same experience in other regions.', 29, CAST(N'2023-05-12' AS Date), CAST(N'2023-06-21' AS Date), 1, N'assets/images/courses/pic1.jpg', 5, 5, 1)
GO
INSERT [dbo].[Subject] ([id_Subject], [subject_Name], [description], [numberLession], [create_Date], [update_Date], [status], [image_Subject], [id_Course], [id_Catergory_Subject], [id_User]) VALUES (19, N'HTML and CSS in depth', N'You will then dive deeper into CSS by applying increasingly specific styling to various elements.', 27, CAST(N'2023-06-05' AS Date), CAST(N'2023-06-19' AS Date), 1, N'assets/images/courses/pic1.jpg', 5, 5, 1)
GO
INSERT [dbo].[Subject] ([id_Subject], [subject_Name], [description], [numberLession], [create_Date], [update_Date], [status], [image_Subject], [id_Course], [id_Catergory_Subject], [id_User]) VALUES (20, N'Google Digital Marketing & E-commerce', N'Prepare for a new career in the high-growth fields of digital marketing and e-commerce, in under six months, no experience or degree required. Businesses need digital marketing and e-commerce talent more than ever before;', 25, CAST(N'2023-06-03' AS Date), CAST(N'2023-06-20' AS Date), 1, N'assets/images/courses/pic1.jpg', 6, 6, 1)
GO
INSERT [dbo].[Subject] ([id_Subject], [subject_Name], [description], [numberLession], [create_Date], [update_Date], [status], [image_Subject], [id_Course], [id_Catergory_Subject], [id_User]) VALUES (21, N'Optimizing a Website for Google Search', N'Learn the ins and outs of optimizing a website, from conducting an initial audit to presenting your findings and recommendations. Hands-on activities include learning how to select and apply appropriate keywords throughout a website, ', 29, CAST(N'2023-06-02' AS Date), CAST(N'2023-06-12' AS Date), 1, N'assets/images/courses/pic1.jpg', 6, 6, 1)
GO
INSERT [dbo].[Subject] ([id_Subject], [subject_Name], [description], [numberLession], [create_Date], [update_Date], [status], [image_Subject], [id_Course], [id_Catergory_Subject], [id_User]) VALUES (22, N'
Social Media Marketing', N'In a 2018 survey of businesses, Buffer found that only 29% had effective social media marketing programs.    A recent survey of consumers by Tomoson found 92% of consumers trust recommendations from other people over brand content', 27, CAST(N'2023-06-10' AS Date), CAST(N'2023-06-14' AS Date), 1, N'assets/images/courses/pic1.jpg', 6, 6, 1)
GO
INSERT [dbo].[Subject] ([id_Subject], [subject_Name], [description], [numberLession], [create_Date], [update_Date], [status], [image_Subject], [id_Course], [id_Catergory_Subject], [id_User]) VALUES (23, N'Google Professional Workspace Administrator', N'This specialization has been developed to help administrators master the foundations of establishing and managing Google Workspace for their organization.', 25, CAST(N'2023-05-15' AS Date), CAST(N'2023-06-06' AS Date), 1, N'assets/images/courses/pic1.jpg', 7, 7, 1)
GO
INSERT [dbo].[Subject] ([id_Subject], [subject_Name], [description], [numberLession], [create_Date], [update_Date], [status], [image_Subject], [id_Course], [id_Catergory_Subject], [id_User]) VALUES (24, N'App Deployment, Debugging, and Performance', N'In this course, application developers learn how to design and develop cloud-native applications that seamlessly integrate components from the Google Cloud ecosystem.', 29, CAST(N'2023-05-25' AS Date), CAST(N'2023-06-07' AS Date), 1, N'assets/images/courses/pic1.jpg', 7, 7, 1)
GO
INSERT [dbo].[Subject] ([id_Subject], [subject_Name], [description], [numberLession], [create_Date], [update_Date], [status], [image_Subject], [id_Course], [id_Catergory_Subject], [id_User]) VALUES (25, N'Securing and Integrating Components of your Application', N'In this course, application developers learn how to design and develop cloud-native applications that seamlessly integrate managed services from Google Cloud.', 27, CAST(N'2023-05-27' AS Date), CAST(N'2023-06-05' AS Date), 1, N'assets/images/courses/pic1.jpg', 7, 7, 1)
GO
INSERT [dbo].[Subject] ([id_Subject], [subject_Name], [description], [numberLession], [create_Date], [update_Date], [status], [image_Subject], [id_Course], [id_Catergory_Subject], [id_User]) VALUES (26, N'Basic Google Analytics', N'By the end of this project, you will be equipped with basic understanding and skills in using Google Analytics to gain consumer insights from a website, making it possible to develop digital campaigns which meet the needs of the audience as well as deliver benefits that optimize campaign performance.', 25, CAST(N'2023-05-28' AS Date), CAST(N'2023-06-03' AS Date), 1, N'assets/images/courses/pic1.jpg', 8, 2, 1)
GO
INSERT [dbo].[Subject] ([id_Subject], [subject_Name], [description], [numberLession], [create_Date], [update_Date], [status], [image_Subject], [id_Course], [id_Catergory_Subject], [id_User]) VALUES (27, N'Google Data Analytics', N'Prepare for a new career in the high-growth field of data analytics, no experience or degree required. Get professional training designed by Google and have the opportunity to connect with top employers.', 29, CAST(N'2023-05-31' AS Date), CAST(N'2023-06-01' AS Date), 1, N'assets/images/courses/pic1.jpg', 8, 3, 1)
GO
INSERT [dbo].[Subject] ([id_Subject], [subject_Name], [description], [numberLession], [create_Date], [update_Date], [status], [image_Subject], [id_Course], [id_Catergory_Subject], [id_User]) VALUES (28, N'Google IT Support', N'Prepare for a career in the high-growth field of IT, no experience or degree required. Get professional training designed by Google and get on the fast-track to a competitively paid job', 29, CAST(N'2023-05-30' AS Date), CAST(N'2023-06-11' AS Date), 1, N'assets/images/courses/pic1.jpg', 8, 4, 1)
GO
INSERT [dbo].[Subject] ([id_Subject], [subject_Name], [description], [numberLession], [create_Date], [update_Date], [status], [image_Subject], [id_Course], [id_Catergory_Subject], [id_User]) VALUES (29, N'SAP Technology Consultant', N'Launch your career in the high-demand field of SAP consulting. Build the consulting skills successful companies are looking for through tailored training and hands-on projects designed by SAP. No experience or degree required to enroll.', 29, CAST(N'2023-05-29' AS Date), CAST(N'2023-06-14' AS Date), 1, N'assets/images/courses/pic1.jpg', 9, 4, 1)
GO
INSERT [dbo].[Subject] ([id_Subject], [subject_Name], [description], [numberLession], [create_Date], [update_Date], [status], [image_Subject], [id_Course], [id_Catergory_Subject], [id_User]) VALUES (30, N'SAP Professional Fundamentals', N'SAP Professional Fundamentals is the first course in the SAP Technology Consultant Professional Certificate program. The course introduces you to the world of consulting and the range of skills that you need to succeed in this competitive field.', 29, CAST(N'2023-06-02' AS Date), CAST(N'2023-06-10' AS Date), 1, N'assets/images/courses/pic1.jpg', 9, 5, 1)
GO
INSERT [dbo].[Subject] ([id_Subject], [subject_Name], [description], [numberLession], [create_Date], [update_Date], [status], [image_Subject], [id_Course], [id_Catergory_Subject], [id_User]) VALUES (31, N'SAP Functional & Technical Consultant Roles & Skills', N'SAP Professional Fundamentals is the first course in the SAP Technology Consultant Professional Certificate program. The course introduces you to the world of consulting and the range of skills that you need to succeed in this competitive field.', 29, CAST(N'2023-05-31' AS Date), CAST(N'2023-06-12' AS Date), 1, N'assets/images/courses/pic1.jpg', 9, 6, 1)
GO
INSERT [dbo].[Subject] ([id_Subject], [subject_Name], [description], [numberLession], [create_Date], [update_Date], [status], [image_Subject], [id_Course], [id_Catergory_Subject], [id_User]) VALUES (32, N'adfasdfa', N'adfad', 0, CAST(N'2023-06-26' AS Date), CAST(N'2023-06-26' AS Date), 0, N'./assets/images/subject/1687745979558.jpg', 1, 1, 2)
GO
INSERT [dbo].[Subject] ([id_Subject], [subject_Name], [description], [numberLession], [create_Date], [update_Date], [status], [image_Subject], [id_Course], [id_Catergory_Subject], [id_User]) VALUES (33, N'Java Couse 134', N'ASsDsd', 0, CAST(N'2023-06-29' AS Date), CAST(N'2023-06-29' AS Date), 0, N'./assets/images/subject/1688017348917.jpg', 1, 1, 2)
GO
INSERT [dbo].[Subject] ([id_Subject], [subject_Name], [description], [numberLession], [create_Date], [update_Date], [status], [image_Subject], [id_Course], [id_Catergory_Subject], [id_User]) VALUES (34, N'Java Couse 1345huy', N'ASsDsd', 0, CAST(N'2023-06-29' AS Date), CAST(N'2023-06-29' AS Date), 0, N'./assets/images/subject/1688017492955.png', 1, 1, 2)
GO
INSERT [dbo].[Subject] ([id_Subject], [subject_Name], [description], [numberLession], [create_Date], [update_Date], [status], [image_Subject], [id_Course], [id_Catergory_Subject], [id_User]) VALUES (35, N'Java Couse 1345huy4', N'ASsDsd', 0, CAST(N'2023-06-29' AS Date), CAST(N'2023-06-29' AS Date), 0, N'./assets/images/course/1688017580296.png', 1, 1, 2)
GO
INSERT [dbo].[Subject] ([id_Subject], [subject_Name], [description], [numberLession], [create_Date], [update_Date], [status], [image_Subject], [id_Course], [id_Catergory_Subject], [id_User]) VALUES (36, N'Java Couse 1345huy41', N'ASsDsd', 0, CAST(N'2023-06-29' AS Date), CAST(N'2023-06-29' AS Date), 0, N'./assets/images/courses/1688017729364.png', 1, 1, 2)
GO
SET IDENTITY_INSERT [dbo].[Subject] OFF
GO
INSERT [dbo].[Type_Catergory_Subject] ([id_Type_Catergory_Subjec], [name_Catergory_Subject], [description]) VALUES (1, N'Domain', N'In the context of learning systems, the term "domain" refers to a specific 
area or subject of knowledge or expertise. It represents a well-defined problem space or 
field of study that is characterized by its own set of rules, concepts, and data. In machine 
learning, a domain can be thought of as a distinct category or topic for which a learning algorithm
or model is designed.')
GO
INSERT [dbo].[Type_Catergory_Subject] ([id_Type_Catergory_Subjec], [name_Catergory_Subject], [description]) VALUES (2, N'Group', N'In systems learning, the keyword "Group" is often used to refer to a collection of objects or
data grouped together based on a common criterion. Grouping can be applied in many contexts and has different
meanings, depending on the specific area to which it is applied.')
GO
INSERT [dbo].[Type_Catergory_Subject] ([id_Type_Catergory_Subjec], [name_Catergory_Subject], [description]) VALUES (3, N'abc', NULL)
GO
INSERT [dbo].[Type_Catergory_Subject] ([id_Type_Catergory_Subjec], [name_Catergory_Subject], [description]) VALUES (4, N'BlockChain', NULL)
GO
INSERT [dbo].[Type_Question] ([id_Type], [name_TypeQuestion], [description]) VALUES (1, N'Single choice', N'This question has only one correct answer')
GO
INSERT [dbo].[Type_Question] ([id_Type], [name_TypeQuestion], [description]) VALUES (2, N'Multiple choice', N'This question can include more than one correct answer')
GO
INSERT [dbo].[TypeLesson] ([id_Type], [name_TypeLesson]) VALUES (1, N'Quiz')
GO
INSERT [dbo].[TypeLesson] ([id_Type], [name_TypeLesson]) VALUES (2, N'Reading')
GO
INSERT [dbo].[TypeLesson] ([id_Type], [name_TypeLesson]) VALUES (3, N'Watch Video')
GO
SET IDENTITY_INSERT [dbo].[User] ON 
GO
INSERT [dbo].[User] ([id_User], [email], [password], [full_Name], [phone_Number], [gender], [avartar], [address], [status], [id_Role], [create_Date]) VALUES (1, N'phanhuy05092002@gmail.com', N'234', N'Huy Phan', N'0927199000', 1, N'assets/images/post/1689551980426.jpg', N'123 Main Street, Anytown, Canada', 1, 3, CAST(N'2023-06-06' AS Date))
GO
INSERT [dbo].[User] ([id_User], [email], [password], [full_Name], [phone_Number], [gender], [avartar], [address], [status], [id_Role], [create_Date]) VALUES (2, N'phanhuy05092002@gmail.com', N'1234', N'Olivia Parker', N'0938271625', 0, N'assets/images/post/1689516517820.jpg', N'456 Elm Avenue, Springfield, United Kingdom', 1, 2, CAST(N'2023-06-06' AS Date))
GO
INSERT [dbo].[User] ([id_User], [email], [password], [full_Name], [phone_Number], [gender], [avartar], [address], [status], [id_Role], [create_Date]) VALUES (3, N'phankien@gmail.com', N'234', N'Ethan Campbell', N'0989876543', 0, N'assets/images/courses/348936568_199854206262047_836329652168873432_n.jpg', N'789 Oak Drive, Lakeside, Australia', 1, 3, CAST(N'2023-06-06' AS Date))
GO
INSERT [dbo].[User] ([id_User], [email], [password], [full_Name], [phone_Number], [gender], [avartar], [address], [status], [id_Role], [create_Date]) VALUES (4, N'trantien123@gmail.com', N'123', N'Ava Mitchell', N'096543210', 1, N'assets/images/courses/348936568_199854206262047_836329652168873432_n.jpg', N'987 Maple Lane, Riverside, Germany', 1, 4, CAST(N'2023-06-06' AS Date))
GO
INSERT [dbo].[User] ([id_User], [email], [password], [full_Name], [phone_Number], [gender], [avartar], [address], [status], [id_Role], [create_Date]) VALUES (5, N'phanhuy1234@gmail.com', N'123', N'Noah Turner', N'0912345678', 0, N'assets/images/courses/348936568_199854206262047_836329652168873432_n.jpg', N'321 Pine Street, Mountainview, France', 0, 5, CAST(N'2023-06-06' AS Date))
GO
INSERT [dbo].[User] ([id_User], [email], [password], [full_Name], [phone_Number], [gender], [avartar], [address], [status], [id_Role], [create_Date]) VALUES (6, N'simpleemail123@gmail.com', N'password123', N'Olivia Davis', N'0945678910', 1, N'assets/images/courses/348936568_199854206262047_836329652168873432_n.jpg', N'Oceanview, California, USA', 0, 3, CAST(N'2023-06-06' AS Date))
GO
INSERT [dbo].[User] ([id_User], [email], [password], [full_Name], [phone_Number], [gender], [avartar], [address], [status], [id_Role], [create_Date]) VALUES (7, N'quickmail456@gmail.com', N'123', N'Liam Thompson', N'0976543210', 1, N'assets/images/courses/348936568_199854206262047_836329652168873432_n.jpg', N'Emerald City, Oz', 1, 3, CAST(N'2023-06-06' AS Date))
GO
INSERT [dbo].[User] ([id_User], [email], [password], [full_Name], [phone_Number], [gender], [avartar], [address], [status], [id_Role], [create_Date]) VALUES (8, N'na@gmail.com', N'123', N'Pham Khai Van', N'0384478185', 1, N'assets/images/courses/348936568_199854206262047_836329652168873432_n.jpg', N'Hoa Lac', 1, 3, CAST(N'2023-06-19' AS Date))
GO
INSERT [dbo].[User] ([id_User], [email], [password], [full_Name], [phone_Number], [gender], [avartar], [address], [status], [id_Role], [create_Date]) VALUES (9, N'hung@gmail.com', N'123', N'Cao Thịnh Hưng Expert', N'0918271611', 1, N'assets/images/courses/348936568_199854206262047_836329652168873432_n.jpg', N'hello', 1, 2, CAST(N'2023-06-19' AS Date))
GO
INSERT [dbo].[User] ([id_User], [email], [password], [full_Name], [phone_Number], [gender], [avartar], [address], [status], [id_Role], [create_Date]) VALUES (10, N'hung2@gmail.com', N'123', N'Hung Admin', N'0123912341', 0, N'assets/images/courses/348936568_199854206262047_836329652168873432_n.jpg', NULL, 1, 1, CAST(N'2023-06-19' AS Date))
GO
INSERT [dbo].[User] ([id_User], [email], [password], [full_Name], [phone_Number], [gender], [avartar], [address], [status], [id_Role], [create_Date]) VALUES (11, N'he@gmail.com', N'111', N'Tran Nhan Tong', N'0912738917', 1, N'assets/images/courses/348936568_199854206262047_836329652168873432_n.jpg', NULL, 1, 2, CAST(N'2023-06-19' AS Date))
GO
INSERT [dbo].[User] ([id_User], [email], [password], [full_Name], [phone_Number], [gender], [avartar], [address], [status], [id_Role], [create_Date]) VALUES (12, N'aweo@gmail.com', N'111', N'Tran Khanh Tong', N'0912731782', 1, N'assets/images/courses/348936568_199854206262047_836329652168873432_n.jpg', NULL, 1, 2, CAST(N'2023-06-19' AS Date))
GO
INSERT [dbo].[User] ([id_User], [email], [password], [full_Name], [phone_Number], [gender], [avartar], [address], [status], [id_Role], [create_Date]) VALUES (13, N'phanhuy05092002@gmail.com', N'12345', N'Phan Quang Huy', N'0912731782', 1, N'assets/images/courses/348936568_199854206262047_836329652168873432_n.jpg', N'Emerald City, Oz', 1, 1, CAST(N'2023-06-19' AS Date))
GO
INSERT [dbo].[User] ([id_User], [email], [password], [full_Name], [phone_Number], [gender], [avartar], [address], [status], [id_Role], [create_Date]) VALUES (14, N'tupche163707@fpt.edu.vn', N'123', N'', N'12345', 1, N'assets/images/courses/348936568_199854206262047_836329652168873432_n.jpg', N'Hoa Lac', 1, 3, CAST(N'2023-06-21' AS Date))
GO
INSERT [dbo].[User] ([id_User], [email], [password], [full_Name], [phone_Number], [gender], [avartar], [address], [status], [id_Role], [create_Date]) VALUES (15, N'congtu01102002@gmail.com', N'1234', N'Phung cong tu', N'0323456700', 1, N'assets/images/courses/348936568_199854206262047_836329652168873432_n.jpg', N'Hohohola', 1, 3, CAST(N'2023-06-22' AS Date))
GO
INSERT [dbo].[User] ([id_User], [email], [password], [full_Name], [phone_Number], [gender], [avartar], [address], [status], [id_Role], [create_Date]) VALUES (16, N'hungcaothinh431@gmail.com', N'123', N'Cao Thịnh Hưng', N'0918271611', 1, N'assestsAdmin/images/Avatar/Avatar1689522731676.png', N'', 1, 3, CAST(N'2023-07-16' AS Date))
GO
INSERT [dbo].[User] ([id_User], [email], [password], [full_Name], [phone_Number], [gender], [avartar], [address], [status], [id_Role], [create_Date]) VALUES (17, N'hungsale@gmail.com', N'123', N'Cao Thịnh Hưng', N'0918271611', 1, N'assets/images/courses/1689507936354.jpg', N'hello', 1, 5, CAST(N'2023-07-16' AS Date))
GO
SET IDENTITY_INSERT [dbo].[User] OFF
GO
ALTER TABLE [dbo].[Quiz] ADD  DEFAULT ((0)) FOR [number_Quesson]
GO
ALTER TABLE [dbo].[Quiz] ADD  DEFAULT ((0)) FOR [time_Limit]
GO
ALTER TABLE [dbo].[Answer]  WITH CHECK ADD  CONSTRAINT [Answer_Question] FOREIGN KEY([id_Question])
REFERENCES [dbo].[Question] ([id_Question])
GO
ALTER TABLE [dbo].[Answer] CHECK CONSTRAINT [Answer_Question]
GO
ALTER TABLE [dbo].[Blog]  WITH CHECK ADD  CONSTRAINT [blog_CatergoryBlogs] FOREIGN KEY([id_CatergoryBlog])
REFERENCES [dbo].[CatergoryBlog] ([id_CatergoryBlog])
GO
ALTER TABLE [dbo].[Blog] CHECK CONSTRAINT [blog_CatergoryBlogs]
GO
ALTER TABLE [dbo].[Blog]  WITH CHECK ADD  CONSTRAINT [Blog_User] FOREIGN KEY([id_User])
REFERENCES [dbo].[User] ([id_User])
GO
ALTER TABLE [dbo].[Blog] CHECK CONSTRAINT [Blog_User]
GO
ALTER TABLE [dbo].[Catergory_Subject]  WITH CHECK ADD  CONSTRAINT [Type_Catergory] FOREIGN KEY([id_Type_Catergory_Subjec])
REFERENCES [dbo].[Type_Catergory_Subject] ([id_Type_Catergory_Subjec])
GO
ALTER TABLE [dbo].[Catergory_Subject] CHECK CONSTRAINT [Type_Catergory]
GO
ALTER TABLE [dbo].[Course]  WITH CHECK ADD  CONSTRAINT [Course_CatergoryCourse] FOREIGN KEY([id_Catergory])
REFERENCES [dbo].[Course_Catergory] ([id_Catergory])
GO
ALTER TABLE [dbo].[Course] CHECK CONSTRAINT [Course_CatergoryCourse]
GO
ALTER TABLE [dbo].[Course]  WITH CHECK ADD  CONSTRAINT [Course_User] FOREIGN KEY([id_User])
REFERENCES [dbo].[User] ([id_User])
GO
ALTER TABLE [dbo].[Course] CHECK CONSTRAINT [Course_User]
GO
ALTER TABLE [dbo].[Checkbox_Learn]  WITH CHECK ADD  CONSTRAINT [Check_Lesson] FOREIGN KEY([id_Lesson])
REFERENCES [dbo].[Lesson] ([id_Lesson])
GO
ALTER TABLE [dbo].[Checkbox_Learn] CHECK CONSTRAINT [Check_Lesson]
GO
ALTER TABLE [dbo].[Checkbox_Learn]  WITH CHECK ADD  CONSTRAINT [Check_User] FOREIGN KEY([id_User])
REFERENCES [dbo].[User] ([id_User])
GO
ALTER TABLE [dbo].[Checkbox_Learn] CHECK CONSTRAINT [Check_User]
GO
ALTER TABLE [dbo].[Lesson]  WITH CHECK ADD  CONSTRAINT [Lesson_Subject] FOREIGN KEY([id_Subject])
REFERENCES [dbo].[Subject] ([id_Subject])
GO
ALTER TABLE [dbo].[Lesson] CHECK CONSTRAINT [Lesson_Subject]
GO
ALTER TABLE [dbo].[Lesson]  WITH CHECK ADD  CONSTRAINT [Lesson_typeLesson] FOREIGN KEY([id_Type])
REFERENCES [dbo].[TypeLesson] ([id_Type])
GO
ALTER TABLE [dbo].[Lesson] CHECK CONSTRAINT [Lesson_typeLesson]
GO
ALTER TABLE [dbo].[PackagePrice]  WITH CHECK ADD  CONSTRAINT [PackagePrice_Course] FOREIGN KEY([id_Course])
REFERENCES [dbo].[Course] ([id_Course])
GO
ALTER TABLE [dbo].[PackagePrice] CHECK CONSTRAINT [PackagePrice_Course]
GO
ALTER TABLE [dbo].[Question]  WITH CHECK ADD  CONSTRAINT [Quiz_Question] FOREIGN KEY([id_Quiz])
REFERENCES [dbo].[Quiz] ([id_Quiz])
GO
ALTER TABLE [dbo].[Question] CHECK CONSTRAINT [Quiz_Question]
GO
ALTER TABLE [dbo].[Question]  WITH CHECK ADD  CONSTRAINT [Type_Question1] FOREIGN KEY([id_Type])
REFERENCES [dbo].[Type_Question] ([id_Type])
GO
ALTER TABLE [dbo].[Question] CHECK CONSTRAINT [Type_Question1]
GO
ALTER TABLE [dbo].[Quiz]  WITH CHECK ADD  CONSTRAINT [Quiz_Level] FOREIGN KEY([id_Level])
REFERENCES [dbo].[Level] ([id_Level])
GO
ALTER TABLE [dbo].[Quiz] CHECK CONSTRAINT [Quiz_Level]
GO
ALTER TABLE [dbo].[Quiz]  WITH CHECK ADD  CONSTRAINT [Quiz_Subject] FOREIGN KEY([id_Lesson])
REFERENCES [dbo].[Lesson] ([id_Lesson])
GO
ALTER TABLE [dbo].[Quiz] CHECK CONSTRAINT [Quiz_Subject]
GO
ALTER TABLE [dbo].[Quiz]  WITH CHECK ADD  CONSTRAINT [Quiz_User] FOREIGN KEY([id_User])
REFERENCES [dbo].[User] ([id_User])
GO
ALTER TABLE [dbo].[Quiz] CHECK CONSTRAINT [Quiz_User]
GO
ALTER TABLE [dbo].[Registration]  WITH CHECK ADD  CONSTRAINT [Course_Registration] FOREIGN KEY([id_Course])
REFERENCES [dbo].[Course] ([id_Course])
GO
ALTER TABLE [dbo].[Registration] CHECK CONSTRAINT [Course_Registration]
GO
ALTER TABLE [dbo].[Registration]  WITH CHECK ADD  CONSTRAINT [User_Registration] FOREIGN KEY([id_User])
REFERENCES [dbo].[User] ([id_User])
GO
ALTER TABLE [dbo].[Registration] CHECK CONSTRAINT [User_Registration]
GO
ALTER TABLE [dbo].[Result]  WITH CHECK ADD  CONSTRAINT [Result_Quiz] FOREIGN KEY([id_Quiz])
REFERENCES [dbo].[Quiz] ([id_Quiz])
GO
ALTER TABLE [dbo].[Result] CHECK CONSTRAINT [Result_Quiz]
GO
ALTER TABLE [dbo].[Result]  WITH CHECK ADD  CONSTRAINT [Result_User] FOREIGN KEY([id_User])
REFERENCES [dbo].[User] ([id_User])
GO
ALTER TABLE [dbo].[Result] CHECK CONSTRAINT [Result_User]
GO
ALTER TABLE [dbo].[Slider]  WITH CHECK ADD  CONSTRAINT [slider_User] FOREIGN KEY([id_User])
REFERENCES [dbo].[User] ([id_User])
GO
ALTER TABLE [dbo].[Slider] CHECK CONSTRAINT [slider_User]
GO
ALTER TABLE [dbo].[Subject]  WITH CHECK ADD  CONSTRAINT [Subject_Catergory] FOREIGN KEY([id_Catergory_Subject])
REFERENCES [dbo].[Catergory_Subject] ([id_Catergory_Subject])
GO
ALTER TABLE [dbo].[Subject] CHECK CONSTRAINT [Subject_Catergory]
GO
ALTER TABLE [dbo].[Subject]  WITH CHECK ADD  CONSTRAINT [Subject_Course] FOREIGN KEY([id_Course])
REFERENCES [dbo].[Course] ([id_Course])
GO
ALTER TABLE [dbo].[Subject] CHECK CONSTRAINT [Subject_Course]
GO
ALTER TABLE [dbo].[Subject]  WITH CHECK ADD  CONSTRAINT [Subject_User] FOREIGN KEY([id_User])
REFERENCES [dbo].[User] ([id_User])
GO
ALTER TABLE [dbo].[Subject] CHECK CONSTRAINT [Subject_User]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [user_role] FOREIGN KEY([id_Role])
REFERENCES [dbo].[Role] ([id_Role])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [user_role]
GO
/****** Object:  StoredProcedure [dbo].[dropDB]    Script Date: 7/17/2023 10:50:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[dropDB]
as
begin
	drop table Result
	drop table Answer
	drop table Question
	drop table Quiz
	drop table [Level]
	drop table Type_Question
end
GO
/****** Object:  StoredProcedure [dbo].[recreatequiz]    Script Date: 7/17/2023 10:50:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[recreatequiz]
as
begin
drop table result
drop table answer
drop table Question
drop table Quiz

create table Quiz(
id_Quiz int identity(1,1) primary key,
name_Quiz ntext not null,
number_Quesson int default 0,
time_Limit int default 0,
description_Quiz ntext,
id_Lesson int,
id_Level int,
create_Date date,
update_Date date,
id_User int,
passpercent int
--them ngay update
constraint Quiz_Subject foreign key (id_Lesson) references [Lesson](id_Lesson),
constraint Quiz_Level foreign key (id_Level) references Level(id_Level),
constraint Quiz_User foreign key (id_User) references [User](id_User)
)


create table Question(
id_Question int identity(1,1) primary key,
content_Question ntext,
[description] ntext, -- van dan bao them
id_Quiz int,
createDate date,
update_Date date,
id_Type int,
constraint Quiz_Question foreign key (id_Quiz) references [Quiz](id_Quiz),
constraint Type_Question1 foreign key (id_Type) references Type_Question(id_Type)
)
create table Answer(
id_answer int identity(1,1) primary key,
answer_Content ntext,
is_Correct ntext,
id_Question int
constraint Answer_Question foreign key (id_Question) references [Question](id_Question)
)

create table Result(
 id_Result int PRIMARY KEY identity(1,1),
 start_Time datetime,
 duration int,
 doResult text,
 correctAnwser text,
 id_User int,
 id_Quiz int,
 constraint Result_Quiz foreign key (id_Quiz) references Quiz(id_Quiz),
 constraint Result_User foreign key (id_User) references [User](id_User)
)

end
GO
USE [master]
GO
ALTER DATABASE [OnlineLearningSystem] SET  READ_WRITE 
GO
